<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tamanos extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tamanos_modelo');
	}

	function index()
	{
		$data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $data['tamanos'] = $this->Tamanos_modelo->get_all();
	    }

		$this->load->view('lista_tamanos', $data);
	}

	function crear()
	{
		$data = array();
		$roles = $this->session->userdata('roles');

        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
        	$this->load->module('sitios/Sitios_getters');
            $data['sitios'] = $this->sitios_getters->list_sitios();
        }

        $this->load->view('crear_tamano_form', $data);
	}

	function eliminar($tamano_id)
	{
		$result = $this->Tamanos_modelo->delete_tamano($tamano_id);
        if($result)
        {
            $this->session->set_flashdata('success', '¡Tamaño de imagen eliminado!');
            redirect('/tamanos');
        }
        else
        {
            $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
            redirect('/tamanos');
        }
	}

	function procesar()
	{
		// Verificar que el usuario es admin o moderador
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Tamanos_modelo->procesar_tamano();
            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Tamaño de imagen grabado!');
                redirect('/tamanos');
            }
            else
            {
            	$this->session->set_flashdata('error', 'Hubo un problema. Por favor inténtalo de nuevo');
            	redirect('/tamanos');
            }
        }
	}

}