<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tamanos_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	function get_all()
	{
		$this->db->select('ti.tamano_id, ti.sitio_id, ti.tamano_nombre, ti.tamano_ancho, ti.tamano_alto, ti.usuario_id, u.usuario_nombre, s.sitio_nombre');
		$this->db->from('tamanos_imagen_sitio ti');
		$this->db->join('usuarios u', 'u.usuario_id = ti.usuario_id');
		$this->db->join('sitios s', 's.sitio_id = ti.sitio_id');
		$this->db->where('ti.tamano_estado', '1');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function procesar_tamano()
	{
		$this->form_validation->set_rules('sitio_id', 'Sitio', 'required');
		$this->form_validation->set_rules('tamano_nombre', 'Tamaño Nombre', 'trim');
		$this->form_validation->set_rules('tamano_alto', 'Alto', 'numeric|trim');
		$this->form_validation->set_rules('tamano_ancho', 'Ancho', 'numeric|trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$sitio_id = $this->input->post('sitio_id', TRUE);
			$tamano_nuevo = $this->input->post('tamano_nuevo', TRUE);
			$tamano_id = $this->input->post('tamano_id', TRUE);

			if($tamano_nuevo)
			{
				$tamano_nombre = $this->input->post('tamano_nombre');
				$medidas = array(
					'ancho' => $this->input->post('tamano_ancho'),
					'alto' => $this->input->post('tamano_alto')
				);
			}
			else
			{
				$tamano_nombre = $this->input->post('tamano');
				$medidas = $this->_get_medidas($tamano_nombre);
			}

			$tamano_array = array(
				'sitio_id' => $sitio_id,
				'tamano_nombre' => $tamano_nombre,
				'tamano_ancho' => (int)$medidas['ancho'],
				'tamano_alto' => (int)$medidas['alto'],
				'usuario_id' => $usuario_mod,
				'tamano_estado' => '1',
			);

			$this->db->insert('tamanos_imagen_sitio', $tamano_array);
			
			return $this->db->affected_rows();
		}
	}

	function delete_tamano($tamano_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'tamano_estado' => '0',
			'fech_update' => date('Y-m-d G:i:s'),
			'usuario_id' => $usuario_mod
		);

		$this->db->where('tamano_id', $tamano_id);
		$this->db->update('tamanos_imagen_sitio', $array);

		return $this->db->affected_rows();
	}


	function _get_medidas($tamano_nombre)
	{
		switch ($tamano_nombre) {
			case 'apaisado':
				$medidas['ancho'] = 630;
				$medidas['alto'] = 355;
				return $medidas;
			case '4x3':
				$medidas['ancho'] = 490;
				$medidas['alto'] = 369;
				return $medidas;
			case 'mediano':
				$medidas['ancho'] = 308;
				$medidas['alto'] = 173;
				return $medidas;
			case 'thumb':
				$medidas['ancho'] = 146;
				$medidas['alto'] = 70;
				return $medidas;
			case 'ranking':
				$medidas['ancho'] = 117;
				$medidas['alto'] = 183;
				return $medidas;
			case 'rankingthm':
				$medidas['ancho'] = 38;
				$medidas['alto'] = 59;
				return $medidas;
			case 'square':
				$medidas['ancho'] = 308;
				$medidas['alto'] = 308;
				return $medidas;
			case 'rankingsquare':
				$medidas['ancho'] = 183;
				$medidas['alto'] = 183;
				return $medidas;
			case 'minisquare':
				$medidas['ancho'] = 92;
				$medidas['alto'] = 92;
				return $medidas;
			default: // POR DEFAULT ES APAISADO
				$medidas['ancho'] = 630;
				$medidas['alto'] = 355;
				return $medidas;
		}
	}
}
