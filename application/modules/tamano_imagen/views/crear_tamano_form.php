<?php 
$titulo = "Crear tamaño | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <div class="box-typical box-typical-padding">
          <h5 class="m-t-lg with-border">Crear tamaño</h5>

          <form method="post" action="<?php echo base_url();?>tamanos/procesar">
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Sitio *</label>
              <div class="col-sm-6">
                <select id="sitios" name="sitio_id" class="form-control sitio" data-placeholder="Elegir un sitio" required>
                  <option value="">&nbsp;</option>
                  <?php foreach($sitios as $sitio): ?>
                    <option value="<?php echo $sitio->sitio_id;?>"><?php echo $sitio->sitio_nombre;?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Tamaño</label>
              <div class="col-sm-6">
                <select id="tamanos1" class="form-control tamano" name="tamano" style="width: 100%" data-placeholder="Elegir un tamaño de imagen">
                  <option value="">&nbsp;</option>
                  <option value="apaisado">Apaisado</option>
                  <option value="4x3">4x3</option>
                  <option value="mediano">Mediano</option>
                  <option value="thumb">Thumb</option>
                  <option value="ranking">Ranking</option>
                  <option value="rankingthm">Ranking Thumb</option>
                  <option value="rankingsquare">Ranking Square</option>
                  <option value="square">Square</option>
                  <option value="minisquare">Mini Square</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Tamaño nuevo</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="checkbox" id="tamanoNuevo" name="tamano_nuevo" value="1"></p>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label"></label>
              <div class="col-sm-3">
                <input type="text" id="tamanoNombre" name="tamano_nombre" class="form-control" placeholder="Nombre del tamaño de imagen..." disabled="disabled" />
              </div>
              <div class="col-sm-2">
                <input type="text" id="tamanoAncho" name="tamano_ancho" class="form-control" placeholder="Ancho..." disabled="disabled" />
              </div>
              <div class="col-sm-2">
                <input type="text" id="tamanoAlto" name="tamano_alto" class="form-control" placeholder="Alto..." disabled="disabled" />
              </div>
            </div>

            <hr>
            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-rounded btn-inline btn-primary">Crear</button>
                <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
              </div>
            </div>  
                   
          </form>
        </div>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/plugins.js"></script>

  <script src="<?php echo base_url();?>assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>

<script>
  $('#cancelar').click(function(e){
    e.preventDefault();
    window.location.href="<?php echo base_url();?>tamanos";
  });

  $('#tamanoNuevo').change(function(){
    var ischecked = $(this).is(':checked');
    var nombre = $('#tamanoNombre');
    var ancho = $('#tamanoAncho');
    var alto = $('#tamanoAlto');

    if(ischecked){
      $(nombre).removeAttr('disabled');
      $(ancho).removeAttr('disabled');
      $(alto).removeAttr('disabled');
      $("#tamanos1").attr('disabled', 'disabled');
    } else {
      $(nombre).attr('disabled', 'disabled');
      $(ancho).attr('disabled', 'disabled');
      $(alto).attr('disabled', 'disabled');
      $("#tamanos1").removeAttr('disabled');
    }
    
  });
</script>
</body>
</html>
