<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitios extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('sitios');
		//$this->load->model(array('Sitios_modelo', 'secciones/Secciones_modelo'));
		$this->load->model('Sitios_modelo');
	}

	public function index() // dashboard
	{
		$roles = $this->session->userdata('roles');	
		$sitios = $this->session->userdata('sitios');

		$sitios_permitidos = get_sitios_data($sitios, $roles);

		$data['sitios'] = $sitios_permitidos;
		$this->load->view('sitios', $data);
	}

	public function sitio_subhome($sitio_id)
	{
    $this->session->set_userdata('sitio_id_session', $sitio_id);
		$this->load->model('notas/Notas_getters_modelo');
		$sitios = $this->session->userdata('sitios');
		$roles = $this->session->userdata('roles');

		$data = array();

		if(!in_array($sitio_id, $sitios))//~(comprueba si un valor existe existe en un array) = no existe
		{
			$this->session->set_flashdata('error', 'No tienes permiso para acceder a este sitio.');
		}
		else
		{
			$data['notas'] = $this->Notas_getters_modelo->get_info(10, $sitio_id);
			$data['sitio'] = $this->Sitios_modelo->get_sitio_by_id($sitio_id);
                        $data['sitio_id'] = $sitio_id;
		}
		
		$this->load->view('sitio_subhome', $data);
	}

	public function administrar_sitios()
	{
		$data = array();
		$roles = $this->session->userdata('roles');

		if(!in_array('1', $roles))
		{
			$this->session->set_flashdata('error', 'No tienes permisos de administración.');
		}
		else
		{
			// conseguir sitios
			$data['sitios'] = $this->Sitios_modelo->get_all();
		}
		
		$this->load->view('lista_sitios', $data);        
	}

	function crear()
	{
		$this->load->view('crear_sitio_form'); 
	}

	function editar($sitio_id)
	{
		$data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $sitio = $this->Sitios_modelo->get_sitio_by_id($sitio_id);

            $data['sitio'] = $sitio;
        }

        $this->load->view('editar_sitio_form', $data);
	}

	function eliminar($sitio_id)
	{
		$result = $this->Sitios_modelo->delete_sitio($sitio_id);
        if($result)
        {
            $this->session->set_flashdata('success', '¡Sitio eliminado!');
            redirect('sitios/administrar-sitios');
        }
        else
        {
            $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
        }
	}

	function procesar()
	{
		// Verificar que el usuario es admin
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Sitios_modelo->procesar_sitio();
            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Sitio grabado!');
                redirect('sitios/administrar-sitios');
            }
        }
	}

	// funciones ajax
  function ultimos_contenidos($sitio_id)
  {
    $this->load->model('notas/Notas_getters_modelo');
    $notas = $this->Notas_getters_modelo->get_info(10, $sitio_id);

    $data = '<div class="box-typical-body panel-body">
              <table class="tbl-typical">
                  <tr>
                      <th><div>Titular</div></th>
                      <th><div>Sección</div></th>
                      <th align="center"><div>Categoría</div></th>
                      <th align="center"><div>Fecha modificación</div></th>
                      <th><div>Acciones</div></th>
                  </tr>';
    if(!empty($notas))
    {
      foreach($notas as $nota)
      {
        $categoria = !empty($nota->categoria_nombre) ? $nota->categoria_nombre : '--';
        $data .=    '<tr>
                        <td>'.$nota->nota_titulo.'</td>
                        <td>'.$nota->seccion_nombre.'</td>
                        <td>'.$categoria.'</td>
                        <td>'.$nota->nota_fech_update.'</td>
                        <td><a href="/notas/editar/'.$nota->sitio_id.'/'.$nota->seccion_id.'/'.$nota->nota_id.'"><span class="fa fa-edit" title="editar"></span></a></td>
                    </tr>';
      }
    }
    $data .= '</table>
          </div><!--.box-typical-body-->';
    echo $data;
  }

  function ultima_actividad($sitio_id)
  {
    $this->load->model('notas/Notas_getters_modelo');
    $notas = $this->Notas_getters_modelo->get_info(5, $sitio_id);
    $data = '<div class="box-typical-body panel-body">
                      <div class="contact-row-list">';

    if(!empty($notas))
    {
      foreach($notas as $nota)
      {
        $data .= '<article class="contact-row">
                    <div class="user-card-row">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <p class="user-card-row-name"><a href="#">'.$nota->usuario_nombre.'</a></p>
                                <p class="user-card-row-mail">Ha publicado en '.$nota->seccion_nombre.'</p>
                            </div>
                            <div class="tbl-cell tbl-cell-status">'.time_elapsed_string($nota->nota_fech_update).'</div>
                        </div>
                    </div>
                </article>';
      }
    }

    $data .= '</div></div><!--.box-typical-body-->';

    echo $data;
  }

}