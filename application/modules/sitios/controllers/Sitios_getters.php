<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitios_getters extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('sitios');
		$this->load->model(array('Sitios_modelo', 'Sitios_getters_modelo'));
	}

	public function list_sitios()
	{
		return $this->Sitios_modelo->get_all();
	}

	public function list_sitios_by_ids($sitios_id)
	{
		return $this->Sitios_getters_modelo->get_sitios_by_id($sitios_id);
	}

	public function get_bucket($sitio_id)
	{
		$bucket = $this->Sitios_getters_modelo->get_ruta_cdn($sitio_id);
		return $bucket->sitio_ruta_cdn;
	}

	function list_sitios_id_by_usuario($usuario_id)
	{
		return $this->Sitios_getters_modelo->get_sitios_by_usuario($usuario_id);
	}

}