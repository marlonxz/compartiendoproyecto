<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sitios_getters_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
	}

	function get_credentials($sitio_id)
	{
		$query = $this->db->select('sitio_usuario_cdn, sitio_pass_cdn, sitio_ruta_cdn')
							->where('sitio_id', $sitio_id)
							->where('sitio_estado', '1')->get('sitios');
		if($query->num_rows() > 0)
		{
			return $query->row();
		}

		return FALSE;
	}

	function get_ruta_cdn($sitio_id)
	{
		$query = $this->db->select('sitio_ruta_cdn')->where('sitio_id', $sitio_id)->get('sitios');
		if($query->num_rows() > 0)
		{
			return $query->row();
		}

		return FALSE;
	}

	function get_name_by_id($sitio_id)
	{
		$this->db->select('sitio_nombre');
		$this->db->from('sitios');
		$this->db->where('sitio_id', $sitio_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}

		return FALSE;
	}

	function get_sitios_by_usuario($usuario_id)
	{
		$sitios_id = array();

		$this->db->select('sitio_id');
		$this->db->from('usuario_rol_sitio');
		$this->db->where('usuario_id', $usuario_id);
		// $this->db->where('rol_id !=', 4); // que no sea blogger
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			$usuarios = $query->result();
			foreach($usuarios as $usuario)
			{
				$sitios_id[] = $usuario->sitio_id;
			}
			return $sitios_id;
		}
		return FALSE;
	}

	function get_sitios_by_id($sitios_id)
	{
		$this->db->from('sitios');
		$this->db->where_in('sitio_id', $sitios_id);
		$this->db->where('sitio_estado', '1');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return FALSE;		
	}
}