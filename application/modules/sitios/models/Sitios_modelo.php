<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sitios_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
	}

	function get_sitio_by_id($sitio_id)
	{
		$this->db->from('sitios')
					->where('sitio_id', $sitio_id)
					->where('sitio_estado', '1');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		return FALSE;
	}

	function procesar_sitio()
	{
		$this->form_validation->set_rules('sitio_nombre', 'Nombre', 'required|trim');
		$this->form_validation->set_rules('sitio_descripcion', 'Descripcion', 'trim');
		$this->form_validation->set_rules('sitio_ruta_cdn', 'Ruta CDN', 'trim');
		$this->form_validation->set_rules('sitio_usuario_cdn', 'Usuario CDN', 'trim');
		$this->form_validation->set_rules('sitio_password_cdn', 'Password CDN', 'trim');
		$this->form_validation->set_rules('sitio_estado', 'Estado', 'trim');
		$this->form_validation->set_rules('sitio_mantenimiento', 'Mantenimiento', 'trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$sitio_id = $this->input->post('sitio_id', TRUE);
			$sitio_nombre = $this->input->post('sitio_nombre', TRUE);
			$sitio_descripcion = $this->input->post('sitio_descripcion', TRUE);
			$sitio_dominio = $this->input->post('sitio_dominio', TRUE);
			$sitio_ruta_cdn = $this->input->post('sitio_ruta_cdn', TRUE);
			$sitio_usuario_cdn = $this->input->post('sitio_usuario_cdn', TRUE);
			$sitio_password_cdn = $this->input->post('sitio_password_cdn', TRUE);
			$sitio_estado = $this->input->post('sitio_estado', TRUE);
			$sitio_mantenimiento = $this->input->post('sitio_mantenimiento', TRUE);

			$sitio_array = array(
				'sitio_nombre' => $sitio_nombre,
				'sitio_descripcion' => $sitio_descripcion,
				'sitio_dominio' => $sitio_dominio,
				'sitio_ruta_cdn' => $sitio_ruta_cdn,
				'sitio_usuario_cdn' => $sitio_usuario_cdn,
				'sitio_pass_cdn' => $sitio_password_cdn,
				'sitio_estado' => $sitio_estado,
				'flag_mantenimiento' => $sitio_mantenimiento,
				'sitio_usuario_mod' => $usuario_mod
			);

			if(empty($sitio_id))
			{
				// insertar a la tabla sitios
				$sitio_array['sitio_fech_creacion'] = date('Y-m-d G:i:s');
				$this->db->insert('sitios', $sitio_array);
			}
			else
			{
				// editar datos básicos
				$sitio_array['sitio_fech_update'] = date('Y-m-d G:i:s');
				$this->db->where('sitio_id', $sitio_id);
				$this->db->update('sitios', $sitio_array);
			}
			
			return $this->db->affected_rows();
		}
	}

	function delete_sitio($sitio_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'sitio_estado' => '0',
			'sitio_fech_update' => date('Y-m-d G:i:s'),
			'sitio_usuario_mod' => $usuario_mod
		);

		$this->db->where('sitio_id', $sitio_id);
		$this->db->update('sitios', $array);

		return $this->db->affected_rows();
	}

	function get_all()
	{
		$this->db->from('sitios');
		$this->db->where('sitio_estado', '1');

		return $this->db->get()->result();
	}

	function get_sitios($sitios_id)
	{
		$this->db->from('sitios');
		$this->db->where_in('sitio_id', $sitios_id);
		$sitios = $this->db->get()->result();

		
		
		$this->session->set_userdata('sidebar', TRUE);
		$this->session->set_userdata('sitios_permitidos', json_encode($sitios));
		return $sitios;
	}





}
