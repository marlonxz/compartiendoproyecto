<?php 
$titulo = $sitio->sitio_nombre." | Zeus CMS";
$sitio_id =  $sitio->sitio_id;
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/lobipanel/lobipanel.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/lobipanel.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
       
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h2><?php echo $sitio->sitio_nombre;?></h2>
                <div class="subtitle">Bienvenido al panel de <?php echo $sitio->sitio_nombre;?></div>
              </div>
            </div>
            <br>
            <div class="tbl-row">
              <div class="tbl-cell">
                  <a href="<?php echo base_url();?>notas/crear/<?php echo $sitio->sitio_id;?>" class="btn btn-rounded btn-inline">Crear contenido</a>
              </div>
            </div>
          </div>
        </header>
      
        <div class="row">
          <div class="col-xl-8 dahsboard-column">
              <section class="box-typical box-typical-dashboard panel panel-default scrollable contenidos">
                  <header class="box-typical-header panel-heading">
                      <h3 class="panel-title">Últimos contenidos</h3>
                  </header>
                  <div class="box-typical-body panel-body">
                      <table class="tbl-typical">
                          <tr>
                              <th><div>Titular</div></th>
                              <th><div>Sección</div></th>
                              <th align="center"><div>Categoría</div></th>
                              <th align="center"><div>Fecha modificación</div></th>
                              <th><div>Acciones</div></th>
                          </tr>
                          <?php if(!empty($notas)):?>
                          <?php foreach($notas as $nota):?>
                          <tr>
                              <td><?php echo $nota->nota_titulo;?></td>
                              <td><?php echo $nota->seccion_nombre;?></td>
                              <td align="center"><?php echo !empty($nota->categoria_nombre) ? $nota->categoria_nombre : '-';?></td>
                              <td><?php echo $nota->nota_fech_update;?></td>
                              <td><a href="<?php echo base_url();?>notas/editar/<?php echo $nota->sitio_id;?>/<?php echo $nota->seccion_id;?>/<?php echo $nota->nota_id;?>"><span class="fa fa-edit" title="editar"></span></a></td>
                          </tr>
                          <?php endforeach;?>
                          <?php endif;?>
                      </table>
                  </div><!--.box-typical-body-->
              </section><!--.box-typical-dashboard-->
          </div><!--.col-->
          <div class="col-xl-4 dahsboard-column">
              <section class="box-typical box-typical-dashboard panel panel-default scrollable actividad">
                  <header class="box-typical-header panel-heading">
                      <h3 class="panel-title">Actividad por usuarios</h3>
                  </header>
                  <div class="box-typical-body panel-body">
                      <div class="contact-row-list">
                        <?php if(!empty($notas)):
                          $i = 1; foreach($notas as $nota):?>
                          <article class="contact-row">
                              <div class="user-card-row">
                                  <div class="tbl-row">
                                      <div class="tbl-cell">
                                          <p class="user-card-row-name"><?php echo $nota->usuario_nombre;?></p>
                                          <p class="user-card-row-mail">Ha publicado en <?php echo $nota->seccion_nombre;?></p>
                                      </div>
                                      <div class="tbl-cell tbl-cell-status"><?php echo time_elapsed_string($nota->nota_fech_update);?></div>
                                  </div>
                              </div>
                          </article>
                        <?php if($i == 5){break;}?>
                        <?php $i++; endforeach;
                        endif;?>
                      </div>
                  </div><!--.box-typical-body-->
              </section><!--.box-typical-dashboard-->
          </div><!--.col-->
        </div>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/plugins.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jqueryui/jquery-ui.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/lobipanel/lobipanel.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/match-height/jquery.matchHeight.min.js"></script>
  <script>
    $(document).ready(function() {
      $('.panel').lobiPanel({
        close: false,
        editTitle: false,
        minimize: false,
        unpin: false,
        sortable: true,
        draggable: false
      });

      $('.contenidos').lobiPanel('setLoadUrl', "<?php echo base_url();?>sitios/ultimos-contenidos/<?php echo $sitio_id;?>");
      $('.actividad').lobiPanel('setLoadUrl', "<?php echo base_url();?>sitios/ultima-actividad/<?php echo $sitio_id;?>");
      
      /*$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
        $('.dahsboard-column').matchHeight();
      });*/
    });
  </script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
