<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	function get_sitios_data($sitios_id, $roles)
	{
		$CI = get_instance();
		$CI->load->model('sitios/Sitios_modelo');
		$CI->load->model('secciones/Secciones_getters_modelo');
		$CI->load->model('roles/Roles_modelo');

		// verificar si ya se creó la sesión del usuario
		if($CI->session->userdata('sidebar'))
		{
			$sitios = json_decode($CI->session->userdata('sitios_permitidos')); // array de ids
			if(!empty($sitios))
			{
				foreach($sitios as $sitio)
				{	
					$secciones = json_decode($CI->session->userdata('secciones_by_sitio'));
					$sitio_id = $sitio->sitio_id;
					$sitio->secciones = $secciones->$sitio_id;
				}
				return $sitios;
			}
			
		}
		else
		{
			$sitios = $CI->Sitios_modelo->get_sitios($sitios_id);
			$CI->session->set_userdata('informacion_sitios', json_encode($sitios));

			if(!empty($sitios))
			{
				foreach($sitios as $sitio)
				{
					// conseguir el nivel de acceso de acuerdo al rol
					$nivel_acceso = $CI->Roles_modelo->get_nivel_acceso_by_rol($roles[$sitio->sitio_id]);
					$sitio->secciones = $CI->Secciones_getters_modelo->get_by_sitio_id($sitio->sitio_id, $nivel_acceso);			
				}

				return $sitios;
			}
		}
				
		return FALSE;
	}

	function get_sitio_nombre($sitio_id)
	{
		$CI = get_instance();
		if ($CI->session->userdata('informacion_sitios'))
		{
			$sitios = json_decode($CI->session->userdata('informacion_sitios'));
			foreach($sitios as $sitio)
			{
				if ($sitio->sitio_id == $sitio_id)
				{
					return $sitio->sitio_nombre;
				}
			}
		}
		return FALSE;
	}

	function time_elapsed_string($datetime, $full = false) 
	{
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'año',
	        'm' => 'mes',
	        'w' => 'semana',
	        'd' => 'día',
	        'h' => 'hora',
	        'i' => 'minuto',
	        's' => 'segundo',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? 'Hace ' . implode(', ', $string) : 'Justo ahora';
	}