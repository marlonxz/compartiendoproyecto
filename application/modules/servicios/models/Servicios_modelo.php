<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios_modelo extends CI_Model
{
	function get_all_servicios()
	{
		$this->db->select('servicio_id, servicio_url,
			c.categoria_id, c.categoria_nombre,
			sitios.sitio_id, sitios.sitio_nombre, 
			secciones.seccion_id, secciones.seccion_nombre');
		$this->db->from('servicios');
		$this->db->join('categorias c', 'c.categoria_id = servicios.categoria_id', 'left');
		$this->db->join('sitios', 'sitios.sitio_id = servicios.sitio_id');
		$this->db->join('secciones', 'secciones.seccion_id = servicios.seccion_id', 'left');
		$query = $this->db->get()->result();

		if(!empty($query))
		{
			return $query;
		}
		return FALSE;
	}

	function list_servicios_by_sitio($sitio_id)
	{
		$this->db->select('servicio_id, servicio_url,
			c.categoria_id, c.categoria_nombre,
			sitios.sitio_id, sitios.sitio_nombre, 
			secciones.seccion_id, secciones.seccion_nombre');
		$this->db->from('servicios');
		$this->db->join('categorias c', 'c.categoria_id = servicios.categoria_id', 'left');
		$this->db->join('sitios', 'sitios.sitio_id = servicios.sitio_id');
		$this->db->join('secciones', 'secciones.seccion_id = servicios.seccion_id', 'left');
		$this->db->where('servicios.sitio_id', $sitio_id);
		$query = $this->db->get()->result();
		
		foreach($query as $q)
		{
			$url = substr($q->servicio_url, strrpos($q->servicio_url, '/') + 1);
			if ($url == 'update')
			{
				$url = "sitemap";
			}
			else if ($url == 'news') 
			{
				$url = "sitemap noticias";
			}
			else
			{
				$url = str_replace('_service', '', $url);
				$url = str_replace('_manual', '', $url);
			}
			
			$q->servicio_url = $url;
		}

		if(!empty($query))
		{
			return $query;
		}
		return FALSE;
	}

	function procesar_servicio()
	{
		$this->form_validation->set_rules('servicio_url', 'URL', 'trim|required');
		$this->form_validation->set_rules('sitio_id', 'Sitio', 'required|trim');
		$this->form_validation->set_rules('seccion_id', 'Sección', 'trim');
		$this->form_validation->set_rules('categoria_id', 'Categoría', 'trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$servicio_id = $this->input->post('servicio_id', TRUE);
			$categoria_id = $this->input->post('categoria_id', TRUE);
			$seccion_id = $this->input->post('seccion_id', TRUE);
			$sitio_id = $this->input->post('sitio_id', TRUE);
			$servicio_url = $this->input->post('servicio_url', TRUE);

			$servicio_array = array(
				'sitio_id' => $sitio_id,
				'seccion_id' => $seccion_id,
				'categoria_id' => $categoria_id,
				'servicio_url' => $servicio_url
			);

			if(empty($servicio_id))
			{
				// insertar a la tabla usuarios
				$servicio_array['servicio_fecha_creacion'] = date('Y-m-d G:i:s');
				$this->db->insert('servicios', $servicio_array);
			}
			else
			{
				// editar datos básicos
				$this->db->where('servicio_id', $servicio_id);
				$this->db->update('servicios', $servicio_array);
			}
			
			return $this->db->affected_rows();
		}
	}

	function get_servicio_by_id($servicio_id)
	{
		$this->db->select('servicios.servicio_id, servicios.servicio_url, 
			c.categoria_id, c.categoria_nombre,
			sitios.sitio_id, sitios.sitio_nombre, 
			secciones.seccion_id, secciones.seccion_nombre');
		$this->db->from('servicios');
		$this->db->join('sitios', 'sitios.sitio_id = servicios.sitio_id');
		$this->db->join('secciones', 'secciones.seccion_id = servicios.seccion_id', 'left');
		$this->db->join('categorias c', 'c.categoria_id = servicios.categoria_id', 'left');
		$this->db->where('servicios.servicio_id', $servicio_id);
		$query = $this->db->get()->row();

		if(!empty($query))
		{
			return $query;
		}
		return FALSE;
	}

	function delete_servicio($servicio_id)
	{
		$this->db->where('servicio_id', $servicio_id);
		$this->db->delete('servicios');

		return $this->db->affected_rows();
	}

	function check_update_time($servicio_id)
	{
		$this->db->select('last_update');
		$this->db->from('servicios');
		$this->db->where('servicio_id', $servicio_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$query = $query->row();
			$dbdate = strtotime($query->last_update);
			if (time() - $dbdate > 5 * 60) 
			{
				return TRUE;    
			}
		}
		return FALSE;
	}

	function update_time($servicio_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array['usuario_id'] = $usuario_mod;
		$array['last_update'] = date('Y-m-d G:i:s');
		$this->db->where('servicio_id', $servicio_id);
		$this->db->update('servicios', $array);
	}

	function get_servicio_url($servicio_id)
	{
		$this->db->select('servicio_url');
		$this->db->from('servicios');
		$this->db->where('servicio_id', $servicio_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$query = $query->row();
			return $query->servicio_url;
		}
	}

	function validar_tipo_nota($sitio_id, $nota_id)
	{
		$this->db->select('tipo_nota_id');
		$this->db->from('notas');
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('nota_id', $nota_id);
		$this->db->where('nota_estado', '1');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}
}