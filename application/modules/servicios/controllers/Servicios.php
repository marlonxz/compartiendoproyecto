<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends MY_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('Servicios_modelo');
        $this->load->library('form_validation');
	}

	function index()
	{
		$this->load->module('sitios/sitios_getters');
		$usuario_id = $this->session->userdata('usuario_id');
		$sitios_id = $this->sitios_getters->list_sitios_id_by_usuario($usuario_id);		
        $data['sitios'] = $this->sitios_getters->list_sitios_by_ids($sitios_id);

        $this->load->view('servicios_lista', $data);
	}

	function administrar_servicios()
	{
		$data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $data['servicios'] = $this->Servicios_modelo->get_all_servicios();
        }
        
        $this->load->view('servicios_form', $data);    
	}

	function actualizar($servicio_id)
	{
		// verificar si se actualizó hace menos de 5 minutos
		$validacion = $this->Servicios_modelo->check_update_time($servicio_id);

		if ($validacion) // si han pasado más de 5 minutos
		{
			// conseguir la url del servicio
			$url = $this->Servicios_modelo->get_servicio_url($servicio_id).'?'.date("dmYHis");

			// llamar a la url
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_exec($ch);

            $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($resultStatus == 200) 
            {
                // actualizar el tiempo
                $this->Servicios_modelo->update_time($servicio_id);
                
                $this->session->set_flashdata('success', 'Servicio actualizado');
            } 
            else 
            {
                $this->session->set_flashdata('error', 'Hubo un error, por favor inténtalo de nuevo');
            }
			
			curl_close($ch);
		}
		else
		{
			$this->session->set_flashdata('error', 'Debes esperar 5 minutos para actualizar nuevamente.');
		}

		redirect('servicios/mensaje', 'refresh');
	}

    function actualizar_individual($sitio_id, $nota_id)
    {
        // validar el tipo de nota
        $tiponota = $this->Servicios_modelo->validar_tipo_nota($sitio_id, $nota_id);

        if($tiponota)
        {
            // ejecutar el llamado al json
            $url = 'http://xcms.plustv.pe/json/getnota?pid='.$sitio_id.'&nid='.$nota_id;
            // $url = 'http://zeus.dev/json/getnota?pid='.$sitio_id.'&nid='.$nota_id;

            // llamar a la url
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);

            $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($resultStatus == 200)
            {
                curl_close($ch);
                // ejecutar el llamado al servicio
                switch ($sitio_id) {
                    case '1':
                        $servicio = 'http://movistarplus.pe/services/nota_service?'.date("dmYHis");
                        break;
                    case '2':
                        $servicio = 'http://movistardeportes.pe/services/nota_service?'.date("dmYHis");
                        break;
                }
                // 
                // switch ($sitio_id) {
                //     case '1':
                //         $servicio = 'http://plus.dev/services/nota_service?'.date("dmYHis");
                //         break;
                //     case '2':
                //         $servicio = 'http://cmd.dev/services/nota_service?'.date("dmYHis");
                //         break;
                // }

                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $servicio);
                curl_setopt($ch2, CURLOPT_HEADER, 0);
                curl_exec($ch2);

                $resultStatus2 = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                if($resultStatus2 == 200)
                {
                    $this->session->set_flashdata('success', 'Servicio actualizado');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hubo un error, por favor inténtalo de nuevo');
                    curl_close($ch2); 
                }

            }
            else
            {
                $this->session->set_flashdata('error', 'Hubo un error, por favor inténtalo de nuevo');
                curl_close($ch); 
            }


            
        }
        else
        {
            $this->session->set_flashdata('error', 'Tipo de nota incorrecta / No existe el id');
        }

        redirect('servicios/mensaje', 'refresh');
        
    }

	function mensaje()
	{
		$this->load->view('servicios_mensaje');
	}

	function crear()
    {
        $data = array();
        $roles = $this->session->userdata('roles');

        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $this->load->module('sitios/sitios_getters');
            $data['sitios'] = $this->sitios_getters->list_sitios();            
        }

        $this->load->view('crear_servicio_form', $data);
    }

    function editar($servicio_id)
    {
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $this->load->module('sitios/sitios_getters');
            $this->load->module('secciones/secciones_getters');
            $this->load->module('categorias');
            
            $data['sitios'] = $this->sitios_getters->list_sitios();
            $data['secciones'] = json_encode($this->secciones_getters->list_secciones());
            $data['categorias'] = json_encode($this->categorias->list_categorias());  

            $servicio = $this->Servicios_modelo->get_servicio_by_id($servicio_id);

            $data['servicio'] = $servicio;
        }

        $this->load->view('editar_servicio_form', $data);
    }

    function eliminar($servicio_id)
    {
        $result = $this->Servicios_modelo->delete_servicio($servicio_id);
        if($result)
        {
            $this->session->set_flashdata('success', '¡Servicio eliminado!');
            redirect('servicios/administrar-servicios');
        }
        else
        {
            $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
            redirect('servicios/administrar-servicios');
        }
    }

    function procesar()
    {
        // Verificar que el usuario es admin o moderador
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Servicios_modelo->procesar_servicio();
            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Servicio grabado!');
            }
            else
            {
                $this->session->set_flashdata('error', 'Hubo un problema. Por favor inténtalo de nuevo');
            }
            redirect('servicios/administrar-servicios');
        }
    }

    // funciones ajax
	function get_secciones()
	{
		$sitio_id = $this->input->post('sitio_id', TRUE);
		$this->load->module('secciones/secciones_getters');
		$secciones = $this->secciones_getters->list_secciones_by_sitio($sitio_id);
		echo json_encode($secciones);
	}

	function get_categorias()
	{
		$seccion_id = $this->input->post('seccion_id', TRUE);
		$this->load->module('categorias/categorias_getters');
		$categorias = $this->categorias_getters->list_categorias_by_seccion($seccion_id);
		echo json_encode($categorias);
	}

	function get_servicios()
	{
		$sitio_id = $this->input->post('sitio_id', TRUE);
		$servicios = $this->Servicios_modelo->list_servicios_by_sitio($sitio_id);
		echo json_encode($servicios);
	}

}