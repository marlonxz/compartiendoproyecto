<?php 
$titulo = "Actualizar servicios| Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/datatables-net/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/datatables-net.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
<?php if($this->session->flashdata('error') != ''): ?>
  <div class="alert alert-danger">
    <?php echo $this->session->flashdata('error'); ?>
  </div>
<?php endif; ?>
<?php if($this->session->flashdata('success') != ''):?>
  <div class="alert alert-success">
    <?php echo $this->session->flashdata('success');?>
  </div>
<?php endif; ?>
<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
