<?php 
$titulo = "Actualizar servicios | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/datatables-net/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/datatables-net.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<style type="text/css">
  #nota_individual_tabla{
    margin-bottom: 30px;
  }
</style>
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
    <div class="container-fluid">
      <div class="box-typical box-typical-padding">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h2>Actualizar servicios</h2>
                <div class="subtitle">En este panel podrás actualizar los servicios</div>
              </div>
            </div>
            <br>
          </div>
        </header>
        <div class="form-group row">
          <label class="col-sm-2 form-control-label">Sitio *</label>
          <div class="col-sm-6">
            <select id="sitios" name="sitio_id" class="form-control sitio" data-placeholder="Elegir un sitio" required>
              <option value="">&nbsp;</option>
              <?php foreach($sitios as $sitio): ?>
                <option value="<?php echo $sitio->sitio_id;?>"><?php echo $sitio->sitio_nombre;?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-12" id="nota_individual_tabla">
            <table id="tabla1"></table>
          </div>
          <br>
          <div class="col-sm-12">
            <table id="dataTable1" class="display table table-bordered" cellspacing="0" width="100%">
            </table>
          </div>
        </div>
      </div><!-- .box-typical-->
    </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/datatables-net/datatables.min.js"></script>

<script>
  $(function() {
    $('.eliminarCategoria').click(function(e){
      return confirm('¿Estás seguro?');
    });

    $("#sitios").change(function(e){
      var sitio_id = $('#sitios').val();
      // conseguir las secciones del sitio
      if( sitio_id != ""){
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url();?>servicios/get_servicios',
          data: {'sitio_id': sitio_id},
          dataType: 'json',
          success: function(data){
            if (data != 0){
                get_table(data);
                get_table_nota();
            }
          },
          error: function(msg){
            console.log(msg);
          }
        });
      }
    });

  });

  $(document).on('change', $("#nota_id"), function(e){
    var tabla = document.getElementById("nota_individual");
    if(tabla) {
      var sitio_id = $("#sitios").val();
      var nota_id = $("#nota_id").val();
      if(nota_id != ""){
        var emptycell3 = tabla.firstChild.firstChild.lastChild;
        emptycell3.innerHTML = "<a onclick=\"window.open('<?php echo base_url();?>servicios/actualizar_individual/"+ sitio_id + "/"+nota_id+"', 'nuevaVentana', 'width=250, height=100');\" title=\"URL\"><span class='fa fa-refresh'></a>"
      }
    }
    
  });

  function get_table_nota() {
    var div_tabla = document.getElementById("nota_individual_tabla");

    // borrar la tabla existente
    while (div_tabla.firstChild) {
      div_tabla.removeChild(div_tabla.firstChild);
    }

    // crear una nueva tabla
    var tabla = document.createElement("table");
    tabla.className = "display table table-bordered";
    tabla.id = "nota_individual";
    var row = tabla.insertRow(0);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);

    cell1.innerHTML = "Nota Individual";
    cell2.innerHTML = "ID: <input type='text' name='nota_id' id='nota_id'>"


// // Add some text to the new cells:
// cell1.innerHTML = "NEW CELL1";
// cell2.innerHTML = "NEW CELL2";


//     tabla.append("<tbody><tr><td>Nota Individual</td><td>ID: <input type='text' name='nota_id' id='nota_id'></td><td></tr></tbody>");
    console.log(tabla);
    div_tabla.appendChild(tabla);

  }

  function get_table(json) {
    if ( $.fn.dataTable.isDataTable( '#dataTable1' ) ) {
      console.log(json);
        $('#dataTable1').dataTable().fnClearTable();
        $('#dataTable1').dataTable().fnAddData(json);
    } 
    else {
        $('#dataTable1').DataTable({
          'iDisplayLength': 25,
          "aoColumns": [
              { "sTitle": "sitio",  "mData": "sitio_nombre" },
              { "sTitle": "Sección",  "mData": "seccion_nombre" },
              { "sTitle": "Categoría",   "mData": "categoria_nombre" },
              { "sTitle": "URL", "mData": "servicio_url" },
              {
                "mData": null,
                "bSortable": false,
                "mRender": function(o){ return '<a onclick="window.open(\'<?php echo base_url();?>servicios/actualizar/'+o.servicio_id+'\',\'nuevaVentana\',\'width=250, height=100\');" title="URL"><span class="fa fa-refresh"></a>'; }
              }
            ],
          responsive: true,
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
          }
        });
        $('#dataTable1').dataTable().fnAddData(json);
    }
  }
</script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
