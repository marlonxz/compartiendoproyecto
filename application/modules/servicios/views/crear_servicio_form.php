<?php 
$titulo = "Crear servicio | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Completa todos los campos para crear un servicio nuevo.</h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link active" role="tab">
                    <span class="nav-link-in">
                      <span class="font-icon font-icon-pencil"></span>
                      Contenido
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="tabs-2-tab-1">
              <form method="post" action="<?php echo base_url();?>servicios/procesar">
                <div class="form-group row">
                  <label for="sitio_id" class="col-sm-2 form-control-label">
                  Sitio *</label>
                  <div class="col-sm-6">
                    <select id="sitios1" name="sitio_id" class="form-control" data-placeholder="Elegir un sitio" required>
                      <option value="">&nbsp;</option>
                      <?php foreach($sitios as $sitio): ?>
                        <option value="<?php echo $sitio->sitio_id;?>"><?php echo $sitio->sitio_nombre;?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="seccion_id" class="col-sm-2 form-control-label">Sección *</label>
                  <div class="col-sm-6">
                    <select id="secciones1" name="seccion_id" class="form-control" data-placeholder="Elegir una sección">
                      <option value="">&nbsp;</option>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="categoria_id" class="col-sm-2 form-control-label">Categoría *</label>
                  <div class="col-sm-6">
                    <select id="categorias1" name="categoria_id" class="form-control" data-placeholder="Elegir una categoría">
                      <option value="">&nbsp;</option>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="servicio_url" class="col-sm-2 form-control-label">URL</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><input type="text" class="form-control" name="servicio_url" placeholder="URL"></p>
                  </div>
                </div>

                <hr>

                <div class="form-group row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-rounded btn-inline btn-primary">Crear</button>
                    <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
                  </div>
                </div>  
              </form>
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script type="text/javascript">

  $('#cancelar').click(function(e){
      e.preventDefault();
      window.location.href="<?php echo base_url();?>servicios/administrar-servicios";
  });

// categorias
  $("#sitios1").change(function(e){
    // limpiamos seccion si se cambia de sitio
    $('#secciones1').find('option').remove().end();
    $("#secciones1").append('<option value="">&nbsp;</option').val('');    
    $('#categorias1').find('option').remove().end();
    $("#categorias1").append('<option value="">&nbsp;</option').val('');
    var sitio_id = $('#sitios1').val();

    // conseguir las secciones del sitio
    if( sitio_id != ""){
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>servicios/get_secciones',
        data: {'sitio_id': sitio_id},
        dataType: 'json',
        success: function(data){
          if (data != 0){
            console.log('test');
            console.log(data);
            $.each(data, function(i, d){
               $("#secciones1").append('<option value="'+ d.seccion_id + '">'
                                    + d.seccion_nombre + '</option>', false);
              });
          }
        },
        error: function(msg){
            console.log(msg);
        }
      });
    }
  });

  $("#secciones1").change(function(e){
    $('#categorias1').find('option').remove().end();
    $("#categorias1").append('<option value="">&nbsp;</option').val('');
    var seccion_id = $('#secciones1').val();
    // conseguir las categorías del blog
    if(seccion_id != ""){
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url();?>servicios/get_categorias',
          data: { 'seccion_id': seccion_id},
          dataType: 'json',
          success: function(data){
            console.log(data);
            if(data != 0){
              $.each(data, function(i, d){
               $("#categorias1").append('<option value="'+ d.categoria_id + '">'
                                    + d.categoria_nombre + '</option>', false);
              });
            } 
          },
          error: function(msg){
            console.log(msg);
          }
        });
    }
  });

</script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
