<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->session->sess_destroy();
    	header('Location: http://localhost/Milhoras-cms-panacea-2ab8427702b6/');	
	}

}