<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('sitios/Sitios_modelo');
		$data['sitios'] = $this->Sitios_modelo->get_all();

		if($_POST)
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$sitio = $this->input->post('sitio');
			$access = FALSE;

			$this->load->model('validador/Validador_modelo');
			$credential = $this->Validador_modelo->validar_usuario($email, $password);

			if($credential)
			{
				// conseguir sitios y roles del usuario
				$this->load->model('usuarios/Usuarios_modelo');
				$usuario = $this->Usuarios_modelo->get_by_email($email);
				$permisos =$this->Validador_modelo->get_rol($usuario->usuario_id);
				
				// validar que el usuario pertenece al sitio que eligió
				foreach($permisos as $key => $permiso)
				{
					if($key == $sitio)
					{
						$access = TRUE;
						break;
					}
				}

				if($access)
				{
					$sitios = $this->Validador_modelo->get_sitios_permitidos($usuario->usuario_id);

					// crear variables de sesión
					$this->session->set_userdata('sitios', $sitios);
			        $this->session->set_userdata('usuario', $usuario->usuario_user);
			        $this->session->set_userdata('nombre', $usuario->usuario_nombre);
			        $this->session->set_userdata('email', $usuario->usuario_email);
			        $this->session->set_userdata('password_hash', $usuario->usuario_password);
			        $this->session->set_userdata('usuario_id', $usuario->usuario_id);
			        $this->session->set_userdata('roles', $permisos);

			        redirect('sitios/'.$sitio);
				}
				else
				{
					$this->session->set_flashdata('error', 'No tiene permisos para este sitio');
				}

			}
			else
			{
				$this->session->set_flashdata('error', 'Usuario y/o Contraseña Incorrectos');
			}
		}

		$this->load->view('login', $data);
	}

}