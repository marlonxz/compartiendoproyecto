<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require FCPATH.'assets/detextile/Html2textile.php';
require_once(APPPATH.'libraries/aws-library/aws-autoloader.php');
set_time_limit(3000);

class Import_data extends MY_Controller {

	function get_noticias()
	{
		$noticias = json_decode(file_get_contents(FCPATH.'/json/datos-actualidad.txt'), TRUE);
		$detextile = new Html2textile();
		$bucket = $this->_set_bucket(1);

		foreach($noticias as $noticia)
		{
			$fecha = $this->_convertir_timestamp($noticia['timestamp']);
			
			// conseguir la sección y su id
			$seccion = $this->_get_seccion($noticia['linkseo']);
			$seccion_id = $this->_get_seccion_id($seccion); // devuelve 4 por default
			$categoria_id = $this->_get_categoria_id($seccion_id); // devuelve por default
			$nota_seolink = $this->_get_seolink($seccion, $noticia['titular']); // actualidad/generales por default

			$nota_titulo = $noticia['titular'];
			$nota_resumen = $noticia['resumen'];
			$nota_contenido = $detextile->detextile($noticia['desarrollo']);
			$sitio_id = 1;
			$tipo_nota_id = 1;
			$nota_estado = $noticia['estado'];
			$usuario_id = 1;

			$noticia_data = array(
				'sitio_id' => $sitio_id,
				'tipo_nota_id' => $tipo_nota_id,
				'seccion_id' => (int)$seccion_id,
				'categoria_id' => (int)$categoria_id,
				'nota_titulo' => $nota_titulo,
				'nota_resumen' => $nota_resumen,
				'nota_contenido' => $nota_contenido,
				'nota_seolink' => $nota_seolink,
				'nota_estado' => $nota_estado,
				'nota_fech_creacion' => $fecha,
				'nota_fech_update' => $fecha,
				'nota_fecha_publicacion' => $fecha,
				'usuario_id' => $usuario_id
			);

				$this->db->insert('notas', $noticia_data);
				$nota_id = $this->db->insert_id();
				var_dump($nota_id);

			// elementos

			// guardar las imagenes en tmp, redimensionarlas y subirlas al bucket
			if(!empty($noticia['fotoportada']))
			{
				$url = $noticia['fotoportada'];
				$nombre = substr($noticia['fotoportada'], strrpos($noticia['fotoportada'], '/') + 1);
				$size = getimagesize($url);
				$imagenes = $this->_get_imagen_info($size, 1);
				$this->_save_image('imagen', $nombre, $url, $imagenes, $bucket);

				// elementos
				$imagen_array = array(
					'tipoelemento_id' => 2,
					'elemento_ruta' => $nombre
				);
				$this->db->insert('elementos', $imagen_array);
				$elemento_id = $this->db->insert_id();

				// notas elemento
				$ne_array = array(
					'nota_id' => $nota_id,
					'elemento_id' => $elemento_id
				);
				$this->db->insert('notas_elemento', $ne_array);

				// tamanio elemento foto
				// insertar en Tamanio_elemento_foto
		        $data_tamanio = array();
		        foreach($imagenes['nombre'] as $key => $value)
		        {
		            $data_tamanio[] = array(
		                'elemento_id' => $elemento_id,
		                'sitio_id' => (int)$sitio_id,
		                'tamanio_elemento_nombre' => $imagenes['nombre'][$key],
		                'tamanio_elemento_ruta' => $imagenes['ruta'][$key].$nombre,
		                'tamanio_elemento_estado' => '1',
		                'tamanio_elemento_ancho' => $imagenes['ancho'][$key],
		                'tamanio_elemento_alto' => $imagenes['alto'][$key],
		            );
		        }
		        $this->db->insert_batch('tamanio_elemento_foto', $data_tamanio);

				unset($imagen_array);
				unset($noticia_data);
				unset($nota_id);
				unset($elemento_id);
				unset($data_tamanio);
			}

		}

		echo 'listo Pedro 2';

	}

	private function _convertir_timestamp($stamp)
	{
		return date('Y-m-d G:i:s', (int)$stamp);
	}

	private function _get_seccion($url)
	{
		$seccion = explode('/', $url);
		switch ($seccion) {
			case 'politica':
				return 'Política';
				break;
			case 'opinion':
				return 'Opinión';
				break;
			case 'deportes':
				return 'Deportes';
				break;
			case 'actualidad':
				return 'Actualidad';
				break;
			case 'espectaculos':
				return 'Espectáculos';
				break;
			default:
				return 'Actualidad';
				break;
		}
	}

	private function _get_seccion_id($seccion)
	{
		if(!empty($seccion))
		{
			$this->db->select('seccion_id');
			$this->db->where('seccion_nombre', $seccion);
			$this->db->where('seccion_estado', '1');
			$query = $this->db->get('secciones');

			if($query->num_rows() > 0)
			{
				$seccion = $query->row();
				return $seccion->seccion_id;
			}
		}

		return 4; // actualidad por default
	}

	private function _get_categoria_id($seccion_id)
	{
		$this->db->select('categoria_id');
		$this->db->where('seccion_id', $seccion_id);
		$this->db->where('categoria_nombre', 'Generales');
		$query = $this->db->get('categorias');

		if($query->num_rows() > 0)
		{
			$categoria = $query->row();
			return $categoria->categoria_id;
		}

		return FALSE;
		
	}

	private function _get_seolink($seccion, $url)
	{
		$linkseo = url_title(convert_accented_characters($url), '-', TRUE);
		switch ($seccion) {
			case 'Política':
				$seccion = 'politica';
				break;
			case 'Deportes':
				$seccion = 'deportes';
				break;
			case 'Actualidad':
				$seccion = 'actualidad';
				break;
			case 'Opinión':
				$seccion = 'opinion';
				break;
			case 'Espectáculos':
				$seccion = 'espectaculos';
				break;
			default:
				$seccion = 'test';
				break;
		}
		return $seccion.'/generales/'.$linkseo;
	}

	function _get_imagen_info($imagen_info, $sitio_id)
	{
		$this->db->select('ti.tamano_nombre nombre, ti.tamano_ancho ancho, ti.tamano_alto alto');
		$this->db->where('sitio_id', $sitio_id);
		$tamaños = $this->db->get('tamanos_imagen_sitio ti')->result_array();

        $imagenes['nombre'] = array('original');
        $imagenes['ancho'] = array($imagen_info[0]);
        $imagenes['alto'] = array($imagen_info[1]);
        $imagenes['ruta'] = array('/original/');

        foreach($tamaños as $tamaño)
        {
        	$imagenes['nombre'][] = $tamaño['nombre'];
        	$imagenes['ancho'][] = $tamaño['ancho'];
        	$imagenes['alto'][] = $tamaño['alto'];
        	$imagenes['ruta'][] = '/'.$tamaño['nombre'].'/';
        }

		return $imagenes;
	}

	function _save_image($dir, $name, $url, $tamaños, $bucket)
    {
        $ext = substr($url, strrpos($url, '.') + 1);

        if(!is_dir($dir))
        {
            $antiguo = umask(0);
            mkdir($dir, 0775, TRUE);
            umask($antiguo);
        }

        //$new_img = 
        $new_img_dir = FCPATH . $dir.'/'.$name;

        switch ($ext) {
            case 'jpeg':
            case 'jpg':
                $mime = 'image/jpeg';
                break;
            case 'png':
                $mime = 'image/png';
                break;
            case 'gif':
            	$mime = 'image/gif';
            	break;
        }

        // guardar la imagen en local
        copy($url, $new_img_dir);

        // resize
        $this->load->library('Image_moo');
        foreach($tamaños['nombre'] as $key => $value)
        {
        	$this->image_moo->load($new_img_dir)->resize_crop($tamaños['ancho'][$key], $tamaños['alto'][$key])->save('imagen'.$tamaños['ruta'][$key].$name, TRUE);
        }
        $this->image_moo->clear();

        // subir a CDN 
        foreach($tamaños['nombre'] as $key => $img)
        {
        	$promise = $this->client->putObjectAsync(array(
        			'Bucket' => $bucket,
					'Key' => 'imagen'.$tamaños['ruta'][$key].$name, // nombre del archivo
					'SourceFile' => 'imagen'.$tamaños['ruta'][$key].$name, // ruta
					'ACL' => 'public-read',
					'StorageClass' => 'STANDARD',
					'ContentType' => $mime
        		));
        	$result = $promise->wait();
        }    

        $result = $promise->wait();
    }

    function _set_bucket($sitio_id)
    {
		$this->db->select('sitio_ruta_cdn, sitio_usuario_cdn, sitio_pass_cdn');
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('sitio_estado', '1');
		$bucket = $this->db->get('sitios')->row_array();

		$key = $bucket['sitio_usuario_cdn'];
		$secret_key = $bucket['sitio_pass_cdn'];
		$bucket = $bucket['sitio_ruta_cdn'];
		$sharedConfig = [
            'region' => 'us-east-1',
            'version' => 'latest'
        ];
        // Create an SDK class used to share configuration across clients
        $sdk = new Aws\Sdk($sharedConfig);

        // Create an Amazon S3 client using shared configuration data
        $credentials = new Aws\Credentials\Credentials($key, $secret_key);
        $this->client = $sdk->createS3(['credentials' => $credentials, 'http' => ['verify' => false]]);

        return $bucket; 
    }
}