<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_getters_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'tags';
	}

	function get_tags_by_sitio($sitio_id)
	{
		$this->db->select('tags.tag_id, tags.tipotag_id, tags.sitio_id, tags.tag_nombre, tags.tag_ruta, tags.tag_estado, tags.tag_fech_creacion, tags.tag_fech_update, tags.tag_usuario_mod, sitios.sitio_nombre, tipotag.tipotag_nombre, usuarios.usuario_user');
		$this->db->from($this->__tabla);
		$this->db->join('usuarios', 'usuarios.usuario_id = tags.tag_usuario_mod');
		$this->db->join('sitios', 'sitios.sitio_id = tags.sitio_id');
		$this->db->join('tipotag', 'tipotag.tipotag_id = tags.tipotag_id');
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.flag_extendido !=', '1');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function get_tags_simples($sitio_id)
	{
		$this->db->select('tag_nombre, tag_ruta, tag_id, tipotag_id, sitio_id');
		$this->db->from($this->__tabla);
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('tag_estado', '1');
		$this->db->where('flag_extendido !=', '1');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function get_tags_extendidos($sitio_id)
	{
		$this->db->select('tag_nombre, tag_ruta, tag_id, tipotag_id, sitio_id');
		$this->db->from($this->__tabla);
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('tag_estado', '1');
		$this->db->where('flag_extendido', '1');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function get_tags_patrocinados($sitio_id)
	{
		$this->db->from($this->__tabla);
		$this->db->where('tag_estado', '1');
		$this->db->where('tipotag_id', '4');
		$this->db->where('sitio_id', $sitio_id);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function get_simples_by_elemento($elemento_id)
	{
		$this->db->select('tags.tag_id, tags.tag_nombre, tags.tag_ruta, tags.flag_extendido');
		$this->db->from($this->__tabla);
		$this->db->join('tag_elementos te', "te.tag_id = tags.tag_id AND te.elemento_id = $elemento_id");
		$this->db->where('tipotag_id', 1);
		$this->db->where('tags.tag_estado', '1');
		$this->db->group_by('tags.tag_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return FALSE;
	}

	function get_extendidos_by_elemento($elemento_id)
	{
		$this->db->select('tags.tag_id, tags.tag_nombre, tags.tag_ruta, tags.flag_extendido');
		$this->db->from($this->__tabla);
		$this->db->join('tag_elementos te', "te.tag_id = tags.tag_id AND te.elemento_id = $elemento_id");
		$this->db->where('tipotag_id', 3);
		$this->db->where('tags.tag_estado', '1');
		$this->db->group_by('tags.tag_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return FALSE;
	}

	function get_simples_by_nota($nota_id)
	{
		$this->db->select('tags.tag_id, tags.tag_nombre, tags.tag_ruta, tags.flag_extendido');
		$this->db->from($this->__tabla);
		$this->db->join('tag_notas tn', "tn.tag_id = tags.tag_id AND tn.nota_id = $nota_id");
		$this->db->where('tipotag_id', 1);
		$this->db->where('tags.tag_estado', '1');
		$this->db->group_by('tags.tag_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return FALSE;
	}

	function get_extendidos_by_nota($nota_id)
	{
		$this->db->select('tags.tag_id, tags.tag_nombre, tags.tag_ruta, tags.flag_extendido');
		$this->db->from($this->__tabla);
		$this->db->join('tag_notas tn', "tn.tag_id = tags.tag_id AND tn.nota_id = $nota_id");
		$this->db->where('tipotag_id', 3);
		$this->db->where('tags.tag_estado', '1');
		$this->db->group_by('tags.tag_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return FALSE;
	}

	function get_patrocinados_by_nota($nota_id)
	{
		$this->db->select('tags.tag_id, tags.tag_nombre, tags.tag_ruta, tags.flag_extendido');
		$this->db->from($this->__tabla);
		$this->db->join('tag_notas tn', "tn.tag_id = tags.tag_id AND tn.nota_id = $nota_id");
		$this->db->where('tipotag_id', 4);
		$this->db->where('tags.tag_estado', '1');
		$this->db->group_by('tags.tag_id');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return FALSE;
	}

	function check_tag_destacado($nota_id, $sitio_id)
	{
		// conseguir el id del tag
		$query = $this->db->select('tn.tag_id')
						->from('tag_notas tn')
						->join('tags', "tags.tag_id = tn.tag_id")
						->where('tags.tipotag_id', 3)
						->where('tn.nota_id', $nota_id)
						->get()
						->row();
		
		// conseguir tag destacado
		$query2 = $this->db->select('flag_destacado')->where('tag_id', $query->tag_id)->get('tags');
		if($query2->num_rows() > 0)
		{
			$query2 = $query2->row();
			return $query2->flag_destacado;
		}
		return FALSE;
	}

	function get_tags_by_lista($lista_id, $sitio_id)
	{
		$this->db->distinct();
		$this->db->from($this->__tabla);
		$this->db->join('tag_listas', $this->__tabla.'.tag_id = tag_listas.tag_id');
		$this->db->where('taglistas_estado', '1');
		$this->db->where('tipotag_id !=', 5);
		$this->db->where('lista_id', $lista_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function get_tag_patrocinado_by_lista($lista_id, $sitio_id)
	{
		$this->db->distinct();
		$this->db->select('tags.tag_id, tags.tag_nombre');
		$this->db->from($this->__tabla);
		$this->db->join('tag_listas', 'tag_listas.tag_id = tags.tag_id');
		$this->db->where('tag_listas.lista_id', $lista_id);
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.tipotag_id =', '5');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	

}