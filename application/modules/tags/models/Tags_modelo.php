<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'tags';
	}

	function get_tag_by_id($tag_id)
	{
		$this->db->select('tags.tag_id, tags.tag_nombre, tags.tag_ruta, tags.tag_fech_creacion, tags.tag_fech_update, ttag.tipotag_id, ttag.tipotag_nombre, sitios.sitio_id, sitios.sitio_nombre, usuarios.usuario_user');
		$this->db->join('sitios', 'sitios.sitio_id = tags.sitio_id');
		$this->db->join('tipotag ttag', 'ttag.tipotag_id = tags.tipotag_id');
		$this->db->join('usuarios', 'usuarios.usuario_id = tags.tag_usuario_mod');
		$this->db->where('tags.tag_id', $tag_id);
		$query = $this->db->get('tags')->row();

		if(!empty($query))
		{
			return $query;
		}
	}

	function get_tags_by_sitio($sitio_id)
	{
		$this->db->select('tags.tag_id, tags.tipotag_id, tags.sitio_id, tags.tag_nombre, tags.tag_ruta, tags.tag_estado, tags.tag_fech_creacion, tags.tag_fech_update, tags.tag_usuario_mod, sitios.sitio_nombre, tipotag.tipotag_nombre, usuarios.usuario_user');
		$this->db->from($this->__tabla);
		$this->db->join('usuarios', 'usuarios.usuario_id = tags.tag_usuario_mod');
		$this->db->join('sitios', 'sitios.sitio_id = tags.sitio_id');
		$this->db->join('tipotag', 'tipotag.tipotag_id = tags.tipotag_id');
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.flag_extendido !=', '1');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function get_tags_by_sitios($sitios_id)
	{
		$this->db->select('tags.tag_id, tags.tipotag_id, tags.sitio_id, tags.tag_nombre, tags.tag_ruta, tags.tag_estado, tags.tag_fech_creacion, usuarios.usuario_user, tags.tag_usuario_mod, sitios.sitio_nombre, tipotag.tipotag_nombre');
		$this->db->from($this->__tabla);
		$this->db->join('sitios', 'sitios.sitio_id = tags.sitio_id');
		$this->db->join('tipotag', 'tipotag.tipotag_id = tags.tipotag_id');
		$this->db->join('usuarios', 'usuarios.usuario_id = tags.tag_usuario_mod');
		$this->db->where_in('tags.sitio_id', $sitios_id);
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.flag_extendido !=','1');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function get_all_tags()
	{
		$this->db->select('tags.tag_id, tags.tipotag_id, tags.sitio_id, tags.tag_nombre, tags.tag_ruta, tags.tag_estado, tags.tag_fech_creacion, tags.tag_fech_update, tags.tag_usuario_mod, sitios.sitio_nombre, tipotag.tipotag_nombre, usuarios.usuario_user');
		$this->db->from($this->__tabla);
		$this->db->join('usuarios', 'usuarios.usuario_id = tags.tag_usuario_mod');
		$this->db->join('sitios', 'sitios.sitio_id = tags.sitio_id');
		$this->db->join('tipotag', 'tipotag.tipotag_id = tags.tipotag_id');
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.flag_extendido !=', '1');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function procesar_tag()
	{
		$this->form_validation->set_rules('tag_nombre', 'Tag', 'required|trim');
		$this->form_validation->set_rules('tag_ruta', 'Ruta del tag', 'required|trim');
		$this->form_validation->set_rules('tipotag_id', 'Tipo de Tag', 'required');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$tag_id = $this->input->post('tag_id', TRUE);
			$tag_nombre = $this->input->post('tag_nombre', TRUE);
			$tag_ruta = $this->input->post('tag_ruta', TRUE);
			$tipotag_id = $this->input->post('tipotag_id', TRUE);
			$sitio_id = $this->input->post('sitio_id', TRUE);
			$flag_extendido = $this->_validar_tag_extendido($tipotag_id);

			if(empty($tag_id))
			{
				// insertar a la tabla
				$tag_array_mult = array();
				foreach($sitio_id as $sid)
				{
					$tag_array_mult[] = array(
						'tag_nombre' => $tag_nombre,
						'tag_ruta' => url_title(convert_accented_characters($tag_ruta), '-', TRUE),
						'tipotag_id' => $tipotag_id,
						'tag_estado' => '1',
						'sitio_id' => $sid,
						'tag_usuario_mod' => $usuario_mod,
						'tag_fech_creacion' => date('Y-m-d G:i:s'),
						'flag_extendido' => $flag_extendido
					);
				}
				$this->db->insert_batch('tags', $tag_array_mult);
				return $this->db->affected_rows();
			}
			else
			{
				// editar datos básicos
				$tag_array = array(
					'tag_nombre' => $tag_nombre,
					'tag_ruta' => url_title(convert_accented_characters($tag_ruta), '-', TRUE),
					'tipotag_id' => $tipotag_id,
					'tag_estado' => '1',
					'sitio_id' => $sitio_id,
					'tag_usuario_mod' => $usuario_mod,
					'tag_fech_update' => date('Y-m-d G:i:s'),
					'flag_extendido' => $flag_extendido
				);
				$this->db->where('tag_id', $tag_id);
				$this->db->update('tags', $tag_array);
				return TRUE;
			}
			
			return FALSE;
		}
	}

	function delete_tag($tag_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'tag_estado' => '0',
			'tag_fech_update' => date('Y-m-d G:i:s'),
			'tag_usuario_mod' => $usuario_mod
		);

		$this->db->where('tag_id', $tag_id);
		$this->db->update('tags', $array);

		return $this->db->affected_rows();
	}

	function _validar_tag_extendido($tipotag_id)
	{
		$this->db->select('tipotag_extendido');
		$this->db->from('tipotag');
		$this->db->where('tipotag_id', $tipotag_id);
		$result = $this->db->get()->row();

		switch ($result->tipotag_extendido) 
		{
			case '0':
				return '0';
			case '1':
				return '1';			
			default: // en caso de error
				return '0';
		}
	}







	

	function get_tags_by_nota($nota_id, $sitio_id)
	{
		$this->_select_database($sitio_id);

		$this->db->distinct();
		$this->db->from($this->__tabla);
		$this->db->join('tag_notas', $this->__tabla.'.tag_id = tag_notas.tag_id');
		$this->db->where('tagnotas_estado', '1');
		$this->db->where_in('tipotag_id', [1,2,3]);
/*		$this->db->where('tipotag_id !=', 5);
		$this->db->where('tipotag_id !=', 6);*/
		$this->db->where('nota_id', $nota_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function get_tags_by_nota_canciones($nota_id, $sitio_id, $featured = null)
	{
		$this->_select_database($sitio_id);

		$this->db->distinct();
		$this->db->select('tags.tag_id, tags.tag_nombre');
		$this->db->from($this->__tabla);
		$this->db->join('tag_notas', 'tag_notas.tag_id = tags.tag_id');
		$this->db->where('tag_notas.nota_id', $nota_id);
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.tipotag_id !=', 1);
		$this->db->where('tipotag_id !=', 5);
		$this->db->where('tags.sitio_id', $sitio_id);
		if(!empty($featured))
		{
			$this->db->where('tag_notas.tag_featured', '1');
		}
		else
		{
			$this->db->where('tag_notas.tag_featured', '0');
		}
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}

	function get_tags_noticia_by_nota_canciones($nota_id, $sitio_id)
	{
		$this->_select_database($sitio_id);
		$this->db->distinct();
		$this->db->select('tags.tag_id, tags.tag_nombre');
		$this->db->from($this->__tabla);
		$this->db->join('tag_notas', 'tag_notas.tag_id = tags.tag_id');
		$this->db->where('tag_notas.nota_id', $nota_id);
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.tipotag_id', 1);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}

	function get_tags_by_sitio_canciones($sitio_id)
	{
		$this->_select_database($sitio_id);	// base de datos default
		$this->db->from($this->__tabla);
		$this->db->where('tag_estado', '1');
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('tipotag_id !=', 1);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}


	function get_tags_noticias_by_sitio_canciones($sitio_id)
	{
		$this->_select_database($sitio_id); // base de datos default
		$this->db->from($this->__tabla);
		$this->db->where('tag_estado', '1');
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('tipotag_id', 1);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function get_tags_by_elemento($elemento_id, $sitio_id)
	{
		$this->_select_database($sitio_id);

		$this->db->from($this->__tabla);
		$this->db->join('tag_elementos', $this->__tabla.'.tag_id = tag_elementos.tag_id');
		$this->db->where('tag_elemento_estado', '1');
		$this->db->where('elemento_id', $elemento_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	function get_noticias_tags($sitio_id)
	{
		$this->_select_database(1);		// base de datos default
		$this->db->from($this->__tabla);
		$this->db->where('tag_estado', '1');
		$this->db->where('tipotag_id', 1);
		$this->db->where('sitio_id', $sitio_id);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	
/*
	function delete_tags_from_galeria_fotos($nota_id, $tags, $sitio_id)
	{
		$this->_select_database($sitio_id);
		// seleccionar ids de los elementos de la nota
		$elementos = $this->Elementos_modelo->get_galerias_fotos_by_nota($nota_id, $sitio_id, TRUE);
		
		if($elementos)
		{
			$this->_select_database($sitio_id);
			// conseguir info de elementos
			$this->db->trans_start();
			foreach($elementos as $elemento)
			{
				// ELiminar Tags		    
				foreach($tags as $tag)
				{			
					$this->db->where('tag_id', $tag);
					$this->db->where('elemento_id', $elemento->elemento_id);
					$this->db->delete('tag_elementos');
				}
			}			
			$this->db->trans_complete();
		}
	}

	function delete_tags_from_galeria_videos($nota_id, $tags, $sitio_id)
	{
		$elementos = $this->Elementos_modelo->get_galerias_videos_by_nota($nota_id, $sitio_id, TRUE);
		if(!empty($elementos))
		{
			$this->_select_database($sitio_id);
			// conseguir info de elementos
			$this->db->trans_start();
			foreach($elementos as $elemento)
			{
				// ELiminar Tags		    
				foreach($tags as $tag)
				{			
					$this->db->where('tag_id', $tag);
					$this->db->where('elemento_id', $elemento->elemento_id);
					$this->db->delete('tag_elementos');
				}
			}			
			$this->db->trans_complete();
		}
	}

	function delete_tags_from_audio($nota_id, $tags, $sitio_id)
	{
		$elemento = $this->Elementos_modelo->get_audio_by_nota($nota_id, $sitio_id, TRUE);
		
		if($elemento)
		{
			$this->_select_database($sitio_id);
			$this->db->trans_start();
			// ELiminar Tags		    
			foreach($tags as $tag)
			{			
				$this->db->where('tag_id', $tag);
				$this->db->where('elemento_id', $elemento->elemento_id);
				$this->db->delete('tag_elementos');
			}		
			$this->db->trans_complete();
		}
	}

	function add_tags_from_galeria_fotos($nota_id, $tags, $sitio_id)
	{
		// seleccionar ids de los elementos de la nota
		$elementos = $this->Elementos_modelo->get_galerias_fotos_by_nota($nota_id, $sitio_id, TRUE);

		if($elementos)
		{
			$this->_select_database($sitio_id);
			foreach($elementos as $elemento)
			{
				$tag_elementos = array();
				// Insertar Tags		    
				foreach($tags as $tag)
				{				
					$tag_elementos[] = array(
						'tag_id' => $tag,
						'elemento_id' => $elemento->elemento_id,
						'tag_elemento_estado' => '1'
					);
				}
				$this->db->insert_batch('tag_elementos', $tag_elementos);	
			}			
		}
	}	

	function add_tags_from_galeria_videos($nota_id, $tags, $sitio_id)
	{
		$elementos = $this->Elementos_modelo->get_galerias_videos_by_nota($nota_id, $sitio_id, TRUE);
		
		if($elementos)
		{
			$this->_select_database($sitio_id);
			foreach($elementos as $elemento)
			{
				$tag_elementos = array();
				// ELiminar Tags		    
				foreach($tags as $tag)
				{				
					$tag_elementos[] = array(
						'tag_id' => $tag,
						'elemento_id' => $elemento->elemento_id,
						'tag_elemento_estado' => '1'
					);
				}
				$this->db->insert_batch('tag_elementos', $tag_elementos);	
			}			
		}
	}

	function add_tags_from_audio($nota_id, $tags, $sitio_id)
	{
		$elemento = $this->Elementos_modelo->get_audio_by_nota($nota_id, $sitio_id, TRUE);
		
		if($elemento)
		{
			$this->_select_database($sitio_id);
			$tag_elementos = array();
			// ELiminar Tags		    
			foreach($tags as $tag)
			{				
				$tag_elementos[] = array(
					'tag_id' => $tag,
					'elemento_id' => $elemento->elemento_id,
					'tag_elemento_estado' => '1'
				);
			}
			$this->db->insert_batch('tag_elementos', $tag_elementos);				
		}
	}
*/
/*
	function get_tags_autor($sitio_id)
	{
		$this->_select_database($sitio_id);		// base de datos default
		$this->db->from($this->__tabla);
		$this->db->where('tag_estado', '1');
		$this->db->where('tipotag_id', '6');
		$this->db->where('sitio_id', $sitio_id);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
*/
	function get_tag_patrocinado_by_nota($nota_id, $sitio_id)
	{
		$this->_select_database($sitio_id);

		$this->db->distinct();
		$this->db->select('tags.tag_id, tags.tag_nombre');
		$this->db->from($this->__tabla);
		$this->db->join('tag_notas', 'tag_notas.tag_id = tags.tag_id');
		$this->db->where('tag_notas.nota_id', $nota_id);
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.tipotag_id =', '5');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
/*
	function get_tag_autor_by_nota($nota_id, $sitio_id)
	{
		$this->_select_database($sitio_id);

		$this->db->distinct();
		$this->db->select('tags.tag_id, tags.tag_nombre');
		$this->db->from($this->__tabla);
		$this->db->join('tag_notas', 'tag_notas.tag_id = tags.tag_id');
		$this->db->where('tag_notas.nota_id', $nota_id);
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.tipotag_id =', '6');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
*/
	function get_tags_by_lista($lista_id, $sitio_id)
	{
		$this->_select_database($sitio_id);

		$this->db->distinct();
		$this->db->from($this->__tabla);
		$this->db->join('tag_listas', $this->__tabla.'.tag_id = tag_listas.tag_id');
		$this->db->where('taglistas_estado', '1');
		$this->db->where('tipotag_id !=', 5);
		$this->db->where('lista_id', $lista_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function get_tag_patrocinado_by_lista($lista_id, $sitio_id)
	{
		$this->_select_database($sitio_id);

		$this->db->distinct();
		$this->db->select('tags.tag_id, tags.tag_nombre');
		$this->db->from($this->__tabla);
		$this->db->join('tag_listas', 'tag_listas.tag_id = tags.tag_id');
		$this->db->where('tag_listas.lista_id', $lista_id);
		$this->db->where('tags.tag_estado', '1');
		$this->db->where('tags.tipotag_id =', '5');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

	}
	

}