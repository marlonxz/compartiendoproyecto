<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tags_modelo');
	}

	public function index()
	{
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles) AND !in_array('2', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
        	if(!in_array('1', $roles))
            {
                // get sitios donde es moderador
                $sitios_id = array_keys($roles, '2');
                $data['tags'] = $this->Tags_modelo->get_tags_by_sitios($sitios_id);
            }
            else
            {
            	$data['tags'] = $this->Tags_modelo->get_all_tags();
            }

	    }
		$this->load->view('lista_tags', $data);	
	}

    function listar()
    {
        $data = array();
        $usuario_id = $this->session->userdata('usuario_id');
        $roles = $this->session->userdata('roles');
        // get sitios donde es usuario
        $this->load->module('sitios/sitios_getters');
        $sitios_id = $this->sitios_getters->list_sitios_id_by_usuario($usuario_id);
        $data['tags'] = $this->Tags_modelo->get_tags_by_sitios($sitios_id);
        $this->load->view('listar_tags', $data); 
    }

	function crear()
	{
        $data = array();
		$roles = $this->session->userdata('roles');

        if(!in_array('1', $roles) AND !in_array('2', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
        	$this->load->module('sitios/Sitios_getters');
        	$this->load->module('tipotag');
        	$data['tipotags'] = $this->tipotag->list_tipotags();
        	if(!in_array('1', $roles))
        	{
        		$sitios_id = array_keys($roles, '2');
                $data['sitios'] = $this->sitios_getters->list_sitios_by_ids($sitios_id);
        	}
        	else
        	{
        		$data['sitios'] = $this->sitios_getters->list_sitios();
        	}            	 
        }

        $this->load->view('crear_tag_form', $data);
	}

	function eliminar($tag_id)
	{
		$result = $this->Tags_modelo->delete_tag($tag_id);
        if($result)
        {
            $this->session->set_flashdata('success', '¡Tag eliminado!');
            redirect('tags/index');
        }
        else
        {
            $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
            redirect('tags/index');
        }
	}

	function editar($tag_id)
	{
        $data = array();
		$roles = $this->session->userdata('roles');
        if(!in_array('1', $roles) AND !in_array('2', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $this->load->module('sitios/Sitios_getters');
        	$this->load->module('tipotag');
        	$data['tipotags'] = $this->tipotag->list_tipotags();
        	if(!in_array('1', $roles))
        	{
        		$sitios_id = array_keys($roles, '2');
                $data['sitios'] = $this->sitios_getters->list_sitios_by_ids($sitios_id);
        	}
        	else
        	{
        		$data['sitios'] = $this->sitios_getters->list_sitios();
        	} 

            $tag = $this->Tags_modelo->get_tag_by_id($tag_id);
            $data['tag'] = $tag;
        }

        $this->load->view('editar_tag_form', $data);
	}

	function procesar()
    {
        // Verificar que el usuario es admin o moderador
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles) AND !in_array('2', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {   
            $resultado = $this->Tags_modelo->procesar_tag();

            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Tag grabado!');
                redirect('tags/index');
            }
            else
            {
            	$this->session->set_flashdata('error', 'Hubo un problema. Por favor inténtalo de nuevo');
            	redirect('tags/index');
            }
        }
    }
}