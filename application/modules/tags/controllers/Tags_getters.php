<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_getters extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tags_getters_modelo');
	}

	function get_tags_simples_by_elemento($elemento_id)
	{
		return $this->Tags_getters_modelo->get_simples_by_elemento($elemento_id);
	}

	function get_tags_extendidos_by_elemento($elemento_id)
	{
		return $this->Tags_getters_modelo->get_extendidos_by_elemento($elemento_id);
	}

	function get_tags_simples_by_nota($nota_id)
	{
		return $this->Tags_getters_modelo->get_simples_by_nota($nota_id);
	}

	function get_tags_extendidos_by_nota($nota_id)
	{
		return $this->Tags_getters_modelo->get_extendidos_by_nota($nota_id);
	}

	function get_tags_patrocinados_by_nota($nota_id)
	{
		return $this->Tags_getters_modelo->get_patrocinados_by_nota($nota_id);
	}

}