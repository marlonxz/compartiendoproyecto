<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Feed_modelo extends CI_Model
{
	function get_programacion()
	{
		$hora = date("G");
		$minuto = date("i");	
		$programa_actual = array();
		$dia = date("d");
		$mes = date("m");	

		$json_file = FCPATH.'json/programacion.json';
		$programacion = json_decode(file_get_contents($json_file)); 

		foreach($programacion as $value)
		{
			$timestamp = $value->timestamp;
			$fecha_string = strtotime($timestamp);
			$hora_feed = date('G', $fecha_string);
			$minuto_feed = date('i', $fecha_string);
			
			if($dia == $value->dia AND $mes == $value->mes)
			{
				if($hora == $hora_feed)
				{
					if(!empty($minuto_feed) AND $minuto_feed != '00')
					{
						if($minuto <= $minuto_feed)
						{
							$programa_actual['timestamp'] = $value->timestamp;
							$programa_actual['titulo'] = $value->titulo;
							$programa_actual['descripcion'] = $value->descripcion;
							$programa_actual['categoria'] = $value->categoria;
							$programa_actual['conductor'] = $value->conductor;
							$programa_actual['episodio'] = $value->episodio;
							$programa_actual['fecha'] = $value->fecha;
							return $programa_actual; 
						}
					}
					else
					{
						$programa_actual['timestamp'] = $value->timestamp;
						$programa_actual['titulo'] = $value->titulo;
						$programa_actual['descripcion'] = $value->descripcion;
						$programa_actual['categoria'] = $value->categoria;
						$programa_actual['conductor'] = $value->conductor;
						$programa_actual['episodio'] = $value->episodio;
						$programa_actual['fecha'] = $value->fecha;
						return $programa_actual; 
					}				
					 
				}
			}
						
		}
	}
}