<?php 
$default_apaisado = "http://d37nqvw7m4y1fn.cloudfront.net/assets/images/BACK-LOGO2.jpg";
$default_cuadrada = "http://d37nqvw7m4y1fn.cloudfront.net/assets/images/BACK-LOGO.jpg";
?>
<?php $this->load->view('template/header');?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/portada.min.css?v=sKIN" type="text/css">
<?php $this->load->view('ads/analytics');?>
</head>
<body>
<div id='div-gpt-ad-1484923105844-0' >
<script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1484923105844-0'); });</script>
</div>
<?php $this->load->view('ads/layer');?>
<?php //$this->load->view('ads/comscore');?>

<!--====== LEADER Y LATERALES ADS======-->
<div class="container">
	<div class="wrapper">
		<?php
		$this->load->view('ads/laterales'); 
		$this->load->view('ads/leader');
		?>
	</div>
</div>
<!--====== FIN LEADER Y LATERALES ADS======-->
<!--====== HEADER LOGO, MENU SOCIAL MEDIA======-->
<div class="container wrapper_header">
	<div class="wrapper">
		<?php $this->load->view('template/banner');?>
	</div>
</div>
<!--====== FIN HEADER LOGO, MENU SOCIAL MEDIA======-->
<!--=== SUB MENU ==-->
<div class="container wrapper_submenu">
	<div class="wrapper" id="submenu_content">
		<?php $this->load->view('template/submenu');?>
	</div>
</div>
<!--=== FIN SUB MENU ==-->
<!--====== PUSHDOWN ADS======-->
<div class="container">
	<div class="wrapper">
		<?php $this->load->view('ads/pushdown');?>
	</div>
</div>
<!--====== FIN PUSHDOWN ADS======-->
<!--====== CONTENEDOR PRINCIPAL ======-->
<div class="container">
	<div class="wrapper">
		<div class="contenedor-principal">
			<!--====== NOTICIA PRINCIPAL Y RECTMED A ======-->
			<div class="col-md-12">
				<div class="content-grilla clearfix">
					<div class="grilla-uno">
						<?php if(!empty($portadas)):?>
						<div id="slider-principal" class="margin-bottom">						<?php foreach($portadas as $portada):?>
							<div class="bloque-nota">
								<a href="<?php echo $portada->categoriaUrl;?>" class="tag_slider">
									<i class="fa fa-plane"></i> <?php echo $portada->categoria;?>
								</a>
								<a href="/<?php echo $portada->linkseo.'-'.$portada->nid;?>" class="img_slider_portada">
									<img src="<?php echo !empty($portada->fotoportada) ? $portada->fotoportada : $default_apaisado;?>" alt="<?php echo $portada->titular;?>">
								</a>
								<a href="/<?php echo $portada->linkseo.'-'.$portada->nid;?>" class="descrip_slider">
									<div class="titulo-nota">
										<?php echo $portada->titular;?>
									</div>
									<div class="descrip-nota">
										<?php echo limpiar_detalle($portada->desarrollo).'...';?>
									</div>
								</a>
							</div>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>
						<div class="col-row margin-menos-lr clearfix">
							<div class="col-sx-12 col-sm-6 col-md-6 margin-bottom padding-menos-lr">
								<div class="block_home">
									<a href="/<?php echo $nota_videos->linkseo.'-'.$nota_videos->nid;?>" class="title title_magenta">
										<span>Video destacado</span>
										<div class="line_title"></div>
									</a>
									<a href="/<?php echo $nota_videos->linkseo.'-'.$nota_videos->nid;?>" class="block_destacado">
										<div class="descrip_video_destacado">
											<span class="btn_play"><i class="fa fa-play"></i></span> <?php echo $nota_videos->titular;?>
										</div>
										<img src="<?php echo $nota_videos->fotointerna;?>" alt="<?php echo $nota_videos->titular;?>">
									</a>							
								</div>
							</div>
							<?php if(!empty($fotogaleria)):?>
							<div class="col-sx-12 col-sm-6 col-md-6 margin-bottom padding-menos-lr">
								<div class="block_home">
									<a href="/fotogalerias" class="title title_turqueza">
										<span>Galería de fotos</span>
										<div class="line_title"></div>
									</a>
									<a href="/<?php echo $fotogaleria[0]->linkseo.'-'.$fotogaleria[0]->nid;?>" class="block_destacado">
										<div class="descrip_video_destacado">
											<?php echo $fotogaleria[0]->titular;?>
										</div>
										<div class="count_gale">
											<span><i class="fa fa-camera"></i></span> <?php echo count($fotogaleria[0]->fotosURL);?> Fotos
										</div>
										<img src="http://static.plustv.pe/imagen/mediano/<?php echo $fotogaleria[0]->fotosURL[0];?>" alt="<?php echo $fotogaleria[0]->titular;?>">
									</a>							
								</div>
							</div>
							<?php endif; ?>
						</div>
						
					</div>
					<div class="grilla-dos">
						<div class="wrapper-ad margin-bottom">
							<?php $this->load->view('ads/rectmedA'); ?>
							
						</div>				
					</div>
				</div>

			</div>
			<!--====== FIN NOTICIA PRINCIPAL Y RECTMED A ======-->

			<!--====== IMAGEN ESPECIALES PLUS TV=======-->
			<div class="col-md-12 margin-bottom">
				<div class="block_home block_especial">
					<!-- /27185205/Plus_Home_Midle1 -->
					<div id='div-gpt-ad-1484924381578-0'>
						<script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1484924381578-0'); });</script>
					</div>
				</div>
			</div>
			<!--====== FIN IMAGEN ESPECIALES PLUS TV=====-->
			
			<!--PUBLICDAD RECTMED Y PROGRAMAS -->
			<div class="col-md-12">
				<div class="content-grilla clearfix">
					<div class="grilla-uno-b margin-bottom">
						<div class="wrapper-ad">
							<?php $this->load->view('ads/rectmedB'); ?>
						</div>					
					</div>
					<div class="grilla-dos-b">
						<div class="col-row margin-menos-lr clearfix">
							<div class="col-sx-12 col-sm-6 col-md-6 margin-bottom padding-menos-lr">
								<div class="block_home">
									<a href="#">
										<img src="<?php echo base_url();?>assets/images/programa-consultorio-buena-vida.jpg" class="image_responsive" alt="">
									</a>
								</div>
							</div>
							<div class="col-sx-12 col-sm-6 col-md-6 margin-bottom padding-menos-lr">
								<div class="block_home">
									<a href="#">
										<img src="<?php echo base_url();?>assets/images/programa-dulces-secretos.jpg" class="image_responsive" alt="">
									</a>
								</div>
							</div>
							<div class="col-sx-12 col-sm-6 col-md-6 margin-bottom padding-menos-lr">
								<div class="block_home">
									<a href="#">
										<img src="<?php echo base_url();?>assets/images/programa-cocina-en-un-toque.jpg" class="image_responsive" alt="">
									</a>
								</div>
							</div>
							<div class="col-sx-12 col-sm-6 col-md-6 margin-bottom padding-menos-lr">
								<div class="block_home">
									<a href="#">
										<img src="<?php echo base_url();?>assets/images/programa-diario-de-carretera.jpg" class="image_responsive" alt="">
									</a>
								</div>
							</div>
						</div>				
					</div>
				</div>
			</div>
			<!-- FIN PUBLICIDAD RECTMED Y PROGRAMAS-->

			<!--====== IMAGEN boletin =======-->
			<div class="col-md-12 margin-bottom">
				<div class="block_home">
					<!-- /27185205/Plus_Home_Midle2 -->
					<div id='div-gpt-ad-1484924638786-0' style='height:90px; width:950px;'>
						<script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1484924638786-0'); });</script>
					</div>
				</div>
			</div>
			<!--====== FIN IMAGEN boletin =====-->

			<!--====== Noticias y SOCIAL ======-->
			<div class="col-md-12">
				<div class="content-grilla clearfix">
					<div class="grilla-uno" padding-menos-lr">
						<div class="block_home">
							<a href="/mundo-plus" class="title title_magenta">
								<span>Mundo Plus</span>
								<div class="line_title"></div>
							</a>						
						</div>
						<?php if(!empty($noticias)):?>
						<div class="col-row margin-menos-lr clearfix">
							<?php foreach($noticias as $noticia):?>
							<div class="col-sx-6 col-md-6 margin-bottom padding-menos-lr">
								<div class="block_home">
									<a href="/<?php echo $noticia->linkseo.'-'.$noticia->nid;?>" class="block_new">
										<div class="img_new_home">
											<i class="fa fa-search"></i>
											<img src="<?php echo !empty($noticia->fotoportada) ? $noticia->fotoportada : $default_apaisado;?>" alt="<?php echo $noticia->titular;?>" alt="<?php echo $noticia->titular;?>">
										</div>
										<div class="cat_time clearfix">
											<div class="cat"><?php echo $noticia->categoria;?></div>
											
										</div>
										<div class="title_new">
											<?php echo $noticia->titular;?>
										</div>
										<div class="time_new"><i class="fa fa-clock-o"></i> <?php echo $noticia->timestamp;?></div>
										<p><?php echo limpiar_detalle($noticia->desarrollo).'...';?></p>
									</a>
								</div>	
							</div>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>

						
					</div>
					<div class="grilla-dos clearfix">
						<div class="clearfix">
						<!--facebook-->
						<div class="col-md-12 col-sm-6 margin-bottom">
							<div class="block_home">
							<div class="fb-page" data-href="https://www.facebook.com/PlusTV6" data-tabs="timeline" data-width="300" data-height="409" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/PlusTV6" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/PlusTV6">Plus TV</a></blockquote></div>
							</div>
						</div>						
						<!--facebook-->
						<!--twitter-->
						<div class="col-md-12 col-sm-6 margin-bottom">
							<a class="twitter-timeline" href="https://twitter.com/PlusTV6" data-width="300" data-height="409">
							Tweets by @TwitterDev
							</a>
						</div>
						<!--Instagram-->
						<div class="col-md-12 col-sm-6 margin-bottom">
							<blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BP2uR9ZDOib/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank"> PlusTV (@plustv6)</a></p></div></blockquote>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
						</div>
						<!--Instagram-->  

						</div>
						
					</div>
				</div>

			</div>
			<!--====== FIN Noticias y SOCIAL ======-->

			<!--==VIDEOS DESTACADOS===-->
			<?php if(!empty($lista[0])):?>
			<div class="bloque_video_destacados">
				<a href="/<?php echo $lista[0]->seolink.'-'.$lista[0]->nid;?>" class="title_home margin-bottom">
					Videos destacados
				</a>
				<div class="content_videos_destacados margin-bottom">
					<div class="col-md-12 clearfix">
						<?php if(!empty($video_principal[0])):?>
						<div class="col-md-6 padding_video_first">
							<a href="/<?php echo $lista[0]->seolink.'-'.$lista[0]->nid.'/'.$video_principal[0]->nid;?>" class="block_vid_dest">
								<div class="img_vid_dest">
									<div class="play_vid">
										<i class="fa fa-play"></i>
										<span>Play</span>
									</div>
									<img src="<?php echo $video_principal[0]->apaisado;?>" alt="<?php echo $video_principal[0]->titular;?>">
								</div>
								<div class="title_vid_dest">
									<?php echo $video_principal[0]->titular;?>
								</div>
								<p><?php echo limpiar_detalle($video_principal[0]->descripcion);?></p>
							</a>
						</div>
						<?php endif; ?>
						<div class="col-md-6">
							<?php if(!empty($videos)):?>
							<div class="col-md-12 clearfix">
								<?php foreach($videos as $video):?>
								<div class="col-sx-6 col-md-6 col-sm-6">
									<a href="/<?php echo $lista[0]->seolink.'-'.$lista[0]->nid.'/'.$video->nid;?>" class="block_vid_dest">
										<div class="img_vid_dest">
											<div class="play_vid">
												<i class="fa fa-play"></i>
												<span>Play</span>
											</div>
											<img src="<?php echo $video->foto;?>" alt="<?php echo $video->titular;?>">
										</div>
										<div class="title_vid_dest_mini">
											<?php echo $video->titular;?>
										</div>
									</a>
								</div>
								<?php endforeach;?>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<!--FIN VIDEOS DESTACADOS====-->

		</div>
	</div>
</div>
<!--====== FIN CONTENEDOR PRINCIPAL ======-->

<!--====== FOOOTER MAS NAA ====-->

<?php $this->load->view('template/footer'); ?>
<!--====== FIN FOOOTER MAS NAA ====-->
<link href="https://fonts.googleapis.com/css?family=Lora:400,700|Roboto:400,700|Satisfy" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="<?php echo base_url();?>assets/js/slick.js?v=1"></script>

<script src="<?php echo base_url();?>assets/js/main.min.js?v=1"></script>
	
</body>

</html>