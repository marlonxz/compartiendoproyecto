<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends MX_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$desde = date('Y-m-d', time());
		$hasta = time();
		$hasta = strtotime("+6 day", $hasta);
		$hasta = date('Y-m-d', $hasta); 

		$feed = "http://app.mnlatam.tv/smart-ci/api/schedules/epg-mn/5/$desde/$hasta?user=b1a381c62d41c0723d0b1ac0f0126742bff58b54&token=04a6464e15c38abf0e92af59a981c4c1a324486a";

		$json_feed = json_decode(file_get_contents($feed));
		$programacion = array();

		foreach($json_feed->events as $key => $evento)
		{
			$programacion[$key]['timestamp'] = $evento->dt->per;
			$programacion[$key]['fecha'] = $evento->dt->cd;
			$programacion[$key]['dia'] = $evento->dt->dd;
			$programacion[$key]['mes'] = $evento->dt->mm;
			$programacion[$key]['anho'] = $evento->dt->yy;
			$programacion[$key]['titulo'] = $evento->tt->dn;
			$programacion[$key]['descripcion'] = $evento->tt->ds;
			$programacion[$key]['categoria'] = $evento->tt->ca;
			$programacion[$key]['conductor'] = $evento->tt->cl;
			$programacion[$key]['episodio'] = $evento->ep;
		}

		file_put_contents(FCPATH.'json/programacion.json', json_encode($programacion));
	}

	function leer_feed()
	{
		$this->load->model('Feed_modelo');
		$programa = $this->Feed_modelo->get_programacion();
		var_dump($programa);
	}

}