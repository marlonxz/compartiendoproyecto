<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Usuarios extends MY_Controller {

	public function __construct()
	{
		parent::__construct();	
        $this->load->model('Usuarios_modelo');
	}

	public function index()
	{
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles) AND !in_array('2', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            if(!in_array('1', $roles))
            {
                $sitios_id = array_keys($roles, '2');
                $data['usuarios'] = $this->Usuarios_modelo->get_usuario_for_moderador($sitios_id);
            }
            else
            {
                $data['usuarios'] = $this->Usuarios_modelo->get_all();
            }
		  
        }

		$this->load->view('lista_usuarios',$data);  
	}

    public function crear()
    {
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles) AND !in_array('2', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            if(!in_array('1', $roles))
            {
                $sitios_id = array_keys($roles, '2');
                $this->load->module('roles');
                $this->load->module('sitios/Sitios_getters');
                $data['sitios'] = $this->sitios_getters->list_sitios_by_ids($sitios_id);
                $data['roles'] = $this->roles->list_roles_for_moderador();
            }
            else
            {
                $this->load->module('roles');
                $this->load->module('sitios/Sitios_getters');
                $data['sitios'] = $this->sitios_getters->list_sitios();
                $data['roles'] = $this->roles->list_roles();
            }
        }

        $this->load->view('crear_usuario_form', $data);
    }

    public function leer($usuario_id)
    {
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles) AND !in_array('2', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $usuario = $this->Usuarios_modelo->get_usuario_by_id($usuario_id);
            $data['usuario'] = $usuario;
        }

        $this->load->view('ver_usuario', $data);
    }

    public function editar($usuario_id)
    {
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles) AND !in_array('2', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $this->load->module('roles');
            $this->load->module('sitios/Sitios_getters');
            $data['sitios'] = $this->sitios_getters->list_sitios();
            $data['roles'] = $this->roles->list_roles();

            $usuario = $this->Usuarios_modelo->get_usuario_by_id($usuario_id);
            $group = array();
            foreach($usuario['roles_sitios'] as $roles)
            {
                $group[$roles->sitio_id][] = $roles;
            }
            $usuario['roles_sitios'] = $group;
            $data['usuario'] = $usuario;
        }

        $this->load->view('editar_usuario_form', $data);
    }

    public function eliminar($usuario_id)
    {
        $result = $this->Usuarios_modelo->delete_usuario($usuario_id);
        if($result)
        {
            $this->session->set_flashdata('success', '¡Usuario eliminado!');
            redirect('usuarios/index');
        }
        else
        {
            $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
        }
    }

    public function procesar()
    {
        // Verificar que el usuario es admin o moderador
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles) AND !in_array('2', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Usuarios_modelo->procesar_usuario();
            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Usuario grabado!');
                redirect('usuarios/index');
            }
        }
    }

}