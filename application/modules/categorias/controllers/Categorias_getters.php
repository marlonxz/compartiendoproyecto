<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias_getters extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Categorias_getters_modelo');
	}

	function get_categorias_ids_by_nivel($nivel_permitido, $seccion_id)
	{
		return $this->Categorias_getters_modelo->get_ids_by_nivel($nivel_permitido, $seccion_id);
	}

	public function get_categorias() // funcion ajax
    {
        if($_POST)
        {
            $seccion_id = $this->input->post('seccion_id');
            $usuario_id = $this->session->userdata('usuario_id');
            $sitio_id = $this->session->userdata('sitio_id_session');
            $categorias = $this->Categorias_getters_modelo->get_categorias_by_nivel($seccion_id, $usuario_id, $sitio_id);

            if(!empty($categorias))
            {
                echo json_encode($categorias);
            }

            return FALSE;       
        }
    }

    function list_categorias_by_seccion($seccion_id)
    {
        return $this->Categorias_getters_modelo->get_by_seccion_id($seccion_id);
    }
}