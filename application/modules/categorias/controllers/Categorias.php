<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Categorias_modelo');
	}

    function index()
    {
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $data['categorias'] = $this->Categorias_modelo->get_all_categorias();
        }
        
        $this->load->view('lista_categorias', $data); 
    }

    function crear()
    {
        $data = array();
        $roles = $this->session->userdata('roles');

        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $this->load->module('sitios/Sitios_getters');
            $this->load->module('secciones/Secciones_getters');

            $data['sitios'] = $this->sitios_getters->list_sitios();
            $data['secciones'] = json_encode($this->secciones_getters->list_secciones());                
        }

        $this->load->view('crear_categoria_form', $data);
    }

    function editar($categoria_id)
    {
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $this->load->module('sitios/Sitios_getters');
            $this->load->module('secciones/Secciones_getters');
            
            $data['sitios'] = $this->sitios_getters->list_sitios();
            $data['secciones'] = json_encode($this->secciones_getters->list_secciones());

            $categoria = $this->Categorias_modelo->get_categoria_by_id($categoria_id);
            $data['categoria'] = $categoria;
        }

        $this->load->view('editar_categoria_form', $data);
    }

    function eliminar($categoria_id)
    {
        $result = $this->Categorias_modelo->delete_categoria($categoria_id);
        if($result)
        {
            $this->session->set_flashdata('success', '¡Categoría eliminada!');
            redirect('categorias/index');
        }
        else
        {
            $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
            redirect('categorias/index');
        }
    }

    function procesar()
    {
        // Verificar que el usuario es admin o moderador
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Categorias_modelo->procesar_categoria();
            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Categoría grabada!');
            }
            else
            {
                $this->session->set_flashdata('error', 'Hubo un problema. Por favor inténtalo de nuevo');
            }
            redirect('categorias/index');
        }
    }

    /*function get_noticias_categoria() // ajax call
    {
        $this->load->module('secciones');

        $sitio_id = $this->input->post('sitio_id');
        $seccion_id = $this->secciones->list_seccion_noticias_id($sitio_id);
        $categorias = $this->Categorias_modelo->get_by_seccion_id($seccion_id);

        if(!empty($categorias))
        {
            echo json_encode($categorias);
        }
        else
        {
            echo 0;
        }
    }*/

    function get_blogs_from_usuario() // ajax call
    {
        $this->load->module('secciones');
        
        $sitio_id = $this->input->post('sitio_id');
        $seccion_id = $this->secciones->list_seccion_blog_id($sitio_id);
        $categorias = $this->Categorias_modelo->get_by_seccion_id($seccion_id);

        if(!empty($categorias))
        {
            echo json_encode($categorias);
        }
        else
        {
            echo 0;
        }
    }

    function list_categorias_by_usuario($sitio_id, $usuario_id)
    {
        $categorias_ids = array();
        $categorias = $this->Categorias_modelo->get_by_usuario_id($sitio_id, $usuario_id);

        foreach($categorias as $cat)
        {
            $categorias_ids[] = $cat['categoria_id'];
        }

        return $categorias_ids;
    }

    function list_categorias_by_ids($categorias_ids)
    {
        return $this->Categorias_modelo->get_categorias_by_ids($categorias_ids);
    }

    function list_categorias()
    {
        return $this->Categorias_modelo->list_categorias();
    }
}