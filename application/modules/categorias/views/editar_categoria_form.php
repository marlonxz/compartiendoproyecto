<?php 
$titulo = "Editar categoría | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <div class="box-typical box-typical-padding">
          <h5 class="m-t-lg with-border">Editar categoría</h5>

          <form method="post" action="<?php echo base_url();?>categorias/procesar">
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Nombre *</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" name="categoria_nombre" placeholder="Categoría" value="<?php echo $categoria->categoria_nombre;?>" required></p>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Descripción</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" name="categoria_descripcion" placeholder="Descripción" value="<?php echo $categoria->categoria_desc;?>"></p>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Sitio</label>
              <div class="col-sm-10">
                <select id ="sitios" name="sitio_id" class="form-control" required>
                  <?php foreach($sitios as $sitio): ?>
                    <option value="<?php echo $sitio->sitio_id;?>" <?php echo $sitio->sitio_id == $categoria->sitio_id ? 'selected="selected"' : '';?>><?php echo $sitio->sitio_nombre;?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Sección *</label>
              <div class="col-sm-6">
                <select id="seccion" name="seccion_id" class="form-control sitio" data-placeholder="Elegir una sección" required>
                  <option value="">&nbsp;</option>
                </select>
              </div>
            </div>  

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Nivel de acceso *</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="number" class="form-control" name="nivel_acceso" placeholder="Numérico" value="<?php echo $categoria->nivel_acceso;?>" required></p>
              </div>
            </div>          

            <input type="hidden" name="categoria_id" value="<?php echo $categoria->categoria_id;?>">
            <hr>
            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-rounded btn-inline btn-primary">Editar</button>
                <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
              </div>
            </div>  
                   
          </form>
        </div>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/plugins.js"></script>

  <script src="<?php echo base_url();?>assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>

<script>
  $('#cancelar').click(function(e){
    e.preventDefault();
    window.location.href="<?php echo base_url();?>categorias";
  });

  // seccion por default
  var secciones = <?php echo $secciones;?>;
  var seccionOld = <?php echo $categoria->seccion_id;?>;
  var sitio_id = $('#sitios').val();
  var selectedSec = secciones[sitio_id];

  $.each(selectedSec, function(key, value){
    $("#seccion")
        .append($('<option>', { value: key})
        .text(value));
    if(key == seccionOld) {
      $("#seccion").val(key);
    }
  })

  // secciones cuando cambia
  $('#sitios').on('change', function(){
    sitio_id = $('#sitios').val();
    selectedSec = secciones[sitio_id];

    $('#seccion').empty().append('<option value="" ></option>').val('');

    $.each(selectedSec, function(key, value){
      $("#seccion")
        .append($('<option>', { value: key })
        .text(value));
    });
  })
</script>
</body>
</html>
