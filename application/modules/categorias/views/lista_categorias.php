<?php 
$titulo = "Administración de categorías | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/datatables-net/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/datatables-net.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
      <header class="section-header">
        <div class="tbl">
          <div class="tbl-row">
            <div class="tbl-cell">
              <h2>Administrador de categorías</h2>
              <div class="subtitle">En este panel podrás administrar las categorías</div>
            </div>
          </div>
          <br>
          <div class="tbl-row">
            <div class="tbl-cell">
              <a href="<?php echo base_url();?>categorias/crear" class="btn btn-rounded btn-inline">Crear Categoría</a>
            </div>
          </div>
        </div>
      </header>
      <section class="card">
        <div class="card-block">
          <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
              <th>Sitio</th>
              <th>Sección</th>
              <th>Categoría</th>
              <th>Fecha de creación</th>
              <th>Acciones</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>Sitio</th>
              <th>Sección</th>
              <th>Categoría</th>
              <th>Fecha de creación</th>
              <th>Acciones</th>
            </tr>
            </tfoot>
            <tbody>
            <?php if(!empty($categorias)):?>
            <?php foreach($categorias as $categoria):?>
            <tr>
              <td><?php echo $categoria->sitio_nombre;?></td>
              <td><?php echo $categoria->seccion_nombre;?></td>
              <td><?php echo $categoria->categoria_nombre;?></td>
              <td><?php echo $categoria->categoria_fech_creacion;?></td>
              <td>
                <a href="<?php echo base_url();?>categorias/editar/<?php echo $categoria->categoria_id;?>"><span class="fa fa-edit"></span></a>
                &nbsp;
                <a class="eliminarCategoria" href="<?php echo base_url();?>categorias/eliminar/<?php echo $categoria->categoria_id;?>"><span class="fa fa-close"></span></a>
              </td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
          </table>
        </div>
      </section>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/datatables-net/datatables.min.js"></script>

<script>
  $(function() {
    $('#example').DataTable({
      responsive: true,
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
      }
    });

    $('.eliminarCategoria').click(function(e){
      return confirm('¿Estás seguro?');
    });
  });
</script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
