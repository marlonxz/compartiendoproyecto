<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
	}

	function get_all_categorias()
	{
		$this->db->select('c.categoria_id, c.categoria_nombre, c.categoria_desc, c.categoria_estado, c.categoria_fech_creacion, c.categoria_fech_update, c.nivel_acceso, sitios.sitio_id, sitios.sitio_nombre, secciones.seccion_id, secciones.seccion_nombre');
		$this->db->from('categorias c');
		$this->db->join('sitios', 'sitios.sitio_id = c.sitio_id');
		$this->db->join('secciones', 'secciones.seccion_id = c.seccion_id');
		$this->db->where('c.categoria_estado', '1');
		$query = $this->db->get()->result();
		if(!empty($query))
		{
			return $query;
		}
		return FALSE;
	}

	function get_categoria_by_id($categoria_id)
	{
		$this->db->select('c.categoria_id, c.categoria_nombre, c.categoria_desc, c.categoria_estado, c.categoria_fech_creacion, c.categoria_fech_update, c.nivel_acceso, sitios.sitio_id, sitios.sitio_nombre, s.seccion_id, s.seccion_nombre');
		$this->db->from('categorias c');
		$this->db->join('sitios', 'sitios.sitio_id = c.sitio_id');
		$this->db->join('secciones s', 's.seccion_id = c.seccion_id');
		$this->db->where('c.categoria_estado', '1');
		$this->db->where('c.categoria_id', $categoria_id);
		$query = $this->db->get()->row();

		if(!empty($query))
		{
			return $query;
		}
		return FALSE;
	}

	function get_categorias_by_ids($categorias_ids)
	{
		$this->db->select('c.categoria_id, c.categoria_nombre, c.categoria_desc, c.categoria_estado, c.categoria_fech_creacion, c.categoria_fech_update, c.nivel_acceso, sitios.sitio_id, sitios.sitio_nombre, secciones.seccion_id, secciones.seccion_nombre');
		$this->db->from('categorias c');
		$this->db->join('sitios', 'sitios.sitio_id = c.sitio_id');
		$this->db->join('secciones', 'secciones.seccion_id = c.seccion_id');
		$this->db->where('c.categoria_estado', '1');
		$this->db->where_in('c.categoria_id', $categorias_ids);
		$query = $this->db->get()->result();

		if(!empty($query))
		{
			return $query;
		}
		return FALSE;
	}

	function procesar_categoria()
	{
		$this->form_validation->set_rules('categoria_nombre', 'Categoría', 'required|trim');
		$this->form_validation->set_rules('categoria_descripcion', 'Descripción', 'trim');
		$this->form_validation->set_rules('sitio_id', 'Sitio', 'required');
		$this->form_validation->set_rules('seccion_id', 'Sección', 'required');
		$this->form_validation->set_rules('nivel_acceso', 'Nivel de acceso', 'trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$categoria_id = $this->input->post('categoria_id', TRUE);
			$categoria_nombre = $this->input->post('categoria_nombre', TRUE);
			$categoria_desc = $this->input->post('categoria_descripcion', TRUE);
			$seccion_id = $this->input->post('seccion_id', TRUE);
			$sitio_id = $this->input->post('sitio_id', TRUE);
			$nivel_acceso = $this->input->post('nivel_acceso', TRUE);

			$categoria_array = array(
				'categoria_nombre' => $categoria_nombre,
				'categoria_desc' => $categoria_desc,
				'seccion_id' => $seccion_id,
				'categoria_estado' => '1',
				'sitio_id' => $sitio_id,
				'nivel_acceso' => $nivel_acceso,
				'categoria_usuario_mod' => $usuario_mod
			);

			if(empty($categoria_id))
			{
				// insertar a la tabla usuarios
				$categoria_array['categoria_fech_creacion'] = date('Y-m-d G:i:s');
				$this->db->insert('categorias', $categoria_array);
			}
			else
			{
				// editar datos básicos
				$categoria_array['categoria_fech_update'] = date('Y-m-d G:i:s');
				$this->db->where('categoria_id', $categoria_id);
				$this->db->update('categorias', $categoria_array);
			}
			
			return $this->db->affected_rows();
		}
	}

	function delete_categoria($categoria_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'categoria_estado' => '0',
			'categoria_fech_update' => date('Y-m-d G:i:s'),
			'categoria_usuario_mod' => $usuario_mod
		);

		$this->db->where('categoria_id', $categoria_id);
		$this->db->update('categorias', $array);

		return $this->db->affected_rows();
	}




	function get_by_usuario_id($sitio_id, $usuario_id)
	{
		$this->db->select('categoria_id');
		$this->db->where('usuario_id', $usuario_id);
		$this->db->where('sitio_id', $sitio_id);
		$query = $this->db->get('usuario_rol_sitio');
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		return FALSE;
	}

	// devuelve las categorias como un array con la seccion como llave
	function list_categorias()
	{
		$categorias_array = array();
		$categorias = $this->get_all_categorias();
		foreach($categorias as $categoria)
		{
			$categorias_array[$categoria->seccion_id][$categoria->categoria_id] = $categoria->categoria_nombre;
		}

		return $categorias_array;
	}

}