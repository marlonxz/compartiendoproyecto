<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias_getters_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__table = 'categorias';
		$this->load->helper('validador/validador');
	}

	function get_ids_by_nivel($nivel_permitido, $seccion_id)
	{
		$this->db->from($this->__table);
		$this->db->where('categoria_estado', '1');
		$this->db->where('seccion_id', $seccion_id);
		$this->db->where('nivel_acceso >', $nivel_permitido);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$categorias = $query->result();
			foreach($categorias as $categoria)
			{
				$ids[] = $categoria->categoria_id;
			}
			
			// guardamos los ids en la sesión para no tener que volver a hacer el query
			$categorias_sesion = $this->session->userdata('categorias_ids') ? json_decode($this->session->userdata('categorias_ids')) : new stdClass();
			if(!property_exists($categorias_sesion, $seccion_id))
			{
				$categorias_sesion->$seccion_id = $ids;
				$this->session->set_userdata('categorias_ids', json_encode($categorias_sesion));
			}

			return $ids;
		}
		return FALSE;
	}

	function get_categorias_by_nivel($seccion_id, $usuario_id, $sitio_id)
	{
		// primero conseguimos el nivel permitido
		$roles = $this->session->userdata('roles');
		$nivel_acceso = json_decode($this->session->userdata('nivel_acceso'));
        $rol = $roles[$sitio_id];
        $nivel_acceso = $nivel_acceso->$rol;
        $nivel_permitido = get_nivel_acceso($nivel_acceso);

        // luego seleccionamos las categorías permitidas de la sección
        $this->db->select('categoria_nombre, categoria_id, categoria_desc, categoria_estado, nivel_acceso, seccion_id, sitio_id');
        $this->db->from($this->__table);
        $this->db->where('categoria_estado', '1');
        $this->db->where('nivel_acceso >', $nivel_permitido);
        $this->db->where('seccion_id', $seccion_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function get_by_seccion_id($seccion_id)
	{
		$query = $this->db->get_where('categorias', array('seccion_id' => $seccion_id, 'categoria_estado' => '1'));
		
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}

		return FALSE;
	}
}