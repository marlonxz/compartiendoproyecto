<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Elementos_getters_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'elementos';
	}

	function get_imagen_by_tags($tags_id, $sitio_id)
	{
		// conseguir ids de elementos
		$this->db->select('elemento_id');
		$this->db->from('tag_elementos');
		$this->db->where('tag_elemento_estado', '1');
		$this->db->where_in('tag_id', $tags_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			// conseguir info de elementos
			$elementos_id = $query->result();
			foreach($elementos_id as $elemento_id)
			{
				$ids[] = $elemento_id->elemento_id;
			}
			$this->db->from('elementos');
			$this->db->where('elemento_estado', '1');
			$this->db->where_in('elemento_id', $ids);
			$this->db->where('tipoelemento_id', 2); // imagen

			$query2 = $this->db->get();
			if($query2->num_rows() > 0)
			{
				return $query2->result();
			}
		}
		return FALSE;
	}

	function get_galerias_by_tags($tags_id, $sitio_id)
	{
		// conseguir ids de elementos
		$this->db->select('elemento_id');
		$this->db->from('tag_elementos');
		$this->db->where('tag_elemento_estado', '1');
		$this->db->where_in('tag_id', $tags_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			// conseguir info de elementos
			$elementos_id = $query->result();
			foreach($elementos_id as $elemento_id)
			{
				$ids[] = $elemento_id->elemento_id;
			}
			$this->db->from('elementos');
			$this->db->where('elemento_estado', '1');
			$this->db->where_in('elemento_id', $ids)
						->group_start()
							->where('tipoelemento_id', 2)
							->or_where('tipoelemento_id', 5)
						->group_end();
			$query2 = $this->db->get();
			
			if($query2->num_rows() > 0)
			{
				return $query2->result();
			}
		}
		return FALSE;
	}

	function get_gif_by_tags($tags_id, $sitio_id)
	{
		// conseguir ids de elementos
		$this->db->select('elemento_id');
		$this->db->from('tag_elementos');
		$this->db->where('tag_elemento_estado', '1');
		$this->db->where_in('tag_id', $tags_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			// conseguir info de elementos
			$elementos_id = $query->result();
			foreach($elementos_id as $elemento_id)
			{
				$ids[] = $elemento_id->elemento_id;
			}
			$this->db->from('elementos');
			$this->db->where('elemento_estado', '1');
			$this->db->where_in('elemento_id', $ids);
			$this->db->where('tipoelemento_id', 3); // gif

			$query2 = $this->db->get();

			if($query2->num_rows() > 0)
			{
				return $query2->result();
			}
		}
		return FALSE;
	}

	function get_audio_by_tags($tags_id, $sitio_id)
	{
		// conseguir ids de elementos
		$this->db->select('elemento_id');
		$this->db->from('tag_elementos');
		$this->db->where('tag_elemento_estado', '1');
		$this->db->where_in('tag_id', $tags_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			// conseguir info de elementos
			$elementos_id = $query->result();
			foreach($elementos_id as $elemento_id)
			{
				$ids[] = $elemento_id->elemento_id;
			}
			$this->db->from('elementos');
			$this->db->where('elemento_estado', '1');
			$this->db->where_in('elemento_id', $ids);
			$this->db->where('tipoelemento_id', 1); // audio

			$query2 = $this->db->get();

			if($query2->num_rows() > 0)
			{
				return $query2->result();
			}
		}
		return FALSE;
	}

	function get_imagen_by_nota($nota_id, $sitio_id)
	{
		// conseguir ids de elementos
		$this->db->select('notas_elemento.elemento_id as elemento_id, e.elemento_titulo, e.elemento_desc, e.elemento_ruta, e.tipoelemento_id, e.sitio_id');
		$this->db->from('notas_elemento');
		$this->db->join('elementos e', 'notas_elemento.elemento_id = e.elemento_id');
		$this->db->where('e.tipoelemento_id', 2); // imagen
		$this->db->where('notas_elemento_estado', '1');
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);

		return $query = $this->db->get()->row();
	}

	function get_video_by_nota($nota_id, $sitio_id)
	{
		// conseguir ids de elementos
		$this->db->select('notas_elemento.elemento_id as elemento_id, e.elemento_titulo, e.elemento_desc, e.elemento_ruta, e.tipoelemento_id, e.sitio_id');
		$this->db->from('notas_elemento');
		$this->db->join('elementos e', 'notas_elemento.elemento_id = e.elemento_id');
		$this->db->where('e.tipoelemento_id', 4); // video
		$this->db->where('notas_elemento_estado', '1');
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);

		return $query = $this->db->get()->row();
	}

	function get_gal_imagen_by_nota($nota_id, $sitio_id, $id_flag = NULL)
	{
		// conseguir ids de elementos
		$this->db->select('notas_elemento.elemento_id as elemento_id');
		$this->db->from('notas_elemento');
		$this->db->join('elementos', 'notas_elemento.elemento_id = elementos.elemento_id');
		$this->db->join('notas', 'notas_elemento.nota_id = notas.nota_id');
		$this->db->where('notas.sitio_id', $sitio_id);
		$this->db->where('elementos.tipoelemento_id', 5); // imagen galeria
		$this->db->where('notas_elemento_estado', '1');
		$this->db->where('notas_elemento.nota_id', $nota_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			// si el id flag está activado, solo se devuelven IDS 
			if($id_flag)
			{
				return $query->result();
			}

			// conseguir info de elementos
			$elementos_id = $query->result();
			foreach($elementos_id as $elemento_id)
			{
				$ids[] = $elemento_id->elemento_id;
			}
			$this->db->from('elementos');
			$this->db->where('elemento_estado', '1');
			$this->db->where_in('elemento_id', $ids);
			$query2 = $this->db->get();

			if($query2->num_rows() > 0)
			{
				return $query2->result();
			}
		}
		return FALSE;
	}

	function get_gal_video_by_nota($nota_id, $sitio_id, $id_flag = NULL)
	{
		// conseguir ids de elementos
		$this->db->select('notas_elemento.elemento_id as elemento_id');
		$this->db->from('notas_elemento');
		$this->db->join('elementos', 'notas_elemento.elemento_id = elementos.elemento_id');
		$this->db->join('notas', 'notas_elemento.nota_id = notas.nota_id');
		$this->db->where('elementos.tipoelemento_id', 6); // videos galerias
		$this->db->where('notas_elemento_estado', '1');
		$this->db->where('notas.sitio_id', $sitio_id);
		$this->db->where('notas.nota_id', $nota_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			// si el id_flag está activado devolver solo los ids
			if($id_flag)
			{
				return $query->result();
			}
			// conseguir info de elementos
			$elementos_id = $query->result();
			foreach($elementos_id as $elemento_id)
			{
				$ids[] = $elemento_id->elemento_id;
			}
			$this->db->from('elementos');
			$this->db->where('elemento_estado', '1');
			$this->db->where_in('elemento_id', $ids);
			$query2 = $this->db->get();

			if($query2->num_rows() > 0)
			{
				return $query2->result();
			}
		}
		return FALSE;
	}

	function get_gif_by_nota($nota_id, $sitio_id)
	{
		// conseguir ids de elementos
		$this->db->select('notas_elemento.elemento_id as elemento_id, e.elemento_titulo, e.elemento_desc, e.elemento_ruta, e.tipoelemento_id, e.sitio_id');
		$this->db->from('notas_elemento');
		$this->db->join('elementos e', 'notas_elemento.elemento_id = e.elemento_id');
		$this->db->where('e.tipoelemento_id', 3); // gif
		$this->db->where('notas_elemento_estado', '1');
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);

		return $query = $this->db->get()->row();
	}

	function get_audio_by_nota($nota_id, $sitio_id)
	{
		// conseguir ids de elementos
		$this->db->select('notas_elemento.elemento_id as elemento_id, e.elemento_titulo, e.elemento_desc, e.elemento_ruta, e.tipoelemento_id, e.sitio_id');
		$this->db->from('notas_elemento');
		$this->db->join('elementos e', 'notas_elemento.elemento_id = e.elemento_id');
		$this->db->where('e.tipoelemento_id', 1); // audio
		$this->db->where('notas_elemento_estado', '1');
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);

		return $query = $this->db->get()->row();
	}

	function get_cancion_by_nota($nota_id, $sitio_id)
	{
		// conseguir ids de elementos
		$this->db->select('notas_elemento.elemento_id as elemento_id, e.elemento_titulo, e.elemento_desc, e.elemento_ruta, e.tipoelemento_id, e.sitio_id');
		$this->db->from('notas_elemento');
		$this->db->join('elementos e', 'notas_elemento.elemento_id = e.elemento_id');
		$this->db->where('e.tipoelemento_id', 7); // cancion
		$this->db->where('notas_elemento_estado', '1');
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);

		return $query = $this->db->get()->row();
	}
}