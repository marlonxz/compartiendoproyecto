<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elementos_getters extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Elementos_getters_modelo', 'tipo_elemento/Tipo_elemento_modelo'));
	}

	// funciones ajax
	function get_tags_imagen()
    {
        $sitio_id = $this->session->userdata('sitio_id_session');
        $tags_id = $this->input->post('tags_id', TRUE);
        $fotos = $this->Elementos_getters_modelo->get_imagen_by_tags($tags_id, $sitio_id);

        if(!empty($fotos))
        {
            $ruta = $this->Tipo_elemento_modelo->get_ruta(2); // foto = id
            $this->load->module('sitios/sitios_getters');
            $bucket = $this->sitios_getters->get_bucket($sitio_id);
            foreach($fotos as $foto)
            {
                $foto->ruta = 'http://'.$bucket.$ruta->tipo_elemento_ruta.'/';
            }
        }
       echo json_encode($fotos);
    }

    function get_tags_gif()
    {
        $sitio_id = $this->session->userdata('sitio_id_session');
        $tags_id = $this->input->post('tags_id');
        $fotos = $this->Elementos_getters_modelo->get_gif_by_tags($tags_id, $sitio_id);
        if(!empty($fotos))
        {
            $ruta = $this->Tipo_elemento_modelo->get_ruta(3); // gif = id
            $this->load->module('sitios/sitios_getters');
            $bucket = $this->sitios_getters->get_bucket($sitio_id);

            foreach($fotos as $foto)
            {
                $foto->ruta = 'http://'.$bucket.$ruta->tipo_elemento_ruta.'/';
            }
        }        

        echo json_encode($fotos);
    }

    function get_tags_audio()
    {
        $sitio_id = $this->session->userdata('sitio_id_session');
        $tags_id = $this->input->post('tags_id');
        $notas = $this->Elementos_getters_modelo->get_audio_by_tags($tags_id, $sitio_id);
        echo json_encode($notas);
    }

    public function get_tags_galeria_fotos()
    {
        $sitio_id = $this->session->userdata('sitio_id_session');
        $tags_id = $this->input->post('tags_id');
        $fotos = $this->Elementos_getters_modelo->get_galerias_by_tags($tags_id, $sitio_id);
        if(!empty($fotos))
        {
            $ruta = $this->Tipo_elemento_modelo->get_ruta(5); // galerias 
            $this->load->module('sitios/sitios_getters');
            $bucket = $this->sitios_getters->get_bucket($sitio_id);
            foreach($fotos as $foto)
            {
                $foto->ruta = 'http://'.$bucket.$ruta->tipo_elemento_ruta.'/';
            }
        }        

        echo json_encode($fotos);
        
    }


}