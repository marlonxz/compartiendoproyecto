<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 300);
class Videos extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->DB = $this->load->database('default', TRUE);
        $this->load->model(array('tags/Tags_getters_modelo', 'sitios/Sitios_getters_modelo','videos/Videos_modelo'));
        $this->load->library('form_validation');
	}

    public function validar_sitio()
    {
        $sitio_id = $this->session->userdata('sitio_id_session');
        $sitios = $this->session->userdata('sitios');

        if(!in_array($sitio_id, $sitios))
        {
            $this->session->set_flashdata('error', 'No tienes permisos de administración.');
            $this->load->view('sin_permiso');
            return FALSE;
        }
        return TRUE;        
    }

    public function add_lista()
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
            $sitio_id = $this->session->userdata('sitio_id_session');
            // get nombre del sitio
            $this->load->helper('sitios/sitios');
            $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
            $data['sitio_id'] = $sitio_id;
            
            // enviar tags del sitio      
            $tags = $this->Tags_getters_modelo->get_tags_by_sitio($sitio_id);
            $data['tags'] = $tags; // todos los tipos de tags

            $tags_patrocinado = $this->Tags_getters_modelo->get_tags_patrocinados($sitio_id);
            $data['tags_patrocinado'] = $tags_patrocinado;

            $this->load->view('videos_form', $data);
        }
    }

     public function procesar_lista()
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
            if($_POST)
            {
                $sitio_id = $this->session->userdata('sitio_id_session');
                $lista_procesada = $this->Videos_modelo->insert_contenido(); // devuelve true o false

                if(!empty($lista_procesada))
                {
                    $this->session->set_flashdata('success', "Lista creada");
                    redirect("/videos/$sitio_id");
                }
                else
                {
                    $this->session->set_flashdata('error', "Hubo un error al momento de crear la lista. Por favor inténtalo de nuevo.");
                    redirect("videos/$sitio_id");
                }
            }
            else
            {
                $sitio_id = $this->session->userdata('sitio_id_session');
                redirect('videos/index/'.$sitio_id);
            }
        }
    }

    public function index($sitio_id)
    {
        $data = array();
        $this->session->set_userdata('sitio_id_session', $sitio_id);
        $validacion = $this->validar_sitio();

        if($validacion)
        { 
            // Llamar a la lista de notas de la seccion
            $data['listas'] = $this->Videos_modelo->get_lista_by_sitio($sitio_id);
            $data['sitio_id'] = $sitio_id;

            $this->load->view('videos_subhome', $data);
        }

    }

    public function edit_lista($sitio_id, $lista_id)
    {
        $this->load->module('sitios');

        // Datos de la lista
        $data['lista'] = $this->Videos_modelo->get_lista_by_id($sitio_id, $lista_id);

        // enviar tags del sitio      
        $data['tags']= $this->Tags_getters_modelo->get_tags_by_sitio($sitio_id);
        $data['tags_patrocinado'] = $this->Tags_getters_modelo->get_tags_patrocinados($sitio_id);

        //Tags de la lista
        $data['tags_lista']= $this->Tags_getters_modelo->get_tags_by_lista($lista_id, $sitio_id);
        $data['tags_lista_patrocinado'] = $this->Tags_getters_modelo->get_tag_patrocinado_by_lista($lista_id, $sitio_id);

        // get nombre del sitio
        $this->load->helper('sitios/sitios');
        $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
        $data['sitio_id'] = $sitio_id;

        $this->load->view('videos_edit_form', $data); 

    }

    public function update_lista($sitio_id, $lista_id)
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
            if($_POST)
            {
                $lista_procesada = $this->Videos_modelo->update_contenido($lista_id, $sitio_id); // devuelve true o false
               
                if(!empty($lista_procesada))
                {
                    $this->session->set_flashdata('success', "Lista editada");
                    redirect('videos/'.$sitio_id); 
                }
                else
                {
                    redirect("/videos/edit/$sitio_id/$lista_id");
                }


            }
            else
            {
                redirect('videos/index/'.$sitio_id);
            }
        }
    }

    function delete_lista($sitio_id, $lista_id)
    {
        $resultado = $this->Videos_modelo->delete_contenido($sitio_id, $lista_id);
        $mensaje = $resultado ? 'Lista Eliminada' : 'Hubo un error al eliminar la foto. Por favor inténtalo de nuevo';

        $this->session->set_flashdata('success', $mensaje);
        redirect("/videos/$sitio_id/", 'refresh');
    }

    public function listar_videos($sitio_id, $lista_id)
    {
        // videos de la lista
        $videos = $this->Videos_modelo->get_videos_by_id($sitio_id, $lista_id);
        $lista = $this->Videos_modelo->get_lista_by_id($sitio_id, $lista_id);

        // get nombre del sitio
        $this->load->helper('sitios/sitios');
        $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
        $data['sitio_id'] = $sitio_id;
        $data['lista'] = $lista;
        $data['videos'] = $videos;

        $this->load->view('videos_list_form', $data); 
    }

    public function update_videos($sitio_id, $lista_id)
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
            if($_POST)
            {
                $videos_procesada = $this->Videos_modelo->update_videos($sitio_id,$lista_id); // devuelve true o false
               
                if(!empty($videos_procesada))
                {
                    $this->session->set_flashdata('success', 'Se actualizaron los videos');
                    redirect('videos/'.$sitio_id);
                }
                else
                {
                    redirect("/videos/listar/$sitio_id/$lista_id");
                }


            }
            else
            {
                $sitio_id = $this->session->userdata('sitio_id_session');
                redirect('videos/index/'.$sitio_id);
            }
        }
    }




}