<?php 
$titulo = "Editar lista de videos - ".$sitio_nombre." | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="/assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="/assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="/assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="/assets/css/separate/pages/gallery.min.css">
<link rel="stylesheet" href="/assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="/assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/main.css">

</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Contenido general</h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link active" role="tab">
                    <span class="nav-link-in">
                      <span class="font-icon font-icon-pencil"></span>
                      Editar contenido
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
              <form method="post" action="/videos/update_lista/<?php echo $sitio_id;?>/<?php echo $lista->lista_id;?>">
                <div class="form-group row" id="notaTitulo">
                  <label for="lista_titulo" class="col-sm-2 form-control-label">Título *</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="text" name="lista_titulo" id="lista_titulo" required value="<?php echo $lista->lista_titulo;?>" />
                  </div>
                </div>

                <div class="form-group row" id="notaTitulo">
                  <label for="lista_linkseo" class="col-sm-2 form-control-label">URL</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="text" name="lista_linkseo" id="lista_linkseo" required value="<?php echo $lista->lista_seolink;?>" />
                  </div>
                </div>

                <?php if(!empty($tags)):?>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Tags</label>
                  <div class="col-sm-10">
                    <select name="lista_tags[]" class="select2" data-placeholder="Elegir uno o más tags extendidos" multiple="multiple">
                    <?php foreach($tags as $tag):?>
                      <?php
                        $selected = " ";
                        if(!empty($tags_lista))
                        {
                          foreach($tags_lista as $tag_s)
                          {  
                            if($tag->tag_id == $tag_s->tag_id)
                            {
                              $selected = 'selected';
                              break;
                            }
                          }
                        } 
                      ?>
                      <option value="<?php echo $tag->tag_id;?>" <?php echo $selected;?>><?php echo $tag->tag_nombre;?></option>
                    <?php endforeach;?>
                  </select>
                  </div>
                </div>
                <?php endif; ?>

                <?php if(!empty($tags_patrocinado)):?>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Tags patrocinados</label>
                  <div class="col-sm-10">
                    <select name="lista_tags_patrocinado[]" class="select2" data-placeholder="Elegir uno o más tags patrocinados" multiple="multiple">
                    <?php foreach($tags_patrocinado as $tp):?>
                      <?php
                        $selected = " ";
                        if(!empty($tags_lista_patrocinado))
                        {
                          foreach($tags_lista_patrocinado as $tag_p)
                          {  
                            if($tp->tag_id == $tag_p->tag_id)
                            {
                              $selected = 'selected';
                              break;
                            }
                          }
                        } 
                      ?>
                      <option value="<?php echo $tp->tag_id;?>" <?php echo $selected;?>><?php echo $tp->tag_nombre;?></option>
                    <?php endforeach;?>
                  </select>
                  </div>
                </div>
                <?php endif; ?>

                <hr>

                <div class="form-group row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
                    <button type="submit" id="grabarContenido" class="btn btn-rounded btn-inline btn-primary">Editar</button>
                  </div>
                </div>  
              </form>
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="/assets/js/lib/jquery/jquery.min.js"></script>
<script src="/assets/js/lib/tether/tether.min.js"></script>
<script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/lib/jqueryui/jquery-ui.min.js"></script>
<script src="/assets/js/plugins.js"></script>

<script src="/assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="/assets/js/lib/select2/select2.full.min.js"></script>
<script src="/assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  // SUBMIT
  $("#grabarContenido").click(function(e){

  });

  // CANCEL
  $("#cancelar").click(function(e) {
    e.preventDefault();
    window.location.href="/videos/<?php echo $sitio_id;?>";
  });

});
</script>
<script src="/assets/js/app.js"></script>
</body>
</html>
