<?php 
$titulo = "Crear lista de videos - ".$sitio_nombre." | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="/assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="/assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="/assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="/assets/css/separate/pages/gallery.min.css">
<link rel="stylesheet" href="/assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="/assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/main.css">

</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Contenido general</h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link active" role="tab">
                    <span class="nav-link-in">
                      <span class="font-icon font-icon-pencil"></span>
                      Contenido
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
              <form method="post" action="/videos/procesar_lista">
                <div class="form-group row" id="notaTitulo">
                  <label for="lista_titulo" class="col-sm-2 form-control-label">Título *</label>
                  <div class="col-sm-8">
                    <input class="form-control" type="text" name="lista_titulo" id="lista_titulo" required />
                  </div>
                  <div class="col-sm-2">
                  <button id="linkseo" type="button" class="btn btn-rounded btn-inline btn-success">Generar linkseo</button>
                  </div>
                </div>

                <div class="form-group row" id="notaTitulo">
                  <label for="lista_codigo" class="col-sm-2 form-control-label">Código de la lista *</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="text" name="lista_codigo" id="lista_codigo" required placeholder="ejem: PL6145869F21F9A0C4" />
                  </div>
                </div>

                <?php if(!empty($tags)):?>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Tags</label>
                  <div class="col-sm-10">
                    <select name="lista_tags[]" class="select2" data-placeholder="Elegir uno o más tags extendidos" multiple="multiple">
                    <?php foreach($tags as $tag):?>
                      <option value="<?php echo $tag->tag_id;?>"><?php echo $tag->tag_nombre;?></option>
                    <?php endforeach;?>
                  </select>
                  </div>
                </div>
                <?php endif; ?>

                <?php if(!empty($tags_patrocinado)):?>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Tags patrocinados</label>
                  <div class="col-sm-10">
                    <select name="lista_tags_patrocinado[]" class="select2" data-placeholder="Elegir uno o más tags patrocinados" multiple="multiple">
                    <?php foreach($tags_patrocinado as $tp):?>
                      <option value="<?php echo $tp->tag_id;?>"><?php echo $tp->tag_nombre;?></option>
                    <?php endforeach;?>
                  </select>
                  </div>
                </div>
                <?php endif; ?>

                <hr>

                <div class="form-group row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
                    <button type="submit" id="grabarContenido" class="btn btn-rounded btn-inline btn-primary">Crear</button>
                  </div>
                </div>  
              </form>
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="/assets/js/lib/jquery/jquery.min.js"></script>
<script src="/assets/js/lib/tether/tether.min.js"></script>
<script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/lib/jqueryui/jquery-ui.min.js"></script>
<script src="/assets/js/plugins.js"></script>

<script src="/assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="/assets/js/lib/select2/select2.full.min.js"></script>
<script src="/assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  // linkseo
  $("#linkseo").click(function(e){
    e.preventDefault();
    var linkseo = $("#lista_titulo").val();
    if(linkseo)
    {
      $("#notaTitulo").append('<input type="hidden"  id="lista_linkseo" name="lista_linkseo" value="'+linkseo.replace(/"/g, '\'')+'">');
      $("#linkseo").prop("disabled", true);
    }
  });

  // SUBMIT
  $("#grabarContenido").click(function(e){
    // linkseo obligatorio
    var linkseoBoton = $('#lista_linkseo').val();
    if(linkseoBoton == undefined)
    {     
      e.preventDefault();
      alert('Debes generar un linkseo');
    }
  });

  // CANCEL
  $("#cancelar").click(function(e) {
    e.preventDefault();
    window.location.href="/videos/<?php echo $sitio_id;?>";
  });

});
</script>
<script src="/assets/js/app.js"></script>
</body>
</html>
