<?php 
$titulo = "Editar Nota - ".$sitio_nombre." | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="/assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="/assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="/assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="/assets/css/separate/pages/gallery.min.css">
<link rel="stylesheet" href="/assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="/assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/main.css">
<style>
  #sortable { list-style-type: none; margin: 0; padding: 0; }
  #sortable li { margin: 0 5px 5px 5px;  font-size: 1.2em; min-height: 45px; }
  html>body #sortable li { height: 1.5em; line-height: 1.2em; }
  .ui-state-highlight { height: 1.5em; line-height: 1.2em; }

  .no-border{
    border: 0!important;
    background-color: #fff!important;
  }
  .seleccionado{
    opacity: 0.5;
    transition: opacity 0.5s;
  }
</style>
</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Editar nota</h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="fa fa-edit"></i>
                      Editar
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
              <form id="fileupload" method="post" action="/videos/update_videos/<?php echo $sitio_id.'/'.$lista->lista_id; ?>">
           
                <div class="form-group row" id="notaLinkseo">
                  <label for="nota_linkseo" class="col-sm-2 form-control-label">URL</label>
                  <div class="col-sm-10">
                    <span class="form-control"><?php echo $lista->lista_seolink;?></span>
                  </div>
                </div>

                <?php if(!empty($videos)):?>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Videos</label>
                  <div class="col-sm-10">
                    <ul id="sortable">
                      <?php $i=1; foreach($videos as $video):?>
                      <li order-id="i<?php echo $i;?>" id="i<?php echo $i;?>" class="ui-state-default li_canciones">
                        <div class="col-sm-10">
                          <input type="hidden" name="videos[]" id="videos" value="<?php echo $video->video_id; ?>">               
                            <img src="http://static.cms.cmd.pe/<?php echo $video->video_img_mediano; ?>" width="50">
                          <span><?php echo $video->video_titulo; ?></span>
                        </div>
                      </li>
                      <?php $i++; endforeach; ?>
                    </ul>
                  </div>
                </div>
                <?php endif; ?>

                <hr>

                <div class="form-group row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
                    <button type="submit" name="action" id="siguienteContenido" class="btn btn-rounded btn-inline btn-primary enviar_data" value="corregir">Actualizar</button>
                  </div>
                </div>  
              </form>
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="/assets/js/lib/jquery/jquery.min.js"></script>
<script src="/assets/js/lib/tether/tether.min.js"></script>
<script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/lib/jqueryui/jquery-ui.min.js"></script>
<script src="/assets/js/plugins.js"></script>

<script src="/assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="/assets/js/lib/select2/select2.full.min.js"></script>
<script src="/assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {

  // canciones
  $( "#sortable" ).sortable({
    placeholder: "ui-state-highlight"
  });
  $( "#sortable" ).disableSelection();
 

  // SUBMIT
  $(".enviar_data").click(function(e){

  });

  // CANCEL
  $("#cancelar").click(function(e) {
    e.preventDefault();
    window.location.href="/videos/<?php echo $sitio_id;?>";
  });

});


</script>
<script src="/assets/js/app.js"></script>
</body>
</html>
