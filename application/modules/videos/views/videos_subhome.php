<?php 
$titulo = "Listas de videos | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/datatables-net/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/datatables-net.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
      <header class="section-header">
        <div class="tbl">
          <div class="tbl-row">
            <div class="tbl-cell">
              <h2>Listas de videos</h2>
              <div class="subtitle">En este panel podrás administrar las listas de videos</div>
            </div>
          </div>
          <br>
          <div class="tbl-row">
            <div class="tbl-cell">
              <a href="<?php echo base_url();?>videos/crear/<?php echo $sitio_id;?>" class="btn btn-rounded btn-inline">Crear lista</a>
            </div>
          </div>
        </div>
      </header>
      <section class="card">
        <div class="card-block">
          <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
              <th>ID</th>
              <th>Titular</th>
              <th>URL</th>
              <th>Fecha Creación</th>
              <th>Fecha Update</th>
              <th>Acciones</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>ID</th>
              <th>Titular</th>
              <th>URL</th>
              <th>Fecha Creación</th>
              <th>Fecha Update</th>
              <th>Acciones</th>
            </tr>
            </tfoot>
            <tbody>
            <?php if(!empty($listas)):?>
            <?php foreach($listas as $lista):?>
            <tr>
              <td><?php echo $lista->lista_id;?></td>
              <td><?php echo $lista->lista_titulo;?></td>
              <td><?php echo $lista->lista_seolink;?></td>
              <td><?php echo $lista->lista_fech_creacion;?></td>
              <td><?php echo $lista->lista_fech_update;?></td>
              <td>
                <a href="<?php echo base_url();?>videos/editar/<?php echo $sitio_id;?>/<?php echo $lista->lista_id;?>"><span class="fa fa-edit"></span></a>
                &nbsp;
                <a class="eliminarVideo" href="<?php echo base_url();?>videos/eliminar/<?php echo $sitio_id;?>/<?php echo $lista->lista_id;?>"><span class="fa fa-close"></span></a>
                &nbsp;
                <a href="<?php echo base_url();?>videos/listar/<?php echo $sitio_id;?>/<?php echo $lista->lista_id;?>" title="ListarVideos"><span class="glyphicon glyphicon-facetime-video"></span></a>
              </td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
          </table>
        </div>
      </section>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/datatables-net/datatables.min.js"></script>

<script>
  $(function() {
    $('#example').DataTable({
      responsive: true,
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
      }
    });
  });
</script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
