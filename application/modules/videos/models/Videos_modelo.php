<?php defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(0);

require_once(APPPATH.'libraries/aws-library/aws-autoloader.php');

require_once FCPATH.'assets/Google/Client.php';
require_once FCPATH.'assets/Google/Service/YouTube.php';

class Videos_modelo extends CI_Model
{	
	private $client = "";
	private $DB = NULL;

	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'listas';
		$this->load->model('tags/Tags_modelo');
	}


	function insert_contenido()
	{
		$this->load->library('form_validation');
		$this->load->library('Image_moo');

		// cambiar de acuerdo al tipo de lista
		$this->form_validation->set_rules('lista_titulo', 'Título', 'required|trim');
		$this->form_validation->set_rules('lista_codigo', 'código de Video', 'required|trim');
		$this->form_validation->set_rules('lista_linkseo', 'Linkseo', 'required|trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$sitio_id = $this->session->userdata('sitio_id_session');
			$usuario_id = $this->session->userdata('usuario_id');

			// configuracion bucket
	        $key = $this->config->item('zeus_key');
	        $secret_key = $this->config->item('zeus_secret_key');
	        
	        $sharedConfig = [
	            'region' => $this->config->item('zeus_region'),
	            'version' => $this->config->item('zeus_version')
	        ];

	        // Create an SDK class used to share configuration across clients
	        $sdk = new Aws\Sdk($sharedConfig);

	        // Create an Amazon S3 client using shared configuration data
	        $credentials = new Aws\Credentials\Credentials($key, $secret_key);
	        $this->client = $sdk->createS3(['credentials' => $credentials, 'http' => ['verify' => false]]);
			
			// contenido a guardar		
			$lista_titulo = $this->input->post('lista_titulo');
			$lista_codigo = $this->input->post('lista_codigo');
			$lista_linkseo = $this->input->post('lista_linkseo');			
			$lista_tags = $this->input->post('lista_tags'); // array
			$lista_tags_patrocinado = $this->input->post('lista_tags_patrocinado'); // array			
			// limpiar linkseo
			$linkseo = 'videos-youtube/'.url_title(convert_accented_characters($lista_linkseo), '-', TRUE);
			
			$data = array(
				'sitio_id' => $sitio_id,
				'lista_titulo' => $lista_titulo,
				'lista_codigo' => $lista_codigo,
				'lista_seolink' => $linkseo,
				'usuario_id' => $usuario_id,
				'lista_estado' => '1',
			);

			$this->db->insert($this->__tabla, $data);
			$lista_id = $this->db->insert_id();

			// tags
			$lista_tags = $this->input->post('lista_tags'); // array
			if(!empty($lista_tags))
			{
				$tag_listas = array();
				foreach($lista_tags as $tag)
				{
					$tag_listas[] = array(
						'tag_id' => $tag,
						'lista_id' => $lista_id,
						'taglistas_estado' => '1',
						'taglistas_usuario_mod' => $usuario_id
					);
				}
				$this->db->insert_batch('tag_listas', $tag_listas);
			}

			//Tag Patrocinado
			$lista_tags_patrocinado = $this->input->post('lista_tags_patrocinado');
			if(!empty($lista_tags_patrocinado))
				{
					foreach($lista_tags_patrocinado as $tag)
					{
						$tag_listas_patrocinado[] = array(
							'tag_id' => $tag,
							'lista_id' => $lista_id,
							'taglistas_estado' => '1',
							'taglistas_usuario_mod' => $usuario_id
						);
					}
					$this->db->insert_batch('tag_listas', $tag_listas_patrocinado);
				}

			//Hasta acá se añadio la data en la BD	

			//YAPI
			$this->load->config('bucket');
			$DEVELOPER_KEY = 'AIzaSyCc_-21pE48EQKlWNgCM-2lAoZZ81T4yto';
			$client = new Google_Client();
			$client->setDeveloperKey($DEVELOPER_KEY);

			// Define an object that will be used to make all API requests.
			$youtube = new Google_Service_YouTube($client);
			$htmlBody = '';

			$playlist = $lista_codigo;

			//Playlist
			$searchResponsePlaylist = $youtube->playlists->listPlaylists('snippet', ['id' => $playlist]);
			$imagen_playlist = $searchResponsePlaylist['items'][0]["snippet"]['thumbnails']["high"]["url"];
			$file = $this->clean_string($lista_titulo);
			$name_imagen_playlist = $file;

			$this->save_image('listas/portada',$name_imagen_playlist, $imagen_playlist);
			$lista_imagen = 'listas/portada/'.$name_imagen_playlist.'.jpg';		     

		     //Videos
		    $searchResponse = $youtube->playlistItems->listPlaylistItems('snippet', ['playlistId' => $playlist, 'maxResults' => 50]);		  
		    $videos = array();
		    $i = 0;

			foreach ($searchResponse['items'] as $searchResult) {
				switch ($searchResult['kind']) {
				  case 'youtube#playlistItem':
				    $code = $searchResult['snippet']["resourceId"]["videoId"];
				    $img_thumb = $searchResult['snippet']['thumbnails']["high"]["url"];
				    $img_portada= $searchResult['snippet']['thumbnails']["standard"]["url"];

				    if(empty($img_thumb))
				    	$img_thumb = $searchResult['snippet']['thumbnails']["default"]["url"];

				     if(empty($img_portada))
				    	$img_portada = $searchResult['snippet']['thumbnails']["default"]["url"];
				    
				    $name_imagen = $code.'.jpg';
				    $name_imagen_file = $code;

				    $videos[$i]['titulo'] = $searchResult['snippet']['title'];
				    $videos[$i]['codigo'] = $code;
				    $videos[$i]['img_mediano'] = 'listas/mediano/'.$name_imagen;
				    $videos[$i]['img_apaisado'] = 'listas/apaisado/'.$name_imagen;
				    $videos[$i]['img_thumb'] = 'listas/thumb/'.$name_imagen;
				    $videos[$i]['descripcion'] = $searchResult['snippet']['description'];

				    $this->save_image('listas/mediano',$name_imagen_file, $img_thumb);
				    $this->save_image('listas/apaisado',$name_imagen_file, $img_portada);
				    $this->crop_save_image('listas/thumb',$name_imagen_file, $img_portada , 420, 236);
				    
				  break;
				}
				$i++;
			}

		     // Insertar los Videos en la BD
			foreach($videos as $video)
			{
				$video_data = array(
					'lista_id' => $lista_id,
					'video_titulo' => $video['titulo'],
					'video_codigo' => $video['codigo'],
					'video_img_apaisado' => $video['img_apaisado'],
					'video_img_mediano' => $video['img_mediano'],
					'video_img_thumb' => $video['img_thumb'],
					'video_descripcion' => $video['descripcion'],
					'video_estado' => '1'
				);

				$this->db->insert('videos', $video_data);
				$video_id = $this->db->insert_id();
				unset($video_data);                
			}                    

			$lista_cantidad = $i;

			// Actualizar Lista
			$lista = array(
				'lista_imagen' => $lista_imagen,
				'lista_videos' => $lista_cantidad,
			);

			$this->db->update('listas', $lista, array('lista_id' => $lista_id));

			return TRUE;
		}
	}


	function save_image($dir, $name, $url)
    {
        $ext = substr($url, strrpos($url, '.') + 1);
        
        if(!is_dir($dir))
        {
            $antiguo = umask(0);
            mkdir($dir, 0775, TRUE);
            umask($antiguo);
        }

        $name = $name.".".$ext;
        $new_img = $dir.'/'.$name;
        $mime = "";

        switch ($ext) {
            case 'jpeg':
            case 'jpg':
                $mime = 'image/jpeg';
                break;
            case 'png':
                $mime = 'image/png';
                break;
        }

        // guardar la imagen en local
        if(!is_file($dir.'/'.$name))
        {
        	if(!file_exists($url))
        	{
        		if(!empty($mime))
        		{
        			//copy($url, $new_img); 
        			if (copy($url, $new_img)) 
        			{
        				$bucket = $this->config->item('zeus_bucket');
				        // subir a CDN y borrar en sistema
				        $promise = $this->client->putObjectAsync(array(
				            'Bucket' => $bucket,
				            'Key' => $new_img, // nombre del archivo
				            'SourceFile' => $new_img, // ruta
				            'ACL' => 'public-read',
				            'StorageClass' => 'STANDARD',
				            'ContentType' => $mime
				        ));    

				        $result = $promise->wait(); 		        
			        }   
        		}
        		
        	}            
        }        

    }

    function crop_save_image($dir, $name, $url, $width, $height)
    {
        $ext = substr($url, strrpos($url, '.') + 1);
        
        if(!is_dir($dir))
        {
            $antiguo = umask(0);
            mkdir($dir, 0775, TRUE);
            umask($antiguo);
        }

        $name = $name.".".$ext;
        $new_img = $dir.'/'.$name;
        $mime = "";

        switch ($ext) {
            case 'jpeg':
            case 'jpg':
                $mime = 'image/jpeg';
                break;
            case 'png':
                $mime = 'image/png';
                break;
        }

        // guardar la imagen en local
        if(!is_file($dir.'/'.$name))
        {
        	if(!file_exists($url))
        	{
        		if(!empty($mime))
        		{
        			copy($url, $new_img); 
        			$img_thumb = $this->image_moo->load($new_img)->resize_crop($width, $height)->save($new_img, TRUE);
        			$this->image_moo->clear();

			        // subir a CDN y borrar en sistema
			        $bucket = $this->config->item('zeus_bucket');
			        $promise = $this->client->putObjectAsync(array(
			            'Bucket' => $bucket,
			            'Key' => $new_img, // nombre del archivo
			            'SourceFile' => $new_img, // ruta
			            'ACL' => 'public-read',
			            'StorageClass' => 'STANDARD',
			            'ContentType' => $mime
			        ));    

			        $result = $promise->wait();   
			            
        		}
        		
        	}            
        }        

    }

    // Limpiar el nombre de espacios y caracteres especiales
	function clean_string($string)
	{
	  $new_string = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $string));
	  $new_string = str_replace(" ", "-", $new_string);
	  return $new_string;
	}

    public function get_lista_by_sitio($sitio_id)
    {
		$this->db->select('lista_id, lista_titulo, lista_seolink, lista_fech_update, lista_estado, lista_fech_creacion, lista_usuario_mod');
		$this->db->from('listas');
		$this->db->where("lista_estado", '1');
		$this->db->where('sitio_id', $sitio_id);
		$this->db->limit(300);
		$this->db->order_by('lista_fech_update', 'DESC');
		return $this->db->get()->result();     
    }

    function get_lista_by_id($sitio_id,  $lista_id)
	{
		$this->db->from($this->__tabla);
		$this->db->where('lista_id', $lista_id);
		$this->db->where('lista_estado', '1');
		return $this->db->get()->row();		
	}

	function update_contenido($lista_id, $sitio_id)
	{

		$this->load->library('form_validation');
		$this->load->helper('text');		

		$this->form_validation->set_rules('lista_titulo', 'Título', 'required|trim');
		$action = $this->input->post('action');

		if($this->form_validation->run())
		{
			// contenido a guardar
			$usuario_id = $this->session->userdata('usuario_id');
			$lista_titulo = $this->input->post('lista_titulo');
			$lista_tags = $this->input->post('lista_tags'); // array
			$lista_tags_patrocinado = $this->input->post('lista_tags_patrocinado');
			$lista_seolink = $this->input->post('lista_linkseo');
			
			$fech_actualizacion = date("Y-m-d G:i:s");	

			$lista = array(
				'lista_titulo' => $lista_titulo,
				'lista_fech_update' => $fech_actualizacion,
				'lista_usuario_mod'  =>  $usuario_id,
				'lista_seolink' => $lista_seolink
			);
			
			$this->db->update('listas', $lista, array('lista_id' => $lista_id ));

			$tags_final = array();

			if(!empty($lista_tags))
			{
				$tags_final = $lista_tags;
			}
		
			if(!empty($lista_tags_patrocinado))
			{
				$tags_final = array_merge($tags_final, $lista_tags_patrocinado);
			}

			// borrar los tags anteriores
			$this->db->where('lista_id', $lista_id);
			$this->db->delete('tag_listas');

			// insertar tags nuevos si existen
			if(!empty($tags_final))
			{
				$tags_update = array();

				foreach($tags_final as $tag)
				{
					$tags_update[] = array(
						'tag_id' => $tag,
						'lista_id' => $lista_id,
						'taglistas_estado' => '1',
						'taglistas_usuario_mod' => $usuario_id
					);
				}

				// Insertar tags
				$this->db->insert_batch('tag_listas', $tags_update);
			}

			return TRUE;
		}
	}

	function delete_contenido($sitio_id, $lista_id)
	{
		$usuario_id = $this->session->userdata('usuario_id');

		// relacion con tags
		$this->db->where('lista_id', $lista_id);
		$this->db->update('tag_listas', array('taglistas_estado' => '0', 'taglistas_usuario_mod' => $usuario_id));

		// desactivar en tabla notas
		$this->db->where('lista_id', $lista_id);
		$this->db->where('sitio_id', $sitio_id);
		$this->db->update('listas', array('lista_estado' => '0', 'lista_usuario_mod' => $usuario_id));

		return $this->db->affected_rows();
	}

	function get_videos_by_id($sitio_id, $lista_id)
	{
		$this->db->from('videos');
		$this->db->where('lista_id', $lista_id);
		$this->db->order_by("video_orden", "asc"); 
		return $this->db->get()->result();		
	}

	function update_videos($sitio_id, $lista_id)
	{
		$usuario_id = $this->session->userdata('usuario_id');			
		$videos_ids = $this->input->post('videos');
		$i = 1;

		foreach($videos_ids as $id)
		{
			$relacion = array(				
				'video_orden' => $i,
				'video_fech_update' => date("Y-m-d G:i:s")
			);
			$i++;
			$this->db->where('video_id', $id);
			$this->db->update('videos', $relacion);
		}
		return TRUE;
	}



}