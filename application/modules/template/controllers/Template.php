<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MY_Controller { // cambiar a MY_Controller cuando hayan usuarios

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('sitios/sitios');	
	}

	function show_header($titulo)
	{
		$data['title'] = $titulo;
		$this->load->view('header', $data);
	}

	function show_sidebar()
	{	
		$roles = $this->session->userdata('roles');	
		$sitios = $this->session->userdata('sitios');
		$admin = FALSE;
		$moderador = FALSE;

		$sitios_permitidos = get_sitios_data($sitios, $roles);
	
		if(in_array('1', $roles))
		{
			$admin = TRUE;
		}
		else
		{
			if(in_array('2', $roles))
			{
				$moderador = TRUE;
			}
		}
		
		$data['admin'] = $admin;
		$data['moderador'] = $moderador; 
		$data['sitios'] = $sitios_permitidos;
		$data['roles'] = $roles;
		$this->load->view('sidebar', $data);
	}

	function show_banner()
	{
		$this->load->view('banner');
	}

}