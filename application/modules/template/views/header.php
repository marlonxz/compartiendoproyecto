<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <title><?php echo $title;?></title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="/assets/lib/html5shiv/html5shiv.js"></script>
  <script src="/assets/lib/respond/respond.src.js"></script>
  <![endif]-->

  <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/icon/bank.png_16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/icon/bank.png_24x24.png" sizes="24x24">
  <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/icon/bank.png_32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/icon/bank.png_48x48.png" sizes="48x48">
  <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/icon/bank.png_64x64.png" sizes="64x64">
  <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/icon/bank.png_96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/icon/bank.png_128x128.png" sizes="128x128">
  <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/icon/bank.png_256x256.png" sizes="256x256">
  <link rel="manifest" href="<?php echo base_url();?>assets/icon/manifest.json">