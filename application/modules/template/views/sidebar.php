<nav class="side-menu">
      <?php if($admin || $moderador):?>
      <section>
        <header class="side-menu-title">Principales</header>
        <ul class="side-menu-list">
          <?php if($admin):?>
          <li class="green with-sub">
              <span>
                  <i class="font-icon font-icon-cogwheel"></i>
                  <span class="lbl">Administración</span>
              </span>
              <ul>
                  <li><a href="<?php echo base_url();?>sitios/administrar-sitios"><span class="lbl">Sitios</span></a></li>
                  <li><a href="<?php echo base_url();?>secciones/administrar-secciones"><span class="lbl">Secciones</span></a></li>
                  <li><a href="<?php echo base_url();?>categorias"><span class="lbl">Categorías</span></a></li>
                  <li><a href="<?php echo base_url();?>roles"><span class="lbl">Roles</span></a></li>
                  <li><a href="<?php echo base_url();?>tipo-elemento"><span class="lbl">Tipo Elemento</span></a></li>
                  <li><a href="<?php echo base_url();?>tipo-nota"><span class="lbl">Tipo Nota</span></a></li>
                  <li><a href="<?php echo base_url();?>tipotag"><span class="lbl">Tipo Tag</span></a></li>
                  <li><a href="<?php echo base_url();?>tamanos"><span class="lbl">Tamaño de imagen</span></a></li>
                  <li><a href="<?php echo base_url();?>servicios/administrar-servicios"><span class="lbl">Servicios</span></a></li>
              </ul>
          </li>
          <?php endif;?>
          
          <li class="purple">
            <a href="<?php echo base_url();?>tags">
                <i class="fa fa-tags"></i>
                <span class="lbl">Crear Tags</span>
            </a>
          </li>
          <li class="blue">
            <a href="<?php echo base_url();?>usuarios">
                <i class="font-icon font-icon-users"></i>
                <span class="lbl">Usuarios</span>
            </a>
          </li>
        </ul>
      </section>
      <?php endif;?>

      <section>
        <header class="side-menu-title">Herramientas</header>
        <ul class="side-menu-list">
          <li class="gold with-sub">
            <?php if($admin || $moderador):?>
            <li class="gold">
              <a href="<?php echo base_url();?>reportes/grafico">
                  <i class="font-icon font-icon-chart-2"></i>
                  <span class="lbl">Reportes</span>
              </a>
            </li>
            <?php endif; ?>
            <li class="orange-red">
              <a href="<?php echo base_url();?>clones">
                  <i class="fa fa-clone"></i>
                  <span class="lbl">Clones</span>
              </a>
            </li>
            <li class="brown">
              <a href="<?php echo base_url();?>servicios">
                  <i class="fa fa-star"></i>
                  <span class="lbl">Servicios</span>
              </a>
            </li>
          </li>
        </ul>
      </section>
  
      <section>
          <header class="side-menu-title">Sitios</header>
          <?php if(!empty($sitios)):?>
          <ul class="side-menu-list">
              <?php foreach($sitios as $sitio):?>
              <li class="grey-blue with-sub">
              <span>
                  <i class="tag-color grey-blue"></i>
                  <span class="lbl"><?php echo $sitio->sitio_nombre;?></span>
              </span>
              <?php if(!empty($sitio->secciones)):?>
              <ul>
              <li><a href="<?php echo base_url();?>sitios/<?php echo $sitio->sitio_id;?>"><span class="lbl">Panel</span></a></li>
                <?php foreach($sitio->secciones as $seccion):?>
                  <li><a href="<?php echo base_url();?>secciones/<?php echo $sitio->sitio_id.'/'.$seccion->seccion_id;?>"><span class="lbl"><?php echo $seccion->seccion_nombre;?></span></a></li>
                <?php endforeach; ?>
                  <li><a href="<?php echo base_url();?>videos/<?php echo $sitio->sitio_id;?>"><span class="lbl">Videos Youtube</span></a></li>
              </ul>
              <?php endif;?>
              </li>
              <?php endforeach; ?>
          </ul>
          <?php endif; ?>
      </section>
  </nav><!--.side-menu-->