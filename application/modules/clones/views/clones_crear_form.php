<?php 
$titulo = "Crear clon - ".$sitio_nombre." | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/pages/gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

<!-- markitup-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/markitup/skins/markitup/style.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/markitup/sets/textile/style.css" />

<!-- uploader -->
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload-ui-noscript.css"></noscript>

<style>
  #sortable { list-style-type: none; margin: 0; padding: 0; }
  #sortable li { margin: 0 5px 5px 5px;  font-size: 1.2em; min-height: 45px; }
  html>body #sortable li { height: 1.5em; line-height: 1.2em; }
  .ui-state-highlight { height: 1.5em; line-height: 1.2em; }

  .no-border{
    border: 0!important;
    background-color: #fff!important;
  }
  .seleccionado{
    opacity: 0.5;
    transition: opacity 0.5s;
  }
</style>

</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Contenido general</h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link active" role="tab">
                    <span class="nav-link-in">
                      <span class="font-icon font-icon-pencil"></span>
                      Contenido
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
              <form id="fileupload" method="post" action="<?php echo base_url();?>clones/procesar" enctype="multipart/form-data">
                <div class="form-group row" id="notaTitulo">
                  <label for="nota_titulo" class="col-sm-2 form-control-label">Titular *</label>
                  <div class="col-sm-8">
                    <input class="form-control" type="text" name="nota_titulo" id="nota_titulo" required />
                  </div>
                  <div class="col-sm-2">
                  <button id="linkseo" type="button" class="btn btn-rounded btn-inline btn-success">Generar linkseo</button>
                  </div>
                </div>

                <div class="form-group row" id="contenido-wrapper">
                  <label for="nota_contenido" class="col-sm-2 form-control-label">Contenido *</label>
                  <div class="col-sm-10">
                    <textarea id="nota_contenido" name="nota_contenido" class="form-control" required></textarea>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Elementos</label>
                  <div class="col-sm-10">
                  <div id="elementos">
                    <section class="tabs-section">
                      <div class="tabs-section-nav tabs-section-nav-icons">
                        <div class="tbl">
                          <ul class="nav" role="tablist">
                            <li class="nav-item" id="tabImagen">
                              <a class="nav-link active" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <span class="fa fa-image"></span>
                                  Imagen
                                </span>
                              </a>
                            </li>
                            <li class="nav-item" id="tabVideo">
                              <a class="nav-link" href="#tabs-1-tab-3" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <i class="fa fa-file-video-o"></i>
                                  Video
                                </span>
                              </a>
                            </li>
                            <li class="nav-item" id="tabGalImagen">
                              <a class="nav-link" href="#tabs-1-tab-6" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <i class="font-icon font-icon-cam-photo"></i>
                                  Galería fotos
                                </span>
                              </a>
                            </li>
                            <li class="nav-item" id="tabGalVideo">
                              <a class="nav-link" href="#tabs-1-tab-7" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <i class="fa fa-film"></i>
                                  Galería videos
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div><!--.tabs-section-nav-->

                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-2">
                          <div class="form-group row">
                            <label for="imagen_nombre" class="col-sm-2 form-control-label">Nombre de archivo *</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="imagen_nombre" id="imagen_nombre" required />
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="imagen_leyenda" class="col-sm-2 form-control-label">Leyenda</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="imagen_leyenda" id="imagen_leyenda" />
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Imagen </label>
                            <div class="col-sm-10">
                              <input type="file" id="imagen_file" name="imagen_file" required>
                            </div>
                          </div> 
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-3">
                          <div class="form-group row">
                            <label for="video_url" class="col-sm-2 form-control-label">URL del video</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="video_url" id="video_url" placeholder="youtube, vimeo o dailymotion" />
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="video_leyenda" class="col-sm-2 form-control-label">Leyenda</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="video_leyenda" id="video_leyenda" />
                            </div>
                          </div>
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-6">
                          <div class="form-group row">
                            <label for="gal_imagen_nombre" class="col-sm-2 form-control-label">Nombre de archivo *</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="gal_imagen_nombre" id="gal_imagen_nombre" required />
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="gal_imagen_leyenda_gen" class="col-sm-2 form-control-label">Leyenda general</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="gal_imagen_leyenda_gen" id="gal_imagen_leyenda_gen" />
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Imágenes </label>
                            <div class="col-sm-10">
                              <div class="row fileupload-buttonbar">
                                <div class="col-lg-7">
                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                    <span class="btn btn-success fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Añadir fotos...</span>
                                        <input type="file" name="userfile" multiple class="file_galeria">
                                    </span>
                                    <button type="submit" class="btn btn-primary start">
                                        <i class="glyphicon glyphicon-upload"></i>
                                        <span>Subir</span>
                                    </button>
                                    <button type="button" class="btn btn-danger delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Borrar</span>
                                    </button>
                                    <input type="checkbox" class="toggle">
                                    <!-- The global file processing state -->
                                    <span class="fileupload-process"></span>
                                </div>
                                <!-- The global progress state -->
                                <div class="col-lg-5 fileupload-progress fade">
                                    <!-- The global progress bar -->
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                    </div>
                                    <!-- The extended global progress state -->
                                    <div class="progress-extended">&nbsp;</div>
                                </div>
                              </div>
                              <!-- The table listing the files available for upload/download -->
                              <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                            </div>
                          </div> 
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-7">
                          <div class="form-group row">
                            <label for="gal_video_leyenda" class="col-sm-2 form-control-label">Leyenda</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="gal_video_leyenda" id="gal_video_leyenda" />
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                              <button id="addVideos" class="btn btn-success btn-rounded">Agregar más videos</button>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="gal_video_url" class="col-sm-2 form-control-label">Videos</label> 
                            <div class="col-sm-10" id="videos">
                              <input class="form-control" type="text" name="gal_video_url[]" placeholder="youtube, vimeo o dailymotion" /><br>
                              <input class="form-control" type="text" name="gal_video_url[]" placeholder="youtube, vimeo o dailymotion" /><br>
                              <input class="form-control" type="text" name="gal_video_url[]" placeholder="youtube, vimeo o dailymotion" /><br>
                            </div>
                          </div>
                        </div><!--.tab-pane-->
                      </div><!--.tab-content-->
                    </section><!--.tabs-section-->
                  </div>
                  </div>
                </div>

                <hr>

                <div class="form-group row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
                    <button type="submit" id="siguienteContenido" class="btn btn-rounded btn-inline btn-primary">Siguiente</button>
                  </div>
                </div>  
              </form>
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jqueryui/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- markitup -->
<script src="<?php echo base_url();?>assets/markitup/jquery.markitup.js"></script>
<script src="<?php echo base_url();?>assets/markitup/sets/textile/set.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/match-height/jquery.matchHeight.min.js"></script>

<!-- uploader -->
<script src="<?php echo base_url();?>assets/js/jquery.fileupload.js"></script>
<script src="<?php echo base_url();?>assets/lib/jquery-validate/jquery.validate.js"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.13.1/additional-methods.js"></script>
<?php $this->load->view('footer_upload');?>


<script type="text/javascript">
$(document).ready(function() {
  // markitup
  $('#nota_contenido').markItUp(myTextileSettings);

  // linkseo
  $("#linkseo").click(function(e){
    e.preventDefault();
    var linkseo = $("#nota_titulo").val();
    if(linkseo)
    {
      $("#notaTitulo").append('<input type="hidden"  id="nota_linkseo" name="nota_linkseo" value="'+linkseo.replace(/"/g, '\'')+'">');
      $("#linkseo").prop("disabled", true);
    }
  });

   // agregar más videos
  var max_videos_fields = 13;
  var wrapper = $("#videos");
  var add_button = $("#addVideos");
  var x = 1;
  $(add_button).click(function(e){
    e.preventDefault();
    if( x < max_videos_fields){
      x++;
      $(wrapper).append('<input class="form-control" type="text" name="gal_video_url[]" placeholder="youtube, vimeo o dailymotion" /><br>');
    }
  });
 
  // file upload
  $('#fileupload').validate({
    rules: {
      cancion_file: {required: false, extension: 'mp3', filesize: 5242880},
      audio_file: {required: false, extension: 'mp3', filesize: 5242880},
      imagen_file: {extension: 'png|jpe?g', filesize: 1048576},
      gif_file: {required: false, extension: 'gif', filesize: 1048576}
    },
    messages: {
      cancion_file: 'El archivo debe ser mp3 y pesar menos de 5MB',
      audio_file: 'El archivo debe ser mp3 y pesar menos de 5MB',
      imagen_file: 'El archivo debe ser jpg o png, y pesar menos de 1MB',
      gif_file: 'El archivo debe ser gif y pesar menos de 1MB'
    },
    highlight: function(element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    submitHandler: function(form) {
      $('#siguienteContenido').text('Cargando...');
      $('#siguienteContenido').prop("disabled", true);
      form.submit();
    }
  });

  // SUBMIT
  $("#siguienteContenido").click(function(e){
    // linkseo obligatorio
    var linkseoBoton = $('#nota_linkseo').val();
    if(linkseoBoton == undefined)
    {     
      e.preventDefault();
      alert('Debes generar un linkseo');
    }
  });

  // CANCEL
  $("#cancelar").click(function() {
    e.preventDefault();
    window.location.href="<?php echo base_url();?>clones";
  });

});
</script>
<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
