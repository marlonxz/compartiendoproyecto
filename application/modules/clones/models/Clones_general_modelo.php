<?php defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(300);

class Clones_general_modelo extends Clones_modelo
{	
	function __construct()
	{
		parent::__construct();
	}

	function list_noticias($limit = null)
	{
		$limit = !empty($limit) ? $limit : 500;
		$this->select_database(15); // clones db

		$this->DB->select('nota_id, nota_titulo');
		$this->DB->from('notas');
		$this->DB->where('nota_estado', '1');
		$this->DB->where('fuente_id', 1);
		$this->DB->limit($limit);
		$query = $this->DB->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function get_nota_by_id($nota_id)
	{
		$this->select_database(15);

		$this->DB->where('nota_id', $nota_id);
		$this->DB->where('nota_estado', '1');
		
		$query = $this->DB->get('notas');

		if($query->num_rows() > 0)
		{
			return $query->row();
		}

		return FALSE;
	}

	function get_elemento($elemento_nombre, $nota_id)
	{
		$this->select_database(15);

		switch ($elemento_nombre) {
			case 'imagen':
				$this->DB->select('e.elemento_id, e.elemento_titulo, e.elemento_desc as leyenda, e.elemento_ruta');
				$this->DB->where('e.elemento_estado', '1');
				$this->DB->where('e.tipoelemento_id', 2);
				$this->DB->where('ne.notas_elemento_estado', '1');
				$this->DB->join('notas_elemento ne', "ne.elemento_id = e.elemento_id AND ne.nota_id = $nota_id");
				$elemento = $this->DB->get('elementos e')->row();
				break;

			case 'video':
				$this->DB->select('e.elemento_id, e.elemento_titulo as leyenda, e.elemento_desc as provider, e.elemento_ruta as codigo');
				$this->DB->where('e.elemento_estado', '1');
				$this->DB->where('e.tipoelemento_id', 4);
				$this->DB->join('notas_elemento ne', "ne.elemento_id = e.elemento_id AND ne.nota_id = $nota_id");
				$elemento = $this->DB->get('elementos e')->row();
				break;

			case 'galeria_imagen':
				$this->DB->select('e.elemento_id, e.elemento_titulo, e.elemento_desc as leyenda, e.elemento_ruta');
				$this->DB->where('e.elemento_estado', '1');
				$this->DB->where('e.tipoelemento_id', 5);
				$this->DB->join('notas_elemento ne', "ne.elemento_id = e.elemento_id AND ne.nota_id = $nota_id");
				$elemento = $this->DB->get('elementos e')->result();
				break;

			case 'galeria_video':
				$this->DB->select('e.elemento_id, e.elemento_titulo as leyenda, e.elemento_desc as provider, e.elemento_ruta as codigo');
				$this->DB->where('e.elemento_estado', '1');
				$this->DB->where('e.tipoelemento_id', 6);
				$this->DB->join('notas_elemento ne', "ne.elemento_id = e.elemento_id AND ne.nota_id = $nota_id");
				$elemento = $this->DB->get('elementos e')->result();
				break;
		}

		return $elemento;
	}

	function eliminar_nota($nota_id)
	{
		$this->select_database(15); // clones

		// desactivar en tabla notas
		$this->DB->where('nota_id', $nota_id);
		$this->DB->update('notas', array('nota_estado' => '0', 'nota_fech_update' => date(DATE_ATOM)));

		return $this->DB->affected_rows();
	}

}