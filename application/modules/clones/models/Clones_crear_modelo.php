<?php defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(300);

class Clones_crear_modelo extends Clones_modelo
{	
	public $DB = NULL;
	public $client = '';

	function __construct()
	{
		parent::__construct();
	}

	function save_nota()
	{
		// Create an SDK class used to share configuration across clients
		$this->load->config('bucket');
		$key = $this->config->item('zeus_key');
	    $secret_key = $this->config->item('zeus_secret_key');

		$sharedConfig = [
            'region' => $this->config->item('zeus_region'),
            'version' => $this->config->item('zeus_version')
        ];
        $sdk = new Aws\Sdk($sharedConfig);

        // Create an Amazon S3 client using shared configuration data
        $credentials = new Aws\Credentials\Credentials($key, $secret_key);
        $this->client = $sdk->createS3(['credentials' => $credentials, 'http' => ['verify' => false]]);

		// validacion
		$this->form_validation->set_rules('nota_titulo', 'Titular', 'required|trim');
		$this->form_validation->set_rules('nota_linkseo', 'Linkseo', 'required|trim');
		$this->form_validation->set_rules('nota_contenido', 'Contenido', 'required|trim');

		if($this->form_validation->run())
		{
			// contenido
			$titulo = $this->input->post('nota_titulo', TRUE);
			$nota_linkseo = $this->input->post('nota_linkseo', TRUE);
			$contenido = $this->input->post('nota_contenido', TRUE);
			$usuario_id = $this->session->userdata('usuario_id');

			// limpiar linkseo
			$linkseo = url_title(convert_accented_characters($nota_linkseo), '-', TRUE);

			$this->select_database(15); 

			$nota_array = array(
				'tipo_nota_id' => 1, // noticia
				'nota_titulo' => $titulo,
				'nota_contenido' => $contenido,
				'nota_seolink' => $linkseo,
				'nota_estado' => '1',
				'fuente_id' => 1, // thor clon
				'usuario_id' => $usuario_id
			);
			$this->DB->insert('notas', $nota_array);
			$nota_id = $this->DB->insert_id();

			// elementos

			// imagen
			if(!empty($_FILES['imagen_file']['name']))
			{
				$nota_imagen_file = $_FILES['imagen_file'];
				$nota_imagen_nombre = $this->input->post('imagen_nombre');
				$nota_imagen_leyenda = $this->input->post('imagen_leyenda');

				if(empty($nota_imagen_file['tmp_name']) || !exif_imagetype($nota_imagen_file['tmp_name']))
				{
					$this->session->set_userdata('error_imagen', 'El archivo que subiste no es una imagen');
					return FALSE;
				} 		

				// subir a la base de datos
				$this->store_imagen($nota_imagen_file, $nota_imagen_nombre, $nota_imagen_leyenda, $nota_id);
			}

			// video
			$video_url = $this->input->post('video_url');
			if(!empty($video_url))
			{
				$video_leyenda = $this->input->post('video_leyenda');
				$this->store_video($video_url, $video_leyenda, $nota_id);
			}

			// galeria imagenes
			$fotos_ruta_original = $this->input->post('gal_imagen_name'); // array
			if(!empty($fotos_ruta_original))
			{
				$galeria_fotos_nombre = $this->input->post('gal_imagen_nombre');
				$fotos_leyenda = $this->input->post('gal_imagen_leyenda'); // array
				$general_leyenda = $this->input->post('gal_imagen_leyenda_gen');

				$galeria_leyenda = empty($general_leyenda) ? $fotos_leyenda : $general_leyenda;
								
				$this->store_galeria_imagen($fotos_ruta_original, $galeria_fotos_nombre, $galeria_leyenda, $nota_id);
			}

			// galeria videos
			$nota_videos_url = $this->input->post('gal_video_url');
			if(!empty(array_filter($nota_videos_url)))
			{
				$nota_videos_leyenda = $this->input->post('gal_video_leyenda');				
				$this->store_galeria_video($nota_videos_url, $nota_videos_leyenda, $nota_id);
			}

			return TRUE;
		}

	}

	function store_video($video_url, $video_leyenda, $nota_id)
	{
		$video = $this->get_provider($video_url);
		$usuario_id = $this->session->userdata('usuario_id');

		if(is_array($video))
		{
			$this->select_database(15);

			$video_array = array(
				'tipoelemento_id' => 4, // video
				'elemento_titulo' => $video_leyenda,
				'elemento_desc' => $video['provider'],
				'elemento_ruta' => $video['video_id'],
				'elemento_estado' => '1',
				'elemento_usuario_mod' => $usuario_id
			);
			$this->DB->insert('elementos', $video_array);
			$video_id = $this->DB->insert_id();

			$ne_vid_array = array(
				'nota_id' => $nota_id,
				'elemento_id' => $video_id,
				'notas_elemento_estado' => '1',
				'notas_elemento_usuario_mod' => $usuario_id
			);
			$this->DB->insert('notas_elemento', $ne_vid_array);

			return TRUE;
		}

		return FALSE;
	}

	function store_imagen($file, $nombre, $leyenda, $nota_id, $cliente = NULL)
	{
		if(isset($cliente))
		{
			// die('entro');
			$this->client = $cliente;

		}
		//rutas
		$ruta = FCPATH . 'imagen/';

		// new name and upload
        $nombre = str_replace(' ', '-', $nombre);
        $rand = substr(md5(microtime()),rand(0,26),5);
        $ext = substr($file['name'], strrpos($file['name'], '.'));
        $new_name = strtolower(url_title(convert_accented_characters($nombre.'-'.$rand))).$ext;
        $tempFile = $file['tmp_name']; // file
        $file_uploaded = $ruta.$new_name; // new name

        if(!empty($file['tmp_name']) && exif_imagetype($tempFile))
        {
        	rename($tempFile, $file_uploaded); // upload

	        $size = getimagesize($file_uploaded);

	        switch ($ext) {
	        	case '.jpeg':
	        	case '.jpg':
	        		$mime = 'image/jpeg';
	        		break;
	        	case '.png':
	        		$mime = 'image/png';
	        		break;
	        }

	        // subir a CDN de THOR
	        
	        $bucket = $this->config->item('zeus_bucket');

        	$promise = $this->client->putObjectAsync(array(
        			'Bucket' => $bucket,
					'Key' => 'imagen/clones/'.$new_name, // nombre del archivo
					'SourceFile' => 'imagen/'.$new_name, // ruta origen
					'ACL' => 'public-read',
					'StorageClass' => 'STANDARD',
					'ContentType' => $mime
        		));
        	$result = $promise->wait();

        	// cargar la base de datos de clones
        	$this->select_database(15);
        	$usuario_id = $this->session->userdata('usuario_id');

	        // guardar en la base de datos
        	$imagen_array = array(
        		'tipoelemento_id' => 2, // imagen
        		'elemento_titulo' => $nombre,
        		'elemento_desc' => $leyenda,
        		'elemento_ruta' => $new_name,
        		'elemento_estado' => '1',
        		'elemento_usuario_mod' => $usuario_id
        	);
        	$this->DB->insert('elementos', $imagen_array);
        	$imagen_id = $this->DB->insert_id();

        	$ne_array = array(
        		'nota_id' => $nota_id,
        		'elemento_id' => $imagen_id,
        		'notas_elemento_estado' => '1',
        		'notas_elemento_usuario_mod' => $usuario_id
        	);
        	$this->DB->insert('notas_elemento', $ne_array);

        	return TRUE;
        }
	}

	function store_galeria_imagen($originales, $nombre, $leyendas, $nota_id, $cliente = NULL)
	{
		if(isset($cliente))
		{
			$this->client = $cliente;
		}
		// rutas
		$ruta = FCPATH . 'imagen/';

		// new name and upload
        $nombre = str_replace(' ', '-', $nombre);
        $rand = substr(md5(microtime()),rand(0,26),5);
        $nombre_nuevo = array();

		$num = 1;
		ob_start(null, 0, PHP_OUTPUT_HANDLER_CLEANABLE);

		$elementos_id = array();

		foreach($originales as $key => $original)
		{
			$ext = substr($original, strrpos($original, '.'));
			$new_name = strtolower(url_title(convert_accented_characters($nombre.$num.'-'.$rand))).$ext;
			$original_file = $ruta.'tmp/'.$original;
			$file_uploaded = $ruta.$new_name;
			
			if(!empty($original) && exif_imagetype($original_file)) // validacion, solo se renombra si es una imagen
			{
				rename($original_file, $file_uploaded); // cambiar nombre de foto original

				// guardar tamaño original en sesión para guardar luego en BD
		        switch ($ext) {
		        	case '.jpeg':
		        	case '.jpg':
		        		$mime = 'image/jpeg';
		        		break;
		        	case '.png':
		        		$mime = 'image/png';
		        		break;
	        	}

	        	// subir a CDN
		        $bucket = $this->config->item('zeus_bucket');

	        	$promise = $this->client->putObjectAsync(array(
	        			'Bucket' => $bucket,
						'Key' => 'imagen/clones/'.$new_name, // nombre del archivo
						'SourceFile' => 'imagen/'.$new_name, // ruta origen
						'ACL' => 'public-read',
						'StorageClass' => 'STANDARD',
						'ContentType' => $mime
	        		));
	        	$result = $promise->wait();

	        	// cargar la base de datos de clones
	        	$this->select_database(15);
	        	$usuario_id = $this->session->userdata('usuario_id');

	        	// guardar en la base de datos

				// Tabla Elementos
				if(!is_array($leyendas)) // es la leyenda general
				{
						$elementos = array(
							'tipoelemento_id' => 5, // galeria de fotos
							'elemento_titulo' => $nombre,
							'elemento_desc' => $leyendas,
							'elemento_ruta' => $new_name,
							'elemento_estado' => '1',
							'elemento_usuario_mod' => $usuario_id
						);
						$this->DB->insert('elementos', $elementos);
						$elementos_id[] = $this->DB->insert_id();
				}
				else
				{
						$elementos = array(
							'tipoelemento_id' => 5, // galeria de fotos
							'elemento_titulo' => $nombre,
							'elemento_desc' => $leyendas[$key],
							'elemento_ruta' => $new_name,
							'elemento_estado' => '1',
							'elemento_usuario_mod' => $usuario_id
						);

						$this->DB->insert('elementos', $elementos);
						$elementos_id[] = $this->DB->insert_id();
				}
			}
			$num++;
			ob_clean();
		}

		// Tabla Relacion_elementos
		$relacion_elementos = array();
		$total = count($elementos_id);

		for($i=0; $i<$total; $i++)
		{
			$relacion_elementos[] = array(
				'tipo_elemento_id' => 5, // galeria de fotos
				'elemento_padre_id' => $elementos_id[0],
				'elemento_hijo_id' => $elementos_id[$i],
				'orden_elemento' => $i+1
			);
		}
		$this->DB->trans_start();
		$this->DB->insert_batch('relacion_elementos', $relacion_elementos);

		// Tabla Notas elemento
		$notas_elemento = array();
		foreach($elementos_id as $elemento_id)
		{
			$notas_elemento[] = array(
				'nota_id' => $nota_id,
	        	'elemento_id' => $elemento_id,
	        	'notas_elemento_usuario_mod' => $usuario_id
			);
		}
		$this->DB->insert_batch('notas_elemento', $notas_elemento);
		$this->DB->trans_complete();

		return TRUE;
	}

	function store_galeria_video($videos_url, $leyenda, $nota_id)
	{
		$usuario_id = $this->session->userdata('usuario_id');
		$elementos_id = array();

		foreach($videos_url as $video_url)
		{
			$video = $this->get_provider($video_url);
		    if(is_array($video))
		    {
		    	$this->select_database(15);
		    	// tabla elementos
		    	$data = array(
		    		'tipoelemento_id' => 6, // galeria videos id
		    		'elemento_titulo' => $leyenda,
		    		'elemento_desc' => $video['provider'],
		    		'elemento_ruta' => $video['video_id'],
		    		'elemento_estado' => '1',
		    		'elemento_usuario_mod' => $usuario_id
		    	);

		    	$this->DB->insert('elementos', $data);
		    	$elementos_id[] = $this->DB->insert_id();
			}
		}

		$notas_elemento = array();
		foreach($elementos_id as $elemento_id)
    	{
	    	// tabla notas_elemento
	    	$notas_elemento[] = array(
	    		'nota_id' => $nota_id,
	    		'elemento_id' => $elemento_id,
	    		'notas_elemento_usuario_mod' => $usuario_id
	    	); 	
	    }
	    $this->DB->insert_batch('notas_elemento', $notas_elemento);

    	// Tabla Relacion_elementos
		$relacion_elementos = array();
		$total = count($elementos_id);

		for($i=0; $i<$total; $i++)
		{
			$relacion_elementos_videos[] = array(
				'tipo_elemento_id' => 6, // galeria de videos
				'elemento_padre_id' => $elementos_id[0],
				'elemento_hijo_id' => $elementos_id[$i],
				'orden_elemento' => $i+1
			);
		}
		$this->DB->insert_batch('relacion_elementos', $relacion_elementos_videos);		
	}

}