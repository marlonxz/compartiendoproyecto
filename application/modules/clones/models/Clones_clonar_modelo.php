<?php defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(300);
require_once(APPPATH.'libraries/aws-library/aws-autoloader.php');

class Clones_clonar_modelo extends CI_Model
{
	public $DB = NULL;
	public $client = '';

	function __construct()
	{
		parent::__construct();
	}

	function _select_database($sitio_id)
	{
		switch ($sitio_id) {
			case 1:
			case 2:
				$this->DB = $this->load->database('default', TRUE);
				break;
			case 3:
			case 4:
			case 5:
				$this->DB = $this->load->database('oasis', TRUE);
				break;
			case 6:
			case 7:
			case 8:
				$this->DB = $this->load->database('moda', TRUE);
				break;
			case 15:
				$this->DB = $this->load->database('clones', TRUE);
		}
	}

	function validar_nota()
	{
		$this->form_validation->set_rules('seccion_id', 'Sección', 'required');
		$this->form_validation->set_rules('sitio_id', 'Sitio', 'required');

		return $this->form_validation->run();
	}
	
	function clonar_nota()
	{
		$sitio_id = $this->input->post('sitio_id', TRUE);
		$seccion_id = $this->input->post('seccion_id', TRUE);
		$categoria_id = $this->input->post('categoria_id', TRUE);
		$noticia_id = $this->input->post('noticia_id', TRUE);
		$usuario_id = $this->session->userdata('usuario_id');

		// verificar que la nota no se haya migrado antes
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('origen_id', $noticia_id);
		$this->db->where('nota_estado', '1');
		$nota_creada = $this->db->get('notas');
		if($nota_creada->num_rows() > 0)
		{
			return FALSE;
		}
		// conseguir datos de la nota
		$noticia = $this->_get_noticia($noticia_id);

		if($noticia)
		{
			// limpiar el array de noticias
			$elementos = $this->_get_elementos($noticia);

			// conseguir el nombre de la seccion y la categoria para el linkseo
			$seccion = $this->_get_seccion($seccion_id, $sitio_id);
			$categoria = $this->_get_categoria($categoria_id, $sitio_id);
			$linkseo = !empty($categoria) ? reset($seccion).'/'.$categoria.'/'.$noticia[0]->nota_seolink : reset($seccion).'/'.$noticia[0]->nota_seolink;

			// insertar en Notas
			$nota_array = array(
				'sitio_id'			=> $sitio_id,
				'origen_id'			=> $noticia_id,
				'seccion_id'		=> $seccion_id,
				'fuente_id'			=> 15,
				'tipo_nota_id'		=> key($seccion),
				'categoria_id'		=> $categoria_id,
				'nota_titulo'		=> $noticia[0]->nota_titulo,
				'nota_contenido'	=> $noticia[0]->nota_contenido,
				'nota_seolink'		=> $linkseo,
				'nota_estado'		=> '1',
				'usuario_id'		=> $usuario_id
			);

			$this->db->insert('notas', $nota_array);
			$nota_id = $this->db->insert_id();

			// set bucket
			$bucket = $this->_set_bucket($sitio_id);

			// elementos
			if(!empty($elementos['imagen']))
			{
				$this->_clonar_imagen($elementos['imagen'], $nota_id, $sitio_id, $bucket);
			}

			if(!empty($elementos['galeria_imagen']))
			{
				$this->_clonar_galeria_imagen($elementos['galeria_imagen'], $nota_id, $sitio_id, $bucket);
			}
			
			if(!empty($elementos['video']))
			{
				$this->_clonar_video($elementos['video'], $nota_id, $sitio_id);
			}

			if(!empty($elementos['galeria_video']))
			{
				$this->_clonar_galeria_video($elementos['galeria_video'], $nota_id, $sitio_id);
			}			

			return TRUE;
		}

		return FALSE;
	}

	function _clonar_imagen($imagen, $nota_id, $sitio_id, $bucket)
	{
		// conseguir la info de las imágenes
		$size = getimagesize('http://static.cms.cmd.pe/imagen/clones/'.$imagen['elemento_ruta']);
		$imagenes = $this->_get_imagen_info($size, $sitio_id);

		// guardar en bucket del sitio y hacer resize
		$this->_guardar_imagen($sitio_id, $imagen['elemento_ruta'], $imagenes, $bucket);

		// base de datos
		// insertar en elementos
		$elemento_array = array(
			'tipoelemento_id' => 2, //imagen
			'sitio_id' => $sitio_id,
			'elemento_titulo' => $imagen['elemento_titulo'],
			'elemento_desc' => $imagen['elemento_desc'],
			'elemento_ruta' => $imagen['elemento_ruta'],
			'elemento_estado' => '1'
		);
		$this->db->insert('elementos', $elemento_array);
		$elemento_id = $this->DB->insert_id();

		// insertar en Tamanio_elemento_foto
        $data = array();
        foreach($imagenes['nombre'] as $key => $value)
        {
            $data[] = array(
                'elemento_id' => $elemento_id,
                'sitio_id' => (int)$sitio_id,
                'tamanio_elemento_nombre' => $imagenes['nombre'][$key],
                'tamanio_elemento_ruta' => $imagenes['ruta'][$key].$imagen['elemento_ruta'],
                'tamanio_elemento_estado' => '1',
                'tamanio_elemento_ancho' => $imagenes['ancho'][$key],
                'tamanio_elemento_alto' => $imagenes['alto'][$key],
            );
        }
        $this->db->insert_batch('tamanio_elemento_foto', $data);

        // Insertar en Notas_elemento
        $notas_elemento_array = array(
        	'nota_id' => $nota_id,
        	'elemento_id' => $elemento_id
        );
        $this->db->insert('notas_elemento', $notas_elemento_array);
	}

	function _clonar_galeria_imagen($galeria, $nota_id, $sitio_id, $bucket)
	{
		$elementos_id = array();

		foreach($galeria as $imagen)
		{
			$size = getimagesize('http://static.cms.cmd.pe/imagen/clones/'.$imagen['elemento_ruta']);
			$tamaños = $this->_get_imagen_info($size, $sitio_id);

			// guardar en el bucket y hacer resize
			$this->_guardar_imagen($sitio_id, $imagen['elemento_ruta'], $tamaños, $bucket);

			// base de datos
			
			// elementos
			$elemento_galeria_imagen_array = array(
				'tipoelemento_id' => $imagen['tipoelemento_id'], //galeria imagen
				'sitio_id' => $sitio_id,
				'elemento_titulo' => $imagen['elemento_titulo'],
				'elemento_desc' => $imagen['elemento_desc'],
				'elemento_ruta' => $imagen['elemento_ruta'],
				'elemento_estado' => '1'
			);
			$this->db->insert('elementos', $elemento_galeria_imagen_array);
			$elemento_id = $this->db->insert_id();
			$elementos_id[] = $elemento_id;

			// insertar en Tamanio_elemento_foto
	        $data = array();
	        foreach($tamaños['nombre'] as $key => $value)
	        {
	            $data[] = array(
	                'elemento_id' => $elemento_id,
	                'sitio_id' => (int)$sitio_id,
	                'tamanio_elemento_nombre' => $tamaños['nombre'][$key],
	                'tamanio_elemento_ruta' => $tamaños['ruta'][$key].$imagen['elemento_ruta'],
	                'tamanio_elemento_estado' => '1',
	                'tamanio_elemento_ancho' => $tamaños['ancho'][$key],
	                'tamanio_elemento_alto' => $tamaños['alto'][$key],
	            );
	        }
	        $this->db->insert_batch('tamanio_elemento_foto', $data);
		}

		// Tabla Relacion_elementos
		$relacion_elementos = array();
		$total = count($elementos_id);

		for($i=0; $i<$total; $i++)
		{
			$relacion_elementos[] = array(
				'tipo_elemento_id' => 5, // galeria de fotos
				'elemento_padre_id' => $elementos_id[0],
				'elemento_hijo_id' => $elementos_id[$i],
				'orden_elemento' => $i+1
			);
		}
		$this->db->insert_batch('relacion_elementos', $relacion_elementos);

		// Tabla Notas elemento
		$notas_elemento = array();
		foreach($elementos_id as $elem_id)
		{
			$notas_elemento[] = array(
				'nota_id' => $nota_id,
	        	'elemento_id' => $elem_id,
			);
		}
		$this->db->insert_batch('notas_elemento', $notas_elemento);
	}

	function _clonar_video($video, $nota_id, $sitio_id)
	{
		$data_video = array(
			'tipoelemento_id' => $video['tipoelemento_id'], // video id
    		'sitio_id' => $sitio_id,
    		'elemento_titulo' => $video['elemento_titulo'],
    		'elemento_desc' => $video['elemento_desc'],
	    	'elemento_ruta' => $video['elemento_ruta'],
    		'elemento_estado' => '1',
		);
		$this->db->insert('elementos', $data_video);
	    $video_id = $this->db->insert_id();

		$notas_elemento = array(
    		'nota_id' => $nota_id,
    		'elemento_id' => $video_id,
    	);
    	$this->db->insert('notas_elemento', $notas_elemento);
	}

	function _clonar_galeria_video($videos, $nota_id, $sitio_id)
	{
		$videos_id = array();

		foreach($videos as $video)
		{
			// elementos	
			$data_video = array(
				'tipoelemento_id' => $video['tipoelemento_id'], // video id
	    		'sitio_id' => $sitio_id,
	    		'elemento_titulo' => $video['elemento_titulo'],
	    		'elemento_desc' => $video['elemento_desc'],
		    	'elemento_ruta' => $video['elemento_ruta'],
	    		'elemento_estado' => '1',
			);
			$this->db->insert('elementos', $data_video);
		    $video_id = $this->db->insert_id();
		    $videos_id[] = $video_id;
		}

		// notas elemento
		$notas_elemento = array();
		foreach($videos_id as $video_id)
		{
			$notas_elemento[] = array(
	    		'nota_id' => $nota_id,
	    		'elemento_id' => $video_id,
	    	);
		}
		$this->db->insert_batch('notas_elemento', $notas_elemento);

		// Tabla Relacion_elementos
		$relacion_elementos = array();
		$total = count($videos_id);

		for($i=0; $i<$total; $i++)
		{
			$relacion_elementos[] = array(
				'tipo_elemento_id' => 6, // galeria de videos
				'elemento_padre_id' => $videos_id[0],
				'elemento_hijo_id' => $videos_id[$i],
				'orden_elemento' => $i+1
			);
		}
		$this->db->insert_batch('relacion_elementos', $relacion_elementos);
	}

	function _get_noticia($noticia_id)
	{
		$this->_select_database(15); // clones

		$this->DB->select('notas.nota_id, notas.fuente_id, notas.nota_titulo, notas.nota_contenido, notas.nota_seolink, notas.nota_fech_creacion, notas.nota_fech_update, notas.usuario_id, notas.nota_usuario_mod, e.elemento_id, e.tipoelemento_id, e.elemento_titulo, e.elemento_desc, e.elemento_ruta');
		$this->DB->join('notas_elemento ne', "ne.nota_id = $noticia_id");
		$this->DB->join('elementos e', 'e.elemento_id = ne.elemento_id');
		$this->DB->where('notas.nota_id', $noticia_id);
		$this->DB->where('e.elemento_estado', '1');

		return $this->DB->get('notas')->result();
	}

	function _get_elementos($noticia)
	{
		$elementos = array();
		foreach($noticia as $elem)
		{
			// galeria de videos
			if($elem->tipoelemento_id == '6')
			{
				$elementos['galeria_video'][] = array(
					'elemento_id' => $elem->elemento_id,
					'tipoelemento_id' => $elem->tipoelemento_id,
					'elemento_titulo' => $elem->elemento_titulo,
					'elemento_desc' => $elem->elemento_desc,
					'elemento_ruta' => $elem->elemento_ruta
				);
			}

			// galeria de imagenes
			if($elem->tipoelemento_id == '5')
			{
				$elementos['galeria_imagen'][] = array(
					'elemento_id' => $elem->elemento_id,
					'tipoelemento_id' => $elem->tipoelemento_id,
					'elemento_titulo' => $elem->elemento_titulo,
					'elemento_desc' => $elem->elemento_desc,
					'elemento_ruta' => $elem->elemento_ruta
				);
			}

			// imagen
			if($elem->tipoelemento_id == '2')
			{
				$elementos['imagen'] = array(
					'elemento_id' => $elem->elemento_id,
					'tipoelemento_id' => $elem->tipoelemento_id,
					'elemento_titulo' => $elem->elemento_titulo,
					'elemento_desc' => $elem->elemento_desc,
					'elemento_ruta' => $elem->elemento_ruta
				);
			}

			// video
			if($elem->tipoelemento_id == '4')
			{
				$elementos['video'] = array(
					'elemento_id' => $elem->elemento_id,
					'tipoelemento_id' => $elem->tipoelemento_id,
					'elemento_titulo' => $elem->elemento_titulo,
					'elemento_desc' => $elem->elemento_desc,
					'elemento_ruta' => $elem->elemento_ruta
				);
			}
			
		}
		return $elementos;
	}

	function _get_categoria($categoria_id, $sitio_id)
	{
		$this->load->model('categorias/Categorias_modelo');

		$categoria = $this->Categorias_modelo->get_categoria_by_id($categoria_id);
		$categoria_nombre = url_title(convert_accented_characters($categoria->categoria_nombre), '-', TRUE);
		$categoria_nombre = $categoria_nombre == 'cine-y-tv' ? 'cineytv' : $categoria_nombre;
		return $categoria_nombre;
	}

	function _get_seccion($seccion_id, $sitio_id)
	{
		$this->load->model('secciones/Secciones_modelo');

		$seccion = $this->Secciones_modelo->get_seccion_by_id($seccion_id);
		$seccion_nombre = array(
			$seccion->tipo_nota_id => url_title(convert_accented_characters($seccion->seccion_nombre), '-', TRUE)
		);
		return $seccion_nombre;
	}

	function _guardar_imagen($sitio_id, $name, $tamaños, $bucket)
	{
		$ext = substr($name, strrpos($name, '.'));
		$url = 'http://sss.thor.crp.pe/imagen/clones/'.$name;
		$file_uploaded = FCPATH.'imagen/'.$name;

		// descargar del bucket de thor a carpeta local
        copy($url, $file_uploaded);

        // resize
        $this->load->library('Image_moo');
        foreach($tamaños['nombre'] as $key => $value)
        {
        	$this->image_moo->load($file_uploaded)->resize_crop($tamaños['ancho'][$key], $tamaños['alto'][$key])->save('imagen'.$tamaños['ruta'][$key].$name, TRUE);
        }
        $this->image_moo->clear();

        switch ($ext) {
        	case '.jpeg':
        	case '.jpg':
        		$mime = 'image/jpeg';
        		break;
        	case '.png':
        		$mime = 'image/png';
        		break;
        }

        // subir a CDN
        $directorios = array('original/');
        foreach($tamaños['nombre'] as $nombre)
        {
        	$directorios[] = $nombre.'/';
        }

        foreach($directorios as $folder)
        {
        	$promise = $this->client->putObjectAsync(array(
        			'Bucket' => $bucket,
					'Key' => 'imagen/'.$folder.$name, // nombre del archivo
					'SourceFile' => 'imagen/'.$folder.$name, // ruta
					'ACL' => 'public-read',
					'StorageClass' => 'STANDARD',
					'ContentType' => $mime
        		));
        	$result = $promise->wait();
        }
	}

	function _get_imagen_info($imagen_info, $sitio_id)
	{
		$this->db->select('ti.tamano_nombre nombre, ti.tamano_ancho ancho, ti.tamano_alto alto');
		$this->db->where('sitio_id', $sitio_id);
		$tamaños = $this->db->get('tamanos_imagen_sitio ti')->result_array();

        $imagenes['nombre'] = array('original');
        $imagenes['ancho'] = array($imagen_info[0]);
        $imagenes['alto'] = array($imagen_info[1]);
        $imagenes['ruta'] = array('/original/');

        foreach($tamaños as $tamaño)
        {
        	$imagenes['nombre'][] = $tamaño['nombre'];
        	$imagenes['ancho'][] = $tamaño['ancho'];
        	$imagenes['alto'][] = $tamaño['alto'];
        	$imagenes['ruta'][] = '/'.$tamaño['nombre'].'/';
        }

		return $imagenes;
	}

	function _set_bucket($sitio_id)
    {
		$this->db->select('sitio_ruta_cdn, sitio_usuario_cdn, sitio_pass_cdn');
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('sitio_estado', '1');
		$bucket = $this->db->get('sitios')->row_array();

		$key = $bucket['sitio_usuario_cdn'];
		$secret_key = $bucket['sitio_pass_cdn'];
		$bucket = $bucket['sitio_ruta_cdn'];
		$sharedConfig = [
            'region' => 'us-east-1',
            'version' => 'latest'
        ];
        // Create an SDK class used to share configuration across clients
        $sdk = new Aws\Sdk($sharedConfig);

        // Create an Amazon S3 client using shared configuration data
        $credentials = new Aws\Credentials\Credentials($key, $secret_key);
        $this->client = $sdk->createS3(['credentials' => $credentials, 'http' => ['verify' => false]]);

        return $bucket; 
    }

}