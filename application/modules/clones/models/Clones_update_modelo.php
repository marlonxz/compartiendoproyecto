<?php defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(300);

class Clones_update_modelo extends Clones_modelo
{	
	public $DB = NULL;
	public $client = '';

	function __construct()
	{
		parent::__construct();
	}

	function save_nota($nota_id)
	{
		// Create an SDK class used to share configuration across clients
		$this->load->config('bucket');
		$key = $this->config->item('zeus_key');
	    $secret_key = $this->config->item('zeus_secret_key');
		$sharedConfig = [
            'region' => $this->config->item('zeus_region'),
            'version' => $this->config->item('zeus_version')
        ];
        $sdk = new Aws\Sdk($sharedConfig);

        // Create an Amazon S3 client using shared configuration data
        $credentials = new Aws\Credentials\Credentials($key, $secret_key);
        $this->client = $sdk->createS3(['credentials' => $credentials, 'http' => ['verify' => false]]);

        // validacion
        $this->form_validation->set_rules('nota_titulo', 'Titular', 'required|trim');
        $this->form_validation->set_rules('nota_contenido', 'Contenido', 'required|trim');

        if($this->form_validation->run())
        {
        	// contenido
        	$titulo = $this->input->post('nota_titulo', TRUE);
        	$contenido = $this->input->post('nota_contenido', TRUE);
        	$usuario_id = $this->session->userdata('usuario_id');

        	$this->select_database(15);
        	$nota_update_array = array(
        		'nota_titulo' => $titulo,
        		'nota_contenido' => $contenido,
        		'nota_usuario_mod' => $usuario_id
        	);
        	$this->DB->where('nota_id', $nota_id);
        	$this->DB->update('notas', $nota_update_array);

        	// elementos

        	// imagen
        	$imagen_id = $this->input->post('nota_imagen_id', TRUE);
        	$imagen_file = $_FILES['imagen_file'];
        	$imagen_nombre = $this->input->post('imagen_nombre', TRUE);
			$imagen_leyenda = $this->input->post('imagen_leyenda', TRUE);

			$this->_update_imagen($imagen_id, $imagen_file, $imagen_nombre, $imagen_leyenda, $nota_id);

			// videos
			$video_id = $this->input->post('video_id', TRUE);
			$video_url = $this->input->post('video_url', TRUE);
			$video_leyenda = $this->input->post('video_leyenda', TRUE);

			$this->_update_video($video_id, $video_url, $video_leyenda, $nota_id);

			// galeria imagen
			$imagenes_nuevas = $this->input->post('gal_imagen_name', TRUE);
			$leyendas_nuevas = $this->input->post('gal_imagen_leyenda', TRUE);
			$galeria_nombre = $this->input->post('gal_imagen_nombre', TRUE);
			$imgs_antiguas_id = $this->input->post('galeria_fotos_actual', TRUE);
			$leyendas_antiguas = $this->input->post('leyenda_galeria', TRUE);
			$imagenes_borradas = $this->input->post('galeria_fotos_borradas', TRUE);

			$this->_update_galeria_imagen($imagenes_nuevas, $leyendas_nuevas, $imgs_antiguas_id, $galeria_nombre, $leyendas_antiguas, $imagenes_borradas, $nota_id);

			// galeria video
			$videos_url = $this->input->post('gal_video_url', TRUE);
			$videos_leyenda = $this->input->post('gal_video_leyenda', TRUE);
			$videos_borrar = $this->input->post('videos_antiguos', TRUE);

			$this->_update_galeria_video($videos_url, $videos_leyenda, $videos_borrar, $nota_id);

			return TRUE;      	
        }

        return FALSE;
	}

	function _update_video($video_id, $video_url, $leyenda, $nota_id)
	{
		if(empty($video_id))
		{
			// si video id está vacío, quiere decir que se ha insertado un elemento nuevo
			$this->load->model('Clones_crear_modelo');
			$this->Clones_crear_modelo->store_video($video_url, $leyenda, $nota_id);
		}
		else
		{
			// si no, se actualiza el elemento existente
			if(!empty($video_url))
			{
				$usuario_id = $this->session->userdata('usuario_id');
				$video = $this->get_provider($video_url);
				
				$video_update['elemento_titulo'] = $leyenda;
				$video_update['elemento_desc'] = $video['provider'];
				$video_update['elemento_ruta'] = $video['video_id'];
				$video_update['elemento_usuario_mod'] = $usuario_id;
				
				$this->DB->where('elemento_id', $video_id);
	    		$this->DB->update('elementos', $video_update);
			}
			else
			{
				// si video url está vacío, se borra el elemento
				$this->_delete_elemento($video_id);
			}
			
		}
	}

	function _update_imagen($imagen_id, $file, $nombre, $leyenda, $nota_id)
	{
		// se subió una imagen nueva
    	if(!empty($file['name']))
    	{
			if(empty($file['tmp_name']) || !exif_imagetype($file['tmp_name']))
			{
				$this->session->set_userdata('error_imagen', 'El archivo que subiste no es una imagen');
				return FALSE;
			}

			// si había un audio anterior, desactivar el elemento
			if(!empty($imagen_id)) { 
				$this->_delete_elemento($imagen_id);	
			} else {
			// borrar los elementos tipo canción relacionados con la nota
				$this->_delete_elemento_by_nota($nota_id, 2);	
			}

			// guardar la imagen nueva en la BD
			$this->load->model('Clones_crear_modelo');
			$this->Clones_crear_modelo->store_imagen($file, $nombre, $leyenda, $nota_id, $this->client);
    	}
    	else
    	{
    		// si ya existía un elemento imagen, se actualiza
    		if(!empty($imagen_id))
    		{
    			$usuario_id = $this->session->userdata('usuario_id');

    			$this->select_database(15);
    			$imagen_update['elemento_titulo'] = $nombre;
    			$imagen_update['elemento_desc'] = $leyenda;
    			$video_update['elemento_usuario_mod'] = $usuario_id;
    			$this->DB->where('elemento_id', $imagen_id);
    			$this->DB->update('elementos', $imagen_update);
    		}
    	}
	}

	function _update_galeria_imagen($nuevas, $leyendas_nuevas, $ids, $nombre, $leyendas, $borradas, $nota_id)
	{
		// Si existen imágenes nuevas, se insertan
		if(!empty($nuevas))
		{						
			$this->load->model('Clones_crear_modelo');
			$this->Clones_crear_modelo->store_galeria_imagen($nuevas, $nombre, $leyendas_nuevas, $nota_id, $this->client);			
		}

		// actualizar la leyenda
		if(!empty($ids))
		{
			$update_galeria = array();
			$this->select_database(15);
			foreach($ids as $key => $id)
			{
				$update_galeria[] = array(
					'elemento_id' => $id,
					'elemento_desc' => $leyendas[$key]
				);
			}
			$this->DB->update_batch('elementos', $update_galeria, 'elemento_id');
		}

		// Eliminar fotos
		if(!empty($borradas))
		{
			$borradas = json_decode($borradas);
			foreach($borradas as $img_id)
			{
				$this->_delete_elemento($img_id);
			}
		}
	}

	function _update_galeria_video($videos_url, $videos_leyenda, $videos_borrar, $nota_id)
	{	
		// insertar los videos	
		if(is_array($videos_url))
		{
			if(!empty(array_filter($videos_url)))
			{
				$this->load->model('Clones_crear_modelo');
				$this->Clones_crear_modelo->store_galeria_video($videos_url, $videos_leyenda, $nota_id);
			}
		}

		// Eliminar Videos
		$videos_borrar = json_decode($videos_borrar);
		if(!empty($videos_borrar))
		{
			foreach($videos_borrar as $video_id)
			{
				$this->_delete_elemento($video_id);
			}
		}
	}

	function _delete_elemento($elemento_id)
	{
		$this->select_database(15);

		$data['notas_elemento_estado'] = '0';
		$this->DB->where('elemento_id',$elemento_id);
        $this->DB->update('notas_elemento', $data); 

        $data_elemento['elemento_estado'] = '0';
        $this->DB->where('elemento_id', $elemento_id);
        $this->DB->update('elementos', $data_elemento);

	}

	function _delete_elemento_by_nota($nota_id, $tipoelemento_id)
	{
		$this->select_database(15);

		// conseguir el id de los elementos
		$this->DB->select('e.elemento_id');
		$this->DB->from('elementos e');
		$this->DB->join('notas_elemento ne', "ne.elemento_id = e.elemento_id AND ne.nota_id = $nota_id");
		$this->DB->where('e.elemento_estado', '1');
		$this->DB->where('e.tipoelemento_id', $tipoelemento_id);
		$query = $this->DB->get();

		// borrar de la tabla notas elementos
		if($query->num_rows() > 0)
		{
			$elemento = $query->result();
			foreach($elemento as $e)
			{
				$this->DB->where('elemento_id', $e->elemento_id);
				$this->DB->where('nota_id', $nota_id);
				$this->DB->delete('notas_elemento');
			}
		}
	}
}
