<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clones extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$data = array();
        
        // Llamar a la lista de notas de la seccion
        $this->load->model('Clones_general_modelo');
        $data['notas'] = $this->Clones_general_modelo->list_noticias(); // default: 500

        $this->load->view('clones_subhome', $data);
	}

	function crear()
	{
		$sitio_id = $this->session->userdata('sitio_id_session');
		$this->load->helper('sitios/sitios');
        $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
        $data['sitio_id'] = $sitio_id;

		$this->load->view('clones_crear_form', $data);
	}

	function editar($nota_id)
	{
		$this->load->model('Clones_general_modelo');

		$data['nota'] = $this->Clones_general_modelo->get_nota_by_id($nota_id);

		$sitio_id = $this->session->userdata('sitio_id_session');
		$this->load->helper('sitios/sitios');
        $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
        $data['sitio_id'] = $sitio_id;
		
		// get elementos
		$data['imagen'] = $this->Clones_general_modelo->get_elemento('imagen', $nota_id);
		$data['video'] = $this->Clones_general_modelo->get_elemento('video', $nota_id);
		$data['galeria_imagen'] = $this->Clones_general_modelo->get_elemento('galeria_imagen', $nota_id);
		$data['galeria_video'] = $this->Clones_general_modelo->get_elemento('galeria_video', $nota_id);

		$this->load->view('clones_editar_form', $data);
	}

	function clonar($nota_id)
	{		
		$this->load->model('Clones_general_modelo');
		$this->load->module('sitios/Sitios_getters');

		$usuario_id = $this->session->userdata('usuario_id');
		$sitios_id = $this->sitios_getters->list_sitios_id_by_usuario($usuario_id);
		$sitio_id = $this->session->userdata('sitio_id_session');
		$this->load->helper('sitios/sitios');
        $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
        $data['sitio_id'] = $sitio_id;
        $data['sitios'] = $this->sitios_getters->list_sitios_by_ids($sitios_id);
		$data['nota'] = $this->Clones_general_modelo->get_nota_by_id($nota_id);

		$this->load->view('clones_clonar_form', $data);
	}

	function clonar_nota()
	{
		if($_POST)
		{
			$this->load->model('Clones_clonar_modelo');
			$validacion = $this->Clones_clonar_modelo->validar_nota();
			if($validacion)
			{
				$resultado = $this->Clones_clonar_modelo->clonar_nota();

				$mensaje = $resultado ? 'Nota Clonada' : 'Error al clonar. Es posible que la nota ya haya sido clonada.';
				
				$this->session->set_flashdata('success', $mensaje);
				redirect('clones/index', 'refresh');
			}
			else
			{
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
		}
		else
		{
			die('Not allowed');
		}
	}

	function eliminar($nota_id)
	{
		$this->load->model('Clones_general_modelo');
		$resultado = $this->Clones_general_modelo->eliminar_nota($nota_id);
        $mensaje = $resultado ? 'Nota Eliminada' : 'Hubo un error al eliminar la foto. Por favor inténtalo de nuevo';

        $this->session->set_flashdata('success', $mensaje);
        redirect("/clones/index", 'refresh');
	}

	function procesar()
	{
		$nota_id = $this->input->post('nota_id', TRUE);
		if(empty($nota_id))
		{

			$this->load->model('Clones_crear_modelo');
			$resultado = $this->Clones_crear_modelo->save_nota();
		}
		else
		{
			$this->load->model('Clones_update_modelo');
			$resultado = $this->Clones_update_modelo->save_nota($nota_id);
		}		

		$mensaje = $resultado ? '¡Contenido listo!' : 'Hubo un problema, inténtalo de nuevo.';

		$this->session->set_flashdata('success', $mensaje);
		redirect('clones/index');
	}

	function clonacion_nota()
	{
		if($_POST)
		{
			$this->load->model('Noticias_clonar_modelo');
			$validacion = $this->Noticias_clonar_modelo->validar_nota();
			if($validacion)
			{
				$resultado = $this->Noticias_clonar_modelo->clonar_nota();
				$mensaje = $resultado ? 'Nota Clonada' : 'Error al clonar. Es posible que la nota ya haya sido clonada.';
				$sitio_id_origen = $this->input->post('sitio_id_origen', TRUE);
				$seccion_id_origen = $this->input->post('seccion_id_origen', TRUE);

				$this->session->set_flashdata('success', $mensaje);
				redirect('secciones/'.$sitio_id_origen.'/'.$seccion_id_origen);
			}
			else
			{
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
		}
		else
		{
			die('Not allowed');
		}
	}

	// funciones ajax
	function get_secciones()
	{
		$sitio_id = $this->input->post('sitio_id', TRUE);
		$this->load->module('secciones/Secciones_getters');
		$secciones = $this->secciones_getters->list_secciones_noticias_by_sitio($sitio_id);
		echo json_encode($secciones);
	}

	function get_categorias()
	{
		$seccion_id = $this->input->post('seccion_id', TRUE);
		$this->load->module('categorias/Categorias_getters');
		$categorias = $this->categorias_getters->list_categorias_by_seccion($seccion_id);
		echo json_encode($categorias);
	}



}