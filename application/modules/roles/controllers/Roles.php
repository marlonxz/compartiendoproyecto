<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends MY_Controller {

	public function __construct()
	{
		parent::__construct();	
		$this->load->model('Roles_modelo');
	}

	public function index()
	{
        $data = array();
        $roles = $this->session->userdata('roles');

        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes permisos de administración.');
        }
        else
        {	
			$data['roles'] = $this->Roles_modelo->get_roles();
	    }
 
		$this->load->view('lista_roles',$data);  
	}

	function crear()
	{
		$this->load->view('crear_rol_form'); 
	}

	function editar($rol_id)
	{
		$data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $rol = $this->Roles_modelo->get_rol_by_id($rol_id);

            $data['rol'] = $rol;
        }

        $this->load->view('editar_rol_form', $data);
	}

	function eliminar($rol_id)
	{
		$result = $this->Roles_modelo->delete_rol($rol_id);
        if($result)
        {
            $this->session->set_flashdata('success', '¡Rol eliminado!');
            redirect('roles');
        }
        else
        {
            $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
        }
	}

	function procesar()
	{
		// Verificar que el usuario es admin
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Roles_modelo->procesar_rol();
            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Rol grabado!');
                redirect('roles');
            }
        }
	}

	public function list_roles()
	{
		return $this->Roles_modelo->get_roles();
	}

	function list_roles_for_moderador()
	{
		return $this->Roles_modelo->get_roles_for_moderador();
	}

}