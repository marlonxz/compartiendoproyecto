<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Roles_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'roles';
	}

	function get_rol_by_id($rol_id)
	{
		$this->db->from($this->__tabla)
					->where('rol_id', $rol_id)
					->where('rol_estado', '1');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		return FALSE;
	}

	function get_roles()
	{
		$this->db->from($this->__tabla);
		$this->db->where('rol_estado', '1');
		$roles = $this->db->get()->result();

		if(!empty($roles))
		{
			return $roles;
		}
		
		return FALSE;
	}

	function procesar_rol()
	{
		$this->form_validation->set_rules('rol_nombre', 'Nombre', 'required|trim');
		$this->form_validation->set_rules('rol_descripcion', 'Descripcion', 'trim');
		$this->form_validation->set_rules('nivel_acceso', 'Nivel de acceso', 'trim|required');
		$this->form_validation->set_rules('rol_estado', 'Estado', 'trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$rol_id = $this->input->post('rol_id', TRUE);
			$rol_nombre = $this->input->post('rol_nombre', TRUE);
			$rol_descripcion = $this->input->post('rol_descripcion', TRUE);
			$rol_estado = $this->input->post('rol_estado', TRUE);
			$nivel_acceso = $this->input->post('nivel_acceso', TRUE);

			$rol_array = array(
				'rol_nombre' => $rol_nombre,
				'rol_desc' => $rol_descripcion,
				'rol_estado' => $rol_estado,
				'nivel_acceso' => $nivel_acceso,
				'rol_usuario_mod' => $usuario_mod
			);

			if(empty($rol_id))
			{
				// insertar a la tabla roles
				$rol_array['rol_fech_creacion'] = date('Y-m-d G:i:s');
				$this->db->insert('roles', $rol_array);
			}
			else
			{
				// editar datos básicos
				$rol_array['rol_fech_update'] = date('Y-m-d G:i:s');
				$this->db->where('rol_id', $rol_id);
				$this->db->update('roles', $rol_array);
			}
			
			return $this->db->affected_rows();
		}
	}

	function delete_rol($rol_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'rol_estado' => '0',
			'rol_fech_update' => date('Y-m-d G:i:s'),
			'rol_usuario_mod' => $usuario_mod
		);

		$this->db->where('rol_id', $rol_id);
		$this->db->update('roles', $array);

		return $this->db->affected_rows();
	}

	function get_nivel_acceso_by_rol($rol_id)
	{
		if(is_numeric($rol_id)) // seguridad
		{
			$query = $this->db->select('nivel_acceso')
								->where('rol_id', $rol_id)
								->get('roles');
			
			if($query->num_rows() > 0)
			{
				$nivel = $query->row();

				$nivel_acceso = $this->session->userdata('nivel_acceso') ? json_decode($this->session->userdata('nivel_acceso')) : new stdClass();
				if(!property_exists($nivel_acceso, $rol_id))
				{
					$nivel_acceso->$rol_id = $nivel->nivel_acceso;
					$this->session->set_userdata('nivel_acceso', json_encode($nivel_acceso));
				}
				return $nivel->nivel_acceso;
			}
			else
			{
				$nivel_acceso = $this->session->userdata('nivel_acceso') ? json_decode($this->session->userdata('nivel_acceso')) : new stdClass();
				if(!property_exists($nivel_acceso, $rol_id))
				{
					$nivel_acceso->$rol_id = 150;
					$this->session->set_userdata('nivel_acceso', json_encode($nivel_acceso));
				}
				return 150;
			}
		}
		return FALSE;
	}

	/*function get_roles_for_moderador()
	{
		$this->db->from($this->__tabla);
		$this->db->where('rol_estado', '1');
		$this->db->where('rol_id !=', '1');
		$query = $this->db->get()->result();

		if(!empty($query))
		{
			return $query;
		}

		return FALSE;
	}*/
}
