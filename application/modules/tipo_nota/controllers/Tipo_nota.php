<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_nota extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Tipo_nota_modelo');
	}

	public function index()
	{
        $data = array();
        $roles = $this->session->userdata('roles');

        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes permisos de administración.');
        }
        else
        {   		
    		$data['tipo_notas'] = $this->Tipo_nota_modelo->get_all_tiponotas();
        }
 
		$this->load->view('lista_tipo_nota',$data);  
	}

    function crear()
    {
        $this->load->view('crear_tipo_nota_form');
    }

    function editar($tipo_nota_id)
    {
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $tipo_nota = $this->Tipo_nota_modelo->get_tipo_nota_by_id($tipo_nota_id);

            $data['tipo_nota'] = $tipo_nota;
        }

        $this->load->view('editar_tipo_nota_form', $data);
    }

    function eliminar($tipo_nota_id)
    {
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $result = $this->Tipo_nota_modelo->delete_tipo_nota($tipo_nota_id);
            if($result)
            {
                $this->session->set_flashdata('success', '¡Tipo de nota eliminado!');
                redirect('tipo-nota');
            }
            else
            {
                $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
            }
        }
        
    }

    function procesar()
    {
        // Verificar que el usuario es admin
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Tipo_nota_modelo->procesar_tipo_nota();
            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Tipo de nota grabado!');
                redirect('tipo-nota');
            }
        }
    }

    function list_tiponotas()
    {
        return $this->Tipo_nota_modelo->get_all_tiponotas();
    }


}