<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_nota_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = "tipo_nota";
	}

	function get_tipo_nota_by_id($tipo_nota_id)
	{
		$this->db->from($this->__tabla)
					->where('tipo_nota_id', $tipo_nota_id)
					->where('tipo_nota_estado', '1');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		return FALSE;
	}

	function get_tipo_nota() // quitar el sitio id
	{
		$this->db->select('tipo_nota_id, tipo_nota_nombre, tipo_nota_desc');
		$this->db->where('tipo_nota_estado', '1');
		$query = $this->db->get('tipo_nota');

		if($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	function procesar_tipo_nota()
	{
		$this->form_validation->set_rules('tipo_nota_nombre', 'Nombre', 'required|trim');
		$this->form_validation->set_rules('tipo_nota_descripcion', 'Descripcion', 'trim');
		$this->form_validation->set_rules('tipo_nota_estado', 'Estado', 'required|trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$tipo_nota_id = $this->input->post('tipo_nota_id', TRUE);
			$tipo_nota_nombre = $this->input->post('tipo_nota_nombre', TRUE);
			$tipo_nota_descripcion = $this->input->post('tipo_nota_descripcion', TRUE);
			$tipo_nota_estado = $this->input->post('tipo_nota_estado', TRUE);

			$tipo_nota_array = array(
				'tipo_nota_nombre' => $tipo_nota_nombre,
				'tipo_nota_desc' => $tipo_nota_descripcion,
				'tipo_nota_estado' => $tipo_nota_estado,
				'tipo_nota_usuario_mod' => $usuario_mod
			);

			if(empty($tipo_nota_id))
			{
				// insertar a la tabla tipo_nota
				$tipo_nota_array['tipo_nota_fech_creacion'] = date('Y-m-d G:i:s');
				$this->db->insert('tipo_nota', $tipo_nota_array);
			}
			else
			{
				// editar datos básicos
				$tipo_nota_array['tipo_nota_fech_update'] = date('Y-m-d G:i:s');
				$this->db->where('tipo_nota_id', $tipo_nota_id);
				$this->db->update('tipo_nota', $tipo_nota_array);
			}
			
			return $this->db->affected_rows();
		}
	}

	function delete_tipo_nota($tipo_nota_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'tipo_nota_estado' => '0',
			'tipo_nota_fech_update' => date('Y-m-d G:i:s'),
			'tipo_nota_usuario_mod' => $usuario_mod
		);

		$this->db->where('tipo_nota_id', $tipo_nota_id);
		$this->db->update('tipo_nota', $array);

		return $this->db->affected_rows();
	}

	function get_all_tiponotas()
	{
		$this->db->select('tipo_nota_id, tipo_nota_nombre, tipo_nota_desc, tipo_nota_fech_creacion, tipo_nota_fech_update');
		$this->db->where('tipo_nota_estado', '1');
		$query = $this->db->get('tipo_nota')->result();
		if(!empty($query))
		{
			return $query;
		}

		return FALSE;
	}

}