<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipotag extends MY_Controller {

    public function __construct()
    {
    	parent::__construct();
        $this->load->model('Tipotag_modelo');
    }

    function index()
    {
        $data = array();
        $roles = $this->session->userdata('roles');

        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes permisos de administración.');
        }
        else
        {           
            $data['tipo_tags'] = $this->Tipotag_modelo->get_tipotag();
        }
 
        $this->load->view('lista_tipo_tag',$data); 
    }

    function crear()
    {
        $this->load->view('crear_tipo_tag_form');
    }

    function editar($tipotag_id)
    {
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $tipotag = $this->Tipotag_modelo->get_tipo_tag_by_id($tipotag_id);

            $data['tipotag'] = $tipotag;
        }

        $this->load->view('editar_tipo_tag_form', $data);
    }

    function eliminar($tipotag_id)
    {
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $result = $this->Tipotag_modelo->delete_tipotag($tipotag_id);
            if($result)
            {
                $this->session->set_flashdata('success', '¡Tipo de tag eliminado!');
                redirect('tipotag');
            }
            else
            {
                $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
            }
        }
    }

    function procesar()
    {
        // Verificar que el usuario es admin
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Tipotag_modelo->procesar_tipo_tag();
            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Tipo de tag grabado!');
                redirect('tipotag');
            }
        }
    }

    public function list_tipotags()
    {
        return $this->Tipotag_modelo->get_tipotag();
    }

    public function list_tipotags_by_sitios($sitios_id)
    {
        return $this->Tipotag_modelo->get_tipotag_by_sitios($sitios_id);
    }

}