<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tipotag_modelo extends CI_Model
{	

	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'tipotag';
	}

	function get_tipo_tag_by_id($tipotag_id)
	{
		$this->db->from($this->__tabla)
					->where('tipotag_id', $tipotag_id)
					->where('tipotag_estado', '1');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		return FALSE;
	}

	function get_tipotag()
	{
		$this->db->select('tipotag_id, tipotag_nombre, tipotag_extendido, tipotag_estado, tipotag_fech_creacion, tipotag_fech_update, tipotag_usuario_mod, u.usuario_nombre');
		$this->db->from($this->__tabla);
		$this->db->join('usuarios u', 'u.usuario_id = tipotag_usuario_mod');
		$this->db->where('tipotag_estado', '1');
		$tipotags = $this->db->get()->result();

		if(!empty($tipotags))
		{
			return $tipotags;
		}
		return FALSE;
	}

	function procesar_tipo_tag()
	{
		$this->form_validation->set_rules('tipotag_nombre', 'Nombre', 'required|trim');
		$this->form_validation->set_rules('tipotag_extendido', 'Extendido', 'required|trim');
		$this->form_validation->set_rules('tipotag_estado', 'Estado', 'required|trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$tipotag_id = $this->input->post('tipotag_id', TRUE);
			$tipotag_nombre = $this->input->post('tipotag_nombre', TRUE);
			$tipotag_extendido = $this->input->post('tipotag_extendido', TRUE);
			$tipotag_estado = $this->input->post('tipotag_estado', TRUE);

			$tipotag_array = array(
				'tipotag_nombre' => $tipotag_nombre,
				'tipotag_extendido' => $tipotag_extendido,
				'tipotag_estado' => $tipotag_estado,
				'tipotag_usuario_mod' => $usuario_mod
			);

			if(empty($tipotag_id))
			{
				// insertar a la tabla tipotag
				$tipotag_array['tipotag_fech_creacion'] = date('Y-m-d G:i:s');
				$this->db->insert('tipotag', $tipotag_array);
			}
			else
			{
				// editar datos básicos
				$tipotag_array['tipotag_fech_update'] = date('Y-m-d G:i:s');
				$this->db->where('tipotag_id', $tipotag_id);
				$this->db->update('tipotag', $tipotag_array);
			}
			
			return $this->db->affected_rows();
		}
	}

	function delete_tipotag($tipotag_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'tipotag_estado' => '0',
			'tipotag_fech_update' => date('Y-m-d G:i:s'),
			'tipotag_usuario_mod' => $usuario_mod
		);

		$this->db->where('tipotag_id', $tipotag_id);
		$this->db->update('tipotag', $array);

		return $this->db->affected_rows();
	}

}
