<?php 
$titulo = "Editar tipo de tag | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <div class="box-typical box-typical-padding">
          <h5 class="m-t-lg with-border">Editar tipo de tag</h5>

          <form method="post" action="<?php echo base_url();?>tipotag/procesar">
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Nombre *</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" name="tipotag_nombre" placeholder="Tipo de nota" value="<?php echo $tipotag->tipotag_nombre;?>" required></p>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Extendido</label>
              <div class="col-sm-10">
                <select name="tipotag_extendido" class="form-control" required>
                  <?php for ($i = 0; $i <= 1; $i++):?>
                    <option value="<?php echo $i;?>" <?php echo $i == $tipotag->tipotag_extendido ? 'selected' : '';?>><?php echo $i;?></option>
                  <?php endfor; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Estado</label>
              <div class="col-sm-10">
                <select name="tipotag_estado" class="form-control" required>
                  <?php for ($i = 0; $i <= 2; $i++):?>
                    <option value="<?php echo $i;?>" <?php echo $i == $tipotag->tipotag_estado ? 'selected' : '';?>><?php echo $i;?></option>
                  <?php endfor; ?>
                </select>
              </div>
            </div>

            <input type="hidden" name="tipotag_id" value="<?php echo $tipotag->tipotag_id;?>">
            <hr>
            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-rounded btn-inline btn-primary">Editar</button>
                <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
              </div>
            </div>  
                   
          </form>
        </div>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/plugins.js"></script>

  <script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>

<script>
  $('#cancelar').click(function(e){
    e.preventDefault();
    window.location.href="<?php echo base_url();?>tipotag";
  });
</script>
</body>
</html>
