<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Validador_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'usuarios';
	}

	function validar_usuario($email, $password)
	{
		$this->db->from($this->__tabla);
		$this->db->where('usuario_email', $email);
		$user = $this->db->get();

		if($user->num_rows() > 0)
		{
			$usuario = $user->row();
			$hash = $usuario->usuario_password;
			return password_verify($password, $hash);
		}

		return FALSE;
	}

	function get_sitios_permitidos($usuario_id)
	{
		$this->db->select('sitio_id');
		$this->db->where('usuario_id', $usuario_id);
		$this->db->where('registro_estado', '1');
		$query = $this->db->get('usuario_rol_sitio');

		if($query->num_rows() > 0)
		{
			$ids = array();
			$sitios = $query->result();
			foreach($sitios as $sitio)
			{
				$ids[] = $sitio->sitio_id;
			}

			return $ids;
		}

		return FALSE;
	}

	function validar_permiso_sitio($sitio_id, $usuario_id)
	{
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('usuario_id', $usuario_id);
		$query = $this->db->get('usuario_rol_sitio');

		return $query->num_rows();
	}

	/*
	 * Devuelve un array de roles, con id de sitio como llave
	 */ 
	function get_rol($usuario_id)
	{
		$this->db->select('rol_id, sitio_id');
		$this->db->where('usuario_id', $usuario_id);
		$query = $this->db->get('usuario_rol_sitio');

		if($query->num_rows() > 0)
		{
			$data = array();
			$roles = $query->result();
			foreach($roles as $key => $rol)
			{
				$data[$rol->sitio_id] = $rol->rol_id;
			}

			return $data;
		}

		return FALSE;
	}

}