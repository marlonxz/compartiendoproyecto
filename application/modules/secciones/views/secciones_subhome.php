<?php 
$titulo = $seccion_nombre . " - ".$sitio_nombre." | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/datatables-net/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/datatables-net.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
      <header class="section-header">
        <div class="tbl">
          <div class="tbl-row">
            <div class="tbl-cell">
              <h2>Panel de <?php echo $seccion_nombre;?> - <?php echo $sitio_nombre;?></h2>
              <div class="subtitle">Aquí podrás crear contenidos para la sección <?php echo $seccion_nombre;?></div>
            </div>
          </div>
          <br>
          <div class="tbl-row">
            <div class="tbl-cell">
              <a href="<?php echo base_url();?><?php echo $sitio_id;?>/notas/crear" class="btn btn-rounded btn-inline">Crear contenido</a>
            </div>
          </div>
        </div>
      </header>
      <section class="card">
        <div class="card-block">
          <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
              <th>ID</th>
              <th>Titular</th>
              <th>URL</th>
              <th>Usuario de creación</th>
              <th>Fecha de update</th>
              <th>Portada</th>
              <th>Acciones</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>ID</th>
              <th>Titular</th>
              <th>URL</th>
              <th>Usuario de creación</th>
              <th>Fecha de update</th>
              <th>Portada</th>
              <th>Acciones</th>
            </tr>
            </tfoot>
            <tbody>
            <?php if(!empty($notas)):?>
            <?php foreach($notas as $nota):?>
            <tr>
              <td><?php echo $nota->nota_id;?></td>
              <td><?php echo $nota->nota_titulo;?></td>
              <td><?php echo $nota->nota_seolink;?></td>
              <td><?php echo $nota->usuario_user;?></td>
              <td><?php echo $nota->nota_fecha_publicacion;?></td>
              <td><?php echo $nota->nota_flag_portada;?></td>
              <td>
                <?php if($clonacion):?>
                <a href="<?php echo base_url();?>notas/clonar/<?php echo $sitio_id;?>/<?php echo $seccion_id;?>/<?php echo $nota->nota_id;?>" title="clonar"><span class="fa fa-copy"></span></a>
                <?php endif; ?>
                &nbsp;
                <a href="<?php echo base_url();?>notas/editar/<?php echo $sitio_id;?>/<?php echo $seccion_id;?>/<?php echo $nota->nota_id;?>"><span class="fa fa-edit"></span></a>
                &nbsp;
                <a class="eliminarNota" href="<?php echo base_url();?>notas/eliminar/<?php echo $sitio_id;?>/<?php echo $seccion_id;?>/<?php echo $nota->nota_id;?>"><span class="fa fa-close"></span></a>
              </td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
          </table>
        </div>
      </section>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/datatables-net/datatables.min.js"></script>

<script>
  $(function() {
    $('#example').DataTable({
      responsive: true,
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
      },
      "order": [[ 4, "desc" ]]
    });

    $('.eliminarNota').click(function(e){
      return confirm('¿Estás seguro?');
    });

  });
</script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
