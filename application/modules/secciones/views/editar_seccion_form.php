<?php 
$titulo = "Editar sección | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <div class="box-typical box-typical-padding">
          <h5 class="m-t-lg with-border">Editar sección</h5>

          <form method="post" action="<?php echo base_url();?>secciones/procesar">
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Nombre *</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" name="seccion_nombre" placeholder="Sección" value="<?php echo $seccion->seccion_nombre;?>" required></p>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Sitio</label>
              <div class="col-sm-10">
                <select name="sitio_id" class="form-control" required>
                  <?php foreach($sitios as $sitio): ?>
                    <option value="<?php echo $sitio->sitio_id;?>" <?php echo $sitio->sitio_id == $seccion->sitio_id ? 'selected="selected"' : '';?>><?php echo $sitio->sitio_nombre;?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Tipo de nota *</label>
              <div class="col-sm-6">
                <select name="tiponota_id[]" class="select2" data-placeholder="Elegir uno o más tipos de notas" multiple="multiple">
                <?php foreach($tipo_notas as $tn):?>
                  <?php echo "<option ".(in_array($tn->tipo_nota_id, $seccion->tipo_nota)?"selected":"")." value='$tn->tipo_nota_id'>$tn->tipo_nota_nombre</option>";?>
                <?php endforeach;?>
              </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Nivel de acceso *</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="number" class="form-control" name="nivel_acceso" placeholder="Numérico" value="<?php echo $seccion->nivel_acceso;?>" required></p>
              </div>
            </div>

            <input type="hidden" name="seccion_id" value="<?php echo $seccion->seccion_id;?>">
            <hr>
            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-rounded btn-inline btn-primary">Editar</button>
                <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
              </div>
            </div>  
                   
          </form>
        </div>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/plugins.js"></script>

  <script src="<?php echo base_url();?>assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>

<script>
  $('.select2').select2();
  $('#cancelar').click(function(e){
    e.preventDefault();
    window.location.href="<?php echo base_url();?>secciones/administrar-secciones";
  });
</script>
</body>
</html>
