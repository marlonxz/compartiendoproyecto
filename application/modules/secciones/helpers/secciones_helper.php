<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function get_seccion_nombre($seccion_id, $sitio_id)
{
	$CI = get_instance();
	if($CI->session->userdata('secciones_by_sitio'))
	{
		$secciones = json_decode($CI->session->userdata('secciones_by_sitio'));
		foreach($secciones->$sitio_id as $seccion)
		{
			if ($seccion->seccion_id == $seccion_id)
			{
				return $seccion->seccion_nombre;
			}
		}
	}

	return FALSE;
}