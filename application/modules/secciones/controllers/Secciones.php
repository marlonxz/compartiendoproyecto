<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Secciones extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Secciones_modelo', 'secciones/Secciones_getters_modelo', 'roles/Roles_modelo'));
        $this->load->helper(array('secciones/secciones', 'sitios/sitios'));
	}

	public function index($sitio_id, $seccion_id) // subhome de secciones
	{
		// Por ahora se cargan solo las notas en el subhome
		$data = array();
        $this->session->set_userdata('sitio_id_session', $sitio_id);
        $roles = $this->session->userdata('roles');
        $usuario_id = $this->session->userdata('usuario_id');

        $validacion = $this->_validar_sitio();

        // si tiene acceso al sitio
        if($validacion)
        {
            // verificar que se haya creado la sesion
            if($this->session->userdata('nivel_acceso'))
            {
                $nivel_acceso = json_decode($this->session->userdata('nivel_acceso'));
                $rol = $roles[$sitio_id];
                $nivel_acceso = $nivel_acceso->$rol;
            }
            else
            {
                $nivel_acceso = $this->Roles_modelo->get_nivel_acceso_by_rol($roles[$sitio_id]);
            }
           
            // data a cargar en la vista   
            $this->load->module('notas/notas_getters');
            $data['notas'] = $this->notas_getters->list_notas_by_nivel($seccion_id, $sitio_id, $nivel_acceso);
            $data['seccion_id'] = $seccion_id;
            $data['seccion_nombre'] = get_seccion_nombre($seccion_id, $sitio_id);
            $data['sitio_id'] = $sitio_id;
            $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
            
            // clonacion si es tipo nota noticia (para el botón)
            $this->load->model('Secciones_getters_modelo');
            $data['clonacion'] = $this->Secciones_getters_modelo->check_tipo_nota_noticia($seccion_id);

            $this->load->view('secciones_subhome', $data);
        }
	}

	function administrar_secciones()
	{
		$data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $data['secciones'] = $this->Secciones_getters_modelo->get_all();
	    }

		$this->load->view('lista_secciones', $data);
	}

	function crear()
	{
		$data = array();
		$roles = $this->session->userdata('roles');

        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
        	$this->load->module('sitios/Sitios_getters', 'sitios');
        	$this->load->module('tipo_nota');
        	$data['tipo_notas'] = $this->tipo_nota->list_tiponotas();
        	$data['sitios'] = $this->sitios_getters->list_sitios();
        }

        $this->load->view('crear_seccion_form', $data);
	}

	function editar($seccion_id)
	{
		$data = array();
		$roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $this->load->module('sitios/Sitios_getters');
        	$this->load->module('tipo_nota');
        	$data['tipo_notas'] = $this->tipo_nota->list_tiponotas();
        	$data['sitios'] = $this->sitios_getters->list_sitios();
        	 
            $seccion = $this->Secciones_modelo->get_seccion_by_id($seccion_id);
            $data['seccion'] = $seccion;
        }

        $this->load->view('editar_seccion_form', $data);
	}

	function eliminar($seccion_id)
	{
		$result = $this->Secciones_modelo->delete_seccion($seccion_id);
        if($result)
        {
            $this->session->set_flashdata('success', '¡Sección eliminada!');
            redirect('secciones/administrar-secciones');
        }
        else
        {
            $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
            redirect('secciones/administrar-secciones');
        }
	}

	function procesar()
	{
		// Verificar que el usuario es admin o moderador
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Secciones_modelo->procesar_seccion();

            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Sección grabada!');
                redirect('secciones/administrar-secciones');
            }
            else
            {
            	$this->session->set_flashdata('error', 'Hubo un problema. Por favor inténtalo de nuevo');
            	redirect('secciones/administrar-secciones');
            }
        }
	}

	function _validar_sitio()
    {
        $sitio_id = $this->session->userdata('sitio_id_session');
        $sitios = $this->session->userdata('sitios');

        if(!in_array($sitio_id, $sitios))
        {
            $this->session->set_flashdata('error', 'No tienes permisos de administración.');
            $this->load->view('Notas/sin_permiso');
            return FALSE;
        }
        return TRUE;        
    }

    // accion del botón para republicar
    /*public function rep_button($primary_key, $row)
    {
        return site_url('/notas/republicar/'.$row->sitio_id.'/'.$primary_key);
    }

    // cargar la vista de republicación
    public function republicar($sitio_id, $nota_id)
    {
        $data = array(
            'sitio_id' => $sitio_id,
            'nota_id' => $nota_id,
            'active' => 'adicional'
        );

        // procesar datos
        if($_POST)
        {
            $rep = $this->Notas_modelo->republicar_nota();
            if($rep)
            {
                // se redirecciona para evitar conflictos al dar 'actualizar'
                redirect('notas/republicado');
            }
        }
        $this->load->view('notas_republicar', $data);
    }

    // aviso de republicación exitoso
    public function republicado()
    {
        $data['active'] = 'sent';
        $this->load->view('notas_republicar', $data);
    }*/
}