<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Secciones_getters extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Secciones_getters_modelo'));
	}

	function get_nivel_acceso_seccion($seccion_id)
	{
		return $this->Secciones_getters_modelo->get_nivel_acceso($seccion_id);
	}

	function check_categorias($seccion_id)
	{
		$this->db->select('categoria_id');
		$this->db->from('categorias');
		$this->db->where('seccion_id', $seccion_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

    /* LLAMADAS AJAX */
    function get_secciones()
    {
        if($_POST)
        {
            $tiponota_id = $this->input->post('tiponota_id');
            $sitio_id = $this->session->userdata('sitio_id_session');
            $usuario_id = $this->session->userdata('usuario_id');
            $secciones = $this->Secciones_getters_modelo->get_by_tiponota_and_nivel($tiponota_id, $sitio_id, $usuario_id);
            
            echo json_encode($secciones);
        }
    } 

	/*TODO: VERIFICAR QUE SE USEN*/
	function list_secciones()
    {
        return $this->Secciones_getters_modelo->get_all_secciones();
    }

    function list_secciones_by_sitio($sitio_id)
    {
        return $this->Secciones_getters_modelo->get_by_sitio_id($sitio_id);
    }

    function list_secciones_noticias_by_sitio($sitio_id)
    {
       return $this->Secciones_getters_modelo->get_noticias_by_sitio_id($sitio_id);
    }

    function list_seccion_blog_id($sitio_id)
    {
        $seccion = $this->Secciones_getters_modelo->get_seccion_blog($sitio_id);
        return $seccion->seccion_id;
    }

    function list_seccion_noticias_id($sitio_id)
    {
        $seccion = $this->Secciones_getters_modelo->get_seccion_noticias($sitio_id);
        return $seccion->seccion_id;
    }

}