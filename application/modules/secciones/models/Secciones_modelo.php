<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Secciones_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
	}

	// devuelve la seccion con el tipo de nota id como array, y el nombre como array
	function get_seccion_by_id($seccion_id)
	{
		$query = $this->db->query('select s.seccion_id, s.seccion_nombre, s.seccion_estado, s.seccion_fech_creacion, s.seccion_fech_update, s.nivel_acceso, s.sitio_id, sitios.sitio_nombre, tn.tipo_nota_nombre, tn.tipo_nota_id, 
			GROUP_CONCAT(tn.tipo_nota_id) as tipo_nota,
			GROUP_CONCAT(tn.tipo_nota_nombre) as tipo_nota_name
			FROM secciones as s, sitios, tipo_nota as tn, seccion_tipo_nota 
			WHERE seccion_estado = "1" 
			AND s.sitio_id = sitios.sitio_id 
			AND s.seccion_id = seccion_tipo_nota.seccion_id 
			AND seccion_tipo_nota.tipo_nota_id = tn.tipo_nota_id
			AND s.seccion_id = '.$seccion_id.'
			GROUP BY s.seccion_id');
		$result =  $query->row();
		$result->tipo_nota = explode(',', $result->tipo_nota);
		return $result;
	}


	function procesar_seccion()
	{
		$this->form_validation->set_rules('seccion_nombre', 'Sección', 'required|trim');
		$this->form_validation->set_rules('tiponota_id[]', 'Tipo de Nota', 'required');
		$this->form_validation->set_rules('sitio_id', 'Sitio', 'required');
		$this->form_validation->set_rules('nivel_acceso', 'Nivel de acceso', 'trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$seccion_id = $this->input->post('seccion_id', TRUE);
			$seccion_nombre = $this->input->post('seccion_nombre', TRUE);
			$tiponota_id = $this->input->post('tiponota_id', TRUE);
			$sitio_id = $this->input->post('sitio_id', TRUE);
			$nivel_acceso = $this->input->post('nivel_acceso', TRUE);

			$seccion_array = array(
				'seccion_nombre' => $seccion_nombre,
				'seccion_estado' => '1',
				'sitio_id' => $sitio_id,
				'nivel_acceso' => $nivel_acceso,
				'seccion_usuario_mod' => $usuario_mod
			);

			if(empty($seccion_id))
			{
				// insertar a la tabla secciones
				$seccion_array['seccion_fech_creacion'] = date('Y-m-d G:i:s');
				$this->db->insert('secciones', $seccion_array);
				$new_seccion_id = $this->db->insert_id();
				// insertar en la tabla seccion_nota
				foreach($tiponota_id as $tn_id)
				{
					$tiponota_array[] = array(
						'seccion_id' => $new_seccion_id,
						'tipo_nota_id' => $tn_id,
						'sitio_id' => $sitio_id
					);
				}

				$this->db->insert_batch('seccion_tipo_nota', $tiponota_array);

			}
			else
			{
				// editar datos básicos
				$seccion_array['seccion_fech_update'] = date('Y-m-d G:i:s');
				$this->db->where('seccion_id', $seccion_id);
				$this->db->update('secciones', $seccion_array);

				// eliminar relaciones en seccion_tipo_nota
				$this->db->where('seccion_id', $seccion_id);
				$this->db->delete('seccion_tipo_nota');

				// agregar nueva relación
				foreach($tiponota_id as $tn_id)
				{
					$tiponota_array[] = array(
						'seccion_id' => $seccion_id,
						'tipo_nota_id' => $tn_id,
						'sitio_id' => $sitio_id
					);
				}
				$this->db->insert_batch('seccion_tipo_nota', $tiponota_array);
			}
			
			return $this->db->affected_rows();
		}
	}

	function delete_seccion($seccion_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'seccion_estado' => '0',
			'seccion_fech_update' => date('Y-m-d G:i:s'),
			'seccion_usuario_mod' => $usuario_mod
		);

		$this->db->where('seccion_id', $seccion_id);
		$this->db->update('secciones', $array);

		return $this->db->affected_rows();
	}

	

}