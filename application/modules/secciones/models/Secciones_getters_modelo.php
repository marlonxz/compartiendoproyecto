<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Secciones_getters_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__table = 'secciones';
		$this->load->helper('validador/validador');
	}

	function get_nivel_acceso($seccion_id)
	{
		// verificar que exista la sesion y que la sección esté agregada
		if($this->session->userdata('nivel_acceso_seccion') && property_exists(json_decode($this->session->userdata('nivel_acceso_seccion')), $seccion_id))
		{
			$seccion = json_decode($this->session->userdata('nivel_acceso_seccion'));
			return $seccion->$seccion_id;
		}
		else
		{
			$this->db->select('nivel_acceso');
			$this->db->from($this->__table);
			$this->db->where('seccion_id', $seccion_id);
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				$query = $query->row();

				$secciones = $this->session->userdata('nivel_acceso_seccion') ? json_decode($this->session->userdata('nivel_acceso_seccion')) : new stdClass();
				$secciones->$seccion_id = $query->nivel_acceso;	
				$this->session->set_userdata('nivel_acceso_seccion', json_encode($secciones));
				
				return $query->nivel_acceso;
			}
		}	
	}

	function get_by_sitio_id($sitio_id, $nivel_acceso = null)
	{
		$this->load->helper('validador/validador');
		
		if(empty($nivel_acceso))
		{
			// verificar que se haya creado la sesion
            if($this->session->userdata('nivel_acceso'))
            {
            	$roles = $this->session->userdata('roles');
                $nivel_acceso = json_decode($this->session->userdata('nivel_acceso'));
                $rol = $roles[$sitio_id];
                $nivel_acceso = $nivel_acceso->$rol;
            }
            else
            {
            	$this->load->model('Roles_modelo');
                $nivel_acceso = $this->Roles_modelo->get_nivel_acceso_by_rol($roles[$sitio_id]);
            }
		}

		$nivel = get_nivel_acceso($nivel_acceso);

		$this->db->from('secciones');
		$this->db->where('seccion_estado', '1');
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('nivel_acceso >', $nivel);
		$this->db->order_by('seccion_nombre', 'asc');

		$secciones = $this->db->get()->result();

		$secciones_by_sitio = $this->session->userdata('secciones_by_sitio') ? json_decode($this->session->userdata('secciones_by_sitio')) : new stdClass();

		$secciones_by_sitio->$sitio_id = $secciones;
		
		$this->session->set_userdata('secciones_by_sitio', json_encode($secciones_by_sitio));

		return $secciones;
	}

	function get_noticias_by_sitio_id($sitio_id)
	{
		$this->db->from('secciones');
		$this->db->join('seccion_tipo_nota stn', 'stn.seccion_id = secciones.seccion_id AND stn.tipo_nota_id = 1');
		$this->db->where('secciones.seccion_estado', '1');
		$this->db->where('secciones.sitio_id', $sitio_id);

		return $this->db->get()->result();
	}

	function get_name_by_id($seccion_id)
	{   
		$this->db->select('seccion_nombre');
		$this->db->from('secciones');
		$this->db->where('seccion_estado', '1');
		$this->db->where('seccion_id', $seccion_id);

		return $this->db->get()->result();
	}

	function get_by_tiponota_and_nivel($tiponota_id, $sitio_id, $usuario_id)
	{
		// primero conseguimos el nivel de acceso
		$roles = $this->session->userdata('roles');
		$nivel_acceso = json_decode($this->session->userdata('nivel_acceso'));
        $rol = $roles[$sitio_id];
        $nivel_acceso = $nivel_acceso->$rol;
        $nivel_permitido = get_nivel_acceso($nivel_acceso);

        // luego seleccionamos las secciones permitidas del sitio en base al tipo de nota
		$this->db->select('sec.seccion_nombre, sec.seccion_id, sec.seccion_estado, sec.nivel_acceso');
		$this->db->from('secciones sec');
		$this->db->join('seccion_tipo_nota stn', "stn.tipo_nota_id = $tiponota_id AND stn.seccion_id = sec.seccion_id AND stn.sitio_id = $sitio_id");
		$this->db->where('nivel_acceso >', $nivel_permitido);
		$this->db->where('seccion_estado', '1');
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}

		return FALSE;
	}

	// devuelve las secciones como un array con el sitio como llave
	function get_all_secciones()
	{
		$secciones_array = array();
		$secciones = $this->get_all();
		foreach($secciones as $seccion)
		{
			$secciones_array[$seccion->sitio_id][$seccion->seccion_id] = $seccion->seccion_nombre;
		}
		return $secciones_array;
	}

	function get_all()
	{
		$query = $this->db->query("select s.seccion_id, s.seccion_nombre, s.seccion_estado, s.seccion_fech_creacion, s.seccion_fech_update, s.nivel_acceso, s.sitio_id, sitios.sitio_nombre, tn.tipo_nota_nombre as tipo_nota_nombre
			FROM secciones as s
			LEFT JOIN seccion_tipo_nota ON s.seccion_id = seccion_tipo_nota.seccion_id 
			LEFT JOIN tipo_nota tn ON tn.tipo_nota_id = seccion_tipo_nota.tipo_nota_id
			INNER JOIN sitios ON sitios.sitio_id = s.sitio_id
			WHERE seccion_estado = '1' 
			GROUP BY s.seccion_id");
		return $query->result();
	}

/*	function get_all()
	{
		$query = $this->db->query("select s.seccion_id, s.seccion_nombre, s.seccion_estado, s.seccion_fech_creacion, s.seccion_fech_update, s.nivel_acceso, s.sitio_id, sitios.sitio_nombre, tn.tipo_nota_nombre, 
			GROUP_CONCAT(tn.tipo_nota_nombre) as tipo_nota
			FROM secciones as s, sitios, tipo_nota as tn, seccion_tipo_nota 
			WHERE seccion_estado = '1' 
			AND s.sitio_id = sitios.sitio_id 
			AND s.seccion_id = seccion_tipo_nota.seccion_id 
			AND seccion_tipo_nota.tipo_nota_id = tn.tipo_nota_id
			GROUP BY s.seccion_id");
		return $query->result();
	}*/

	function check_tipo_nota_noticia($seccion_id)
	{
		$this->db->select('registro_nota_id');
		$this->db->from('seccion_tipo_nota');
		$this->db->where('seccion_id', $seccion_id);
		$this->db->where('tipo_nota_id', '1'); // tipo noticia
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}



	

	function get_seccion_noticias_id()
	{
		$secciones_id = array();

		$this->db->select('s.seccion_id');
		$this->db->from('secciones s');
		$this->db->join('seccion_tipo_nota stn', 'stn.seccion_id = s.seccion_id and stn.tipo_nota_id = 2');
		$this->db->where('s.seccion_estado', '1');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach($query->result() as $seccion)
			{
				$secciones_id[] = $seccion->seccion_id;
			}
			return $secciones_id;
		}
		return FALSE;
	}

	function get_seccion_noticias($sitio_id)
	{
		switch ($sitio_id) {
			case '1':
			case '2':
			case '3':
			case '5':
			case '6':
				$seccion_nombre = 'noticias';
				break;
			case '4': // nueva q
				$seccion_nombre = 'enterate';
				break;
			default:
				$seccion_nombre = 'noticias';
		}

		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('seccion_nombre', $seccion_nombre);
		$query = $this->db->get('secciones');

		if($query->num_rows() > 0)
		{
			return $query->row();
		}

		return FALSE;
	}

	function get_url_by_id($seccion_id)
	{   
		$this->db->select('seccion_nombre');
		$this->db->from('secciones');
		$this->db->where('seccion_estado', '1');
		$this->db->where('seccion_id', $seccion_id);

		$seccion =  $this->db->get()->result();
	}

}