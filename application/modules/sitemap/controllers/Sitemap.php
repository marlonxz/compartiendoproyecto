<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends MX_Controller {

	public function __construct()
	{
		parent::__construct();	
		$this->load->model('Sitemap_modelo');
	}

	public function index()
	{

		if ($_GET){
			$pid = $this->input->get('pid', true);
			$data = array(
					'registros' => $this->Sitemap_modelo->getSitemap($pid),
					'pid' => $pid
			);
		}

		$this->load->view('sitemap',$data);
	}

	public function update()
	{
		if ($_GET){
			$pid = $this->input->get('pid', true);
			$data = array(
					'registros' => $this->Sitemap_modelo->updateSitemap($pid),
					'pid' => $pid
			);
		}

		$this->load->view('sitemap',$data);
	}

	public function news(){
		if ($_GET){
			$pid = $this->input->get('pid', true);
			$data = array(
					'registros' => $this->Sitemap_modelo->getSitemapNews($pid),
					'pid' => $pid
			);
		}

		$this->load->view('sitemap_news',$data);
	}

}