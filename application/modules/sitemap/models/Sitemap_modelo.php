<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap_modelo extends CI_Model
{	

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	function getSitemap($pid) 
	{

		if (isset($pid)) {
			$myquery = "select nota_id, nota_fech_update, nota_seolink, secciones.seccion_id, secciones.seccion_nombre
						FROM  notas 
						INNER JOIN secciones ON secciones.seccion_id = notas.seccion_id
						WHERE notas.sitio_id = ".$pid."
						AND nota_seolink <>  ''
						AND nota_estado = '1'
						ORDER BY  secciones.seccion_nombre ASC";
			$query = $this->db->query($myquery);	
			$principal = $query->result();

			foreach ($principal as $nota) {
				$timestamp = strtotime($nota->nota_fech_update);
				$nota->fecha = date(DATE_ATOM, $timestamp);
				$nota->nota_seolink = convert_accented_characters($nota->nota_seolink);
			}
			return $principal;
		}

	}

	function updateSitemap($pid)
	{

		if (isset($pid)) 
		{
			$fecha = date('Y-m');
			$myquery = "select nota_id, nota_fech_update, nota_seolink, secciones.seccion_id, secciones.seccion_nombre
						FROM  notas
						INNER JOIN secciones ON secciones.seccion_id = notas.seccion_id
						WHERE notas.sitio_id = ".$pid."
						AND nota_seolink <>  ''
						AND nota_estado = '1'
						AND DATE_FORMAT( nota_fech_update,  '%Y-%m' ) =  '".$fecha."'
						ORDER BY  `secciones`.`seccion_nombre` ASC ";

			$query = $this->db->query($myquery);	
			$principal = $query->result();

			foreach ($principal as $nota) 
			{
				$timestamp = strtotime($nota->nota_fech_update);
				$nota->fecha = date(DATE_ATOM, $timestamp);
				$nota->nota_seolink = convert_accented_characters($nota->nota_seolink);
			}
			return $principal;
		}

	}

	function getSitemapNews($pid){
		if (isset($pid)) 
		{
			//$this->_select_database($pid);
			$myquery = "select notas.nota_id, notas.nota_titulo, notas.nota_fecha_publicacion, notas.nota_seolink, tags.tag_nombre
						FROM  notas, tags, tag_notas 
						WHERE notas.sitio_id = ".$pid."
						AND nota_seolink <>  ''
						AND tags.sitio_id = ".$pid."
						AND (notas.seccion_id IN (1,2,5,6,7))
						AND tag_notas.nota_id = notas.nota_id
						AND tag_notas.tag_id = tags.tag_id
						and notas.nota_fecha_publicacion >= (CURDATE() - INTERVAL 2 DAY)
						ORDER BY  nota_fecha_publicacion";
			$query = $this->db->query($myquery);	
			$principal = $query->result();

			foreach ($principal as $nota) 
			{
				$timestamp = strtotime($nota->nota_fecha_publicacion);
				$nota->fecha = date(DATE_ATOM, $timestamp);
				$nota->nota_seolink = convert_accented_characters($nota->nota_seolink);
			}
			return $principal;
		}

	}


}