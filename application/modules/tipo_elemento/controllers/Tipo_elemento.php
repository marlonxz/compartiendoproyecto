<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_elemento extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Tipo_elemento_modelo');
	}

	public function index()
	{
        $data = array();
        $roles = $this->session->userdata('roles');

        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes permisos de administración.');
        }
        else
        {   		
    		$data['tipo_elementos'] = $this->Tipo_elemento_modelo->get_tipos_elementos();
        }
 
		$this->load->view('lista_tipo_elemento.php', $data);  
	}

    function crear()
    {
        $this->load->view('crear_tipo_elemento_form'); 
    }

    function editar($tipo_elemento_id)
    {
        $data = array();
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $tipo_elemento = $this->Tipo_elemento_modelo->get_tipo_by_id($tipo_elemento_id);

            $data['tipo_elemento'] = $tipo_elemento;
        }

        $this->load->view('editar_tipo_elemento_form', $data);
    }

    function eliminar($tipo_elemento_id)
    {
        $result = $this->Tipo_elemento_modelo->delete_tipo_elemento($tipo_elemento_id);
        if($result)
        {
            $this->session->set_flashdata('success', '¡Tipo de elemento eliminado!');
            redirect('tipo_elemento');
        }
        else
        {
            $this->session->set_flashdata('success', 'Hubo un error por favor inténtalo de nuevo.');
        }
    }

    function procesar()
    {
        // Verificar que el usuario es admin
        $roles = $this->session->userdata('roles');
        if(!in_array('1', $roles))
        {
            $this->session->set_flashdata('error', 'No tienes acceso a esta área.');
        }
        else
        {
            $resultado = $this->Tipo_elemento_modelo->procesar_tipo_elemento();
            if($resultado)
            {
                $this->session->set_flashdata('success', '¡Tipo de elemento grabado!');
                redirect('tipo_elemento');
            }
        }
    }
}