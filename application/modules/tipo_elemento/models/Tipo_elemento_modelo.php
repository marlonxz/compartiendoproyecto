<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_elemento_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'tipo_elemento';
	}

	function get_tipo_by_id($tipo_elemento_id)
	{
		$this->db->from($this->__tabla)
					->where('tipo_elemento_id', $tipo_elemento_id)
					->where('tipo_elemento_estado', '1');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		return FALSE;
	}

	function get_tipos_elementos()
	{
		$this->db->from($this->__tabla);
		$this->db->where('tipo_elemento_estado', '1');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		return FALSE;
	}

	function procesar_tipo_elemento()
	{
		$this->form_validation->set_rules('tipo_elemento_nombre', 'Nombre', 'required|trim');
		$this->form_validation->set_rules('tipo_elemento_ruta', 'Ruta', 'required|trim');
		$this->form_validation->set_rules('tipo_elemento_estado', 'Estado', 'required|trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$tipo_elemento_id = $this->input->post('tipo_elemento_id', TRUE);
			$tipo_elemento_nombre = $this->input->post('tipo_elemento_nombre', TRUE);
			$tipo_elemento_ruta = $this->input->post('tipo_elemento_ruta', TRUE);
			$tipo_elemento_estado = $this->input->post('tipo_elemento_estado', TRUE);

			$tipo_elemento_array = array(
				'tipo_elemento_nombre' => $tipo_elemento_nombre,
				'tipo_elemento_ruta' => $tipo_elemento_ruta,
				'tipo_elemento_estado' => $tipo_elemento_estado,
				'tipo_elemento_usuario_mod' => $usuario_mod
			);

			if(empty($tipo_elemento_id))
			{
				// insertar a la tabla tipo elemento
				$tipo_elemento_array['tipo_elemento_fech_creacion'] = date('Y-m-d G:i:s');
				$this->db->insert('tipo_elemento', $tipo_elemento_array);
			}
			else
			{
				// editar datos básicos
				$tipo_elemento_array['tipo_elemento_fech_update'] = date('Y-m-d G:i:s');
				$this->db->where('tipo_elemento_id', $tipo_elemento_id);
				$this->db->update('tipo_elemento', $tipo_elemento_array);
			}
			
			return $this->db->affected_rows();
		}
	}

	function delete_tipo_elemento($tipo_elemento_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'tipo_elemento_estado' => '0',
			'tipo_elemento_fech_update' => date('Y-m-d G:i:s'),
			'tipo_elemento_usuario_mod' => $usuario_mod
		);

		$this->db->where('tipo_elemento_id', $tipo_elemento_id);
		$this->db->update('tipo_elemento', $array);

		return $this->db->affected_rows();
	}

	function get_ruta($tipo_id)
	{
		$this->db->select('tipo_elemento_ruta');
		$this->db->from($this->__tabla);
		$this->db->where('tipo_elemento_estado', '1');
		$this->db->where('tipo_elemento_id', $tipo_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->row();
		}		
	}

}