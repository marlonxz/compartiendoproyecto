<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require FCPATH.'/assets/Netcarver/Textile/Parser.php';
require FCPATH.'/assets/Netcarver/Textile/DataBag.php';
require FCPATH.'/assets/Netcarver/Textile/Tag.php';

class Json_model extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('string');
	}

	function GetDatosNotasNuevas($pid, $sid, $limit) {
		$fecha = date('Y-m-d G:i:s');
		if (isset($pid) && isset($sid) ) {
			$myquery ="select nota_id as nid, origen_id as clon, n.seccion_id as sid, s.seccion_nombre as seccion, n.categoria_id as cid, 
							  c.categoria_nombre as categoria, nota_fecha_publicacion as timestamp, nota_estado as estado,nota_titulo as titular,
							  nota_bajada as bajada,
							  nota_resumen as resumen, (select usuarios.usuario_user from usuarios where usuario_id= n.usuario_id) as usuario ,
							  trim(nota_seolink) as linkseo, nota_contenido as desarrollo, nota_crudo as crudo, nota_flag_portada as portada,
							  		(select tipo_nota_nombre 
									from tipo_nota where tipo_nota_id= n.tipo_nota_id) as tiponoticia
 					from notas as n
 					inner join secciones as s on s.seccion_id = n.seccion_id
 					left outer join categorias as c on c.categoria_id = n.categoria_id
 					where n.nota_titulo <> '' and n.sitio_id=".$pid." 
 											  and n.seccion_id=".$sid." 
 											  and nota_estado='1' 
 											  and nota_fecha_publicacion <= '".$fecha."'
 					order by nota_fecha_publicacion DESC limit ".$limit ;
			$query = $this->db->query($myquery);

			$principal = $query->result();

			$parser = new Netcarver\Textile\Parser();
			foreach ($principal as $nota) {

				$nota->clon = !empty($nota->clon) ? TRUE : FALSE;

				$desarrollo = str_replace('&quot;', '', $nota->desarrollo);
				$nota->desarrollo = $parser->textileThis($desarrollo);
				$elementotamanios =array();
				$elementosarray =array();
				$tagsarray=array();
				$videoarray=array();
				$audioarray = array();
				$gifarray = array();
				$cancionarray = array();
				$elementoimagen = array();			
				

				$querytag="select distinct(t.tag_ruta), tag_nombre from tags as t, tag_notas tn where tn.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and tn.nota_id=".$nota->nid;
				$queryelemento ="select e.elemento_id,te.tipo_elemento_id, e.elemento_titulo, e.elemento_ruta, te.tipo_elemento_nombre, te.tipo_elemento_ruta,e.elemento_desc 
				from notas_elemento as  ne , elementos as e, tipo_elemento as te 
				where ne.nota_id=".$nota->nid." 
				and ne.notas_elemento_estado='1' 
				and e.elemento_estado='1' 
				and te.tipo_elemento_estado='1' 
				and  e.elemento_id=ne.elemento_id 
				and e.sitio_id =".$pid." 
				and te.tipo_elemento_id=e.tipoelemento_id";
				
				$query = $this->db->query($querytag);
				$tagsnota = $query->result();
			
				// $tagsarray=array();
				// foreach($tagsnota as $tag)
				// {
				// 	if(!empty($tag->tag_ruta))
				// 	$tagsarray[] = $tag->tag_ruta;
				// }
				// $nota->tags = $tagsarray;

				$tagsarray=array();
				$tagsarraynombre=array();
				foreach($tagsnota as $tag)
				{
					if(!empty($tag->tag_ruta)){
						$tagsarray[] = $tag->tag_ruta;
						$tagsarraynombre[] = $tag->tag_nombre;
					}
				}
				$nota->tags = $tagsarray;
				$nota->tagsnombre = $tagsarraynombre;

				// Tags patrocinado
				$querytagpatrocinado ="select distinct(t.tag_ruta) from tags as t, tag_notas tn where tn.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and t.tipotag_id = '4' and tn.nota_id=".$nota->nid;
				
				$querytag = $this->db->query($querytagpatrocinado);
				$tag_patrocinado = $querytag->row();
				if(!empty($tag_patrocinado->tag_ruta))
				{
					$nota->tagpatrocinado = $tag_patrocinado->tag_ruta;
				}


		        //  para agregar imagenes jpg y galerias al JSON
				//var_dump($queryelemento);
				$query = $this->db->query($queryelemento);
				$elementosnota = $query->result();

				foreach($elementosnota as $elemento)
				 { 
				 	// para asignar imagen al json
					if ($elemento->tipo_elemento_id==2)
					{				 		
						unset($elementoimagen);
						$elementoimagen = array();
						$querytamaniofoto = "select tef.tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto,tef.tamanio_elemento_ancho,s.sitio_ruta_cdn,e.elemento_ruta,te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id 
						and e.elemento_id=".$elemento->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id";
							$query = $this->db->query($querytamaniofoto);
							$tamaniofoto = $query->result();

						if(!empty($tamaniofoto)){

							$elementotamanios = array();

							foreach($tamaniofoto as $tamanio)
							{

								$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
										//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho,
										'descripcion' => $elemento->elemento_desc
										);
							}

							$elementoimagen[] = $elementotamanios;
						}

					}

					// para asignar galerias de fotos al json
					if ($elemento->tipo_elemento_id==5)
					{
						$querytamaniofoto = "select tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto, tef.tamanio_elemento_ancho, s.sitio_ruta_cdn, e.elemento_ruta, te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id 
						and e.elemento_id=".$elemento->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id";
						$query = $this->db->query($querytamaniofoto);
						$tamaniofoto = $query->result();
						
						if(!empty($tamaniofoto)){
							$elementotamanios = array();
							foreach($tamaniofoto as $tamanio)
							{
								$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
										//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho,
										'descripcion' => $elemento->elemento_desc
										);
							}
							$elementosarray[] = $elementotamanios;
						}

					}

					// para asignar los videos al json
					if($elemento->tipo_elemento_id == 4 || $elemento->tipo_elemento_id == 6)
					{
							
						$videoarray[] =array(
	                      'videovalue'     =>$elemento->elemento_ruta,
					  	  'videoprovider'  =>$elemento->elemento_desc,
					  	  'leyenda'        =>$elemento->elemento_titulo						  		
					  	) ;
					}
				  
					//para asignar los audios al Json
					if($elemento->tipo_elemento_id == 1)
					{
						// get ruta elemento
						$ruta = $this->db->select('tipo_elemento_ruta')->where('tipo_elemento_id', 1)->get('tipo_elemento')->row();
						// get bucket
						$bucket = $this->db->select('sitio_ruta_cdn')->where('sitio_id', $pid)->get('sitios')->row();
						
						$audioarray[] = array(
					  	   'audiofilepath' => 'http://'.$bucket->sitio_ruta_cdn.$ruta->tipo_elemento_ruta.$elemento->elemento_ruta,
					  		'audiotitle' => $elemento->elemento_titulo	
					  );
					}
					  
					// asignar gif al Json
					if($elemento->tipo_elemento_id == 3)
					{
						// get ruta elemento
						$ruta = $this->db->select('tipo_elemento_ruta')->where('tipo_elemento_id', 3)->get('tipo_elemento')->row();
						// get bucket
						$bucket = $this->db->select('sitio_ruta_cdn')->where('sitio_id', $pid)->get('sitios')->row();
						
						$gifarray[] = array(
								'ruta' => 'http://'.$bucket->sitio_ruta_cdn.$ruta->tipo_elemento_ruta.'/'.$elemento->elemento_ruta,
								'titulo' => $elemento->elemento_titulo,
								'descripcion' => $elemento->elemento_desc
						);
					}
				 
				}

				// ids canciones de RELACION_NOTAS
				$query2 = "select nota_interna_id, relacion_orden_item as orden
							from relacion_notas
							where nota_principal_id = ".$nota->nid."
							and relacion_estado = '1'";
				$query2 = $this->db->query($query2);
				$ids_canciones = $query2->result_array();

				// titulo y letra de canciones
				$i = 0;
				unset($cancion_nota);
				$cancion_nota = array();
				foreach($ids_canciones as $cancion)
				{
					$imagen = array();
					$query3 = "select nota_titulo as audiotitle, nota_seolink as linkseo
							   from notas
							   where nota_id =".$cancion['nota_interna_id']."
							   and nota_estado = '1'";
					$query3 = $this->db->query($query3);
					$cancion_nota[$i] = $query3->row_array();

					// audios de canciones
					$query5 = "select e.elemento_ruta as ruta_audio, s.sitio_ruta_cdn, te.tipo_elemento_ruta
							   from elementos e, sitios as s, tipo_elemento as te, notas_elemento ne
							   WHERE s.sitio_id =".$pid."
							   AND ne.elemento_id = e.elemento_id
							   AND te.tipo_elemento_id=e.tipoelemento_id 
							   AND ne.nota_id =".$cancion['nota_interna_id']."
							   AND e.tipoelemento_id = 7";
					$query5 = $this->db->query($query5);
					$audio[$i] = $query5->row_array();

					// tags (artistas)
					$query6 = "select t.tag_nombre as artistatitle, t.tag_ruta as ruta
							  FROM tags t
							  INNER JOIN tag_notas as tn ON tn.tag_id = t.tag_id
							  WHERE tn.nota_id = ".$cancion['nota_interna_id'];
					$query6 = $this->db->query($query6);
					$artistas[$i] = $query6->result_array();

					$cancion_nota[$i]['puesto'] = $cancion['orden'];
					$cancion_nota[$i]['audionid'] = $cancion['nota_interna_id'];
					$cancion_nota[$i]['audiofilepath'] = 'http://'.$audio[$i]['sitio_ruta_cdn'].$audio[$i]['tipo_elemento_ruta'].'/'.$audio[$i]['ruta_audio'];				
					
					$cancion_nota[$i]['artistas'] = $artistas[$i];
					$i++;
				}

				$nota->canciones = $cancion_nota;
				$nota->foto = $elementoimagen;
				$nota->fotos = $elementosarray;
				$nota->videos = $videoarray;
				$nota->audio = $audioarray;
				$nota->gif = $gifarray;
				// $nota->canciones = $cancionarray;	
				 	
			}
			 
			
			return array_reverse($principal);
		}
	}

	function GetDatosNotasTags($pid, $tag, $limit) {
		$fecha = date('Y-m-d G:i:s');
		if (isset($pid) && isset($tag) ) {
			$myquery ="select n.nota_id as nid, n.origen_id as clon, n.nota_fecha_publicacion as timestamp, n.nota_estado as estado, n.nota_titulo as titular, n.nota_resumen as resumen, trim(n.nota_seolink) as linkseo, n.nota_contenido as desarrollo, n.nota_crudo as crudo, s.seccion_nombre as seccion, c.categoria_nombre as categoria
				from notas n
				INNER JOIN tag_notas tn ON tn.nota_id = n.nota_id
				INNER JOIN tags t ON t.tag_id = tn.tag_id AND t.tag_ruta = '".$tag."'
				INNER JOIN secciones s ON s.seccion_id = n.seccion_id
				LEFT OUTER JOIN categorias c ON c.categoria_id = n.categoria_id
				where n.nota_titulo <> '' and n.sitio_id=".$pid." 
										  and nota_estado='1' 
										  and nota_fecha_publicacion <= '".$fecha."'
				group by n.nota_id
				order by nota_fecha_publicacion DESC limit ".$limit ;
			
			$query = $this->db->query($myquery);

			$principal = $query->result();

			$parser = new Netcarver\Textile\Parser();
			foreach ($principal as $nota) {

				$nota->clon = !empty($nota->clon) ? TRUE : FALSE;

				$desarrollo = str_replace('&quot;', '', $nota->desarrollo);
				$nota->desarrollo = $parser->textileThis($desarrollo);
				$elementotamanios =array();
				$elementosarray =array();
				$tagsarray=array();
				$videoarray=array();
				$audioarray = array();
				$gifarray = array();
				$cancionarray = array();
				$elementoimagen = array();			
				

				$querytag="select distinct(t.tag_ruta), t.tag_nombre from tags as t, tag_notas tn where tn.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and tn.nota_id=".$nota->nid;

				$queryelemento ="select e.elemento_id,te.tipo_elemento_id, e.elemento_titulo, e.elemento_ruta, te.tipo_elemento_nombre, te.tipo_elemento_ruta,e.elemento_desc 
				from notas_elemento as  ne , elementos as e, tipo_elemento as te 
				where ne.nota_id=".$nota->nid." 
				and ne.notas_elemento_estado='1' 
				and e.elemento_estado='1' 
				and te.tipo_elemento_estado='1' 
				and  e.elemento_id=ne.elemento_id 
				and e.sitio_id =".$pid." 
				and te.tipo_elemento_id=e.tipoelemento_id";
				
				$query = $this->db->query($querytag);
				$tagsnota = $query->result();
			
				$tagsarray=array();
				$tagsarraynombre=array();
				foreach($tagsnota as $tag)
				{
					if(!empty($tag->tag_ruta)){
						$tagsarray[] = $tag->tag_ruta;
						$tagsarraynombre[] = $tag->tag_nombre;
					}
				}
				$nota->tags = $tagsarray;
				$nota->tagsnombre = $tagsarraynombre;

				// Tags patrocinado
				$querytagpatrocinado ="select distinct(t.tag_ruta) from tags as t, tag_notas tn where tn.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and t.tipotag_id = '4' and tn.nota_id=".$nota->nid;
				
				$querytag = $this->db->query($querytagpatrocinado);
				$tag_patrocinado = $querytag->row();
				if(!empty($tag_patrocinado->tag_ruta))
				{
					$nota->tagpatrocinado = $tag_patrocinado->tag_ruta;
				}


		        //  para agregar imagenes jpg y galerias al JSON
				//var_dump($queryelemento);
				$query = $this->db->query($queryelemento);
				$elementosnota = $query->result();

				foreach($elementosnota as $elemento)
				 { 
				 	// para asignar imagen al json
					if ($elemento->tipo_elemento_id==2)
					{				 		
						unset($elementoimagen);
						$elementoimagen = array();
						$querytamaniofoto = "select tef.tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto,tef.tamanio_elemento_ancho,s.sitio_ruta_cdn,e.elemento_ruta,te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id 
						and e.elemento_id=".$elemento->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id";
							$query = $this->db->query($querytamaniofoto);
							$tamaniofoto = $query->result();

						if(!empty($tamaniofoto)){

							$elementotamanios = array();

							foreach($tamaniofoto as $tamanio)
							{

								$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
										//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho,
										'descripcion' => $elemento->elemento_desc
										);
							}

							$elementoimagen[] = $elementotamanios;
						}

					}

					// para asignar galerias de fotos al json
					if ($elemento->tipo_elemento_id==5)
					{
						$querytamaniofoto = "select tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto, tef.tamanio_elemento_ancho, s.sitio_ruta_cdn, e.elemento_ruta, te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id 
						and e.elemento_id=".$elemento->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id";
						$query = $this->db->query($querytamaniofoto);
						$tamaniofoto = $query->result();
						
						if(!empty($tamaniofoto)){
							$elementotamanios = array();
							foreach($tamaniofoto as $tamanio)
							{
								$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
										//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho,
										'descripcion' => $elemento->elemento_desc
										);
							}
							$elementosarray[] = $elementotamanios;
						}

					}

					// para asignar los videos al json
					if($elemento->tipo_elemento_id == 4 || $elemento->tipo_elemento_id == 6)
					{
							
						$videoarray[] =array(
	                      'videovalue'     =>$elemento->elemento_ruta,
					  	  'videoprovider'  =>$elemento->elemento_desc,
					  	  'leyenda'        =>$elemento->elemento_titulo						  		
					  	) ;
					}
				  
					//para asignar los audios al Json
					if($elemento->tipo_elemento_id == 1)
					{
						// get ruta elemento
						$ruta = $this->db->select('tipo_elemento_ruta')->where('tipo_elemento_id', 1)->get('tipo_elemento')->row();
						// get bucket
						$bucket = $this->db->select('sitio_ruta_cdn')->where('sitio_id', $pid)->get('sitios')->row();
						
						$audioarray[] = array(
					  	   'audiofilepath' => 'http://'.$bucket->sitio_ruta_cdn.$ruta->tipo_elemento_ruta.$elemento->elemento_ruta,
					  		'audiotitle' => $elemento->elemento_titulo	
					  );
					}
					  
					// asignar gif al Json
					if($elemento->tipo_elemento_id == 3)
					{
						// get ruta elemento
						$ruta = $this->db->select('tipo_elemento_ruta')->where('tipo_elemento_id', 3)->get('tipo_elemento')->row();
						// get bucket
						$bucket = $this->db->select('sitio_ruta_cdn')->where('sitio_id', $pid)->get('sitios')->row();
						
						$gifarray[] = array(
								'ruta' => 'http://'.$bucket->sitio_ruta_cdn.$ruta->tipo_elemento_ruta.'/'.$elemento->elemento_ruta,
								'titulo' => $elemento->elemento_titulo,
								'descripcion' => $elemento->elemento_desc
						);
					}
				 
				}

				// ids canciones de RELACION_NOTAS
				$query2 = "select nota_interna_id, relacion_orden_item as orden
							from relacion_notas
							where nota_principal_id = ".$nota->nid."
							and relacion_estado = '1'";
				$query2 = $this->db->query($query2);
				$ids_canciones = $query2->result_array();

				// titulo y letra de canciones
				$i = 0;
				unset($cancion_nota);
				$cancion_nota = array();
				foreach($ids_canciones as $cancion)
				{
					$imagen = array();
					$query3 = "select nota_titulo as audiotitle, nota_seolink as linkseo
							   from notas
							   where nota_id =".$cancion['nota_interna_id']."
							   and nota_estado = '1'";
					$query3 = $this->db->query($query3);
					$cancion_nota[$i] = $query3->row_array();

					// audios de canciones
					$query5 = "select e.elemento_ruta as ruta_audio, s.sitio_ruta_cdn, te.tipo_elemento_ruta
							   from elementos e, sitios as s, tipo_elemento as te, notas_elemento ne
							   WHERE s.sitio_id =".$pid."
							   AND ne.elemento_id = e.elemento_id
							   AND te.tipo_elemento_id=e.tipoelemento_id 
							   AND ne.nota_id =".$cancion['nota_interna_id']."
							   AND e.tipoelemento_id = 7";
					$query5 = $this->db->query($query5);
					$audio[$i] = $query5->row_array();

					// tags (artistas)
					$query6 = "select t.tag_nombre as artistatitle, t.tag_ruta as ruta
							  FROM tags t
							  INNER JOIN tag_notas as tn ON tn.tag_id = t.tag_id
							  WHERE tn.nota_id = ".$cancion['nota_interna_id'];
					$query6 = $this->db->query($query6);
					$artistas[$i] = $query6->result_array();

					$cancion_nota[$i]['puesto'] = $cancion['orden'];
					$cancion_nota[$i]['audionid'] = $cancion['nota_interna_id'];
					$cancion_nota[$i]['audiofilepath'] = 'http://'.$audio[$i]['sitio_ruta_cdn'].$audio[$i]['tipo_elemento_ruta'].'/'.$audio[$i]['ruta_audio'];				
					
					$cancion_nota[$i]['artistas'] = $artistas[$i];
					$i++;
				}

				$nota->canciones = $cancion_nota;
				$nota->foto = $elementoimagen;
				$nota->fotos = $elementosarray;
				$nota->videos = $videoarray;
				$nota->audio = $audioarray;
				$nota->gif = $gifarray;
				// $nota->canciones = $cancionarray;	
				 	
			}
			 
			
			return array_reverse($principal);
		}
	}


	function GetPortadas($pid, $limit) {
		$fecha = date('Y-m-d G:i:s');

		if (!empty($pid)) {
			$myquery ="select nota_id as nid, s.seccion_id as sid, s.seccion_nombre as seccion, n.categoria_id as cid, 
							  c.categoria_nombre as categoria, nota_fecha_publicacion as timestamp, nota_estado as estado,nota_titulo as titular,
							  nota_resumen as resumen, (select usuarios.usuario_user from usuarios where usuario_id= n.usuario_id) as usuario ,
							  trim(nota_seolink) as linkseo, nota_contenido as desarrollo, 
							  		(select tipo_nota_nombre 
									from tipo_nota where tipo_nota_id= n.tipo_nota_id) as tiponoticia
 					from notas as n
 					inner join secciones as s on s.seccion_id = n.seccion_id
 					left outer join categorias as c on c.categoria_id = n.categoria_id
 					where n.nota_titulo <> '' and n.sitio_id=".$pid." 
 											  and nota_estado='1' 
 											and nota_flag_portada = '1' 
 											and nota_fecha_publicacion <= '".$fecha."'
 					order by nota_flag_patrocinada DESC, nota_fecha_publicacion DESC limit ".$limit;
			$query = $this->db->query($myquery);
			$principal = $query->result();
			$elementoimagen = array();

			$parser = new Netcarver\Textile\Parser();	
			
			foreach ($principal as $nota) 
			{
				$desarrollo = str_replace('&quot;', '', $nota->desarrollo);
				$nota->desarrollo = $parser->textileThis($desarrollo);
				
				/*if($nota->sid == 9 ){
					if(strpos($nota->linkseo, 'mundo-mujer/tips/') === FALSE)
					{						
						$nota->linkseo = 'mundo-mujer/'.$nota->linkseo;
					}				
				}*/

				$elementotamanios =array();
						
				
				$queryelemento ="select e.elemento_id,te.tipo_elemento_id, e.elemento_titulo, e.elemento_ruta, te.tipo_elemento_nombre, te.tipo_elemento_ruta,e.elemento_desc 
				from notas_elemento as  ne , elementos as e, tipo_elemento as te 
				where ne.nota_id=".$nota->nid." and ne.notas_elemento_estado='1' and e.elemento_estado='1' and te.tipo_elemento_estado='1' and  e.elemento_id=ne.elemento_id and e.sitio_id =".$pid." and te.tipo_elemento_id=e.tipoelemento_id and (e.tipoelemento_id = 2 OR e.tipoelemento_id = 8)";

				$query = $this->db->query($queryelemento);
				$elementosnota = $query->result();

				foreach($elementosnota as $elemento)
				 { 
				 	if ($elemento->tipo_elemento_id==2)
				 	{				 		
				 		unset($elementoimagen);
	                    $querytamaniofoto = "select tef.tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto,tef.tamanio_elemento_ancho,s.sitio_ruta_cdn,e.elemento_ruta,te.tipo_elemento_ruta 
	                    from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e where s.sitio_id=tef.sitio_id and e.elemento_id=".$elemento->elemento_id." and te.tipo_elemento_id=e.tipoelemento_id and e.elemento_id=tef.elemento_id";
					 	$query = $this->db->query($querytamaniofoto);
					 	$tamaniofoto = $query->result();

					    if(!empty($tamaniofoto)){
					    	$elementotamanios = array();

	     					foreach($tamaniofoto as $tamanio)
							{

								$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
										//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho
										);
							}
							$elementoimagen[] = $elementotamanios;
						}
				
				   	}	
		  
				 }
				$nota->foto = $elementoimagen;			 	
			}
			 
			
			return array_reverse($principal);
		}
	}

	function GetRanking($pid, $cid)
	{
		if (isset($pid) && isset($cid) ) 
		{
			$fecha = date('Y-m-d G:i:s');

			$cancion_nota = array();
			$elementoimagen = array();

			$myquery ="select nota_id as nid, n.categoria_id as cid, c.categoria_nombre as categoria, nota_fecha_publicacion as timestamp,nota_titulo as titular, trim(nota_seolink) as linkseo  
 					from notas as n
 					inner join categorias as c on c.categoria_id = n.categoria_id
 					where n.nota_titulo <> '' and n.sitio_id=".$pid." and n.categoria_id=".$cid." 
 											and nota_estado='1' 
 											and nota_fecha_publicacion <= '".$fecha."'
 					order by nota_fecha_publicacion DESC limit 1";
			$query = $this->db->query($myquery);
			$principal = $query->row();

			$parser = new Netcarver\Textile\Parser();

			$queryelemento ="select e.elemento_id,te.tipo_elemento_id, e.elemento_titulo, e.elemento_ruta, te.tipo_elemento_nombre, te.tipo_elemento_ruta,e.elemento_desc 
				from notas_elemento as  ne , elementos as e, tipo_elemento as te 
				where ne.nota_id=".$principal->nid." 
				and ne.notas_elemento_estado='1' 
				and e.elemento_estado='1' 
				and te.tipo_elemento_estado='1' 
				and  e.elemento_id=ne.elemento_id 
				and e.sitio_id =".$pid." 
				and te.tipo_elemento_id=e.tipoelemento_id";

			$query3 = $this->db->query($queryelemento);
			$elementosnota = $query3->result();

			foreach($elementosnota as $elemento)
			{ 
			 	// para asignar imagen al json
				if ($elemento->tipo_elemento_id==2 || $elemento->tipo_elemento_id==8)
				{				 		
					unset($elementoimagen);
					$elementoimagen = array();	
					
					$querytamaniofoto = "select tef.tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto,tef.tamanio_elemento_ancho,s.sitio_ruta_cdn,e.elemento_ruta,te.tipo_elemento_ruta from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e where s.sitio_id=tef.sitio_id and  te.sitio_id=s.sitio_id and e.elemento_id=".$elemento->elemento_id." and te.tipo_elemento_id=e.tipoelemento_id and e.elemento_id=tef.elemento_id";
						$query = $this->db->query($querytamaniofoto);
						$tamaniofoto = $query->result();

					if(!empty($tamaniofoto)){

						$elementotamanios = array();

						foreach($tamaniofoto as $tamanio)
						{

							$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
									//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
								     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									'alto' => $tamanio->tamanio_elemento_alto,
									'ancho' => $tamanio->tamanio_elemento_ancho,
									'descripcion' => $elemento->elemento_desc
									);
						}

						$elementoimagen[] = $elementotamanios;
					}

				}
			}


			// ids canciones de RELACION_NOTAS
			$query2 = "select nota_interna_id, relacion_orden_item as orden
						from relacion_notas
						where nota_principal_id = ".$principal->nid."
						and relacion_estado = '1'
						and tipo_nota_id = 5";
			$query2 = $this->db->query($query2);
			$ids_canciones = $query2->result_array();

			// titulo y letra de canciones
			$i = 0;
			foreach($ids_canciones as $cancion)
			{
				$imagen = array();
				$query3 = "select nota_titulo as audiotitle, nota_contenido as letra, nota_seolink as linkseo
						   from notas
						   where nota_id =".$cancion['nota_interna_id']."
						   and sitio_id = ".$pid."
						   and nota_estado = '1'";
				$query3 = $this->db->query($query3);
				$cancion_nota[$i] = $query3->row_array();

				$cancion_nota[$i]['letra'] = $parser->textileThis($cancion_nota[$i]['letra']);

				// imagenes de canciones
				$query4 = "select e.elemento_ruta as imagenname, tef.tamanio_elemento_ruta as ruta_imagen_ranking, te.tipo_elemento_ruta
							from elementos e 
							inner join tipo_elemento te on te.tipo_elemento_id = e.tipoelemento_id
							inner join notas_elemento ne on ne.elemento_id = e.elemento_id
							left outer join tamanio_elemento_foto tef on tef.elemento_id = e.elemento_id
							WHERE ne.nota_id =".$cancion['nota_interna_id']."
							AND tef.tamanio_elemento_nombre = 'ranking'
							AND te.tipo_elemento_estado='1' 
							AND ne.notas_elemento_estado =  '1'
							AND e.tipoelemento_id = 2";
				$query4 = $this->db->query($query4);
				$imagen[$i] = $query4->row_array();

				// solo nombre de la imagen para no amarrarlo a un tamaño
				$queryfotoURL = "select e.elemento_ruta
						from elementos as e
						join notas_elemento ne on ne.elemento_id = e.elemento_id
						WHERE ne.nota_id =".$cancion['nota_interna_id']."
						AND e.tipoelemento_id = 2";
						$queryURL = $this->db->query($queryfotoURL)->row();
						$imagen_nombre[$i] = $queryURL->elemento_ruta;
				// audios de canciones
				$query5 = "select e.elemento_ruta as ruta_audio, s.sitio_ruta_cdn, te.tipo_elemento_ruta
						   from elementos e, sitios as s, tipo_elemento as te, notas_elemento ne
						   WHERE s.sitio_id =".$pid."
						   AND ne.elemento_id = e.elemento_id
						   AND e.elemento_estado = '1'
						   AND e.sitio_id=s.sitio_id
						   AND te.tipo_elemento_id=e.tipoelemento_id 
						   AND ne.nota_id =".$cancion['nota_interna_id']."
						   AND e.tipoelemento_id = 7
						   ORDER BY ne.elemento_id DESC limit 1";
				$query5 = $this->db->query($query5);
				$audio[$i] = $query5->row_array();

				// tags (artistas)
				$query6 = "select t.tag_nombre as artistatitle, t.tag_id as artistanid, t.tag_ruta as tag_url
						  FROM tags t
						  INNER JOIN tag_notas as tn ON tn.tag_id = t.tag_id and tn.tag_featured = '0'
						  WHERE tn.nota_id = ".$cancion['nota_interna_id']."
						  AND t.sitio_id = ".$pid."
						  AND t.tipotag_id = 3";
				$query6 = $this->db->query($query6);
				$artistas[$i] = $query6->result_array();

				// tags (featured)
				$query7 = "select t.tag_nombre as artistatitle, t.tag_id as artistanid, t.tag_ruta as tag_url
						  FROM tags t
						  INNER JOIN tag_notas as tn ON tn.tag_id = t.tag_id and tn.tag_featured = '1'
						  WHERE tn.nota_id = ".$cancion['nota_interna_id']."
						  AND t.sitio_id = ".$pid."
						  AND t.tipotag_id = 3";
				$query7 = $this->db->query($query7);
				$artistas_ft[$i] = $query7->result_array();

				$cancion_nota[$i]['puesto'] = $cancion['orden'];
				$cancion_nota[$i]['audionid'] = $cancion['nota_interna_id'];
				$cancion_nota[$i]['audiofilepath'] = 'http://'.$audio[$i]['sitio_ruta_cdn'].$audio[$i]['tipo_elemento_ruta'].'/'.$audio[$i]['ruta_audio'];
				$cancion_nota[$i]['imagen_url'] = $imagen_nombre[$i];

				if(!empty($imagen[$i]['ruta_imagen_ranking']))
				{
					$cancion_nota[$i]['imagenfilename'] = 'http://'.$audio[$i]['sitio_ruta_cdn'].$imagen[$i]['tipo_elemento_ruta'].$imagen[$i]['ruta_imagen_ranking'];
				}
				else
				{
					$cancion_nota[$i]['imagenfilename'] = NULL;
				}
				
				
				$cancion_nota[$i]['artistas'] = $artistas[$i];
				$cancion_nota[$i]['featured'] = $artistas_ft[$i];
				$i++;
			}

			$principal->audios = $cancion_nota;
			$principal->foto = $elementoimagen;

			return $principal;
		}
	}

	function GetRankingLista($pid, $cid)
	{
		if (isset($pid) && isset($cid) ) 
		{
			$fecha = date('Y-m-d G:i:s');

			$noticia_nota = array();
			$elementoimagen = array();
			$myquery ="select nota_id as nid, n.categoria_id as cid, c.categoria_nombre as categoria, nota_fecha_publicacion as timestamp,nota_titulo as titular, trim(nota_seolink) as linkseo  
 					from notas as n
 					inner join categorias as c on c.categoria_id = n.categoria_id
 					where n.nota_titulo <> '' and n.sitio_id=".$pid." and n.categoria_id=".$cid." 
 											and nota_estado='1' 
 											and tipo_nota_id = 5
 											and nota_fecha_publicacion <= '".$fecha."'
 					order by nota_fecha_publicacion DESC limit 1";
			$query = $this->db->query($myquery);
			$principal = $query->row();

			$parser = new Netcarver\Textile\Parser();

			$queryelemento ="select e.elemento_id,te.tipo_elemento_id, e.elemento_titulo, e.elemento_ruta, te.tipo_elemento_nombre, te.tipo_elemento_ruta,e.elemento_desc 
				from notas_elemento as  ne , elementos as e, tipo_elemento as te 
				where ne.nota_id=".$principal->nid." 
				and ne.notas_elemento_estado='1' 
				and e.elemento_estado='1' 
				and te.tipo_elemento_estado='1' 
				and  e.elemento_id=ne.elemento_id 
				and e.sitio_id =".$pid." 
				and te.tipo_elemento_id=e.tipoelemento_id";

			$query3 = $this->db->query($queryelemento);
			$elementosnota = $query3->result();

			foreach($elementosnota as $elemento)
			{ 
			 	// para asignar imagen al json
				if ($elemento->tipo_elemento_id==2 || $elemento->tipo_elemento_id==8)
				{				 		
					unset($elementoimagen);
					$elementoimagen = array();	
					
					$querytamaniofoto = "select tef.tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto,tef.tamanio_elemento_ancho,s.sitio_ruta_cdn,e.elemento_ruta,te.tipo_elemento_ruta from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e where s.sitio_id=tef.sitio_id and e.elemento_id=".$elemento->elemento_id." and te.tipo_elemento_id=e.tipoelemento_id and e.elemento_id=tef.elemento_id";
						$query = $this->db->query($querytamaniofoto);

						if($query->num_rows > 0)
						{
							$tamaniofoto = $query->result();
						}
						

					if(!empty($tamaniofoto)){

						$elementotamanios = array();

						foreach($tamaniofoto as $tamanio)
						{

							$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
									//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
								     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									'alto' => $tamanio->tamanio_elemento_alto,
									'ancho' => $tamanio->tamanio_elemento_ancho,
									'descripcion' => $elemento->elemento_desc
									);
						}

						$elementoimagen[] = $elementotamanios;
					}

				}
			}


			// ids canciones de RELACION_NOTAS
			$query2 = "select nota_interna_id, relacion_orden_item as orden
						from relacion_notas
						where nota_principal_id = ".$principal->nid."
						and relacion_estado = '1'
						and tipo_nota_id = 5";
			$query2 = $this->db->query($query2);
			$ids_noticias = $query2->result_array();
			// titulo y letra de canciones
			$i = 0;

			foreach($ids_noticias as $noticia)
			{
				$imagen = array();
				$query3 = "select nota_titulo as titular, nota_contenido as desarrollo, nota_seolink as linkseo
						   from notas
						   where nota_id =".$noticia['nota_interna_id']."
						   and sitio_id = ".$pid."
						   and nota_estado = '1'";
				$query3 = $this->db->query($query3);
				$noticia_nota[$i] = $query3->row_array();

				$noticia_nota[$i]['desarrollo'] = $parser->textileThis($noticia_nota[$i]['desarrollo']);

				// imagenes de noticias
				$query4 = "select e.elemento_ruta as imagenname, tef.tamanio_elemento_ruta as ruta_imagen_ranking, te.tipo_elemento_ruta
							from elementos e 
							inner join tipo_elemento te on te.tipo_elemento_id = e.tipoelemento_id
							inner join notas_elemento ne on ne.elemento_id = e.elemento_id
							left outer join tamanio_elemento_foto tef on tef.elemento_id = e.elemento_id
							WHERE ne.nota_id =".$noticia['nota_interna_id']."
							AND te.tipo_elemento_estado='1' 
							AND ne.notas_elemento_estado =  '1'
							AND e.tipoelemento_id = 2";
				$query4 = $this->db->query($query4);
				$imagen[$i] = $query4->row_array();

				// videos de canciones
				$queryVideo = "select e.elemento_ruta, e.elemento_desc, e.elemento_titulo, s.sitio_ruta_cdn, te.tipo_elemento_ruta
						   from elementos e, sitios as s, tipo_elemento as te, notas_elemento ne
						   WHERE s.sitio_id =".$pid."
						   AND ne.elemento_id = e.elemento_id
						   AND e.elemento_estado = '1'
						   AND e.sitio_id=s.sitio_id
						   AND te.tipo_elemento_id=e.tipoelemento_id 
						   AND ne.nota_id =".$noticia['nota_interna_id']."
						   AND e.tipoelemento_id = 4
						   ORDER BY ne.elemento_id DESC limit 1";
				$queryVideo = $this->db->query($queryVideo);
				$video[$i] = $queryVideo->row_array();

				// tags (artistas)
				$query6 = "select t.tag_nombre as artistatitle, t.tag_id as artistanid, t.tag_ruta as tag_url
						  FROM tags t
						  INNER JOIN tag_notas as tn ON tn.tag_id = t.tag_id and tn.tag_featured = '0'
						  WHERE tn.nota_id = ".$noticia['nota_interna_id']."
						  AND t.sitio_id = ".$pid."
						  AND t.tipotag_id = 3";
				$query6 = $this->db->query($query6);
				$artistas[$i] = $query6->result_array();

				// tags (featured)
				$query7 = "select t.tag_nombre as artistatitle, t.tag_id as artistanid, t.tag_ruta as tag_url
						  FROM tags t
						  INNER JOIN tag_notas as tn ON tn.tag_id = t.tag_id and tn.tag_featured = '1'
						  WHERE tn.nota_id = ".$noticia['nota_interna_id']."
						  AND t.sitio_id = ".$pid."
						  AND t.tipotag_id = 3";
				$query7 = $this->db->query($query7);
				$artistas_ft[$i] = $query7->result_array();

				$noticia_nota[$i]['puesto'] = $noticia['orden'];
				$noticia_nota[$i]['nid'] = $noticia['nota_interna_id'];

				if(!empty($video[$i]['elemento_ruta']))
				{
					$noticia_nota[$i]['videovalue'] = $video[$i]['elemento_ruta'];
					$noticia_nota[$i]['videoprovider'] = $video[$i]['elemento_desc'];
					$noticia_nota[$i]['leyenda'] = $video[$i]['elemento_titulo'];
				}
				else
				{
					$noticia_nota[$i]['videovalue'] = NULL;
					$noticia_nota[$i]['videoprovider'] = NULL;
					$noticia_nota[$i]['leyenda'] = NULL;
				}

				if(!empty($imagen[$i]['ruta_imagen_ranking']))
				{
					$noticia_nota[$i]['imagen_url'] = $imagen[$i]['imagenname'];
				}
				else
				{
					$noticia_nota[$i]['imagen_url'] = NULL;
				}
				
				
				$noticia_nota[$i]['artistas'] = $artistas[$i];
				$noticia_nota[$i]['featured'] = $artistas_ft[$i];
				$i++;
			}

			$principal->noticias = $noticia_nota;
			$principal->foto = $elementoimagen;

			return $principal;
		}
	}

	function GetCancionesNuevas($pid, $sid, $limit) {
		if (isset($pid) && isset($sid) ) {
			$myquery ="select nota_id as nid, n.seccion_id as sid, s.seccion_nombre as seccion, n.categoria_id as cid, 
							  c.categoria_nombre as categoria, nota_fech_creacion as timestamp, nota_estado as estado,nota_titulo as titular,
							  nota_resumen as resumen, (select usuarios.usuario_user from usuarios where usuario_id= n.usuario_id) as usuario ,
							  trim(nota_seolink) as linkseo, nota_contenido as desarrollo, 
							  		(select tipo_nota_nombre 
									from tipo_nota where tipo_nota_id= n.tipo_nota_id) as tiponoticia
 					from notas as n
 					inner join secciones as s on s.seccion_id = n.seccion_id
 					left outer join categorias as c on c.categoria_id = n.categoria_id
 					where n.nota_titulo <> '' and n.sitio_id=".$pid." and n.seccion_id=".$sid." and nota_estado='1' order by nota_fecha_publicacion DESC limit ".$limit ;
			$query = $this->db->query($myquery);
			$principal = $query->result();

			$parser = new Netcarver\Textile\Parser();

			$elementoimagen = array();
			
			foreach ($principal as $nota) {

				$desarrollo = str_replace('&quot;', '', $nota->desarrollo);
				$nota->desarrollo = $parser->textileThis($desarrollo);
				$elementotamanios =array();
				$elementosarray =array();
				$tagsarray=array();
				$videoarray=array();
				$audioarray = array();
				$gifarray = array();
				$cancionarray = array();	
				// $elementoimagen = array();	

				// Tags patrocinado
				$querytagpatrocinado ="select distinct(t.tag_ruta) from tags as t, tag_notas tn where tn.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and t.tipotag_id = '4' and tn.nota_id=".$nota->nid;
				
				$querytag = $this->db->query($querytagpatrocinado);
				$tag_patrocinado = $querytag->row();
				if(!empty($tag_patrocinado->tag_ruta))
				{
					$nota->tagpatrocinado = $tag_patrocinado->tag_ruta;
				}

				// get tags not noticia
				$querytag="select distinct(t.tag_ruta) 
				from tags as t, tag_notas tn 
				where tn.tag_id=t.tag_id 
				and t.tipotag_id <> 1
				and t.sitio_id=".$pid." 
				and tn.tag_featured = '0'
				and t.tag_estado='1' and tn.nota_id=".$nota->nid;

				// get featured
				$querytagft="select distinct(t.tag_ruta)
				from tags as t, tag_notas tn
				where tn.tag_id = t.tag_id
				and t.sitio_id=".$pid."
				and tn.nota_id=".$nota->nid."
				and tn.tag_featured = '1'";

				// get tags noticia
				$querytagnoticia="select distinct(t.tag_ruta) 
				from tags as t, tag_notas tn 
				where tn.tag_id=t.tag_id 
				and t.tipotag_id = 1
				and t.sitio_id=".$pid." 
				and t.tag_estado='1' and tn.nota_id=".$nota->nid;

				$queryelemento ="select e.elemento_id,te.tipo_elemento_id, e.elemento_titulo, e.elemento_ruta, te.tipo_elemento_nombre, te.tipo_elemento_ruta,e.elemento_desc 
				from notas_elemento as  ne , elementos as e, tipo_elemento as te 
				where ne.nota_id=".$nota->nid." 
				and ne.notas_elemento_estado='1' 
				and e.elemento_estado='1' 
				and te.tipo_elemento_estado='1' 
				and  e.elemento_id=ne.elemento_id 
				and e.sitio_id =".$pid." and te.tipo_elemento_id=e.tipoelemento_id";
				
				$query = $this->db->query($querytag);
				$tagsnota = $query->result();

				$tag_query = $this->db->query($querytagnoticia);
				$tagsnotanoticia = $tag_query->result();

				$tag_ft_query = $this->db->query($querytagft);
				$tagsft = $tag_ft_query->result();
			
				$tagsarray=array();
				foreach($tagsnota as $tag)
				{
					if(!empty($tag->tag_ruta))
					$tagsarray[] = $tag->tag_ruta;
				}
				$nota->tags = $tagsarray;

				$tagsftarray=array();
				foreach($tagsft as $tag_ft)
				{
					if(!empty($tag_ft->tag_ruta))
					{
						$tagsftarray[] = $tag_ft->tag_ruta;
					}
				}
				$nota->featured = $tagsftarray;

				$tagsarraynoticia = array();
				foreach($tagsnotanoticia as $tag)
				{
					if(!empty($tag->tag_ruta))
					{
						$tagsarraynoticia[] = $tag->tag_ruta;
					}
				}
				$nota->tags_noticia = $tagsarraynoticia;
		        //  para agregar imagenes jpg y galerias al JSON
				//var_dump($queryelemento);
				$query = $this->db->query($queryelemento);
				$elementosnota = $query->result();


				unset($elementoimagen);
				$elementoimagen = array();
				$nota->audiofilepath ='';
			 	$nota->audiotitle = '';
			 	$nota->artistatitle = '';
				foreach($elementosnota as $elemento)
				 { 
				 	// para asignar imagen
					if ($elemento->tipo_elemento_id==2)
					{				 		
						$querytamaniofoto = "select tef.tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto,tef.tamanio_elemento_ancho,s.sitio_ruta_cdn,e.elemento_ruta,te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id and e.elemento_id=".$elemento->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id";
							$query = $this->db->query($querytamaniofoto);
							$tamaniofoto = $query->result();

						if(!empty($tamaniofoto))
						{

							$elementotamanios = array();

								foreach($tamaniofoto as $tamanio)
							{

								$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
										//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho,
										'descripcion' => $elemento->elemento_desc
										);
							}
							$elementoimagen[] = $elementotamanios;
						}
					}

					// para asignar galerias
					if ($elemento->tipo_elemento_id == 5)
					{
						$elementosarray = array();
						$querytamaniofoto = "select tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto,tef.tamanio_elemento_ancho,s.sitio_ruta_cdn,e.elemento_ruta,te.tipo_elemento_ruta from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e where s.sitio_id=tef.sitio_id and  te.sitio_id=s.sitio_id and e.elemento_id=".$elemento->elemento_id." and te.tipo_elemento_id=e.tipoelemento_id and e.elemento_id=tef.elemento_id";
							$query = $this->db->query($querytamaniofoto);
						$tamaniofoto = $query->result();

						if($tamaniofoto){
								$elementotamaniosgal = array();

								foreach($tamaniofoto as $tamanio)
							{

								$elementotamaniosgal[$tamanio->tamanio_elemento_nombre] = array(
										//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho,
										'descripcion' => $elemento->elemento_desc
										);
							}
							$elementosarray[] = $elementotamaniosgal;
						}
					}
					// para asignar los videos al json
					if($elemento->tipo_elemento_id == 4 || $elemento->tipo_elemento_id == 6)
					{	
						$videoarray[] =array(
	                      'videovalue'     =>$elemento->elemento_ruta,
					  	  'videoprovider'  =>$elemento->elemento_desc,
					  	  'leyenda'        =>$elemento->elemento_titulo						  		
					  	) ;
					}
				  						  
					// asignar gif al Json
					if($elemento->tipo_elemento_id == 3)
					{
					 	// get ruta elemento
					 	$ruta = $this->db->select('tipo_elemento_ruta')->where('tipo_elemento_id', 3)->where('sitio_id', $pid)->get('tipo_elemento')->row();
					 	// get bucket
					 	$bucket = $this->db->select('sitio_ruta_cdn')->where('sitio_id', $pid)->get('sitios')->row();
					 	
					 	$gifarray[] = array(
					 			'ruta' => 'http://'.$bucket->sitio_ruta_cdn.$ruta->tipo_elemento_ruta.'/'.$elemento->elemento_ruta,
					 			'titulo' => $elemento->elemento_titulo,
					 			'descripcion' => $elemento->elemento_desc
					 	);
					}
					 
					// asignar cancion al json
					if($elemento->tipo_elemento_id == 7)
					{
					 	// get ruta elemento
					 	$ruta = $this->db->select('tipo_elemento_ruta')->where('tipo_elemento_id', 7)->get('tipo_elemento')->row();
					 	// get bucket
					 	$bucket = $this->db->select('sitio_ruta_cdn')->where('sitio_id', $pid)->get('sitios')->row();
					 	// conseguir artistas (tags)
					 	$querytagartista="select distinct(t.tag_nombre) from tags as t, tag_elementos te where te.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and te.elemento_id=".$elemento->elemento_id;
					 	$query = $this->db->query($querytagartista);
					 	$tagscancion = $query->result();

					 	$cancionarray = array();
					 	$arraytagscancion = array();
					 	foreach($tagscancion as $tags)
					 	{
					 		$arraytagscancion[] = $tags->tag_nombre;
					 	}

					 	$nota->audiofilepath ='http://'.$bucket->sitio_ruta_cdn.$ruta->tipo_elemento_ruta.'/'.$elemento->elemento_ruta;
					 	$nota->audiotitle = $elemento->elemento_titulo;
					 	$nota->artistatitle = $arraytagscancion;
					}
				  
				 }

				 $nota->foto = $elementoimagen;
				 $nota->fotos = $elementosarray;
				 $nota->videos = $videoarray;
				 $nota->audio = $audioarray;
				 $nota->gif = $gifarray;

				
				 	
			}
			
			return array_reverse($principal);
		}
	}

	function GetArtistas($pid){

		if (isset($pid)) 
		{
			$fecha = date('Y-m-d G:i:s');
			$myquery ="select nota_id, nota_titulo, nota_crudo, nota_contenido from notas where tipo_nota_id = 3 AND sitio_id = ".$pid."
				and nota_titulo IS NOT NULL
				and nota_contenido IS NOT NULL 
				ORDER BY nota_fecha_publicacion DESC
				LIMIT 20";
			$query = $this->db->query($myquery);
			$notas = $query->result();
			$parser = new Netcarver\Textile\Parser();

			foreach ($notas as $nota) 
			{	
				$desarrollo = str_replace('&quot;', '', $nota->nota_contenido);
				$nota->nota_contenido = $parser->textileThis($desarrollo);

				$querytags = "select t.tag_ruta, t.flag_destacado 
				FROM notas n 
				INNER JOIN tag_notas tn ON tn.nota_id = n.nota_id 
				INNER JOIN tags t ON t.tag_id = tn.tag_id AND t.sitio_id = ".$pid." 
				WHERE n.sitio_id = ".$pid." 
				AND n.nota_id =".$nota->nota_id;
				$query = $this->db->query($querytags);
				$tagsruta = $query->row_array();
				$nota->url = $tagsruta['tag_ruta'];

				// imagen
				$queryimagen = "select e.elemento_ruta
				FROM elementos e
				INNER JOIN notas_elemento ne ON e.elemento_id = ne.elemento_id
				INNER JOIN notas n ON n.nota_id = ne.nota_id
				WHERE e.sitio_id = ".$pid." 
				AND ne.nota_id = ".$nota->nota_id.
				" AND e.tipoelemento_id = 2
				ORDER BY e.elemento_id DESC";
				$query2 = $this->db->query($queryimagen);
				$imagenes = $query2->result_array();
				if(!empty($imagenes))
				{
					$nota->imagen = $imagenes[0]['elemento_ruta'];
				}
			}
			// echo '<pre>';
			// print_r($notas);die();
			return $notas;
		}			

		
	}

	function GetArtistaDestacado($pid)
	{
		$nota = new stdClass();

		$querytags = "select t.tag_ruta 
		FROM notas n 
		INNER JOIN tag_notas tn ON tn.nota_id = n.nota_id 
		INNER JOIN tags t ON t.tag_id = tn.tag_id 
		WHERE t.flag_destacado = '1'
		AND n.sitio_id = ".$pid."
		AND n.nota_fecha_publicacion >= (CURDATE() - INTERVAL 60 DAY)
		ORDER BY n.nota_fecha_publicacion ASC";

		$query = $this->db->query($querytags);
		$tagsruta = $query->result_array();
		$ultimo = array_reverse($tagsruta);
		$nota->tagDestacado = $ultimo[0]['tag_ruta'];
		return $nota;
	}

	// para actualizar elementos de artistas
	function getElementos($pid)
	{
		$querytags = "select t.tag_id, t.tag_ruta, e.elemento_ruta, e.tipoelemento_id, e.elemento_desc, e.elemento_titulo
		FROM elementos e
		INNER JOIN tag_elementos te ON te.elemento_id = e.elemento_id
		INNER JOIN tags t ON t.tag_id = te.tag_id
		inner join notas_elemento ne on e.elemento_id=ne.elemento_id
		WHERE e.sitio_id =".$pid."
		and te.tag_elemento_fech_update >= (CURDATE() - INTERVAL 30 DAY)
		and ne.nota_id in (select nota_id from notas where sitio_id=".$pid.")
		GROUP BY e.elemento_id";
		
		$query = $this->db->query($querytags);
		$elementos = $query->result_array();
		$array_definitivo = array();

		foreach($elementos as $key => $elemento)
		{
			$otherkey = $elemento['tag_ruta'];

			switch ($elemento['tipoelemento_id']) {
				case '1':
					$array_definitivo[$otherkey]['audio'][] = $elemento['elemento_ruta'];
					break;
				case '2':
					$array_definitivo[$otherkey]['imagen'][] = $elemento['elemento_ruta'];
					break;
				case '3':
					$array_definitivo[$otherkey]['gif'][] = $elemento['elemento_ruta'];
					break;
				case '4':
					$array_definitivo[$otherkey]['videos'][] = array(
						'codigo' => $elemento['elemento_ruta'],
						'provider' => $elemento['elemento_desc'],
						'leyenda'  => $elemento['elemento_titulo']		
					);
					break;
				case '5':
					$array_definitivo[$otherkey]['galeria_fotos'][] = $elemento['elemento_ruta'];
					break;
				case '6':
					$array_definitivo[$otherkey]['videos'][] = array(
						'codigo' => $elemento['elemento_ruta'],
						'provider' => $elemento['elemento_desc']
					);
					break;
			}
		}

		return $array_definitivo;
	}


	function GetFotogalerias($pid){

		if (isset($pid)) 
		{
			$myquery ="select DISTINCT(n.nota_id) as nid, n.nota_titulo as titular,nota_seolink as linkseo, nota_contenido as desarrollo, nota_fecha_publicacion as timestamp, n.seccion_id as seccion, s.seccion_nombre
						from notas n
						inner join secciones s on s.seccion_id = n.seccion_id
						inner join notas_elemento ne on ne.nota_id = n.nota_id
						INNER JOIN elementos e ON e.elemento_id = ne.elemento_id
						WHERE 
						(e.tipoelemento_id = 5)
						and n.sitio_id = ".$pid."
						and n.nota_estado ='1'
						and ne.notas_elemento_estado = '1'
						ORDER BY n.nota_fecha_publicacion DESC
						LIMIT 30";
						// cambiar por ids reales
			$query = $this->db->query($myquery);
			$notas = $query->result();
			$parser = new Netcarver\Textile\Parser();

			foreach ($notas as $key => $nota) 
			{
				$nota->desarrollo = $parser->textileThis($nota->desarrollo);				
				

				// ELEMENTOS
				$queryelemento = "select  e.elemento_id, e.tipoelemento_id, e.elemento_ruta, e.elemento_desc
								FROM notas_elemento ne
								INNER JOIN elementos e ON e.elemento_id = ne.elemento_id
								INNER JOIN tipo_elemento te ON te.tipo_elemento_id = e.tipoelemento_id
								WHERE ne.notas_elemento_estado =  '1' and (te.tipo_elemento_id = 5)
								AND ne.nota_id =".$nota->nid;
				
				$query = $this->db->query($queryelemento);
				$elementosnota = $query->result();

				$elementotamanios =array();
				$elementoURL = array();
				$elementosarray =array();
				$videoarray=array();



				foreach($elementosnota as $elem)
				{
					if ($elem->tipoelemento_id == 5)
					{
						/*$querytamaniofoto = "select tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto, tef.tamanio_elemento_ancho, s.sitio_ruta_cdn, e.elemento_ruta, te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id 
						and e.elemento_id=".$elem->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id
						and tef.tamanio_elemento_nombre = '4x3'";
						$query = $this->db->query($querytamaniofoto);
						$tamaniofoto = $query->result();

						if(!empty($tamaniofoto)){
							//$elementotamanios = array();
							foreach($tamaniofoto as $tamanio)
							{
								$elementotamanios[] = 'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta;
							}
							//$elementosarray[] = $elementotamanios;
						}*/
						// solo nombre
						$queryfotoURL = "select e.elemento_ruta
						from elementos as e
						where e.elemento_id =".$elem->elemento_id;
						$queryURL = $this->db->query($queryfotoURL);
						$fotoURL = $queryURL->result();
						if(!empty($fotoURL)){
							foreach($fotoURL as $foto)
							{
								$elementoURL[] = $foto->elemento_ruta;
							}
						}

					}
				}

				if(empty($elementoURL)){
					unset($notas[$key]);
				}else{
					// $nota->fotos = $elementotamanios;
					$nota->fotosURL = $elementoURL;

				}
				
			}

			return array_reverse($notas);
		}
	}

	function getVideogalerias($pid)
	{
		if (isset($pid)) 
		{
			$myquery ="select DISTINCT(n.nota_id) as nid, n.nota_fecha_publicacion as timestamp, n.nota_titulo as titular,nota_seolink as linkseo, nota_contenido as desarrollo, n.seccion_id as seccion, s.seccion_nombre
						from notas n
						inner join secciones s on s.seccion_id = n.seccion_id
						inner join notas_elemento ne on ne.nota_id = n.nota_id
						INNER JOIN elementos e ON e.elemento_id = ne.elemento_id
						WHERE 
						(e.tipoelemento_id = 6)
						and n.sitio_id = ".$pid."
						and n.nota_estado ='1'
						and ne.notas_elemento_estado = '1'
						ORDER BY n.nota_fecha_publicacion DESC
						LIMIT 30";
			$query = $this->db->query($myquery);
			$notas = $query->result();
			$parser = new Netcarver\Textile\Parser();

			foreach ($notas as $key => $nota) 
			{
				$nota->desarrollo = $parser->textileThis($nota->desarrollo);				

				// ELEMENTOS
				$queryelemento = "select  e.elemento_id, e.tipoelemento_id, e.elemento_titulo as leyenda, e.elemento_ruta as codigo, e.elemento_desc as provider
								FROM notas_elemento ne
								INNER JOIN elementos e ON e.elemento_id = ne.elemento_id
								INNER JOIN tipo_elemento te ON te.tipo_elemento_id = e.tipoelemento_id
								WHERE ne.notas_elemento_estado =  '1' and (te.tipo_elemento_id = 6)
								AND ne.nota_id =".$nota->nid;
				
				$query = $this->db->query($queryelemento);
				$elementosnota = $query->result();

				$elementotamanios =array();
				$elementosarray =array();
				$videoarray=array();

				$nota->videos = $elementosnota;
				
			}
			return array_reverse($notas);
		}
	}

	function GetNota($pid, $nid){

		if (isset($pid) && isset($nid) ) 
		{
			$myquery ="select nota_id as nid, origen_id as clon, n.seccion_id as sid, s.seccion_nombre as seccion, n.categoria_id as cid, n.tipo_nota_id, 
							  c.categoria_nombre as categoria, nota_fecha_publicacion as timestamp, nota_estado as estado,nota_titulo as titular,
							  nota_resumen as resumen, (select usuarios.usuario_user from usuarios where usuario_id= n.usuario_id) as usuario ,
							  trim(nota_seolink) as linkseo, nota_contenido as desarrollo, 
							  		(select tipo_nota_nombre 
									from tipo_nota where tipo_nota_id= n.tipo_nota_id) as tiponoticia
 					from notas as n
 					inner join secciones as s on s.seccion_id = n.seccion_id
 					left outer join categorias as c on c.categoria_id = n.categoria_id
 					where n.sitio_id=".$pid." 
 						  and n.nota_id=".$nid;
 											  
			$query = $this->db->query($myquery);
			$nota = $query->row();

			if($nota->tipo_nota_id != 1)
			{
				die('tipo de nota incorrecta');
			}
			$parser = new Netcarver\Textile\Parser();

			if(!empty($nota))
			{
				$nota->clon = !empty($nota->clon) ? TRUE : FALSE;				
				// if($nota->sid == 9 ){
				// 	if(strpos($nota->linkseo, 'mundo-mujer/tips/') === FALSE)
				// 	{						
				// 		$nota->linkseo = 'mundo-mujer/'.$nota->linkseo;
				// 	}				
				// }

				$desarrollo = str_replace('&quot;', '', $nota->desarrollo);
				$nota->desarrollo = $parser->textileThis($desarrollo);
				$elementotamanios =array();
				$elementosarray =array();
				$tagsarray=array();
				$videoarray=array();
				$audioarray = array();
				$gifarray = array();
				$cancionarray = array();
				$elementoimagen = array();	

				// Tags patrocinado
				$querytagpatrocinado ="select distinct(t.tag_ruta) from tags as t, tag_notas tn where tn.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and t.tipotag_id = '4' and tn.nota_id=".$nota->nid;
				
				$querytag = $this->db->query($querytagpatrocinado);
				$tag_patrocinado = $querytag->row();
				if(!empty($tag_patrocinado->tag_ruta))
				{
					$nota->tagpatrocinado = $tag_patrocinado->tag_ruta;
				}
							

				// TAGS
				$querytag="select distinct(t.tag_ruta) from tags as t, tag_notas tn where tn.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and tn.nota_id=".$nota->nid;
				
				$queryelemento ="select e.elemento_id,te.tipo_elemento_id, e.elemento_titulo, e.elemento_ruta, te.tipo_elemento_nombre, te.tipo_elemento_ruta,e.elemento_desc 
				from notas_elemento as  ne , elementos as e, tipo_elemento as te 
				where ne.nota_id=".$nota->nid." 
				and ne.notas_elemento_estado='1' 
				and e.elemento_estado='1' 
				and te.tipo_elemento_estado='1' 
				and  e.elemento_id=ne.elemento_id 
				and e.sitio_id =".$pid." 
				and te.tipo_elemento_id=e.tipoelemento_id";
				
				$query = $this->db->query($querytag);
				$tagsnota = $query->result();
			
				$tagsarray=array();
				foreach($tagsnota as $tag)
				{
					if(!empty($tag->tag_ruta))
					$tagsarray[] = $tag->tag_ruta;
				}
				$nota->tags = $tagsarray;

				// ELEMENTOS
				$query = $this->db->query($queryelemento);
				$elementosnota = $query->result();
	
				foreach($elementosnota as $elemento)
				 { 
				 	// para asignar imagen al json

					if ($elemento->tipo_elemento_id==2)
					{				 		
						unset($elementoimagen);
						$querytamaniofoto = "select tef.tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto,tef.tamanio_elemento_ancho,s.sitio_ruta_cdn,e.elemento_ruta,te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id 
						and e.elemento_id=".$elemento->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id";
							$query = $this->db->query($querytamaniofoto);
							$tamaniofoto = $query->result();

						if(!empty($tamaniofoto)){

							$elementotamanios = array();

							foreach($tamaniofoto as $tamanio)
							{
								$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
									     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho,
										'descripcion' => $elemento->elemento_desc
										);
							}
							$elementoimagen[] = $elementotamanios;
						}

					}
						// para asignar galerias de fotos al json
						

					if ($elemento->tipo_elemento_id==5)
					{
						$querytamaniofoto = "select tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto, tef.tamanio_elemento_ancho, s.sitio_ruta_cdn, e.elemento_ruta, te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id 
						and e.elemento_id=".$elemento->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id";
						$query = $this->db->query($querytamaniofoto);
						$tamaniofoto = $query->result();

						if(!empty($tamaniofoto)){
							$elementotamanios = array();
							foreach($tamaniofoto as $tamanio)
							{
								$elementotamanios[$tamanio->tamanio_elemento_nombre] = array(
										//'ruta' => $tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
									     'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho,
										'descripcion' => $elemento->elemento_desc
										);
							}
							$elementosarray[] = $elementotamanios;

						}
					}

					// para asignar los videos al json
					if($elemento->tipo_elemento_id == 4 || $elemento->tipo_elemento_id == 6)
					{
							
						$videoarray[] =array(
	                      'videovalue'     =>$elemento->elemento_ruta,
					  	  'videoprovider'  =>$elemento->elemento_desc,
					  	  'leyenda'        =>$elemento->elemento_titulo						  		
					  	) ;
					}
				  
					//para asignar los audios al Json
					if($elemento->tipo_elemento_id == 1)
					{
						// get ruta elemento
						$ruta = $this->db->select('tipo_elemento_ruta')->where('tipo_elemento_id', 1)->get('tipo_elemento')->row();
						// get bucket
						$bucket = $this->db->select('sitio_ruta_cdn')->where('sitio_id', $pid)->get('sitios')->row();
						
						$audioarray[] = array(
					  	   'audiofilepath' => 'http://'.$bucket->sitio_ruta_cdn.$ruta->tipo_elemento_ruta.$elemento->elemento_ruta,
					  		'audiotitle' => $elemento->elemento_titulo	
					  );
					}
					  
					// asignar gif al Json
					if($elemento->tipo_elemento_id == 3)
					{
						// get ruta elemento
						$ruta = $this->db->select('tipo_elemento_ruta')->where('tipo_elemento_id', 3)->get('tipo_elemento')->row();
						// get bucket
						$bucket = $this->db->select('sitio_ruta_cdn')->where('sitio_id', $pid)->get('sitios')->row();
						
						$gifarray[] = array(
								'ruta' => 'http://'.$bucket->sitio_ruta_cdn.$ruta->tipo_elemento_ruta.'/'.$elemento->elemento_ruta,
								'titulo' => $elemento->elemento_titulo,
								'descripcion' => $elemento->elemento_desc
						);
					}
				}

				// ids canciones de RELACION_NOTAS
				$query2 = "select nota_interna_id, relacion_orden_item as orden
							from relacion_notas
							where nota_principal_id = ".$nota->nid."
							and relacion_estado = '1'";
				$query2 = $this->db->query($query2);
				$ids_canciones = $query2->result_array();

				// titulo y letra de canciones
				$i = 0;
				unset($cancion_nota);
				$cancion_nota = array();
				foreach($ids_canciones as $cancion)
				{
					$imagen = array();
					$query3 = "select nota_titulo as audiotitle, nota_seolink as linkseo
							   from notas
							   where nota_id =".$cancion['nota_interna_id']."
							   and nota_estado = '1'";
					$query3 = $this->db->query($query3);
					$cancion_nota[$i] = $query3->row_array();

					// audios de canciones
					$query5 = "select e.elemento_ruta as ruta_audio, s.sitio_ruta_cdn, te.tipo_elemento_ruta
							   from elementos e, sitios as s, tipo_elemento as te, notas_elemento ne
							   WHERE s.sitio_id =".$pid."
							   AND ne.elemento_id = e.elemento_id
							   AND e.sitio_id=s.sitio_id
							   AND te.tipo_elemento_id=e.tipoelemento_id 
							   AND ne.nota_id =".$cancion['nota_interna_id']."
							   AND e.tipoelemento_id = 7";
					$query5 = $this->db->query($query5);
					$audio[$i] = $query5->row_array();

					// tags (artistas)
					$query6 = "select t.tag_nombre as artistatitle, t.tag_ruta as ruta
							  FROM tags t
							  INNER JOIN tag_notas as tn ON tn.tag_id = t.tag_id
							  WHERE tn.nota_id = ".$cancion['nota_interna_id'];
					$query6 = $this->db->query($query6);
					$artistas[$i] = $query6->result_array();

					$cancion_nota[$i]['puesto'] = $cancion['orden'];
					$cancion_nota[$i]['audionid'] = $cancion['nota_interna_id'];
					$cancion_nota[$i]['audiofilepath'] = 'http://'.$audio[$i]['sitio_ruta_cdn'].$audio[$i]['tipo_elemento_ruta'].'/'.$audio[$i]['ruta_audio'];				
					
					$cancion_nota[$i]['artistas'] = $artistas[$i];
					$i++;
				}

				$nota->canciones = $cancion_nota;
				$nota->foto = $elementoimagen;
				$nota->fotos = $elementosarray;
				$nota->videos = $videoarray;
				$nota->audio = $audioarray;
				$nota->gif = $gifarray;	

				return $nota;
			}
		}
		return FALSE;
	}

	function getTagnav($pid)
	{
		$this->_select_database($pid);

		$query = 'select t.tag_nombre, tipotag.tipotag_nombre as tipotag, t.tag_ruta, count(n.nota_id) as total
				FROM tags t, notas n, tag_notas tn, tipotag
				WHERE
				t.sitio_id = n.sitio_id
				and
				t.tipotag_id = tipotag.tipotag_id 
				and
				tn.nota_id=n.nota_id and t.tag_id=tn.tag_id and
				t.sitio_id='.$pid.' 
				group by t.tag_nombre
				order by 4 desc
				LIMIT 0 , 20';
		$query = $this->db->query($query);
		$nota = $query->result();

		return $nota;

	}

	function getNoticias($limit = null)
	{
		$limit = !empty($limit) ? (int)$limit : 27;
		
		$this->_select_database(15);

		$this->db->select('n.nota_id as nid, n.nota_titulo as titular, trim(n.nota_seolink) as linkseo, n.nota_contenido as desarrollo, n.original_url, e.elemento_id, e.elemento_ruta as ruta');
		$this->db->from('notas n');
		$this->db->join('notas_elemento ne', "ne.nota_id = n.nota_id");
		$this->db->join('elementos e', 'e.elemento_id = ne.elemento_id');
		$this->db->where('n.fuente_id', 1);
		$this->db->where('e.tipoelemento_id', 2);
		$this->db->where('n.nota_estado', '1');
		$this->db->order_by('n.nota_id', 'DESC');
		$this->db->limit($limit);
		$query = $this->db->get()->result_array();
		$array = array_reverse($query);
		return $array;
	}

	function GetDatosRss($pid) {
		$fecha = date('Y-m-d G:i:s');
		
		if (isset($pid)) {
			$this->_select_database($pid);
			$myquery ="select nota_id as nid, n.seccion_id as sid, s.seccion_nombre as seccion, n.categoria_id as cid, 
							  c.categoria_nombre as categoria, nota_fecha_publicacion as timestamp, nota_estado as estado,nota_titulo as titular,
							  nota_resumen as resumen, trim(nota_seolink) as linkseo, nota_texto_rss as crudo							  		
 					from notas as n
 					inner join secciones as s on s.seccion_id = n.seccion_id
 					left outer join categorias as c on c.categoria_id = n.categoria_id
 					where n.nota_titulo <> '' and n.sitio_id=".$pid." 
 											  and nota_estado='1' 
 											  and nota_rss ='1' 
 											  and nota_fecha_publicacion <= '".$fecha."'
 					order by nota_fecha_publicacion DESC limit 30" ;
			$query = $this->db->query($myquery);

			$principal = $query->result();

			foreach ($principal as $nota) {

				$elementotamanios =array();	
				$elementoimagen = array();
				$elementovideo = array();

				$queryelemento ="select e.elemento_id,te.tipo_elemento_id, e.elemento_titulo, e.elemento_ruta, te.tipo_elemento_nombre, te.tipo_elemento_ruta,e.elemento_desc 
				from notas_elemento as  ne , elementos as e, tipo_elemento as te 
				where ne.nota_id=".$nota->nid." 
				and ne.notas_elemento_estado='1' 
				and e.elemento_estado='1' 
				and te.tipo_elemento_estado='1' 
				and  e.elemento_id=ne.elemento_id 
				and e.sitio_id =".$pid." 
				and (te.tipo_elemento_id= 2 OR te.tipo_elemento_id= 4 OR te.tipo_elemento_id= 6)";
											
				$query = $this->db->query($queryelemento);
			
				$elementosnota = $query->result();

				unset($elementogaleria);
				$elementogaleria = array();

				foreach($elementosnota as $elemento)
				 { 
				 	// para asignar imagen al json
					if ($elemento->tipo_elemento_id==2)
					{				 		
						unset($elementoimagen);
						$elementoimagen = array();
						$querytamaniofoto = "select tef.tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto,tef.tamanio_elemento_ancho,s.sitio_ruta_cdn,e.elemento_ruta,te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id 
						and e.elemento_id=".$elemento->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id
						and tef.tamanio_elemento_nombre = 'apaisado'";
							$query = $this->db->query($querytamaniofoto);
							$tamaniofoto = $query->row();

						if(!empty($tamaniofoto)){

							$elementotamanios = array();
							$elementotamanios = array(
								'ruta'  =>'http://'.$tamaniofoto->sitio_ruta_cdn.$tamaniofoto->tipo_elemento_ruta.$tamaniofoto->tamanio_elemento_ruta,
								'alto' => $tamaniofoto->tamanio_elemento_alto,
								'ancho' => $tamaniofoto->tamanio_elemento_ancho,
								'descripcion' => $elemento->elemento_desc
							);							

							$elementoimagen[] = $elementotamanios;
						}

					}	

					// para asignar los videos al json
					if($elemento->tipo_elemento_id == 4 && $elemento->elemento_desc == 'youtube')
					{
						unset($elementovideo);
						$elementovideo = array();

						$elementovideo =array(
	                      'videovalue'     =>$elemento->elemento_ruta,
					  	  'videoprovider'  =>$elemento->elemento_desc,
					  	  'leyenda'        =>$elemento->elemento_titulo						  		
					  	) ;
					}

					// Galería de fotos
					if ($elemento->tipo_elemento_id == 6)
					{						
						$querytamaniogaleria = "select tamanio_elemento_nombre, tef.tamanio_elemento_ruta, tef.tamanio_elemento_alto, tef.tamanio_elemento_ancho, s.sitio_ruta_cdn, e.elemento_ruta, te.tipo_elemento_ruta 
						from tamanio_elemento_foto as tef, sitios as s, tipo_elemento as te,elementos as e 
						where s.sitio_id=tef.sitio_id 
						and e.elemento_id=".$elemento->elemento_id." 
						and te.tipo_elemento_id=e.tipoelemento_id 
						and e.elemento_id=tef.elemento_id
						and tef.tamanio_elemento_nombre = 'apaisado'
						and e.tipoelemento_id = 6";
						$query = $this->db->query($querytamaniogaleria);
			
						$tamaniofotogaleria = $query->result();
						$i = 0;

						if(!empty($tamaniofotogaleria)){
							$elementotamaniosgaleria = array();
							foreach($tamaniofotogaleria as $tamanio)
							{
								$elementotamaniosgaleria = array(
									    'ruta'  =>'http://'.$tamanio->sitio_ruta_cdn.$tamanio->tipo_elemento_ruta.$tamanio->tamanio_elemento_ruta,
										'alto' => $tamanio->tamanio_elemento_alto,
										'ancho' => $tamanio->tamanio_elemento_ancho,
										'descripcion' => $elemento->elemento_desc
										);
								$i++;
							}
							$elementogaleria[] = $elementotamaniosgaleria;
						}

					}
				}				

				$nota->foto = $elementoimagen;	
				$nota->video = $elementovideo;	
				$nota->fotos = $elementogaleria;			 	
			}
			 
			
			return $principal;
		}
	}


	function GetVideos($pid)
	{	
		if (isset($pid)) {

			$query = "select lista_id as nid, lista_seolink as seolink, lista_titulo as titular, lista_imagen as foto, lista_videos as cantidad
				 FROM listas
				 WHERE sitio_id = ".$pid."
				 AND lista_estado = '1'
				 ORDER BY lista_id DESC limit 30";

			$listas = $this->db->query($query)->result();
			$videos_array = array();

			$parser = new Netcarver\Textile\Parser();
			foreach($listas as $lista)
			{
				$lista->foto = 'http://static.cms.cmd.pe/'.$lista->foto; 

				//Tags patrocinados
				$querytagpatrocinado ="select distinct(t.tag_ruta) from tags as t, tag_listas tn where tn.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and t.tipotag_id = '4' and tn.lista_id=".$lista->nid;
				
				$querytag = $this->db->query($querytagpatrocinado);
				$tag_patrocinado = $querytag->row();
				if(!empty($tag_patrocinado->tag_ruta))
				{
					$tag_patrocinado = $tag_patrocinado->tag_ruta;
				}
				$lista->tagpatrocinado = $tag_patrocinado;

				//Tags
				$querytag ="select distinct(t.tag_ruta) from tags as t, tag_listas tn where tn.tag_id=t.tag_id and t.sitio_id=".$pid." and t.tag_estado='1' and tn.lista_id=".$lista->nid;
				$query = $this->db->query($querytag);
				$tagslista = $query->result();
			
				$tagsarray=array();
				foreach($tagslista as $tag)
				{
					if(!empty($tag->tag_ruta))
					$tagsarray[] = $tag->tag_ruta;
				}
				$lista->tags = $tagsarray;


				// Videos de las lista
				unset($videos_array);
				$videos_array = array();

				$query_videos = "select video_id, video_titulo, video_codigo, video_img_mediano,video_img_thumb, video_img_apaisado,video_descripcion from videos v
				where lista_id = ".$lista->nid." 
				and video_estado = '1'
				order by video_orden asc";
				$videos = $this->db->query($query_videos)->result();
				foreach($videos as $video)
				{
					$videos_array[] = array(
						'nid' => $video->video_id,
						'codigo' => $video->video_codigo,
						'foto' => 'http://static.cms.cmd.pe/'.$video->video_img_mediano,
						'thumb' => 'http://static.cms.cmd.pe/'.$video->video_img_thumb,
						'apaisado' => 'http://static.cms.cmd.pe/'.$video->video_img_apaisado,
						'descripcion' => $video->video_descripcion,
						'titular' => $video->video_titulo,
						);
				}				

				$lista->videos = $videos_array;


			}

			return $listas;	

		}
		
	}

}
