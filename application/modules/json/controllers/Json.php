<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Json extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Json_model');	
		$this->load->model('sitios/Sitios_getters_modelo');	
		$this->load->model('secciones/Secciones_getters_modelo');	

	}

	public function gettag()
	{	
		if ($_GET){
			$pid = $this->input->get('pid', true);
			$tag = $this->input->get('tag', true);
			$limit =  $this->input->get('limit', true);
		    if ($limit==0 ) { $limit=27;}
			$data = array(
					'registros' => $this->Json_model->GetDatosNotasTags($pid,$tag,$limit),
					'pid' => $pid,
					'sitio' => $this->Sitios_getters_modelo->get_name_by_id($pid),
			);
		}
		
		$this->load->view('json_nuevo',$data);	
	}


	public function newjson()
	{	
		if ($_GET){
			$pid = $this->input->get('pid', true);
			$sid = $this->input->get('sid', true);
			$limit =  $this->input->get('limit', true);
			$nombreseccion = $this->Secciones_getters_modelo->get_name_by_id($sid);
		    if ($limit==0 ) { $limit=27;}
			$data = array(
					'registros' => $this->Json_model->GetDatosNotasNuevas($pid,$sid,$limit),
					'pid' => $pid,
					'sid' => $sid,
					'sitio' => $this->Sitios_getters_modelo->get_name_by_id($pid),
					'seccion' => $nombreseccion
			);
		}
		
		$this->load->view('json_nuevo',$data);	
	}

	public function portada()
	{	
		if ($_GET){
			$pid = $this->input->get('pid', true);
			$limit = $this->input->get('limit', true);			
			if(empty($limit)) { $limit = 5;}
			$data = array(
					'registros' => $this->Json_model->GetPortadas($pid, $limit),
					'pid' => $pid,					
			);
		}
		
		$this->load->view('json_nuevo',$data);		
	}

	public function ranking()
	{
		if ($_GET){
			$pid = $this->input->get('pid', true);
			$cid = $this->input->get('cid', true);			

			$data = array(
					'registros' => $this->Json_model->GetRanking($pid, $cid),
					'pid' => $pid,
					'cid' => $cid,					
					'sitio' => $this->Sitios_getters_modelo->get_name_by_id($pid),
			);
		}
		
		$this->load->view('json_nuevo',$data);	
	}

	public function lista()
	{
		if ($_GET){
			$pid = $this->input->get('pid', true);
			$cid = $this->input->get('cid', true);			

			$data = array(
					'registros' => $this->Json_model->GetRankingLista($pid, $cid)				
			);
		}
		
		$this->load->view('json_nuevo',$data);	
	}

	public function canciones_nuevas()
	{
		if ($_GET){
			$pid = $this->input->get('pid', true);
			$sid = $this->input->get('sid', true);
			$limit =  $this->input->get('limit', true);

			$limit = $limit == 0 ? 21 : $limit;
			
			$data = array(
					'registros' => $this->Json_model->GetCancionesNuevas($pid,$sid,$limit),
					'pid' => $pid,
					'sid' => $sid,
			);
		}
		
		$this->load->view('json_nuevo',$data);
	}

	public function artistas()
	{
		if ($_GET){
			$pid = $this->input->get('pid', true);
		
			$data = array(
					'registros' => $this->Json_model->GetArtistas($pid),
					'pid' => $pid
			);
		}
		
		$this->load->view('json_nuevo',$data);
	}

	public function artista_destacado()
	{
		if ($_GET)
		{
			$pid = $this->input->get('pid', true);

			$data = array(
				'registros' => $this->Json_model->GetArtistaDestacado($pid),
				'pid' => $pid
			);
		}

		$this->load->view('json_nuevo',$data);	
	}

	public function elementos()
	{
		if ($_GET)
		{
			$pid = $this->input->get('pid', TRUE);
			$data = array(
				'registros' => $this->Json_model->getElementos($pid),
				'pid' => $pid
			);
		
		}

		$this->load->view('json_nuevo',$data);	
		
	}

	

	public function fotogalerias()
	{	
		if ($_GET){
			$pid = $this->input->get('pid', true);			
	
			$data = array(
					'registros' => $this->Json_model->GetFotogalerias($pid),
					'pid' => $pid,					
					'sitio' => $this->Sitios_getters_modelo->get_name_by_id($pid),
			);
		}
		
		$this->load->view('json_nuevo',$data);		
	}

	function videogalerias()
	{
		if ($_GET){
			$pid = $this->input->get('pid', TRUE);

			$data = array(
				'registros' => $this->Json_model->getVideogalerias($pid),
				'pid' => $pid
			);
		}

		$this->load->view('json_nuevo',$data);	
	}

	public function getnota()
	{	
		if ($_GET){

			$pid = $this->input->get('pid', true);
			$nid = $this->input->get('nid', true);

			if(!is_numeric($nid) || !is_numeric($pid))
			{
				die('La URL que ingresaste es incorrecta');
			}	
			// die('test2');		
			$nota = $this->Json_model->GetNota($pid, $nid);
			// var_dump($nota);die();
			$data = array(
					'registros' => $nota,
					'pid' => $pid,					
					//'sitio' => $this->Sitios_getters_modelo->get_name_by_id($pid),
			);
		}

		$this->load->view('json_nota',$data);	
	}

	public function getTagnav()
	{
		if ($_GET) 
		{
			$pid = $this->input->get('pid', true);
			$data = array(
				'registros' => $this->Json_model->getTagnav($pid),
				'pid' => $pid
			);
		}

		$this->load->view('json_tagnav', $data);
	}


	function noticias()
	{
		$limit = $this->input->get('limit', true);

		$json = $this->Json_model->getNoticias($limit);
		
		if(!empty($json))
		{
			$data['registros'] = $json;
			$this->load->view('json_nuevo', $data);
		}
		else
		{
			die('sin registros');
		}
	}

	function cartelera()
	{
		if(is_file(FCPATH.'json/cartelera.json'))
		{
			$data['registros'] = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '',file_get_contents(FCPATH.'json/cartelera.json')));
			$this->load->view('json_nuevo', $data);
		}
		else
		{
			die('sin registros');
		}
		
	}

	public function rss()
	{	
		if ($_GET){
			$pid = $this->input->get('pid', true);

			$data = array(
					'registros' => $this->Json_model->GetDatosRss($pid),
					'sitio' => $this->Sitios_modelo->get_name_by_id($pid)
			);
		}
		
		$this->load->view('json_nuevo',$data);		
	}

	public function videos_yapi()
	{
		if ($_GET){
			$pid = $this->input->get('pid', true);

			if(!is_numeric($pid))
			{
				die('La URL que ingresaste es incorrecta');
			}	
					
	
			$data = array(
					'registros' => $this->Json_model->GetVideos($pid),
					'pid' => $pid,					
			);
		}
		
		$this->load->view('json_nuevo',$data);	
	}

	public function rss_twitter()
	{	
		if ($_GET){
			$pid = $this->input->get('pid', true);
			$limit =  $this->input->get('limit', true);
		    if ($limit==0 ) 
	    	{ 
	    		$limit = 20;
		    }
			$data = array(
					'registros' => $this->Json_model->GetDatosTwitter($pid,$limit),
					'pid' => $pid,
					'sitio' => $this->Sitios_modelo->get_name_by_id($pid),
			);
		}
		
		$this->load->view('json_nuevo',$data);		
	}		
}
 
