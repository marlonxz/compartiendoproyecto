<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Valida que el elemento tenga un json
 * @param  string $galeria el json en string
 * @param  string $key     opcional, en caso se quiera comprobar un key específico en el json
 * @return object          si la galeria no existe devuelve FALSE
 */
if(!function_exists('validar_galeria')) 
{
	function validar_galeria($galeria, $key = NULL)
	{
		if (isset($key))
		{
			$galeria = json_decode($galeria);
			if(isset($galeria->$key) AND !empty($galeria->$key))
			{

				return $galeria->$key;
			}
			else
			{
				return FALSE;
			}
		}

		$array = json_decode($galeria);
		$properties = array();

		if(!empty($array))
		{
			foreach($array as $arr)
			{
				$properties = array_filter(get_object_vars($arr));
			}


			if(!empty(array_filter($properties)))
			{
				return $array;
			}
		}
		
		
		return FALSE;
	}
}

function validar_path_foto($foto, $ruta_antigua)
{
	$file_headers = @get_headers($foto);

	if(!empty($foto))
	{
		if($file_headers[0] == 'HTTP/1.1 403 Forbidden') {
	    return $ruta_antigua;
		}else{
			return $foto;
		}
	}
	else
	{
		return $ruta_antigua;
	}
	
}

function validar_foto($array, $nombre)
	{
		if(!empty($array[0]))
		{
			if(!empty($array[0]->$nombre))
			{
				if(!empty($array[0]->$nombre->ruta))
				{
					return $array[0]->$nombre->ruta;
				}
				
			}			
		}
		return FALSE;
	}

function validar_foto_ruta_cancion($array, $nombre, $nid)
{
	if(!empty($array))
	{
		foreach($array as $arr)
		{
			if(!empty($arr->$nombre))
			{
				if($nid <= 63437)
				{
					return str_replace('/imagen/'.$nombre, '/images', $arr->$nombre->ruta);
				}
				else
				{
					return $arr->$nombre->ruta;
				}
			}
		}
	}
	
		return FALSE;
}

if(!function_exists('toAscii')) 
{
	function toAscii($str, $replace=array(), $delimiter='-') 
	{
		if( !empty($replace) ) 
		{
			$str = str_replace((array)$replace, ' ', $str);
		}
		/*setlocale(LC_CTYPE, 'en_US.UTF-8');
		
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);*/
		$clean = convert_accented_characters($str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}
}

// Para linkseo, mantiene el '/'
if(!function_exists('limpiar_url'))
{
	function limpiar_url($str, $replace=array(), $delimiter='-')
	{ 
		if (!empty($replace) )
		{
			$str = str_replace((array)$replace, ' ', $str);
		}
		
		/*setlocale(LC_CTYPE, 'es_PE.UTF-8');
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);*/
		$clean = convert_accented_characters($str);
		$clean = preg_replace("/[^a-zA-Z0-9_|+ \/-]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[_|+ -]+/", $delimiter, $clean);

		return $clean;
	}
}

if(!function_exists('exclude_seccion'))
{
	function exclude_seccion($seccion, $secciones = array())
	{
		if(($key = array_search($seccion, $secciones)) !== false) 
		{
    		unset($secciones[$key]);
		}

		$new_array = array();
		foreach($secciones as $seccion)
		{
			$new_array[] = $seccion;
		}

		return $new_array;
	}
}

if(!function_exists('escoger_foto'))
{
	function escoger_foto($object)
	{
		if(!empty($object->fotoportada))
		{
			return $object->fotoportada;
		}
		else if(!empty($object->fotointerna))
		{
			return $object->fotointerna;
		}
		else if(!empty($object->fotothumb))
		{
			return $object->fotothumb;
		}
		return FALSE;
	}
}

function limpiar_detalle($detalle){
	return word_limiter(strip_tags($detalle), 25);
}

