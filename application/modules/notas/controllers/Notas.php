<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notas extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model(array('notas/Notas_modelo', 'notas/Notas_getters_modelo', 'tags/Tags_getters_modelo', 'sitios/Sitios_getters_modelo'));
	}

    public function validar_sitio()
    {
        $sitio_id = $this->session->userdata('sitio_id_session');
        $sitios = $this->session->userdata('sitios');

        if(!in_array($sitio_id, $sitios))
        {
            $this->session->set_flashdata('error', 'No tienes permisos de administración.');
            $this->load->view('sin_permiso');
            return FALSE;
        }
        return TRUE;        
    }

    // paso 1 del registro de notas
    function crear($sitio_id)
    {
        // guardar el sitio
        $this->session->set_userdata('sitio_id_session', $sitio_id);
        $validacion = $this->validar_sitio();
        $roles = $this->session->userdata('roles');

        if($validacion)
        {   
            // get tipo de notas
            $this->load->model('tipo_nota/Tipo_nota_modelo');//tipo_nota_id, tipo_nota_nombre, tipo_nota_desc
            $data['tiponotas'] = $this->Tipo_nota_modelo->get_tipo_nota();

            $this->load->helper('sitios/sitios');//retorna el nombre sitio
            $data['sitio_nombre'] = get_sitio_nombre($sitio_id);

            $this->load->view('crear/notas_tab_1', $data);
        }
    }
    
    // procesamiento de datos del paso 1
    function registro_1()
    {        
        $validacion = $this->validar_sitio();
        if($validacion)
        {
            // crear sesiones e insertar en tabla
            $nota_id = $this->Notas_modelo->insert_registro(); // devuelve el id insertado

            if(!empty($nota_id))
            {
                $this->session->set_userdata('nota_id', $nota_id);
                redirect("notas/contenido");
            }
        }
    }

    // paso 2 del registro de notas
    function contenido()
    {
        $this->load->helper('validador/validador');
        $validacion = $this->validar_sitio();
        $nota_id = $this->session->userdata('nota_id');

        if($validacion AND !empty($nota_id))
        {
            $sitio_id = $this->session->userdata('sitio_id_session');

            // get nombre del sitio
            $this->load->helper('sitios/sitios');
            $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
            $data['sitio_id'] = $sitio_id;

            // activar los tags del sitio de acuerdo al tipo de nota
            $tiponota = $this->session->userdata('tiponota_id');
            $data['tiponotaid'] = $tiponota;
            $tags_noticias = $this->Tags_getters_modelo->get_tags_simples($sitio_id);//tag_nombre, tag_ruta, tag_id, tipotag_id, sitio_id where flag_extendido != 1
            $tags_extendidos = $this->Tags_getters_modelo->get_tags_extendidos($sitio_id);//tag_nombre, tag_ruta, tag_id, tipotag_id, sitio_id flag_extendido = 1

            if ($tiponota != 3) // excepto artistas
            {
                $data['tags_noticia'] = $tags_noticias;                
                $data['tags'] = $tags_extendidos;
            }

            if ($tiponota == 2) // canciones
            {
                $data['featured'] = TRUE; // activar tags ft
            }

            if ($tiponota == 4 || $tiponota == 1) // rankings o noticias
            {
                $canciones = $this->Notas_getters_modelo->get_canciones_by_sitio($sitio_id);
                $data['canciones'] = $canciones;
            }

            if ($tiponota == 5) // listas
            {
                $noticias = $this->Notas_getters_modelo->get_noticias_by_sitio($sitio_id);
                $data['lista'] = $noticias;
            }
          
            $data['nota_id'] = $nota_id;

            $tags_patrocinado = $this->Tags_getters_modelo->get_tags_patrocinados($sitio_id);
            $data['tags_patrocinado'] = $tags_patrocinado;

            // tags librería
            $tags = array();
            if(is_array($tags_noticias))
                $tags = array_merge($tags, $tags_noticias);
            if(is_array($tags_extendidos))
                $tags = array_merge($tags, $tags_extendidos);
            $data['tags_libreria'] = $tags;

            $this->load->view('crear/notas_tab_2', $data);
        }
    }

    // procesamiento de datos del paso 2
    function registro_2()
    {
        $validacion = $this->validar_sitio();

        if($validacion AND $_POST)
        {
            $sitio_id = $this->session->userdata('sitio_id_session');
            $credentials = $this->Sitios_getters_modelo->get_credentials($sitio_id);//sitio_usuario_cdn, sitio_pass_cdn, sitio_ruta_cdn
            $tiponota = $this->session->userdata('tiponota_id');

            if($credentials)
            {
                if($credentials)
                {
                    $params[0] = $credentials->sitio_usuario_cdn;
                    $params[1] = $credentials->sitio_pass_cdn;
                    $params[2] = $credentials->sitio_ruta_cdn;
                    $nota_procesada = $this->Notas_modelo->insert_contenido($params); // devuelve true o false
                }

                if(!empty($nota_procesada))
                {
                    redirect("notas/adicional");
                }
                else
                {
                    redirect("notas/contenido");
                }
                

            }
        }
        else
        {
            die('No tienes acceso a este sitio');
        }
    }

    // paso 3 del registro de notas
    function adicional()
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
            $this->load->module('sitios');

            $sitio_id = $this->session->userdata('sitio_id_session');
            $nota_id = $this->session->userdata('nota_id');           
            $tiponota = $this->session->userdata('tiponota_id');

            // get nombre del sitio            
            $this->load->helper('sitios/sitios');
            $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
           
            $data['sitio_id'] = $sitio_id;
            $data['nota_id'] = $nota_id;

            if ($tiponota == 3) // artistas
            {   
                $data['artista_destacado'] = TRUE;
            }

            if ($tiponota == 1) // noticias
            {
                $data['lista'] = TRUE;
            }

            $this->load->view('crear/notas_tab_3', $data);
        }
    }

    function registro_3()
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
            if($_POST)
            {
                $publicado = $this->Notas_modelo->insert_configuracion();

                if($publicado)
                {
                    redirect('notas/publicado');
                }

                // get nombre del sitio
                $sitio_id = $this->session->userdata('sitio_id_session');            
                $this->load->helper('sitios/sitios');
                $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
                $data['nota_id'] = $this->session->userdata('nota_id');
                $this->load->view('crear/notas_tab_3', $data);
            }
            else
            {
                $sitio_id = $this->session->userdata('sitio_id_session');
                redirect('notas/index/'.$sitio_id);
            }
        } 
    }

    public function publicado()
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
            $sitio_id = $this->session->userdata('sitio_id_session');
            $nota_id = $this->session->userdata('nota_id');
            
            // get nombre del sitio            
            $this->load->helper('sitios/sitios');
            $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
            $data['sitio_id'] = $sitio_id;

            // get tipo de nota
            $tipo_nota_id = $this->Notas_getters_modelo->get_tipo_nota_id($nota_id, $sitio_id);

            if($tipo_nota_id == 1)
            {
                switch ($sitio_id) {
                    case '1':
                        $data['url_json'] = 'http://xcms.plustv.pe/json/getnota?pid=1&nid='.$nota_id;
                        $data['url_service'] = 'http://plustv.pncea.4cloud.co/services/nota_service';
                        break;
                }
            }
            
            // destruir todas las sesiones
            $this->session->unset_userdata('nota_id');
            $this->session->unset_userdata('imagen_info');
            $this->session->unset_userdata('tiponota_id');
            $this->session->unset_userdata('seccion_id');
            $this->session->unset_userdata('categoria_id');
            $this->session->unset_userdata('galeria_info');

            $this->load->view('crear/notas_tab_4', $data);
        }
    }

    public function cancelar()
    {
        $sitio_id = $this->session->userdata('sitio_id_session');
        $nota_id = $this->session->userdata('nota_id');
        $this->Notas_modelo->delete_nota($nota_id, $sitio_id);

        $this->session->unset_userdata('nota_id');
        $this->session->unset_userdata('imagen_info');
        $this->session->unset_userdata('tiponota_id');
        $this->session->unset_userdata('seccion_id');
        $this->session->unset_userdata('categoria_id');
        $this->session->unset_userdata('galeria_info');

        echo $sitio_id;
    }

    function eliminar($sitio_id, $seccion_id, $nota_id)
    {
        $resultado = $this->Notas_modelo->eliminar_nota($sitio_id, $nota_id);
        $mensaje = $resultado ? 'Nota Eliminada' : 'Hubo un error al eliminar la foto. Por favor inténtalo de nuevo';

        $this->session->set_flashdata('success', $mensaje);
        redirect("/secciones/$sitio_id/$seccion_id", 'refresh');
    }

    public function fileupload()
    {
        $this->load->view('fileupload', array('error' => ''));
    }

    public function do_upload()
    {
        $upload_path_url = base_url() . 'imagen/tmp/';

        $config['upload_path'] = FCPATH . 'imagen/tmp/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '30000';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('upload', $error);
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($error));
            
        } else {
            $data = $this->upload->data();
            // to re-size for thumbnail images un-comment and set path here and in json array
            $config = array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['full_path'];
            $config['create_thumb'] = TRUE;
            $config['new_image'] = $data['file_path'] . 'thumbs/';
            $config['maintain_ratio'] = TRUE;
            $config['thumb_marker'] = '';
            $config['width'] = 75;
            $config['height'] = 50;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            //set the data for the json array
            $info = new StdClass;
            $info->name = $data['file_name'];
            $info->size = $data['file_size'] * 1024;
            $info->type = $data['file_type'];

            // esta validacion se puso para comprobar si por esto es el error de renombre de la carpeta
            $info->url = !empty($upload_path_url) ? $upload_path_url . $data['file_name'] : base_url() . 'imagen/tmp' . $data['file_name'];

            // ruta thumb para preview
            $info->thumbnailUrl = $upload_path_url . 'thumbs/' . $data['file_name'];
            $info->deleteUrl = base_url() . 'notas/deleteImage/' . $data['file_name'];
            $info->deleteType = 'DELETE';
            $info->error = null;

            $files[] = $info;
            //this is why we put this in the constants to pass only json data
            if (IS_AJAX) {
                echo json_encode(array("files" => $files));
            } else {
                $file_data['upload_data'] = $this->upload->data();
                // TODO:
                $this->load->view('upload/upload_success', $file_data);
            }
        }
    }    

    public function deleteImage($file)
    {
        $success = unlink(FCPATH.'imagen/tmp/' .$file);
        $success = unlink(FCPATH.'imagen/tmp/thumbs/' .$file);
        //info to see if it is doing what it is supposed to
        $info = new StdClass; 
        $info->sucess = $success;
        $info->path = base_url().'imagen/tmp/' .$file;
        $info->file = is_file(FCPATH.'imagen/tmp/' .$file);

        if (IS_AJAX) {
            //I don't think it matters if this is set but good for error checking in the console/firebug
            echo json_encode(array($info));
        } else {
            //here you will need to decide what you want to show for a successful delete        
            $file_data['delete_data'] = $file;
            $this->load->view('admin/delete_success', $file_data); 
        }
    }









    public function republicar($sitio_id, $nota_id)
    {
        $data = array(
            'sitio_id' => $sitio_id,
            'nota_id' => $nota_id,
            'active' => 'adicional'
        );

        // procesar datos
        if($_POST)
        {
            $rep = $this->Notas_modelo->republicar_nota();
            if($rep)
            {
                // se redirecciona para evitar conflictos al dar 'actualizar'
                redirect('notas/republicado');
            }
        }
        $this->load->view('notas_republicar', $data);
    }

    // aviso de republicación exitoso
    public function republicado()
    {
        $data['active'] = 'sent';
        $this->load->view('notas_republicar', $data);
    }

    public function get_tags_cancion()
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
            $sitio_id = $this->session->userdata('sitio_id_session');
            $tags_id = $this->input->post('tags_id');
            // se llama al modelo de notas porque cancion es una nota
            $notas = $this->Notas_modelo->get_canciones_by_tags($tags_id, $sitio_id);
            echo json_encode($notas);
        }
    }

    function clonar($sitio_id, $seccion_id, $nota_id)
    {
        $nota = $this->Notas_modelo->get_nota_by_id($sitio_id, $nota_id);
        $data['nota'] = $nota;
        // get nombre del sitio
        $this->load->helper('sitios/sitios');
        $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
        $data['sitio_id'] = $sitio_id;
        $this->load->module('sitios/Sitios_getters');
        $usuario_id = $this->session->userdata('usuario_id');
        $sitios_id = $this->sitios_getters->list_sitios_id_by_usuario($usuario_id);
        $data['sitios'] = $this->sitios_getters->list_sitios_by_ids($sitios_id);
        $data['sitio_id_origen'] = $sitio_id;
        $data['seccion_id_origen'] = $seccion_id;
        $data['active'] = 'contenido';
        $this->load->view('notas_clonar_form', $data);
    }	
	











    function list_notas_by_seccion($seccion_id, $sitio_id)
    {
        return $this->Notas_modelo->get_by_seccion_id($seccion_id, $sitio_id);
    }

    function list_notas_by_categorias($sitio_id, $categorias_id)
    {
        $notas = $this->Notas_modelo->get_by_categorias_id($sitio_id, $categorias_id);
        return $notas;
    }

    function list_nota_by_id($sitio_id, $nota_id)
    {
        return $this->Notas_modelo->get_nota_by_id($sitio_id, $nota_id);
    }
    


}
