<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notas_editar extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model(array('notas/Notas_modelo', 'notas/Notas_getters_modelo', 'notas/Notas_editar_modelo', 'tags/Tags_getters_modelo', 'sitios/Sitios_getters_modelo'));
	}

	public function validar_sitio($sitio_id = null)
    {
        if(empty($sitio_id))
        {
            $sitio_id = $this->session->userdata('sitio_id_session');
        }
        
        $sitios = $this->session->userdata('sitios');

        if(!in_array($sitio_id, $sitios))
        {
            $this->session->set_flashdata('error', 'No tienes permisos de administración.');
            $this->load->view('sin_permiso');
            return FALSE;
        }
        return TRUE;        
    }

    public function editar($sitio_id, $seccion_id, $nota_id)
    {
        $roles = $this->session->userdata('roles');
        $usuario_id = $this->session->userdata('usuario_id');

        $validacion = $this->Notas_getters_modelo->validar_nota($sitio_id, $seccion_id, $nota_id, $usuario_id, $roles);

        if($validacion)
        {
        	$tiponota_id = $this->Notas_editar_modelo->get_tipo_nota_id($nota_id, $sitio_id);
            $nota = $this->Notas_editar_modelo->get_nota_by_id($nota_id, $sitio_id, $tiponota_id);
        	$data['tiponota_id'] = $tiponota_id;
        	$data['nota'] = $nota;
            //var_dump($data['nota']['elementos']['audio']);die();
        	$data['ruta_cdn'] = $this->Sitios_getters_modelo->get_ruta_cdn($sitio_id);

        	if($tiponota_id == 1 || $tiponota_id == 4) // noticias o rankings
	        {
	            $canciones_nota = $this->Notas_getters_modelo->get_notas_relacionadas($nota_id, $sitio_id);
	            $canciones = $this->Notas_getters_modelo->get_canciones_by_sitio($sitio_id);
                $data['canciones'] = $canciones;
	            $data['canciones_nota'] = $canciones_nota;
	        }

	        // get nombre del sitio
            $this->load->helper('sitios/sitios');
            $data['sitio_nombre'] = get_sitio_nombre($sitio_id);
            $data['sitio_id'] = $sitio_id;
	        $data['seccion_id'] = $seccion_id;

	        $tags_noticias = $this->Tags_getters_modelo->get_tags_simples($sitio_id);
            $tags_extendidos = $this->Tags_getters_modelo->get_tags_extendidos($sitio_id);

            if ($tiponota_id != 3) // excepto artistas
            {
                $data['tags_noticia'] = $tags_noticias;                
                $data['tags'] = $tags_extendidos;
            }

            if($tiponota_id == 1)
            {
                $data['lista'] = TRUE;
                $data['flag_lista'] = $nota['nota']->nota_lista;
            }

	        if($tiponota_id == 3) // si es tipo tag, conseguir tag_destacado
	        {
	            $tag_destacado = $this->Tags_getters_modelo->check_tag_destacado($nota_id, $sitio_id);
                $data['artista_destacado'] = TRUE;
	            $data['flag_destacado'] = $tag_destacado;
	        }

            if($tiponota_id == 5)
            {
                $listas_nota = $this->Notas_getters_modelo->get_notas_relacionadas($nota_id, $sitio_id);
                $lista = $this->Notas_getters_modelo->get_noticias_by_sitio($sitio_id);
                $data['listas'] = $lista;
                $data['listas_nota'] = $listas_nota;
            }

	        $tags_patrocinado = $this->Tags_getters_modelo->get_tags_patrocinados($sitio_id);
            $data['tags_patrocinado'] = $tags_patrocinado;

            // tags librería
            $tags = array();
            if(is_array($tags_noticias))
                $tags = array_merge($tags, $tags_noticias);
            if(is_array($tags_extendidos))
                $tags = array_merge($tags, $tags_extendidos);
            $data['tags_libreria'] = $tags;

	        $this->load->view('notas_edit_form', $data); 
        }
    }

    function procesar_edicion($seccion_id, $nota_id)
    {
        $sitio_id = $this->session->userdata('sitio_id_session');      
        $validacion = $this->validar_sitio($sitio_id);

        if($validacion)
        {
            if($_POST)
            {
                $credentials = $this->Sitios_getters_modelo->get_credentials($sitio_id);
                if($credentials)
                {
                    //print_r($credentials);
                    $params[0] = $credentials->sitio_usuario_cdn;
                    $params[1] = $credentials->sitio_pass_cdn;
                    $params[2] = $credentials->sitio_ruta_cdn;
                    $nota_procesada = $this->Notas_editar_modelo->update_contenido($params, $nota_id); // devuelve true o false
                }

                if(!empty($nota_procesada))
                { 
                    redirect('/secciones/'.$sitio_id.'/'.$seccion_id);            
                }
                else
                {
                    redirect("/notas/editar/$sitio_id/$seccion_id/$nota_id");
                }


            }
            else
            {
                $sitio_id = $this->session->userdata('sitio_id_session');
                redirect('sitios/'.$sitio_id);
            }
        }

    }

    public function actualiza_artista($sitio_id, $nota_id)
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
             // guardar el sitio
            $this->session->set_userdata('sitio_id_session', $sitio_id);
            $this->session->set_userdata('nota_id', $nota_id);
            $nota = $this->Notas_modelo->get_nota_by_id($sitio_id, $nota_id);

            $data['nota'] = $nota;
            $data['sitio_id'] = $sitio_id;
            $data['active'] = 'contenido';
            $this->load->view('notas_artista_form', $data);
        }
    }

    public function procesar_artista($nota_id)
    {
        $validacion = $this->validar_sitio();

        if($validacion)
        {
            if($_POST)
            {
                $sitio_id = $this->session->userdata('sitio_id_session');
                $nota_procesada = $this->Notas_modelo->update_artista($nota_id, $sitio_id); // devuelve true o false
                
                if(!empty($nota_procesada))
                {

                    $data['active'] = 'sent';
                    $this->load->view('notas_artista_form', $data);     

                }
            }
            else
            {
                $sitio_id = $this->session->userdata('sitio_id_session');
                redirect('notas/index/'.$sitio_id);
            }
        }
    }

}

