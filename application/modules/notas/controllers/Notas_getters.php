<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notas_getters extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('validador/validador');
		$this->load->model('Notas_getters_modelo');
	}

	function list_notas_by_nivel($seccion_id, $sitio_id, $nivel)
	{
		return $this->Notas_getters_modelo->get_by_nivel($seccion_id, $sitio_id, $nivel);
	}

}