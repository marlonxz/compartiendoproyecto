<ul class="nav nav-tabs nav-primary">
	<li class="<?php echo $active == 'registro' ? 'active' : '';?>">
		<a><strong>Registro</strong></a>
	</li>
	<li class="<?php echo $active == 'contenido' ? 'active' : '';?>">
		<a><strong>Contenido</strong></a>
	</li>
	<li class="<?php echo $active == 'adicional' ? 'active' : '';?>">
		<a><strong>Datos Adicionales</strong></a>
	</li>
	<li></li>
</ul>