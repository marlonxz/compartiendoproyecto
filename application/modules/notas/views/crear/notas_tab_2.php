<?php 
$titulo = "Crear Nota - ".$sitio_nombre." | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/pages/gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

<!-- markitup-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/markitup/skins/markitup/style.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/markitup/sets/textile/style.css" />

<!-- uploader -->
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload-ui-noscript.css"></noscript>

<style>
  #sortable { list-style-type: none; margin: 0; padding: 0; }
  #sortable li { margin: 0 5px 5px 5px;  font-size: 1.2em; min-height: 45px; }
  html>body #sortable li { height: 1.5em; line-height: 1.2em; }
  .ui-state-highlight { height: 1.5em; line-height: 1.2em; }

  .no-border{
    border: 0!important;
    background-color: #fff!important;
  }
  .seleccionado{
    opacity: 0.5;
    transition: opacity 0.5s;
  }

  #sortablelista { list-style-type: none; margin: 0; padding: 0; }
  #sortablelista li { margin: 0 5px 5px 5px;  font-size: 1.2em; min-height: 45px; }
  html>body #sortablelista li { height: 1.5em; line-height: 1.2em; }
  .ui-state-highlight { height: 1.5em; line-height: 1.2em; }

  .no-border{
    border: 0!important;
    background-color: #fff!important;
  }
  .seleccionado{
    opacity: 0.5;
    transition: opacity 0.5s;
  }
</style>

</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Publicar contenido en <?php echo $sitio_nombre;?></h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="font-icon font-icon-cogwheel"></i>
                      Registro
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link active" role="tab">
                    <span class="nav-link-in">
                      <span class="font-icon font-icon-pencil"></span>
                      Contenido
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="glyphicon glyphicon-plus"></i>
                      Adicionales
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="fa fa-check-circle"></i>
                      Publicado
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
              <form id="fileupload" method="post" action="<?php echo base_url();?>notas/registro-2" enctype="multipart/form-data">
                <div class="form-group row" id="notaTitulo">
                  <label for="nota_titulo" class="col-sm-2 form-control-label">Titular *</label>
                  <div class="col-sm-8">
                    <input class="form-control" type="text" name="nota_titulo" id="nota_titulo" required />
                  </div>
                  <div class="col-sm-2">
                  <button id="linkseo" type="button" class="btn btn-rounded btn-inline btn-success">Generar linkseo</button>
                  </div>
                </div>

                <div class="form-group row" id="contenido-wrapper">
                  <label for="nota_contenido" class="col-sm-2 form-control-label">Contenido *</label>
                  <div class="col-sm-10">
                    <textarea id="nota_contenido" name="nota_contenido" class="form-control" required></textarea>
                  </div>
                </div>

                <?php if(!empty($tags_noticia)):?>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Tags simples</label>
                  <div class="col-sm-10">
                    <select name="nota_tags_simples[]" class="select2" data-placeholder="Elegir uno o más tags simples" multiple="multiple">
                    <?php foreach($tags_noticia as $tn):?>
                      <option value="<?php echo $tn->tag_id;?>"><?php echo $tn->tag_nombre;?></option>
                    <?php endforeach;?>
                  </select>
                  </div>
                </div>
                <?php endif; ?>

                <?php if(!empty($tags)):?>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Tags extendidos</label>
                  <div class="col-sm-10">
                    <select name="nota_tags_extendidos[]" class="select2" data-placeholder="Elegir uno o más tags extendidos" multiple="multiple">
                    <?php foreach($tags as $tag):?>
                      <option value="<?php echo $tag->tag_id;?>"><?php echo $tag->tag_nombre;?></option>
                    <?php endforeach;?>
                  </select>
                  </div>
                </div>
                <?php endif; ?>
                
                <?php if(!empty($tags_patrocinado)):?>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Tags patrocinados</label>
                  <div class="col-sm-10">
                    <select name="nota_tags_patrocinados[]" class="select2" data-placeholder="Elegir uno o más tags patrocinados" multiple="multiple">
                    <?php foreach($tags_patrocinado as $tp):?>
                      <option value="<?php echo $tp->tag_id;?>"><?php echo $tp->tag_nombre;?></option>
                    <?php endforeach;?>
                  </select>
                  </div>
                </div>
                <?php endif; ?>

                <?php if(!empty($canciones)):?>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Canciones</label>
                  <div class="col-sm-10">
                    <button id="masCanciones" type="button" class="btn btn-rounded btn-inline btn-success">Más canciones</button>
                    <ul id="sortable">
                      <?php for($i = 1; $i <= 5; $i++):?>
                      <li order-id="i<?php echo $i;?>" id="i<?php echo $i;?>" class="ui-state-default">
                        <div class="col-sm-10">
                          <select class="form-control">
                            <option value="0">&nbsp;</option>
                            <?php foreach($canciones as $cancion):?>
                              <option value="<?php echo $cancion->nota_id;?>"><?php echo $cancion->nota_titulo.' - '.$cancion->tag_nombre;?></option>
                            <?php endforeach;?>
                          </select>
                        </div>
                      </li>
                      <?php endfor; ?>
                    </ul>
                  </div>
                </div>
                <?php endif; ?>

                <?php if(!empty($lista)):?>
                  <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Lista de Noticias</label>
                  <div class="col-sm-10">
                    <ul id="sortablelista">
                      <?php for($i = 1; $i <= 10; $i++):?>
                      <li order-id="i<?php echo $i;?>" id="i<?php echo $i;?>" class="ui-state-default">
                        <div class="col-sm-10">
                          <select class="form-control">
                            <option value="0">&nbsp;</option>
                            <?php foreach($lista as $l):?>
                              <option value="<?php echo $l->nota_id;?>"><?php echo $l->nota_titulo;?></option>
                            <?php endforeach;?>
                          </select>
                        </div>
                      </li>
                      <?php endfor; ?>
                    </ul>
                  </div>
                </div>
                <?php endif;?>

                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Elementos</label>
                  <div class="col-sm-10">
                  <div id="elementos">
                    <section class="tabs-section">
                      <div class="tabs-section-nav tabs-section-nav-icons">
                        <div class="tbl">
                          <ul class="nav" role="tablist">
                            <li class="nav-item" id="tabCancion">
                              <a class="nav-link" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <i class="glyphicon glyphicon-music"></i>
                                  Canción
                                </span>
                              </a>
                            </li>
                            <li class="nav-item" id="tabImagen">
                              <a class="nav-link active" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <span class="fa fa-image"></span>
                                  Imagen
                                </span>
                              </a>
                            </li>
                            <li class="nav-item" id="tabVideo">
                              <a class="nav-link" href="#tabs-1-tab-3" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <i class="fa fa-file-video-o"></i>
                                  Video
                                </span>
                              </a>
                            </li>
                            <li class="nav-item" id="tabGif">
                              <a class="nav-link" href="#tabs-1-tab-4" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <i class="fa fa-file-image-o"></i>
                                  Gif
                                </span>
                              </a>
                            </li>
                            <li class="nav-item" id="tabAudio">
                              <a class="nav-link" href="#tabs-1-tab-5" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <i class="fa fa-file-audio-o"></i>
                                  Audio
                                </span>
                              </a>
                            </li>
                            <li class="nav-item" id="tabGalImagen">
                              <a class="nav-link" href="#tabs-1-tab-6" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <i class="font-icon font-icon-cam-photo"></i>
                                  Galería fotos
                                </span>
                              </a>
                            </li>
                            <li class="nav-item" id="tabGalVideo">
                              <a class="nav-link" href="#tabs-1-tab-7" role="tab" data-toggle="tab">
                                <span class="nav-link-in">
                                  <i class="fa fa-film"></i>
                                  Galería videos
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div><!--.tabs-section-nav-->

                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-1">
                          <div class="form-group row">
                            <label for="cancion_nombre" class="col-sm-2 form-control-label">Nombre del archivo</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="cancion_nombre" id="cancion_nombre" placeholder="Nombre del archivo de audio" />
                            </div>
                          </div>
                          <?php if(!empty($tags_noticia)):?>
                          <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Tags simples</label>
                            <div class="col-sm-10">
                              <select name="cancion_tags_simples[]" class="select2" multiple="multiple">
                                <?php foreach($tags_noticia as $tn):?>
                                  <option value="<?php echo $tn->tag_id;?>"><?php echo $tn->tag_nombre;?></option>
                                <?php endforeach;?>
                              </select>
                            </div>
                          </div>
                          <?php endif; ?>
                          <?php if(!empty($tags)):?>
                          <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Tags extendidos</label>
                            <div class="col-sm-10">
                              <select name="cancion_tags_extendidos[]" class="select2" multiple="multiple">
                                <?php foreach($tags as $tag):?>
                                  <option value="<?php echo $tag->tag_id;?>"><?php echo $tag->tag_nombre;?></option>
                                <?php endforeach;?>
                              </select>
                            </div>
                          </div>
                          <?php endif; ?>
                          <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Canción </label>
                            <div class="col-sm-10">
                              <input type="file" id="cancion_file" name="cancion_file">
                            </div>
                          </div> 
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-2">
                          <section class="tabs-section">
                            <div class="tabs-section-nav tabs-section-nav-inline">
                              <ul class="nav no-border" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" href="#tabs-4-tab-1" role="tab" data-toggle="tab">
                                    Upload
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="#tabs-4-tab-2" role="tab" data-toggle="tab">
                                    Librería
                                  </a>
                                </li>
                              </ul>
                            </div><!--.tabs-section-nav-->

                            <div class="tab-content no-border">
                              <div role="tabpanel" class="tab-pane fade in active" id="tabs-4-tab-1">
                                <div class="form-group row">
                                  <label for="imagen_nombre" class="col-sm-2 form-control-label">Nombre de archivo *</label>
                                  <div class="col-sm-10">
                                    <input class="form-control" type="text" name="imagen_nombre" id="imagen_nombre" required />
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="imagen_leyenda" class="col-sm-2 form-control-label">Leyenda</label>
                                  <div class="col-sm-10">
                                    <input class="form-control" type="text" name="imagen_leyenda" id="imagen_leyenda" />
                                  </div>
                                </div>
                                <?php if(!empty($tags_noticia)):?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Tags simples</label>
                                  <div class="col-sm-10">
                                    <select name="imagen_tags_simples[]" class="select2" multiple="multiple">
                                      <?php foreach($tags_noticia as $tn):?>
                                        <option value="<?php echo $tn->tag_id;?>"><?php echo $tn->tag_nombre;?></option>
                                      <?php endforeach;?>
                                    </select>
                                  </div>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($tags)):?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Tags extendidos</label>
                                  <div class="col-sm-10">
                                    <select name="imagen_tags_extendidos[]" class="select2" multiple="multiple">
                                      <?php foreach($tags as $tag):?>
                                        <option value="<?php echo $tag->tag_id;?>"><?php echo $tag->tag_nombre;?></option>
                                      <?php endforeach;?>
                                    </select>
                                  </div>
                                </div>
                                <?php endif; ?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Imagen </label>
                                  <div class="col-sm-10">
                                    <input type="file" id="imagen_file" name="imagen_file" required>
                                  </div>
                                </div>   
                              </div><!--.tab-pane-->
                              
                              <div role="tabpanel" class="tab-pane fade" id="tabs-4-tab-2">
                                <div class="form-group row">
                                  <select class="select2 imagenTag" style="width: 100%" data-placeholder="Escribe un tag..." multiple>
                                    <?php 
                                    if(!empty($tags_libreria)):
                                    foreach($tags_libreria as $tag): ?>
                                      <option value="<?php echo $tag->tag_id;?>">
                                        <?php echo $tag->tag_nombre;?>
                                      </option>
                                    <?php endforeach; 
                                    endif;?>
                                  </select>

                                  <div class="box-typical-body">
                                    <div class="gallery-grid imagen-grid">
                                      <input type="hidden" id="imagenNota" name="imagenNota">
                                    </div><!--.gallery-grid-->
                                  </div><!--.box-typical-body-->
                                </div>
                              </div><!--.tab-pane-->
                            </div><!--.tab-content-->
                          </section><!--.tabs-section-->
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-3">
                          <div class="form-group row">
                            <label for="video_url" class="col-sm-2 form-control-label">URL del video</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="video_url" id="video_url" placeholder="youtube, vimeo o dailymotion" />
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="video_leyenda" class="col-sm-2 form-control-label">Leyenda</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="video_leyenda" id="video_leyenda" />
                            </div>
                          </div>
                          <?php if(!empty($tags_noticia)):?>
                          <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Tags simples</label>
                            <div class="col-sm-10">
                              <select name="video_tags_simples[]" class="select2" multiple="multiple">
                                <?php foreach($tags_noticia as $tn):?>
                                  <option value="<?php echo $tn->tag_id;?>"><?php echo $tn->tag_nombre;?></option>
                                <?php endforeach;?>
                              </select>
                            </div>
                          </div>
                          <?php endif; ?>
                          <?php if(!empty($tags)):?>
                          <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Tags extendidos</label>
                            <div class="col-sm-10">
                              <select name="video_tags_extendidos[]" class="select2" multiple="multiple">
                                <?php foreach($tags as $tag):?>
                                  <option value="<?php echo $tag->tag_id;?>"><?php echo $tag->tag_nombre;?></option>
                                <?php endforeach;?>
                              </select>
                            </div>
                          </div>
                          <?php endif; ?>
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-4">
                          <section class="tabs-section">
                            <div class="tabs-section-nav tabs-section-nav-inline">
                              <ul class="nav no-border" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" href="#tabs-5-tab-1" role="tab" data-toggle="tab">
                                    Upload
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="#tabs-5-tab-2" role="tab" data-toggle="tab">
                                    Librería
                                  </a>
                                </li>
                              </ul>
                            </div><!--.tabs-section-nav-->

                            <div class="tab-content no-border">
                              <div role="tabpanel" class="tab-pane fade in active" id="tabs-5-tab-1">
                                <div class="form-group row">
                                  <label for="gif_nombre" class="col-sm-2 form-control-label">Nombre del gif </label>
                                  <div class="col-sm-10">
                                    <input class="form-control" type="text" name="gif_nombre" id="gif_nombre" />
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="gif_leyenda" class="col-sm-2 form-control-label">Leyenda del gif</label>
                                  <div class="col-sm-10">
                                    <input class="form-control" type="text" name="gif_leyenda" id="gif_leyenda" />
                                  </div>
                                </div>
                                <?php if(!empty($tags_noticia)):?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Tags simples</label>
                                  <div class="col-sm-10">
                                    <select name="gif_tags_simples[]" class="select2" multiple="multiple">
                                      <?php foreach($tags_noticia as $tn):?>
                                        <option value="<?php echo $tn->tag_id;?>"><?php echo $tn->tag_nombre;?></option>
                                      <?php endforeach;?>
                                    </select>
                                  </div>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($tags)):?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Tags extendidos</label>
                                  <div class="col-sm-10">
                                    <select name="gif_tags_extendidos[]" class="select2" multiple="multiple">
                                      <?php foreach($tags as $tag):?>
                                        <option value="<?php echo $tag->tag_id;?>"><?php echo $tag->tag_nombre;?></option>
                                      <?php endforeach;?>
                                    </select>
                                  </div>
                                </div>
                                <?php endif; ?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Imagen </label>
                                  <div class="col-sm-10">
                                    <input type="file" id="gif_file" name="gif_file">
                                  </div>
                                </div>   
                              </div><!--.tab-pane-->
                              
                              <div role="tabpanel" class="tab-pane fade" id="tabs-5-tab-2">
                                <div class="form-group row">
                                  <select class="select2 gifTag" style="width: 100%" data-placeholder="Escribe un tag..." multiple>
                                    <?php 
                                    if(!empty($tags_libreria)):
                                    foreach($tags_libreria as $tag): ?>
                                      <option value="<?php echo $tag->tag_id;?>">
                                        <?php echo $tag->tag_nombre;?>
                                      </option>
                                    <?php endforeach; 
                                    endif;?>
                                  </select>

                                  <div class="box-typical-body">
                                    <div class="gallery-grid gif-grid">
                                      <input type="hidden" id="gifNota" name="gifNota">
                                    </div><!--.gallery-grid-->
                                  </div><!--.box-typical-body-->
                                </div>
                              </div><!--.tab-pane-->
                            </div><!--.tab-content-->
                          </section><!--.tabs-section-->
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-5">
                          <section class="tabs-section">
                            <div class="tabs-section-nav tabs-section-nav-inline">
                              <ul class="nav no-border" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" href="#tabs-6-tab-1" role="tab" data-toggle="tab">
                                    Upload
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="#tabs-6-tab-2" role="tab" data-toggle="tab">
                                    Librería
                                  </a>
                                </li>
                              </ul>
                            </div><!--.tabs-section-nav-->

                            <div class="tab-content no-border">
                              <div role="tabpanel" class="tab-pane fade in active" id="tabs-6-tab-1">
                                <div class="form-group row">
                                  <label for="audio_nombre" class="col-sm-2 form-control-label">Nombre </label>
                                  <div class="col-sm-10">
                                    <input class="form-control" type="text" name="audio_nombre" id="audio_nombre" />
                                  </div>
                                </div>
                                <?php if(!empty($tags_noticia)):?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Tags simples</label>
                                  <div class="col-sm-10">
                                    <select name="audio_tags_simples[]" class="select2" multiple="multiple">
                                      <?php foreach($tags_noticia as $tn):?>
                                        <option value="<?php echo $tn->tag_id;?>"><?php echo $tn->tag_nombre;?></option>
                                      <?php endforeach;?>
                                    </select>
                                  </div>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($tags)):?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Tags extendidos</label>
                                  <div class="col-sm-10">
                                    <select name="audio_tags_extendidos[]" class="select2" multiple="multiple">
                                      <?php foreach($tags as $tag):?>
                                        <option value="<?php echo $tag->tag_id;?>"><?php echo $tag->tag_nombre;?></option>
                                      <?php endforeach;?>
                                    </select>
                                  </div>
                                </div>
                                <?php endif; ?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Audio </label>
                                  <div class="col-sm-10">
                                    <input type="file" id="audio_file" name="audio_file">
                                  </div>
                                </div>   
                              </div><!--.tab-pane-->
                              
                              <div role="tabpanel" class="tab-pane fade" id="tabs-6-tab-2">
                                <div class="form-group row">
                                  <select class="select2 audioTag" style="width: 100%" data-placeholder="Escribe un tag..." multiple>
                                    <?php 
                                    if(!empty($tags_libreria)):
                                    foreach($tags_libreria as $tag): ?>
                                      <option value="<?php echo $tag->tag_id;?>">
                                        <?php echo $tag->tag_nombre;?>
                                      </option>
                                    <?php endforeach; 
                                    endif;?>
                                  </select>

                                  <div class="box-typical-body">
                                    <div class="gallery-grid audio-grid">
                                      <input type="hidden" id="audioNota" name="audioNota">
                                    </div><!--.gallery-grid-->
                                  </div><!--.box-typical-body-->
                                </div>
                              </div><!--.tab-pane-->
                            </div><!--.tab-content-->
                          </section><!--.tabs-section-->
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-6">
                          <section class="tabs-section">
                            <div class="tabs-section-nav tabs-section-nav-inline">
                              <ul class="nav no-border" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" href="#tabs-7-tab-1" role="tab" data-toggle="tab">
                                    Upload
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="#tabs-7-tab-2" role="tab" data-toggle="tab">
                                    Librería
                                  </a>
                                </li>
                              </ul>
                            </div><!--.tabs-section-nav-->

                            <div class="tab-content no-border">
                              <div role="tabpanel" class="tab-pane fade in active" id="tabs-7-tab-1">
                                <div class="form-group row">
                                  <label for="gal_imagen_nombre" class="col-sm-2 form-control-label">Nombre de archivo *</label>
                                  <div class="col-sm-10">
                                    <input class="form-control" type="text" name="gal_imagen_nombre" id="gal_imagen_nombre" required />
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="gal_imagen_leyenda_gen" class="col-sm-2 form-control-label">Leyenda general</label>
                                  <div class="col-sm-10">
                                    <input class="form-control" type="text" name="gal_imagen_leyenda_gen" id="gal_imagen_leyenda_gen" />
                                  </div>
                                </div>
                                <?php if(!empty($tags_noticia)):?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Tags simples</label>
                                  <div class="col-sm-10">
                                    <select name="gal_imagen_tags_simples[]" class="select2" multiple="multiple">
                                      <?php foreach($tags_noticia as $tn):?>
                                        <option value="<?php echo $tn->tag_id;?>"><?php echo $tn->tag_nombre;?></option>
                                      <?php endforeach;?>
                                    </select>
                                  </div>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($tags)):?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Tags extendidos</label>
                                  <div class="col-sm-10">
                                    <select name="gal_imagen_tags_extendidos[]" class="select2" multiple="multiple">
                                      <?php foreach($tags as $tag):?>
                                        <option value="<?php echo $tag->tag_id;?>"><?php echo $tag->tag_nombre;?></option>
                                      <?php endforeach;?>
                                    </select>
                                  </div>
                                </div>
                                <?php endif; ?>
                                <div class="form-group row">
                                  <label class="col-sm-2 form-control-label">Imágenes </label>
                                  <div class="col-sm-10">
                                    <div class="row fileupload-buttonbar">
                                      <div class="col-lg-7">
                                          <!-- The fileinput-button span is used to style the file input field as button -->
                                          <span class="btn btn-success fileinput-button">
                                              <i class="glyphicon glyphicon-plus"></i>
                                              <span>Añadir fotos...</span>
                                              <input type="file" name="userfile" multiple class="file_galeria">
                                          </span>
                                          <button type="submit" class="btn btn-primary start">
                                              <i class="glyphicon glyphicon-upload"></i>
                                              <span>Subir</span>
                                          </button>
                                          <button type="button" class="btn btn-danger delete">
                                              <i class="glyphicon glyphicon-trash"></i>
                                              <span>Borrar</span>
                                          </button>
                                          <input type="checkbox" class="toggle">
                                          <!-- The global file processing state -->
                                          <span class="fileupload-process"></span>
                                      </div>
                                      <!-- The global progress state -->
                                      <div class="col-lg-5 fileupload-progress fade">
                                          <!-- The global progress bar -->
                                          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                              <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                          </div>
                                          <!-- The extended global progress state -->
                                          <div class="progress-extended">&nbsp;</div>
                                      </div>
                                    </div>
                                    <!-- The table listing the files available for upload/download -->
                                    <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                                  </div>
                                </div>   
                              </div><!--.tab-pane-->
                              
                              <div role="tabpanel" class="tab-pane fade" id="tabs-7-tab-2">
                                <div class="form-group row">
                                  <select class="select2 galimagenTag" style="width: 100%" data-placeholder="Escribe un tag..." multiple>
                                    <?php 
                                    if(!empty($tags_libreria)):
                                    foreach($tags_libreria as $tag): ?>
                                      <option value="<?php echo $tag->tag_id;?>">
                                        <?php echo $tag->tag_nombre;?>
                                      </option>
                                    <?php endforeach; 
                                    endif;?>
                                  </select>

                                  <div class="box-typical-body">
                                    <div class="gallery-grid gal-imagen-grid">
                                      <input type="hidden" id="galimagenNota" name="galimagenNota">
                                    </div><!--.gallery-grid-->
                                  </div><!--.box-typical-body-->
                                </div>
                              </div><!--.tab-pane-->
                            </div><!--.tab-content-->
                          </section><!--.tabs-section-->
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-tab-7">
                          <?php if(!empty($tags_noticia)):?>
                          <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Tags simples</label>
                            <div class="col-sm-10">
                              <select name="gal_video_tags_simples[]" class="select2" multiple="multiple">
                                <?php foreach($tags_noticia as $tn):?>
                                  <option value="<?php echo $tn->tag_id;?>"><?php echo $tn->tag_nombre;?></option>
                                <?php endforeach;?>
                              </select>
                            </div>
                          </div>
                          <?php endif; ?>
                          <?php if(!empty($tags)):?>
                          <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Tags extendidos</label>
                            <div class="col-sm-10">
                              <select name="gal_video_tags_extendidos[]" class="select2" multiple="multiple">
                                <?php foreach($tags as $tag):?>
                                  <option value="<?php echo $tag->tag_id;?>"><?php echo $tag->tag_nombre;?></option>
                                <?php endforeach;?>
                              </select>
                            </div>
                          </div>
                          <?php endif; ?>
                          <div class="form-group row">
                            <label for="gal_video_leyenda" class="col-sm-2 form-control-label">Leyenda</label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="gal_video_leyenda" id="gal_video_leyenda" />
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                              <button id="addVideos" class="btn btn-success btn-rounded">Agregar más videos</button>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="gal_video_url" class="col-sm-2 form-control-label">Videos</label> 
                            <div class="col-sm-10" id="videos">
                              <input class="form-control" type="text" name="gal_video_url[]" placeholder="youtube, vimeo o dailymotion" /><br>
                              <input class="form-control" type="text" name="gal_video_url[]" placeholder="youtube, vimeo o dailymotion" /><br>
                              <input class="form-control" type="text" name="gal_video_url[]" placeholder="youtube, vimeo o dailymotion" /><br>
                            </div>
                          </div>
                        </div><!--.tab-pane-->
                      </div><!--.tab-content-->
                    </section><!--.tabs-section-->
                  </div>
                  </div>
                </div>

                <hr>

                <div class="form-group row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
                    <button type="submit" id="siguienteContenido" class="btn btn-rounded btn-inline btn-primary">Siguiente</button>
                  </div>
                </div>  
              </form>
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jqueryui/jquery-ui.min.js"></script>

<!-- markitup -->
<script src="<?php echo base_url();?>assets/markitup/jquery.markitup.js"></script>
<script src="<?php echo base_url();?>assets/markitup/sets/textile/set.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/match-height/jquery.matchHeight.min.js"></script>

<!-- uploader -->
<script src="<?php echo base_url();?>assets/js/jquery.fileupload.js"></script>
<script src="<?php echo base_url();?>assets/lib/jquery-validate/jquery.validate.js"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.13.1/additional-methods.js"></script>
<?php $this->load->view('footer_upload');?>


<script type="text/javascript">
$(document).ready(function() {
  // markitup
  $('#nota_contenido').markItUp(myTextileSettings);

  // linkseo
  $("#linkseo").click(function(e){
    e.preventDefault();
    var linkseo = $("#nota_titulo").val();
    if(linkseo)
    {
      $("#notaTitulo").append('<input type="hidden"  id="nota_linkseo" name="nota_linkseo" value="'+linkseo.replace(/"/g, '\'')+'">');
      $("#linkseo").prop("disabled", true);
    }
  });

  // galería (para mostrar las imágenes en librería)
  $('.gallery-item').matchHeight({
    target: $('.gallery-item .gallery-picture')
  });

  // Noticias
  $( "#sortablelista" ).sortable({
    placeholder: "ui-state-highlight"
  });
  $( "#sortablelista" ).disableSelection();

  // canciones
  $( "#sortable" ).sortable({
    placeholder: "ui-state-highlight"
  });
  $( "#sortable" ).disableSelection();

  var max_canciones = 20;
  var cancion_div = $("#sortable");
  var cancion_button = $("#masCanciones");
  var i = 6;

  // agregar más canciones
  $(cancion_button).click(function(e){
    e.preventDefault();
    if (i <= max_canciones){
      $(cancion_div).append('<li id="i'+i+'" class="ui-state-default"><div class="col-sm-10"><select class="form-control"><option>&nbsp;</option><?php if(!empty($canciones)){foreach($canciones as $cancion){ ?><option value="<?php echo $cancion->nota_id; ?>"> <?php echo addslashes(stripslashes($cancion->nota_titulo))." - ".addslashes(stripslashes($cancion->tag_nombre)); ?></option><?php }} ?></select></div></li>');
      i++;
    }
  });


  // agregar más videos
  var max_videos_fields = 13;
  var wrapper = $("#videos");
  var add_button = $("#addVideos");
  var x = 1;
  $(add_button).click(function(e){
    e.preventDefault();
    if( x < max_videos_fields){
      x++;
      $(wrapper).append('<input class="form-control" type="text" name="gal_video_url[]" placeholder="youtube, vimeo o dailymotion" /><br>');
    }
  });

  // ELEMENTOS

  // Librería de imagen
  $(".imagen-grid").on('click', '.yes_imagen', function(e){
    e.preventDefault();
    $(".imagen-col").each(function(){
      $(this).removeClass('seleccionado');
    });
    $(this).closest("div.gallery-col").addClass('seleccionado');
    var elementoId = $(this).closest("article.gallery-item").data('elementoid');
    $("#imagenNota").val(elementoId);
  });

  $(".imagen-grid").on('click', '.no_imagen', function(e){
    e.preventDefault();
    $(this).closest("div.gallery-col").removeClass('seleccionado');
    $("#imagenNota").val('');
  });

  $(".imagenTag").change(function () {

    var tags = $(".imagenTag").val();
    if(tags)
    {
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>elementos/elementos_getters/get_tags_imagen',
        data: { 'tags_id': tags },
        dataType: 'json',
        success: function(data){
          if(data != false){
            $(".imagen-grid").find('p').remove().end();
            $(".imagen-grid").find('.imagen-col').remove().end();

            $.each(data, function(i, d){
              $(".imagen-grid").append('<div class="gallery-col imagen-col"><article class="gallery-item" data-elementoid="'+d.elemento_id+'"><img class="gallery-picture" src="'+d.ruta+'square/'+d.elemento_ruta+'" alt="" height="158"><div class="gallery-hover-layout"><div class="gallery-hover-layout-in"><p class="gallery-item-title">'+d.elemento_titulo+'</p><div class="btn-group"><button type="button" class="btn yes_imagen"><i class="font-icon font-icon-ok"></i></button><button type="button" class="btn no_imagen"><i class="font-icon font-icon-del"></i></button></div></div></div></article></div>');
            });
          }else{
            $(".imagen-grid").find('p').remove().end().append('<p>Sin resultados</p>');
          }
        },
        error: function(msg){
          console.log('error');
          console.log(msg);
        }
      });
    }
    else
    {
      $(".imagen-grid").find('p').remove().end();
      $(".imagen-grid").find('.imagen-col').remove().end();
    }
  });

  // Librería de gif
  $(".gif-grid").on('click', '.yes_gif', function(e){
    console.log('click');
    e.preventDefault();
    $(".gif-col").each(function(){
      $(this).removeClass('seleccionado');
    });
    $(this).closest("div.gallery-col").addClass('seleccionado');
    var gifelementoId = $(this).closest("article.gallery-item").data('elementoid');
    $("#gifNota").val(gifelementoId);
  });

  $(".gif-grid").on('click', '.no_gif', function(e){
    e.preventDefault();
    $(this).closest("div.gallery-col").removeClass('seleccionado');
    $("#gifNota").val('');
  });

  $(".gifTag").change(function () {
    var tags = $(".gifTag").val();
    if(tags)
    {
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>elementos/elementos_getters/get_tags_gif',
        data: { 'tags_id': tags },
        dataType: 'json',
        success: function(data){
          if(data != false){
            $(".gif-grid").find('p').remove().end();
            $(".gif-grid").find('.gif-col').remove().end();

            $.each(data, function(i, d){
              $(".gif-grid").append('<div class="gallery-col gif-col"><article class="gallery-item" data-elementoid="'+d.elemento_id+'"><img class="gallery-picture" src="'+d.ruta+d.elemento_ruta+'" alt="" height="158"><div class="gallery-hover-layout"><div class="gallery-hover-layout-in"><p class="gallery-item-title">'+d.elemento_titulo+'</p><div class="btn-group"><button type="button" class="btn yes_gif"><i class="font-icon font-icon-ok"></i></button><button type="button" class="btn no_gif"><i class="font-icon font-icon-del"></i></button></div></div></div></article></div>');
            });
          }else{
            $(".gif-grid").find('p').remove().end().append('<p>Sin resultados</p>');
          }
        },
        error: function(msg){
          console.log('error');
          console.log(msg);
        }
      });
    }
    else
    {
      $(".gif-grid").find('p').remove().end();
      $(".gif-grid").find('.gif-col').remove().end();
    }
  });

  // Librería de audio
  $(".audio-grid").on('click', '.yes_audio', function(e){
    e.preventDefault();
    $(".audio-col").each(function(){
      $(this).removeClass('seleccionado');
    });
    $(this).closest("div.gallery-col").addClass('seleccionado');
    var gifelementoId = $(this).closest("article.gallery-item").data('elementoid');
    $("#audioNota").val(gifelementoId);
  });

  $(".audio-grid").on('click', '.no_audio', function(e){
    e.preventDefault();
    $(this).closest("div.gallery-col").removeClass('seleccionado');
    $("#audioNota").val('');
  });

  $(".audioTag").change(function () {
    var tags = $(".audioTag").val();
    if(tags)
    {
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>elementos/elementos_getters/get_tags_audio',
        data: { 'tags_id': tags },
        dataType: 'json',
        success: function(data){
          if(data != false){
            $(".audio-grid").find('p').remove().end();
            $(".audio-grid").find('.audio-col').remove().end();

            $.each(data, function(i, d){
              $(".audio-grid").append('<div class="gallery-col audio-col"><article class="gallery-item" data-elementoid="'+d.elemento_id+'" style="height: 158px;"><div class="gallery-doc"><div class="gallery-doc-in"><i class="fa fa-file-audio-o"></i><p>'+d.elemento_ruta+'</p></div></div><div class="gallery-hover-layout"><div class="gallery-hover-layout-in"><p class="gallery-item-title">'+d.elemento_titulo+'</p><div class="btn-group"><button type="button" class="btn yes_audio"><i class="font-icon font-icon-ok"></i></button><button type="button" class="btn no_audio"><i class="font-icon font-icon-del"></i></button></div></div></div></article></div>');
            });
          }else{
            $(".audio-grid").find('p').remove().end().append('<p>Sin resultados</p>');
          }
        },
        error: function(msg){
          console.log('error');
          console.log(msg);
        }
      });
    }
    else
    {
      $(".audio-grid").find('p').remove().end();
      $(".audio-grid").find('.audio-col').remove().end();
    }
  });

  // Librería de galería de imágenes
  var galeriasImagenes = {};

  $(".gal-imagen-grid").on('click', '.yes_gal_imagen', function(e){
    e.preventDefault();
    $(this).closest("div.gallery-col").addClass('seleccionado');
    var elementoId = $(this).closest("article.gallery-item").data('elementoid');

    galeriasImagenes[elementoId] = elementoId;
    $("#galimagenNota").val(JSON.stringify(galeriasImagenes));
  });

  $(".gal-imagen-grid").on('click', '.no_gal_imagen', function(e){
    e.preventDefault();
    $(this).closest("div.gallery-col").removeClass('seleccionado');
    var delelementoId = $(this).closest("article.gallery-item").data('elementoid');
    
    delete galeriasImagenes[delelementoId];
    if (JSON.stringify(galeriasImagenes) == '{}'){
      $("#galimagenNota").val('');
    } else {
      $("#galimagenNota").val(JSON.stringify(galeriasImagenes));
    }
  });

  $(".galimagenTag").change(function () {

    var tags = $(".galimagenTag").val();
    if(tags)
    {
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>elementos/elementos_getters/get_tags_galeria_fotos',
        data: { 'tags_id': tags },
        dataType: 'json',
        success: function(data){
          if(data != false){
            $(".gal-imagen-grid").find('p').remove().end();
            $(".gal-imagen-grid").find('.gal-imagen-col').remove().end();

            $.each(data, function(i, d){
              $(".gal-imagen-grid").append('<div class="gallery-col gal-imagen-col"><article class="gallery-item" data-elementoid="'+d.elemento_id+'"><img class="gallery-picture" src="'+d.ruta+'square/'+d.elemento_ruta+'" alt="" height="158"><div class="gallery-hover-layout"><div class="gallery-hover-layout-in"><p class="gallery-item-title">'+d.elemento_titulo+'</p><div class="btn-group"><button type="button" class="btn yes_gal_imagen"><i class="font-icon font-icon-ok"></i></button><button type="button" class="btn no_gal_imagen"><i class="font-icon font-icon-del"></i></button></div></div></div></article></div>');
            });
          }else{
            $(".gal-imagen-grid").find('p').remove().end().append('<p>Sin resultados</p>');
          }
        },
        error: function(msg){
          console.log('error');
          console.log(msg);
        }
      });
    }
    else
    {
      $(".gal-imagen-grid").find('p').remove().end();
      $(".gal-imagen-grid").find('.gal-imagen-col').remove().end();
    }
  });


  // file upload
  $('#fileupload').validate({
    rules: {
      cancion_file: {required: false, extension: 'mp3', filesize: 5242880},
      audio_file: {required: false, extension: 'mp3', filesize: 5242880},
      imagen_file: {extension: 'png|jpe?g', filesize: 1048576},
      gif_file: {required: false, extension: 'gif', filesize: 1048576}
    },
    messages: {
      cancion_file: 'El archivo debe ser mp3 y pesar menos de 5MB',
      audio_file: 'El archivo debe ser mp3 y pesar menos de 5MB',
      imagen_file: 'El archivo debe ser jpg o png, y pesar menos de 1MB',
      gif_file: 'El archivo debe ser gif y pesar menos de 1MB'
    },
    highlight: function(element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    submitHandler: function(form) {
      $('#siguienteContenido').text('Cargando...');
      $('#siguienteContenido').prop("disabled", true);
      form.submit();
    }
  });

  // TABS
  /*
    tiponota:
    noticia = 1
    canción = 2
    artista = 3
    ranking = 4
  */
  var tiponotaid = <?php echo $tiponotaid;?>;
  switch  (tiponotaid){
    case 1:
      $("#tabCancion").remove();
      break;
    case 2:
      $("#tabImagen").find('a').removeClass("active");
      $("#tabs-1-tab-2").removeClass("in active");
      $("#tabCancion").find('a').addClass("active");
      $("#tabs-1-tab-1").addClass("in active");
      $("#tabGif").remove();
      $("#tabAudio").remove();
      $("#tabGalImagen").remove();
      $("#tabGalVideo").remove();
      break;
    case 3:
      $("#tabCancion").remove();
      $("#tabGif").remove();
      $("#tabAudio").remove();
      break;
    case 4:
    case 5:
      $("#imagen_file").prop('required', false);
      $("#imagen_nombre").prop('required', false);
      $("#nota_contenido").prop('required', false); 
      $("#contenido-wrapper").hide();
      $("#tabCancion").remove();
      $("#tabGif").remove();
      $("#tabAudio").remove();
      $("#tabGalImagen").remove();
      $("#tabGalVideo").remove();
      break;
    }


  // SUBMIT
  $("#siguienteContenido").click(function(e){
    // linkseo obligatorio
    var linkseoBoton = $('#nota_linkseo').val();
    if(linkseoBoton == undefined)
    {     
      e.preventDefault();
      alert('Debes generar un linkseo');
    }

    // si es de librería la imagen de upload ya no es obligatoria
    if($("#imagenNota").val() != ''){
      $("#imagen_file").prop('required', false);
      $("#imagen_nombre").prop('required', false);
    }

    // agregar las canciones al ranking
    var div = $('#sortable');
    $("input[name='listaRanking[]']").each(function(){
      $(this).remove();
    });

    div.find('select option:selected').each(function(){
      $("#sortable").append('<input type="hidden" name="listaRanking[]" value="'+this.value+'" required>');
    });

    // agregar noticias a la lista
    var div2 = $('#sortablelista');
    $("input[name='listaNoticias[]']").each(function(){
      $(this).remove();
    });

    div2.find('select option:selected').each(function(){
      $("#sortablelista").append('<input type="hidden" name="listaNoticias[]" value="'+this.value+'" required>');
    });

  });

  // CANCEL
  $("#cancelar").click(function() {
    $.ajax({
      type: 'POST',
      url: '<?php echo base_url();?>notas/cancelar',
      data: {'nota_id': <?php echo !empty($nota_id) ? $nota_id : '0';?>},
      dataType: 'json',
      success: function(data) {
        console.log(data);
        window.location.href="<?php echo base_url();?>sitios/"+data;
      },
      error: function(msg){
        console.log(msg);
      }
    });
  });

});
</script>
<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
