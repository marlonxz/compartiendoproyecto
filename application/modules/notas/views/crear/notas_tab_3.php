<?php 
$titulo = "Crear Nota - ".$sitio_nombre." | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/timepicker/jquery.timepicker.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Publicar contenido en <?php echo $sitio_nombre;?></h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="font-icon font-icon-cogwheel"></i>
                      Registro
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <span class="font-icon font-icon-pencil"></span>
                      Contenido
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link active" role="tab">
                    <span class="nav-link-in">
                      <i class="glyphicon glyphicon-plus"></i>
                      Adicionales
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="fa fa-check-circle"></i>
                      Publicado
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
              <form method="post" action="<?php echo base_url();?>notas/registro-3">
                <div class="form-group row">
                  <label for="nota_fecha_publicacion" class="col-sm-2 form-control-label">Fecha de publicación</label>
                  <div class="col-sm-6">
                     <i class="glyphicon glyphicon-calendar"></i> <input type="text" id="datepicker" name="nota_fecha_publicacion"> <i class="glyphicon glyphicon-time"></i> <input id="tp2" type="text" name="nota_hora_publicacion"/>
                  </div>
                </div>

                <?php if(!empty($artista_destacado)):?>
                  <div class="form-group row">
                    <label for="artista_destacado" class="col-sm-2 form-control-label">Artista destacado</label>
                    <div class="col-sm-6">
                      <div class="checkbox">
                        <input value="1" type="checkbox" name="artista_destacado" id="check-3">
                        <label for="check-3"></label>
                      </div>
                    </div>
                  </div>
                <?php endif;?>

                <?php if(!empty($lista)):?>
                  <div class="form-group row">
                    <label for="nota_lista" class="col-sm-2 form-control-label">Agregar a lista</label>
                    <div class="col-sm-6">
                      <div class="checkbox">
                        <input value="1" type="checkbox" name="nota_lista" id="check-2">
                        <label for="check-2"></label>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>

                <div class="form-group row">
                  <label for="nota_portada" class="col-sm-2 form-control-label">Nota de portada</label>
                  <div class="col-sm-6">
                    <div class="checkbox">
                      <input value="1" type="checkbox" name="nota_portada" id="check-1">
                      <label for="check-1"></label>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="nota_patrocinada" class="col-sm-2 form-control-label">Nota patrocinada</label>
                  <div class="col-sm-6">
                    <div class="checkbox">
                      <input value="1" type="checkbox" name="nota_patrocinada" id="check-2">
                      <label for="check-2"></label>
                    </div>
                  </div>
                </div>

                <hr>

                <div class="form-group row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-rounded btn-inline btn-primary">Siguiente</button>
                  </div>
                </div>  
              </form>
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/lib/timepicker/jquery.timepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script>
$( function() {
  $( "#datepicker" ).datepicker({
    changeMonth: true,
    changeYear: true
  });

  // Time Picker
  $('#tp2').timepicker();
} );
</script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
