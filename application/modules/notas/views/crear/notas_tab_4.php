<?php 
$titulo = "Crear Nota - ".$sitio_nombre." | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/timepicker/jquery.timepicker.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Publicar contenido en <?php echo $sitio_nombre;?></h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="font-icon font-icon-cogwheel"></i>
                      Registro
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <span class="font-icon font-icon-pencil"></span>
                      Contenido
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="glyphicon glyphicon-plus"></i>
                      Adicionales
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link active" role="tab">
                    <span class="nav-link-in">
                      <i class="fa fa-check-circle"></i>
                      Publicado
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
              <p>Nota publicada</p>
              <?php if(!empty($url_json) AND !empty($url_service)):?>
              <p>Generar json: <a target="_blank" href="<?php echo $url_json;?>"><?php echo $url_json;?></a></p>
              <p>Generar Service: <a target="_blank" href="<?php echo $url_service;?>"><?php echo $url_service;?></a></p>
              <?php endif; ?>
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/lib/timepicker/jquery.timepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script>
$( function() {
  $( "#datepicker" ).datepicker({
    changeMonth: true,
    changeYear: true
  });

  // Time Picker
  $('#tp2').timepicker();
} );
</script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
