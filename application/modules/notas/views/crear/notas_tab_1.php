<?php 
$titulo = "Crear Nota - ".$sitio_nombre." | Zeus CMS";

echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Publicar contenido en <?php echo $sitio_nombre;?></h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link active" role="tab">
                    <span class="nav-link-in">
                      <i class="font-icon font-icon-cogwheel"></i>
                      Registro
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <span class="font-icon font-icon-pencil"></span>
                      Contenido
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="glyphicon glyphicon-plus"></i>
                      Adicionales
                    </span>
                  </div>
                </li>
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="fa fa-check-circle"></i>
                      Publicado
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="tabs-1-tab-1">
              <form method="post" action="<?php echo base_url();?>notas/registro-1">
                <div class="form-group row">
                  <label for="tipo_nota_id" class="col-sm-2 form-control-label">Tipo de contenido *</label>
                  <div class="col-sm-6">
                    <select id="tipo_nota_id" name="tipo_nota_id" class="form-control" data-placeholder="Elegir un tipo de nota" required>
                      <option value="">&nbsp;</option>
                      <?php foreach($tiponotas as $tp): ?>
                        <option value="<?php echo $tp->tipo_nota_id;?>"><?php echo $tp->tipo_nota_nombre;?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="seccion_id" class="col-sm-2 form-control-label">Sección *</label>
                  <div class="col-sm-6">
                    <select id="seccion_id" name="seccion_id" class="form-control" data-placeholder="Elegir una sección" required>
                      <option value="">&nbsp;</option>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="categoria_id" class="col-sm-2 form-control-label">Categoría *</label>
                  <div class="col-sm-6">
                    <select id="categoria_id" name="categoria_id" class="form-control" data-placeholder="Elegir una categoría" required>
                      <option value="">&nbsp;</option>
                    </select>
                  </div>
                </div>

                <hr>

                <div class="form-group row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-rounded btn-inline btn-primary">Siguiente</button>
                    
                  </div>
                </div>  
              </form>
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script type="text/javascript">
  // llamado ajax a las secciones
  $("#tipo_nota_id").change(function () {
    var tiponotaid = $("#tipo_nota_id").val();

    // limpiamos seccion si se cambia de tipo de nota
    $('#seccion_id').find('option').remove().end();
    $("#seccion_id").append('<option value="">&nbsp;</option').val('');

    // limpiamos la categoría también
    $("#categoria_id").find('option').remove().end();
    $("#categoria_id").append('<option value="">&nbsp;</option>').val('');
    $("#categoria_id").prop('required', false); // por default es false

    if(tiponotaid != "")
    {
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>secciones/secciones_getters/get_secciones',
        data: { 'tiponota_id': tiponotaid },
        dataType: 'json',
        success: function(data){
          if(data != false){
            $.each(data, function(i, d){
             $("#seccion_id").append('<option value="'+ d.seccion_id + '">'
                                  + d.seccion_nombre + '</option>', false);
            });
          }
        },
        error: function(msg){
          console.log(msg);
        }
      });
    }
  });

  // llamado ajax a las categorías
  $("#seccion_id").change(function() {
    var seccionId = $("#seccion_id").val();

    // limpiamos categoría se se cambió de sección
    $("#categoria_id").find('option').remove().end();
    $("#categoria_id").append('<option value="">&nbsp;</option>').val('');
    $("#categoria_id").prop('required', false); // por default es false

    if(seccionId != '')
    {
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>categorias/categorias_getters/get_categorias',
        data: { 'seccion_id': seccionId },
        dataType: 'json',
        success: function(data){
          if(data != false || data != ''){
            $("#categoria_id").prop('required', true); // como existen categorías se cambia a true
            $.each(data, function(i, d){
              $("#categoria_id").append('<option value="' + d.categoria_id + '">'
                                    + d.categoria_nombre + '</option>');
            })
          }
        },
        error: function(msg){
          console.log(msg);
        }
      });
    }
  });
  

</script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
