<?php 
$titulo = "Clonar - ".$sitio_nombre." | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/pages/gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

<!-- markitup-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/markitup/skins/markitup/style.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/markitup/sets/textile/style.css" />

<!-- uploader -->
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo base_url();?>assets/fileupload/css/jquery.fileupload-ui-noscript.css"></noscript>

<style>
  #sortable { list-style-type: none; margin: 0; padding: 0; }
  #sortable li { margin: 0 5px 5px 5px;  font-size: 1.2em; min-height: 45px; }
  html>body #sortable li { height: 1.5em; line-height: 1.2em; }
  .ui-state-highlight { height: 1.5em; line-height: 1.2em; }

  .no-border{
    border: 0!important;
    background-color: #fff!important;
  }
  .seleccionado{
    opacity: 0.5;
    transition: opacity 0.5s;
  }
</style>

</head>

<body class="with-side-menu">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <header class="section-header">
          <div class="tbl">
            <div class="tbl-row">
              <div class="tbl-cell">
                <h3>Clonar contenido</h3>
              </div>
            </div>
          </div>
        </header>

        <section class="tabs-section">
          <div class="tabs-section-nav tabs-section-nav-icons">
            <div class="tbl">
              <ul class="nav" role="tablist">
                <li class="nav-item">
                  <div class="nav-link" role="tab">
                    <span class="nav-link-in">
                      <i class="fa fa-edit"></i>
                      Clonar
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div><!--.tabs-section-nav-->

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
              <form id="fileupload" method="post" action="<?php echo base_url();?>clones/clonacion_nota">
                <div class="form-group row">
                  
                  <label for="nota_titulo" class="col-sm-2 form-control-label">Sitio</label>
                  <div class="col-sm-2">
                    <select name="sitio_id" style="width: 100%" id="sitios1" class="select2 form-control sitio" required>
                      <option value="0"></option>
                    <?php if(!empty($sitios)): ?>
                      <?php foreach($sitios as $sitio):?>
                        <option value="<?php echo $sitio->sitio_id;?>"><?php echo $sitio->sitio_nombre;?></option>
                      <?php endforeach; ?>
                    <?php endif; ?>
                    </select>
                  </div>
                  
                  <div id="seccionWrapper">
                    <label for="seccion_id" class="col-sm-1 form-control-label">Sección</label>
                    <div class="col-sm-2">
                    <select name="seccion_id" style="width: 100%" id="secciones1" class="select2 form-control sitio" required>
                    </select>
                    </div>
                  </div>
                  
                  <div id="categoriaWrapper">
                    <label for="categoria_id" class="col-sm-1 form-control-label">Categoría</label>
                    <div class="col-sm-2">
                      <select name="categoria_id" style="width: 100%" id="categorias1" class="select2 form-control sitio">
                      </select>
                    </div>
                  </div>
                  
                  <button id="addRol" class="btn"><i class="fa fa-arrow-up"></i> Clonar</button>
                </div>
                <input type="hidden" value="<?php echo $nota->nota_id;?>" name="noticia_id">
                <input type="hidden" value="<?php echo $sitio_id_origen;?>" name="sitio_id_origen">
                <input type="hidden" value="<?php echo $seccion_id_origen;?>" name="seccion_id_origen">
              </form>

              <hr>
              
                <div class="form-group row" id="notaTitulo">
                  <label for="nota_titulo" class="col-sm-2 form-control-label">Titular *</label>
                  <div class="col-sm-10">
                    <span class="form-control"><?php echo htmlentities($nota->nota_titulo); ?></span>
                  </div>
                </div>

                <div class="form-group row" id="notaLinkseo">
                  <label for="nota_linkseo" class="col-sm-2 form-control-label">URL *</label>
                  <div class="col-sm-10">
                    <span class="form-control"><?php echo @$nota->nota_seolink;?></span>
                  </div>
                </div>

                <div class="form-group row" id="contenido-wrapper">
                  <label for="nota_contenido" class="col-sm-2 form-control-label">Contenido *</label>
                  <div class="col-sm-10">
                    <span class="form-control text-area"><?php echo $nota->nota_contenido; ?></span>
                  </div>
                </div>  
            </div><!--.tab-pane-->
          </div><!--.tab-content-->
        </section><!--.tabs-section-->
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jqueryui/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- markitup -->
<script src="<?php echo base_url();?>assets/markitup/jquery.markitup.js"></script>
<script src="<?php echo base_url();?>assets/markitup/sets/textile/set.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/match-height/jquery.matchHeight.min.js"></script>

<!-- uploader -->
<script src="<?php echo base_url();?>assets/js/jquery.fileupload.js"></script>
<script src="<?php echo base_url();?>assets/lib/jquery-validate/jquery.validate.js"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.13.1/additional-methods.js"></script>
<?php $this->load->view('/crear/footer_upload');?>


<script type="text/javascript">
$(document).ready(function() {
  console.log('test');
    // categorias
  $("#sitios1").change(function(e){

    // limpiamos seccion si se cambia de sitio
    $('#secciones1').find('option').remove().end();
    $("#secciones1").append('<option value="">&nbsp;</option').val('');
    
    $('#categorias1').find('option').remove().end();
    $("#categorias1").append('<option value="">&nbsp;</option').val('');

    var sitio_id = $('#sitios1').val();
    
    // conseguir las secciones del sitio
    if( sitio_id != ""){
      $.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>clones/get_secciones',
        data: {'sitio_id': sitio_id},
        dataType: 'json',
        success: function(data){
          if (data != 0){
            $.each(data, function(i, d){
              console.log(d.seccion_id);
               $("#secciones1").append('<option value="'+ d.seccion_id + '">'
                                    + d.seccion_nombre + '</option>', false);
              });
          }
        },
        error: function(msg){
          console.log(msg);
        }
      });
    }
  });

  $("#secciones1").change(function(e){
    $('#categorias1').find('option').remove().end();
    $("#categorias1").append('<option value="">&nbsp;</option').val('');
    $('#categorias1').select2('val', null, false);
    var seccion_id = $('#secciones1').val();
    // conseguir las categorías del blog
    if(seccion_id != ""){
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url();?>clones/get_categorias',
          data: { 'seccion_id': seccion_id},
          dataType: 'json',
          success: function(data){
            if(data != 0){
              $.each(data, function(i, d){
               $("#categorias1").append('<option value="'+ d.categoria_id + '">'
                                    + d.categoria_nombre + '</option>', false);
              });
            } 
          },
          error: function(msg){
            console.log(msg);
          }
        });
      }
  })



  // SUBMIT
  $(".enviar_data").click(function(e){

  });

  // CANCEL
  $("#cancelar").click(function(e) {
    e.preventDefault();
    window.location.href="<?php echo base_url();?>clones/index";
  });


});
</script>
<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
