<?php defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(300);
require_once(APPPATH.'libraries/aws-library/aws-autoloader.php');
require FCPATH.'/assets/Netcarver/Textile/Parser.php';
require FCPATH.'/assets/Netcarver/Textile/DataBag.php';
require FCPATH.'/assets/Netcarver/Textile/Tag.php';

class Notas_modelo extends CI_Model
{	
	private $client = "";
	private $DB = NULL;

	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'notas';
		//$this->load->model('Elementos/Elementos_modelo');
		$this->load->model('Tags/Tags_modelo');
	}

	function insert_registro()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tipo_nota_id', 'Tipo de Nota', 'required');
		$this->form_validation->set_rules('seccion_id', 'Seccion', 'required');

		if($this->form_validation->run())
		{
			$tiponota_id = $this->input->post('tipo_nota_id');
			$seccion_id = $this->input->post('seccion_id');
			$categoria_id = $this->input->post('categoria_id');
			$sitio_id = $this->session->userdata('sitio_id_session');
			$usuario_id = $this->session->userdata('usuario_id');
			$categoria = empty(trim($categoria)) ? NULL : $categoria;

			// crear variables de sesion
			$this->session->set_userdata('tiponota_id', $tiponota_id);
			$this->session->set_userdata('seccion_id', $seccion_id);
			$this->session->set_userdata('categoria_id', $categoria_id);

			// insertar nota en BD
			$data = array(
				'sitio_id' => $sitio_id,
				'seccion_id' => $seccion_id,
				'tipo_nota_id' => $tiponota_id,
				'categoria_id' => $categoria_id,
				'usuario_id' => $usuario_id
			);

			$this->db->insert($this->__tabla, $data);
			$insert_id = $this->db->insert_id();

			return $insert_id;
		}
	}

	function insert_contenido($params)
	{
		$key = $params[0];
		$secret_key = $params[1];
		$bucket = $params[2];
		$sharedConfig = [
			'region' => $this->config->item('zeus_region'),
            'version' => $this->config->item('zeus_version')
		];

		// Create an SDK class used to share configuration across clients
		$sdk = new Aws\Sdk($sharedConfig);

		// Create an Amazon S3 client using shared configuration data
		$credentials = new Aws\Credentials\Credentials($key, $secret_key);
		$this->client = $sdk->createS3(['credentials' => $credentials, 'http' => ['verify' => false]]);

		/////////////////////////////////////////////////////////////////////////////////////

		// validación
		$this->form_validation->set_rules('nota_titulo', 'Título', 'required|trim');
		$this->form_validation->set_rules('nota_contenido', 'Contenido', 'trim');
		$this->form_validation->set_rules('nota_linkseo', 'Linkseo', 'required|trim');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$sitio_id = $this->session->userdata('sitio_id_session');
			$seccion_id = $this->session->userdata('seccion_id');
			$categoria_id = $this->session->userdata('categoria_id');
			$seccion_url = $this->_get_seccion_url($seccion_id, $sitio_id);
			$categoria_url = $this->_get_categoria_url($categoria_id, $sitio_id);
			$tiponota_id_nota = $this->session->userdata('tiponota_id');

			// contenido a guardar
			$usuario_id = $this->session->userdata('usuario_id');
			$nota_id = $this->session->userdata('nota_id');
			$nota_titulo = $this->input->post('nota_titulo');
			$nota_contenido = $this->input->post('nota_contenido');
			$nota_linkseo = $this->input->post('nota_linkseo');
			$nota_tags_extendidos = $this->input->post('nota_tags_extendidos'); // array
			$nota_tags_simples = $this->input->post('nota_tags_simples'); // array
			$nota_tags_featured = $this->input->post('nota_tags_featured'); // array
			$nota_tags_patrocinado = $this->input->post('nota_tags_patrocinado'); // array

			$linkseo = $seccion_url.$categoria_url.'/'.url_title(convert_accented_characters($nota_linkseo), '-', TRUE);
			$texto_filtrado = $this->validate_embed($nota_contenido);
	
			$nota = array(
				'nota_titulo' => $nota_titulo,
				'nota_contenido' => $nota_contenido,
				'nota_texto_rss' => $texto_filtrado,
				'nota_seolink' => $linkseo,				
			);

			$this->db->update('notas', $nota, array('nota_id' => $nota_id));

			// insertar tags
			$this->_insertar_tags_simples($nota_tags_simples);
			$this->_insertar_tags_extendidos($nota_tags_extendidos);
			$this->_insertar_tags_patrocinados($nota_tags_patrocinado);
			$this->_insertar_tags_featured($nota_tags_featured);

			// Si es tiponota TAG, insertar en tabla tags y tag_notas
			$tag_nuevo_id_array = array();
			if($tiponota_id_nota == 3)
			{
				// tabla tags
				$url = str_replace('artistas/', '', $linkseo);

				$tag_nuevo = array(
					'tag_nombre' => $nota_titulo,
					'sitio_id' => $sitio_id,
					'tipotag_id' => 3,
					'tag_estado' => '1',
					'tag_ruta' => $url,
					'flag_extendido' => '1',
					'tag_usuario_mod' => $usuario_id
				);

				$this->db->insert('tags', $tag_nuevo);
				$tag_nuevo_id = $this->db->insert_id();
				$tag_nuevo_id_array[] = $tag_nuevo_id;

				// tabla tag_notas
				$tag_notas_nuevo = array(
					'tag_id' => $tag_nuevo_id,
					'nota_id' => $nota_id,
					'tagnotas_estado' => '1'
				);
				$this->db->insert('tag_notas', $tag_notas_nuevo);
			}

			// elementos
			
			 $te_audio = 1;
			 $te_imagen = 2;
			 $te_gif= 3;
			 $te_video = 4;
			 $te_galeriafotos = 5;
			 $te_galeriavideos = 6;			
			 $te_cancion = 7;

			// imagen
			if(!empty($_FILES['imagen_file']['name']))
			{
				$nota_imagen_file = $_FILES['imagen_file'];
				$nota_imagen_nombre = $this->input->post('imagen_nombre');
				$nota_imagen_leyenda = $this->input->post('imagen_leyenda');

				if(empty($nota_imagen_file['tmp_name']) || !exif_imagetype($nota_imagen_file['tmp_name']))
				{
					$this->session->set_userdata('error_imagen', 'El archivo que subiste no es una imagen');
					return FALSE;
				} 

				// diferencia entre tiponota tag y las demás
				$tagsSimples = $this->input->post('imagen_tags_simples');
				$tagsExtendidos = $this->input->post('imagen_tags_extendidos');
				$nota_imagen_tags = $this->_get_elemento_tags($tiponota_id_nota, $tagsSimples, $tagsExtendidos, $tag_nuevo_id_array);
				
				// subir a la base de datos
				$this->store_imagen($nota_imagen_file, $nota_imagen_nombre, $nota_imagen_leyenda, $nota_id, $nota_imagen_tags, $bucket, $sitio_id, $te_imagen);
			}
			// imagen de libreria
			else
			{
				$elemento_libreria_id = $this->input->post('imagenNota');

				if(!empty($elemento_libreria_id))
				{
					$notas_elemento = array(
						'nota_id' => $nota_id,
						'elemento_id' => $elemento_libreria_id,
						'notas_elemento_estado' => '1',
						'notas_elemento_usuario_mod' => $usuario_id
					);

					$this->db->trans_start();
					$this->db->insert('notas_elemento', $notas_elemento);
					$this->db->trans_complete();
				}
			}

			// video
			$video_url = $this->input->post('video_url');
			if(!empty($video_url))
			{
				$video_leyenda = $this->input->post('video_leyenda');
				
				// diferencia entre tiponota tag y las demás
				$tagsSimples = $this->input->post('video_tags_simples');
				$tagsExtendidos = $this->input->post('video_tags_extendidos');
				$video_tags = $this->_get_elemento_tags($tiponota_id_nota, $tagsSimples, $tagsExtendidos, $tag_nuevo_id_array);

				$this->store_video($video_url, $video_leyenda, $nota_id, $video_tags, $te_video);
			}

			// gif
			$nota_gif_nuevo = $_FILES['gif_file']['name'];
			if(!empty($nota_gif_nuevo))
			{
				$gif_file = $_FILES['gif_file'];
				$gif_nombre = $this->input->post('gif_nombre');
				$gif_leyenda = $this->input->post('gif_leyenda');
				
				// diferencia entre tiponota tag y las demás
				$giftagsSimples = $this->input->post('gif_tags_simples');
				$giftagsExtendidos = $this->input->post('gif_tags_extendidos');
				$gif_tags = $this->_get_elemento_tags($tiponota_id_nota, $giftagsSimples, $giftagsExtendidos, $tag_nuevo_id_array);

				$this->store_gif($gif_file, $gif_nombre, $gif_leyenda, $gif_tags, $bucket, $te_gif);
			}
			// gif de libreria
			else
			{
				$elemento_libreria_id = $this->input->post('gifNota');

				if(!empty($elemento_libreria_id))
				{
					$notas_elemento = array(
						'nota_id' => $nota_id,
						'elemento_id' => $elemento_libreria_id,
						'notas_elemento_estado' => '1',
						'notas_elemento_usuario_mod' => $usuario_id
					);
					$this->db->insert('notas_elemento', $notas_elemento);
				}
			}

			// audio
			$audio_nuevo = $_FILES['audio_file']['name'];
			if(!empty($audio_nuevo))
			{
				$audio_file = $_FILES['audio_file'];
				$audio_nombre = $this->input->post('audio_nombre');
				
				// diferencia entre tiponota tag y las demás
				$audiotagsSimples = $this->input->post('audio_tags_simples');
				$audiotagsExtendidos = $this->input->post('audio_tags_extendidos');
				$audio_tags = $this->_get_elemento_tags($tiponota_id_nota, $audiotagsSimples, $audiotagsExtendidos, $tag_nuevo_id_array);
				
				$this->store_audio($audio_file, $audio_nombre, $audio_tags, $bucket, $te_audio);
			}
			// audio de libreria
			else
			{
				$elemento_libreria_id = $this->input->post('audioNota');

				if(!empty($elemento_libreria_id))
				{
					$notas_elemento = array(
						'nota_id' => $nota_id,
						'elemento_id' => $elemento_libreria_id,
						'notas_elemento_estado' => '1',
						'notas_elemento_usuario_mod' => $usuario_id
					);
					$this->db->insert('notas_elemento', $notas_elemento);
				}
			}

			// cancion
			$cancion_nuevo = $_FILES['cancion_file']['name'];
			if(!empty($cancion_nuevo))
			{
				$cancion_file = $_FILES['cancion_file'];
				$cancion_nombre = $this->input->post('cancion_nombre');
				
				// diferencia entre tiponota tag y las demás
				$canciontagsSimples = $this->input->post('cancion_tags_simples');
				$canciontagsExtendidos = $this->input->post('cancion_tags_extendidos');
				$cancion_tags = $this->_get_elemento_tags($tiponota_id_nota, $canciontagsSimples, $canciontagsExtendidos, $tag_nuevo_id_array);
				
				$this->store_cancion($cancion_file, $cancion_nombre, $cancion_tags, $bucket, $te_cancion);
			}
			
			// galeria fotos
			$fotos_ruta_original = $this->input->post('gal_imagen_name');
			if(!empty($fotos_ruta_original))
			{
				$fotos_nombre = $this->input->post('gal_imagen_nombre');
				$fotos_leyenda = $this->input->post('gal_imagen_leyenda');
				$fotos_leyenda_general = $this->input->post('gal_imagen_leyenda_gen');
				$fotos_leyenda = empty($fotos_leyenda_general) ? $fotos_leyenda : $fotos_leyenda_general;
				
				// diferencia entre tiponota tag y las demás
				$galtagsSimples = $this->input->post('gal_imagen_tags_simples');
				$galtagsExtendidos = $this->input->post('gal_imagen_tags_extendidos');
				$gal_imagen_tags = $this->_get_elemento_tags($tiponota_id_nota, $galtagsSimples, $galtagsExtendidos, $tag_nuevo_id_array);
				
				// subir a la base de datos
				$this->store_galeria_imagen($fotos_ruta_original, $fotos_nombre, $fotos_leyenda, $nota_id, $gal_imagen_tags, $te_galeriafotos, $bucket, $sitio_id);			
			}
			else
			{
				$galimagenNota = $this->input->post('galimagenNota');
				if(!empty($galimagenNota))
				{
					$galimagenNota = json_decode($galimagenNota, TRUE);
					$notas_elemento = array();
					foreach($galimagenNota as $fotoId)
					{
						$notas_elemento[] = array(
							'nota_id' => $nota_id,
							'elemento_id' => $fotoId,
							'notas_elemento_estado' => '1',
							'notas_elemento_usuario_mod' => $usuario_id
						);
					}
					$this->db->insert_batch('notas_elemento', $notas_elemento);
				}
			}

			// galeria videos
			$gal_videos_url = $this->input->post('gal_video_url');
			if(!empty(array_filter($gal_videos_url)))
			{
				$videos_leyenda = $this->input->post('gal_video_leyenda');

				// diferencia entre tiponota tag y las demás
				$galvideotagsSimples = $this->input->post('gal_video_tags_simples');
				$galvideotagsExtendidos = $this->input->post('gal_video_tags_extendidos');
				$galvideo_tags = $this->_get_elemento_tags($tiponota_id_nota, $galvideotagsSimples, $galvideotagsExtendidos, $tag_nuevo_id_array);

				$this->store_galeria_video($gal_videos_url, $videos_leyenda, $nota_id, $galvideo_tags, $te_galeriavideos, $sitio_id);
			}

			// lista de canciones
			$notas_cancion = $this->input->post('listaRanking');

			if(!empty($notas_cancion))
			{
				$notas_cancion = array_filter(array_map('trim', $notas_cancion));
				// get tipo nota id 
				$tiponota_id = $this->db->select('tipo_nota_id')->where('nota_id', $nota_id)->get('notas')->row();
				$relacion_notas_canciones = array();
				$i = 1;
				foreach($notas_cancion as $nota_cancion)
				{
					if(!empty(trim($nota_cancion)) AND $nota_cancion != "  ")
					{
						$relacion_notas_canciones[] = array(
						'sitio_id' => $sitio_id,
						'tipo_nota_id' => $tiponota_id->tipo_nota_id,
						'nota_principal_id' => $nota_id,
						'nota_interna_id' => $nota_cancion,
						'relacion_orden_item' => $i,
						);
						$i++;
					}
				}

				if(!empty($relacion_notas_canciones))
				{
					$this->db->insert_batch('relacion_notas', $relacion_notas_canciones);
				}	
			}

			// lista de noticias
			$lista_noticias = $this->input->post('listaNoticias');

			if(!empty($lista_noticias))
			{
				$lista_noticias = array_filter(array_map('trim', $lista_noticias));
				// get tipo nota id 
				$tiponota_id_lista = $this->db->select('tipo_nota_id')->where('nota_id', $nota_id)->get('notas')->row();
				$relacion_notas_lista = array();
				$i = 1;
				foreach($lista_noticias as $lista_noticia)
				{
					if(!empty(trim($lista_noticia)) AND $lista_noticia != "  ")
					{
						$relacion_notas_lista[] = array(
						'sitio_id' => $sitio_id,
						'tipo_nota_id' => $tiponota_id_lista->tipo_nota_id,
						'nota_principal_id' => $nota_id,
						'nota_interna_id' => $lista_noticia,
						'relacion_orden_item' => $i,
						);
						$i++;
					}
				}


				if(!empty($relacion_notas_lista))
				{
					$this->db->insert_batch('relacion_notas', $relacion_notas_lista);
				}	
			}
			return TRUE;
		}
	}

	function insert_configuracion()
	{
		// cambiar de acuerdo al tipo de nota
		$this->form_validation->set_rules('nota_portada', 'Flag Portada', 'numeric');
		$this->form_validation->set_rules('nota_patrocinada', 'Flag Patrocinada', 'numeric');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$sitio_id = $this->session->userdata('sitio_id_session');
			$usuario_id = $this->session->userdata('usuario_id');
			$nota_id = $this->session->userdata('nota_id');
			$tiponota = $this->session->userdata('tiponota_id');
			$fecha = $this->input->post('nota_fecha_publicacion');
			$hora = $this->input->post('nota_hora_publicacion');
			$nota_portada = $this->input->post('nota_portada');
			$nota_patrocinada = $this->input->post('nota_patrocinada');
			$nota_lista = empty($this->input->post('nota_lista')) ? 0 : $this->input->post('nota_lista');

			$nota_rss =  $this->input->post('nota_rss'); // array
			if(empty($nota_rss))
			{
				$nota_rss = 0;
			}

			// si es tag se guarda artista destacado
			if($tiponota == 3)
			{
				$artista_destacado = $this->input->post('artista_destacado');
				if(!empty($artista_destacado))
				{
					// conseguir el id del tag
					$this->db->select('tag_id')->from('tag_notas')->where('nota_id', $nota_id);
					$tag_id = $this->db->get()->row();

					// actualizar el tag
					$this->db->where('tag_id', $tag_id->tag_id);
					$this->db->update('tags', array('flag_destacado' => $artista_destacado));
				}
			}

			if(!empty($fecha) AND !empty($hora))
			{
				$fecha_completa = $fecha . ' ' .$hora;
				$stamp = strtotime($fecha_completa);
				$fecha_real = date('Y-m-d G:i:s', $stamp);
			}
			else if(!empty($fecha))
			{
				$stamp = strtotime($fecha);
				$fecha_real = date('Y-m-d G:i:s', $stamp);
			}
			else
			{
				$fecha_real = date(DATE_ATOM);
			}

			$data = array(
				'nota_flag_portada' => $nota_portada,
				'nota_flag_patrocinada' => $nota_patrocinada,
				'nota_fecha_publicacion' => $fecha_real,
				'nota_fech_update' => date(DATE_ATOM),
				'usuario_id' => $usuario_id,
				'nota_rss' => $nota_rss,
				'nota_lista' => $nota_lista
			);

			$this->db->update('notas', $data, array('nota_id' => $nota_id));

			return TRUE;
		}
	}

	// cuando se da click en cancelar (al momento de crear un contenido)
	function delete_nota($nota_id, $sitio_id)
	{
		// tag notas
		$this->db->where('nota_id', $nota_id);
		$this->db->delete('tag_notas');

		// notas_elemento
		$this->db->where('nota_id', $nota_id);
		$this->db->delete('notas_elemento');

		// republicacion
		$this->db->where('nota_id', $nota_id);
		$this->db->delete('notas_republicacion');

		// relacion_notas
		$this->db->where('nota_principal_id', $nota_id);
		$this->db->or_where('nota_interna_id', $nota_id);
		$this->db->delete('relacion_notas');

		// notas
		$this->db->where('nota_id', $nota_id);
		$this->db->delete('notas');
	}

	// cuando se desactiva una nota en el panel
	function eliminar_nota($sitio_id, $nota_id)
	{

		$usuario_id = $this->session->userdata('usuario_id');

		// relacion con elementos
		$this->db->where('nota_id', $nota_id);
		$this->db->update('notas_elemento', array('notas_elemento_estado' => '0', 'notas_elemento_usuario_mod' => $usuario_id));

		// relacion con tags
		$this->db->where('nota_id', $nota_id);
		$this->db->update('tag_notas', array('tagnotas_estado' => '0', 'tagnotas_usuario_mod' => $usuario_id));

		// relacion con otras notas
		$this->db->where('nota_principal_id', $nota_id);
		$this->db->or_where('nota_interna_id', $nota_id);
		$this->db->update('relacion_notas', array('relacion_estado' => '0', 'relacion_usuario_mod' => $usuario_id));

		// desactivar en tabla notas
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);
		$this->db->update('notas', array('nota_estado' => '0', 'nota_usuario_mod' => $usuario_id));

		return $this->db->affected_rows();
	}

	// funciones de elementos
	function store_imagen($file, $nombre, $leyenda, $nota_id, $tags, $bucket, $sitio_id, $te_imagen, $cliente = null)
	{
		// cliente de update
		$cliente = empty($cliente) ? NULL : $cliente;
		//rutas
		$ruta = FCPATH . 'imagen/';

		// new name and upload
        $nombre = str_replace(' ', '-', $nombre);
        $rand = substr(md5(microtime()),rand(0,26),5);
        $ext = substr($file['name'], strrpos($file['name'], '.'));
        $new_name = strtolower(url_title(convert_accented_characters($nombre.'-'.$rand))).$ext;
        $tempFile = $file['tmp_name']; // file
        $file_uploaded = $ruta.$new_name; // new name

        if(!empty($file['tmp_name']) && exif_imagetype($tempFile))
        {
        	rename($tempFile, $file_uploaded); // upload
			
			// conseguir info de la imagen
			$size = getimagesize($file_uploaded);
			$imagenes = $this->get_imagen_info($size, $sitio_id);

			// guardar en bucket del sitio y hacer resize
			$this->guardar_imagen($sitio_id, $file_uploaded, $new_name, $imagenes, $bucket, $cliente);

			// base de datos
			// insertar en elementos
			$elemento_array = array(
				'tipoelemento_id' => $te_imagen, //imagen
				'sitio_id' => $sitio_id,
				'elemento_titulo' => $nombre,
				'elemento_desc' => $leyenda,
				'elemento_ruta' => $new_name,
				'elemento_estado' => '1'
			);
			$this->db->insert('elementos', $elemento_array);
			$elemento_id = $this->db->insert_id();

			// insertar en Tamanio_elemento_foto
	        $data = array();
	        foreach($imagenes['nombre'] as $key => $value)
	        {
	            $data[] = array(
	                'elemento_id' => $elemento_id,
	                'sitio_id' => (int)$sitio_id,
	                'tamanio_elemento_nombre' => $imagenes['nombre'][$key],
	                'tamanio_elemento_ruta' => $imagenes['ruta'][$key].$new_name,
	                'tamanio_elemento_estado' => '1',
	                'tamanio_elemento_ancho' => $imagenes['ancho'][$key],
	                'tamanio_elemento_alto' => $imagenes['alto'][$key],
	            );
	        }
	        $this->db->insert_batch('tamanio_elemento_foto', $data);

	        // Insertar en Notas_elemento
	        $notas_elemento_array = array(
	        	'nota_id' => $nota_id,
	        	'elemento_id' => $elemento_id
	        );
	        $this->db->insert('notas_elemento', $notas_elemento_array);

	        // Insertar en tag elementos
	        if(!empty($tags))
	        {
	        	$tag_elementos = array();
		        foreach($tags as $tag)
		        {
		        	$tag_elementos[] = array(
		        		'elemento_id' => $elemento_id,
		        		'tag_id' => $tag
		        	);
		        }

		        $this->db->insert_batch('tag_elementos', $tag_elementos);
	        }

	        return TRUE;
		}

		return FALSE;
	}

	function store_galeria_imagen($originales, $nombre, $leyendas, $nota_id, $tags, $te_galeriafotos, $bucket, $sitio_id, $client = NULL)
	{
		$client = isset($client) ? $client : NULL;
		$usuario_id = $this->session->userdata('usuario_id');

		// rutas
		$ruta = FCPATH . 'imagen/';

		// new name and upload
        $nombre = str_replace(' ', '-', $nombre);
        $rand = substr(md5(microtime()),rand(0,26),5);
        $nombre_nuevo = array();

		$num = 1;
		ob_start(null, 0, PHP_OUTPUT_HANDLER_CLEANABLE);

		$elementos_id = array();

		foreach($originales as $key => $original)
		{
			$ext = substr($original, strrpos($original, '.'));
			$new_name = strtolower(url_title(convert_accented_characters($nombre.$num.'-'.$rand))).$ext;
			$original_file = $ruta.'tmp/'.$original;
			$file_uploaded = $ruta.$new_name;
			
			if(!empty($original) && exif_imagetype($original_file)) // validacion, solo se renombra si es una imagen
			{
				rename($original_file, $file_uploaded); // cambiar nombre de foto original

				// conseguir info de la imagen
				$size = getimagesize($file_uploaded);
				$imagenes = $this->get_imagen_info($size, $sitio_id);

				// guardar en bucket del sitio y hacer resize
				$this->guardar_imagen($sitio_id, $file_uploaded, $new_name, $imagenes, $bucket, $client);

	        	// guardar en la base de datos

				// Tabla Elementos
				if(!is_array($leyendas)) // es la leyenda general
				{
						$elementos = array(
							'tipoelemento_id' => $te_galeriafotos, // galeria de fotos
							'sitio_id' => $sitio_id,
							'elemento_titulo' => $nombre,
							'elemento_desc' => $leyendas,
							'elemento_ruta' => $new_name,
							'elemento_estado' => '1',
							'elemento_usuario_mod' => $usuario_id
						);
						$this->db->insert('elementos', $elementos);
						$elementos_id[] = $this->db->insert_id();
				}
				else
				{
						$elementos = array(
							'tipoelemento_id' => $te_galeriafotos, // galeria de fotos
							'sitio_id' => $sitio_id,
							'elemento_titulo' => $nombre,
							'elemento_desc' => $leyendas[$key],
							'elemento_ruta' => $new_name,
							'elemento_estado' => '1',
							'elemento_usuario_mod' => $usuario_id
						);

						$this->db->insert('elementos', $elementos);
						$elementos_id[] = $this->db->insert_id();
				}

				// insertar en Tamanio_elemento_foto
		        $data_tam_elem = array();
		        foreach($imagenes['nombre'] as $k => $value)
		        {
		            $data_tam_elem [] = array(
		                'elemento_id' => $elementos_id[$key],
		                'sitio_id' => (int)$sitio_id,
		                'tamanio_elemento_nombre' => $imagenes['nombre'][$k],
		                'tamanio_elemento_ruta' => $imagenes['ruta'][$k].$new_name,
		                'tamanio_elemento_estado' => '1',
		                'tamanio_elemento_ancho' => $imagenes['ancho'][$k],
		                'tamanio_elemento_alto' => $imagenes['alto'][$k],
		            );
		        }
		        $this->db->insert_batch('tamanio_elemento_foto', $data_tam_elem );
			}
			$num++;
			ob_clean();
		}

		// Tabla Relacion_elementos
		$relacion_elementos_galeria = array();
		$total = count($elementos_id);

		for($i=0; $i<$total; $i++)
		{
			$relacion_elementos_galeria[] = array(
				'tipo_elemento_id' => 6, // galeria de fotos
				'elemento_padre_id' => $elementos_id[0],
				'elemento_hijo_id' => $elementos_id[$i],
				'orden_elemento' => $i+1
			);
		}
		$this->db->trans_start();

		if(!empty($relacion_elementos_galeria))
		{
			$this->db->insert_batch('relacion_elementos', $relacion_elementos_galeria);
		}		

		// Tabla Notas elemento
		$notas_elemento = array();
		foreach($elementos_id as $elemento_id)
		{
			$notas_elemento[] = array(
				'nota_id' => $nota_id,
	        	'elemento_id' => $elemento_id,
	        	'notas_elemento_usuario_mod' => $usuario_id
			);
		}
		if(!empty($notas_elemento))
		{
			$this->db->insert_batch('notas_elemento', $notas_elemento);
		}

		// Tabla Tag elementos
		if(!empty($tags))
		{
			foreach($elementos_id as $elemento_id)
			{
				$tag_elementos = array();
				foreach($tags as $tag)
				{
					$tag_elementos[] = array(
						'tag_id' => $tag,
						'elemento_id' => $elemento_id,
						'tag_elemento_estado' => '1'
					);
				}
				$this->db->insert_batch('tag_elementos', $tag_elementos);				
			}
		}

		
		$this->db->trans_complete();

		return TRUE;
	}

	function store_video($video_url, $video_leyenda, $nota_id, $tags, $te_video)
	{
		$sitio_id = $this->session->userdata('sitio_id_session');
		$video = $this->_get_provider($video_url);
		$usuario_id = $this->session->userdata('usuario_id');

		if(is_array($video))
		{
			// elementos
			$video_array = array(
				'tipoelemento_id' => $te_video, // video
				'elemento_titulo' => $video_leyenda,
				'sitio_id' => $sitio_id,
				'elemento_desc' => $video['provider'],
				'elemento_ruta' => $video['video_id'],
				'elemento_estado' => '1',
				'elemento_usuario_mod' => $usuario_id
			);
			$this->db->insert('elementos', $video_array);
			$video_id = $this->db->insert_id();

			// notas elemento
			$ne_vid_array = array(
				'nota_id' => $nota_id,
				'elemento_id' => $video_id,
				'notas_elemento_estado' => '1',
				'notas_elemento_usuario_mod' => $usuario_id
			);
			$this->db->insert('notas_elemento', $ne_vid_array);

			// tags
			if(!empty($tags))
	        {
	        	$tag_elementos = array();
		        foreach($tags as $tag)
		        {
		        	$tag_elementos[] = array(
		        		'elemento_id' => $video_id,
		        		'tag_id' => $tag
		        	);
		        }

		        $this->db->insert_batch('tag_elementos', $tag_elementos);
	        }

			return TRUE;
		}

		return FALSE;
	}

	function store_galeria_video($videos_url, $leyenda, $nota_id, $galvideo_tags, $te_galeriavideos, $sitio_id)
	{
		$usuario_id = $this->session->userdata('usuario_id');
		$elementos_id = array();

		foreach($videos_url as $video_url)
		{
			$video = $this->_get_provider($video_url);
		    if(is_array($video))
		    {
		    	// tabla elementos
		    	$data = array(
		    		'tipoelemento_id' => $te_galeriavideos, // galeria videos id
		    		'elemento_titulo' => $leyenda,
		    		'sitio_id' => $sitio_id,
		    		'elemento_desc' => $video['provider'],
		    		'elemento_ruta' => $video['video_id'],
		    		'elemento_estado' => '1',
		    		'elemento_usuario_mod' => $usuario_id
		    	);

		    	$this->db->insert('elementos', $data);
		    	$elementos_id[] = $this->db->insert_id();
			}
		}

		$notas_elemento = array();
		foreach($elementos_id as $elemento_id)
    	{
	    	// tabla notas_elemento
	    	$notas_elemento[] = array(
	    		'nota_id' => $nota_id,
	    		'elemento_id' => $elemento_id,
	    		'notas_elemento_usuario_mod' => $usuario_id
	    	);

	    	// tags
			if(!empty($tags))
	        {
	        	$tag_elementos = array();
		        foreach($tags as $tag)
		        {
		        	$tag_elementos[] = array(
		        		'elemento_id' => $elemento_id,
		        		'tag_id' => $tag
		        	);
		        }

		        $this->db->insert_batch('tag_elementos', $tag_elementos);
	        } 	
	    }
	    $this->db->insert_batch('notas_elemento', $notas_elemento);

    	// Tabla Relacion_elementos
		$relacion_elementos = array();
		$total = count($elementos_id);

		for($i=0; $i<$total; $i++)
		{
			$relacion_elementos_videos[] = array(
				'tipo_elemento_id' => $te_galeriavideos, // galeria de videos
				'elemento_padre_id' => $elementos_id[0],
				'elemento_hijo_id' => $elementos_id[$i],
				'orden_elemento' => $i+1
			);
		}
		$this->db->insert_batch('relacion_elementos', $relacion_elementos_videos);	
	}

	function store_gif($gif_file, $titulo, $leyenda, $tags, $bucket, $te_gif, $cliente = null, $nota_id = null)
	{
		if(isset($cliente))
		{
			$this->client = $cliente;
		}
		// rename and store
        $nombre = str_replace(' ', '-', $titulo);
        $rand = substr(md5(microtime()),rand(0,26),5);
        $ext = substr($gif_file['name'], strrpos($gif_file['name'], '.'));
        $tempFile = $gif_file['tmp_name']; // original file
        $new_name = strtolower(url_title(convert_accented_characters($nombre.'-'.$rand))).$ext;

        $promise = $this->client->putObjectAsync(array(
        			'Bucket' => $bucket,
					'Key' => 'gif/'.$new_name, // nombre del archivo
					'SourceFile' => $tempFile, // ruta
					'ACL' => 'public-read',
					'StorageClass' => 'STANDARD',
					'ContentType'	=> 'image/gif'
        		));
        $result = $promise->wait();

		// store in BD
        $sitio_id = $this->session->userdata('sitio_id_session');
        $usuario_id = $this->session->userdata('usuario_id');
        if(!isset($nota_id))
        {
        	$nota_id = $this->session->userdata('nota_id');
        }
        $ruta = $nombre.'-'.$rand.$ext;

        // tabla elementos
        $elementos = array(
        	'tipoelemento_id' => $te_gif, // gif id
        	'sitio_id' => $sitio_id,
        	'elemento_titulo' => $titulo,
        	'elemento_desc' => $leyenda,
        	'elemento_ruta' => $ruta,
        	'elemento_estado' => '1',
        	'elemento_usuario_mod' => $usuario_id
        );
        $this->db->trans_start();
        $this->db->insert('elementos', $elementos);
        $elemento_id = $this->db->insert_id();

        // tabla tag_elementos
        if(!empty($tags))
        {
        	$tag_elementos = array();
        	foreach($tags as $tag)
        	{
        		$tag_elementos[] = array(
        			'tag_id' => $tag,
        			'elemento_id' => $elemento_id,
        			'tag_elemento_estado' => '1'
        		);
        	}
        	$this->db->insert_batch('tag_elementos', $tag_elementos);
        }

        // tabla notas_elementos
        $notas_elemento = array(
        	'nota_id' => $nota_id,
        	'elemento_id' => $elemento_id,
        	'notas_elemento_estado' => '1',
        	'notas_elemento_usuario_mod' => $usuario_id
        );
        $this->db->insert('notas_elemento', $notas_elemento);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
        	log_message('error', 'Problema al insertar gif en la nota: '.$nota_id);
        }

	}

	function store_audio($audio_file, $titulo, $tags, $bucket, $tipoelemento_id, $cliente = null, $nota_id = null)
	{
		if(isset($cliente))
		{
			$this->client = $cliente;
		}

		// rutas
        $nombre = str_replace(' ', '-', $titulo);
        $rand = substr(md5(microtime()),rand(0,26),5);
        $ext = substr($audio_file['name'], strrpos($audio_file['name'], '.'));
        $tempFile = $audio_file['tmp_name']; // original file
        $new_name = strtolower(url_title(convert_accented_characters($nombre.'-'.$rand))).$ext;

        $promise = $this->client->putObjectAsync(array(
        			'Bucket' => $bucket,
					'Key' => 'audio/'.$new_name, // nombre del archivo
					'SourceFile' => $tempFile, // ruta
					'ACL' => 'public-read',
					'StorageClass' => 'STANDARD',
					'ContentType' => 'audio/mpeg3'
        		));
        $result = $promise->wait();

		// store in BD
        $sitio_id = $this->session->userdata('sitio_id_session');
        $usuario_id = $this->session->userdata('usuario_id');
        if(!isset($nota_id))
        {
        	$nota_id = $this->session->userdata('nota_id');
        }
        $ruta = $new_name;

        // tabla elementos
        $elementos = array(
        	'tipoelemento_id' => $tipoelemento_id, // audio id
        	'sitio_id' => $sitio_id,
        	'elemento_titulo' => $titulo,
        	'elemento_ruta' => $ruta,
        	'elemento_estado' => '1',
        	'elemento_usuario_mod' => $usuario_id
        );
        $this->db->trans_start();
        $this->db->insert('elementos', $elementos);
        $elemento_id = $this->db->insert_id();

        // tabla tag_elementos
        if(!empty($tags))
        {
        	$tag_elementos = array();
        	foreach($tags as $tag)
        	{
        		$tag_elementos[] = array(
        			'tag_id' => $tag,
        			'elemento_id' => $elemento_id,
        			'tag_elemento_estado' => '1'
        		);
        	}
        	$this->db->insert_batch('tag_elementos', $tag_elementos);
        }

		// tabla notas_elementos
        $notas_elemento = array(
        	'nota_id' => $nota_id,
        	'elemento_id' => $elemento_id,
        	'notas_elemento_estado' => '1',
        	'notas_elemento_usuario_mod' => $usuario_id
        );
        $this->db->insert('notas_elemento', $notas_elemento);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
        	log_message('error', 'Problema al insertar audio en la nota: '.$nota_id);
        }
	}

	function store_cancion($audio_file, $titulo, $tags, $bucket, $tipoelemento_id, $cliente = null, $nota_id = null)
	{
		if(isset($cliente))
		{
			$this->client = $cliente;
		}
		// rutas
        $nombre = str_replace(' ', '-', $titulo);
        $rand = substr(md5(microtime()),rand(0,26),5);
        $ext = substr($audio_file['name'], strrpos($audio_file['name'], '.'));
        $tempFile = $audio_file['tmp_name']; // original file
        $new_name = strtolower(url_title(convert_accented_characters($nombre.'-'.$rand))).$ext;

        $promise = $this->client->putObjectAsync(array(
			'Bucket' => $bucket,
			'Key' => 'audio/'.$new_name, // nombre del archivo
			'SourceFile' => $tempFile, // ruta
			'ACL' => 'public-read',
			'StorageClass' => 'STANDARD',
			'ContentType' => 'audio/mpeg3'
		));
        $result = $promise->wait();
        

		// store in BD
        $sitio_id = $this->session->userdata('sitio_id_session');
        $usuario_id = $this->session->userdata('usuario_id');
        if(!isset($nota_id))
        {
        	$nota_id = $this->session->userdata('nota_id');
        }
        
        $ruta = $new_name;

        // tabla elementos
        $elementos = array(
        	'tipoelemento_id' => $tipoelemento_id, // cancion id
        	'sitio_id' => $sitio_id,
        	'elemento_titulo' => $titulo,
        	'elemento_ruta' => $ruta,
        	'elemento_estado' => '1',
        	'elemento_usuario_mod' => $usuario_id
        );
       
        $this->db->trans_start();
        $this->db->insert('elementos', $elementos);
        $elemento_id = $this->db->insert_id();

        // tabla tag_elementos
        if(!empty($tags))
        {
        	$tag_elementos = array();
        	foreach($tags as $tag)
        	{
        		$tag_elementos[] = array(
        			'tag_id' => $tag,
        			'elemento_id' => $elemento_id,
        			'tag_elemento_estado' => '1'
        		);
        	}
        	$this->db->insert_batch('tag_elementos', $tag_elementos);
        }

		// tabla notas_elementos
        $notas_elemento = array(
        	'nota_id' => $nota_id,
        	'elemento_id' => $elemento_id,
        	'notas_elemento_estado' => '1',
        	'notas_elemento_usuario_mod' => $usuario_id
        );
        $this->db->insert('notas_elemento', $notas_elemento);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
        	log_message('error', 'Problema al insertar audio en la nota: '.$nota_id);
        }
	}

	function guardar_imagen($sitio_id, $file_uploaded, $name, $tamaños, $bucket, $cliente = null)
	{
		if (isset($cliente))
		{
			$this->client = $cliente;
		}

		$ext = substr($name, strrpos($name, '.'));

        // resize
        $this->load->library('Image_moo');
        foreach($tamaños['nombre'] as $key => $value)
        {
        	$this->image_moo->load($file_uploaded)->resize_crop($tamaños['ancho'][$key], $tamaños['alto'][$key])->save('imagen'.$tamaños['ruta'][$key].$name, TRUE);
        }
        $this->image_moo->clear();

        switch ($ext) {
        	case '.jpeg':
        	case '.jpg':
        		$mime = 'image/jpeg';
        		break;
        	case '.png':
        		$mime = 'image/png';
        		break;
        }

        // subir a CDN
        $directorios = array('original/');
        foreach($tamaños['nombre'] as $nombre)
        {
        	$directorios[] = $nombre.'/';
        }

        foreach($directorios as $folder)
        {
        	$promise = $this->client->putObjectAsync(array(
        			'Bucket' => $bucket,
					'Key' => 'imagen/'.$folder.$name, // nombre del archivo
					'SourceFile' => 'imagen/'.$folder.$name, // ruta
					'ACL' => 'public-read',
					'StorageClass' => 'STANDARD',
					'ContentType' => $mime
        		));
        	$result = $promise->wait();
        }
	}

	function get_imagen_info($imagen_info, $sitio_id)
	{
		$this->db->select('ti.tamano_nombre nombre, ti.tamano_ancho ancho, ti.tamano_alto alto');
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('tamano_estado', '1');
		$tamaños = $this->db->get('tamanos_imagen_sitio ti')->result_array();

        $imagenes['nombre'] = array('original');
        $imagenes['ancho'] = array($imagen_info[0]);
        $imagenes['alto'] = array($imagen_info[1]);
        $imagenes['ruta'] = array('/original/');

        foreach($tamaños as $tamaño)
        {
        	$imagenes['nombre'][] = $tamaño['nombre'];
        	$imagenes['ancho'][] = $tamaño['ancho'];
        	$imagenes['alto'][] = $tamaño['alto'];
        	$imagenes['ruta'][] = '/'.$tamaño['nombre'].'/';
        }

		return $imagenes;
	}

	// funciones de tags
	function _insertar_tags_simples($tags_simples)
	{
		if(!empty($tags_simples))
		{
			$usuario_id = $this->session->userdata('usuario_id');
			$nota_id = $this->session->userdata('nota_id');

			$tag_notas_noticia = array();
			foreach($tags_simples as $tag)
			{
				$tag_notas_noticia[] = array(
					'tag_id' => $tag,
					'nota_id' => $nota_id,
					'tagnotas_estado' => '1',
					'tagnotas_usuario_mod' => $usuario_id
				);
			}
			$this->db->insert_batch('tag_notas', $tag_notas_noticia);
		}

		return FALSE;
	}

	function _insertar_tags_extendidos($tags_extendidos)
	{
		if(!empty($tags_extendidos))
		{
			$usuario_id = $this->session->userdata('usuario_id');
			$nota_id = $this->session->userdata('nota_id');

			$tag_notas = array();
			foreach($tags_extendidos as $tag)
			{
				$tag_notas[] = array(
					'tag_id' => $tag,
					'nota_id' => $nota_id,
					'tagnotas_estado' => '1',
					'tagnotas_usuario_mod' => $usuario_id
				);
			}
			$this->db->insert_batch('tag_notas', $tag_notas);
		}

		return FALSE;
	}

	function _insertar_tags_patrocinados($tags_patrocinados)
	{
		if(!empty($tags_patrocinados))
		{
			$usuario_id = $this->session->userdata('usuario_id');
			$nota_id = $this->session->userdata('nota_id');

			foreach($tags_patrocinado as $tag)
			{
				$tag_notas_patrocinado[] = array(
					'tag_id' => $tag,
					'nota_id' => $nota_id,
					'tagnotas_estado' => '1',
					'tagnotas_usuario_mod' => $usuario_id
				);
			}
			$this->db->insert_batch('tag_notas', $tag_notas_patrocinado);
		}

		return FALSE;
	}

	function _insertar_tags_featured($tags_featured)
	{
		if(!empty($tags_featured))
		{
			$usuario_id = $this->session->userdata('usuario_id');
			$nota_id = $this->session->userdata('nota_id');

			$tags_featured = array();
			foreach($nota_tags_featured as $tag_ft)
			{
				$tags_featured[] = array(
					'tag_id' => $tag_ft,
					'nota_id' => $nota_id,
					'tag_featured' => '1',
					'tagnotas_estado' => '1',
					'tagnotas_usuario_mod' => $usuario_id
				);
			}
			$this->db->insert_batch('tag_notas', $tags_featured);
		}

		return FALSE;
	}

	function _get_elemento_tags($tiponota_id, $tagsSimples, $tagsExtendidos, $tag_nuevo_id)
	{
		$nota_imagen_tags = array();
		if($tiponota_id == 3)
		{
			$nota_imagen_tags = $tag_nuevo_id;
		}
		else
		{
			if(!empty($tagsSimples))
			{
				if(!empty($tagsExtendidos))
				{
					$nota_imagen_tags = array_merge($tagsSimples, $tagsExtendidos);
				}
				else
				{
					$nota_imagen_tags = $tagsSimples;
				}
			}
			else if(!empty($tagsExtendidos))
			{
				$nota_imagen_tags = $tagsExtendidos;
			}
		}
		return $nota_imagen_tags;
	}

	// funciones de linkseo
	function _get_categoria_url($categoria_id, $sitio_id)
	{
		if($categoria_id AND $sitio_id)
		{
			$this->load->helper('text');
			$query = $this->db->select('categoria_nombre')->where('categoria_id', $categoria_id)->get('categorias');
			if($query->num_rows() > 0)
			{
				$cat = $query->row();
				$categoria = strtolower(url_title(convert_accented_characters($cat->categoria_nombre)));
				
				return '/'.$categoria;				
			}
		}
		return '';
	}

	function _get_seccion_url($seccion_id, $sitio_id)
	{
		if($seccion_id AND $sitio_id)
		{
			$this->load->helper('text');
			$query = $this->db->select('seccion_nombre')->where('seccion_id', $seccion_id)->get('secciones');
			if($query->num_rows() > 0)
			{
				$sec = $query->row();
				$seccion = strtolower(url_title(convert_accented_characters($sec->seccion_nombre)));
				return $seccion;
			}
		}
		return '';
	}

	// helpers
	function _get_provider($video_url)
	{
	    $pattern_youtube = '#^(?:https?://|//)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com/(?:embed/|v/|watch\?v=|watch\?.+&v=))([\w-]{11})(?![\w-])#';
	    $pattern_daily = '/(?:dailymotion\.com(?:\/video|\/hub)|dai\.ly)\/([0-9a-z]+)(?:[\-_0-9a-zA-Z]+#video=([a-z0-9]+))?/';
	    $pattern_vimeo = '/[http|https]+:\/\/(?:www\.|)vimeo\.com\/([a-zA-Z0-9_\-]+)(&.+)?/i';

	     if(strpos($video_url,'youtu.be') !== FALSE || strpos($video_url,'youtube') !== FALSE)
	     {
	     	preg_match($pattern_youtube, $video_url, $matches);
	     	$provider = 'youtube';
	     }
	     
	     else if (strpos($video_url,'vimeo') !== FALSE )
	     {
	     	preg_match($pattern_vimeo, $video_url, $matches);
	     	$provider = 'vimeo';
	     }
	     else if (strpos($video_url,'dai.ly') !== FALSE || strpos($video_url,'dailymotion') !== FALSE)
	     {
	     	preg_match($pattern_daily, $video_url, $matches);
	     	$provider = 'dailymotion';
	     }else{
	     	return FALSE;
	     }	     	
	    if(!empty($matches))
	    {
	    	$video_id = $matches[1];
		    $video = array('provider'=> $provider, 'video_id' => $video_id);
		    
		    return $video;
	    }
	    return FALSE;
	}		










	
	function get_nota_by_id($sitio_id,  $nota_id)
	{

		$this->db->from('notas');
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);

		return $this->db->get()->row();		
	}

	

	function get_fech_pub($sitio_id, $nota_id)
	{
		$this->_select_database($sitio_id);

		$this->db->select('nota_fecha_publicacion');
		$this->db->from($this->__tabla);
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$fech_pub = $query->row();
			return $fech_pub->nota_fecha_publicacion;
		}
	}

	function get_info_gapi($sitio_id, $nota_id)
	{

		$this->_select_database($sitio_id);

		$this->db->select('notas.nota_fecha_publicacion, CONCAT("http://",s.sitio_ruta_cdn, "/imagen",tef.tamanio_elemento_ruta) as imagen');
		$this->db->from($this->__tabla);
		$this->db->join('notas_elemento as ne', 'ne.nota_id = notas.nota_id');
		$this->db->join('elementos as e', 'e.elemento_id = ne.elemento_id');
		$this->db->join('tamanio_elemento_foto as tef', 'tef.elemento_id = e.elemento_id');
		$this->db->join('sitios as s', 's.sitio_id = notas.sitio_id');
		$this->db->where('e.tipoelemento_id', 2);
		$this->db->where('tef.tamanio_elemento_nombre', 'apaisado');
		$this->db->where('notas.nota_id', $nota_id);

		return $this->db->get()->row();	
		
	}

	function get_categoria($sitio_id, $nota_id)
	{
		$this->_select_database($sitio_id);

		$this->db->select('categoria_id');
		$this->db->where('sitio_id', $sitio_id);
		$this->db->where('nota_id', $nota_id);
		$query = $this->db->get('notas');
		if($query->num_rows() > 0)
		{
			$categoria = $query->row();
			return $categoria->categoria_id;
		}
	}


	// funciones para el instant article
	function validate_embed($texto){
		$contenido = "";

		if(!empty($texto))
		{
			if (strpos($texto, 'notextile') !== false) {
			    return $contenido;
			}
			else
			{
				$parser = new Netcarver\Textile\Parser();
				$contenido = str_replace('&quot;', '', $texto);
				$contenido = $parser->textileThis($contenido);
				$contenido = $this->replace_tag($contenido);
				$contenido = strip_tags($contenido, '<p><a><br><ul><ol><li>');
			}
		}
		
		return $contenido;


	}

	function replace_tag($texto){
		$search  = array('h2', 'h1', 'h3', 'h4', 'h5', 'h6');
	    $replace = 'p';
	    return str_replace($search, $replace, $texto);
	}

}