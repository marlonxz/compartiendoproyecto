<?php defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(300);

class Notas_editar_modelo extends CI_Model
{
	public $cliente;
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'notas';
		$this->load->model('elementos/Elementos_getters_modelo');
	}

	function get_nota_by_id($nota_id, $sitio_id, $tiponota_id)
	{
		$nota = array();

		// datos básicos de la nota
		$this->db->from('notas');
		$this->db->where('nota_id', $nota_id);
		$nota['nota'] = $this->db->get()->row();

		// elementos de la nota
		switch ($tiponota_id) 
		{
			case 1:
				$nota['elementos'] = $this->_get_elementos_noticia($nota_id, $sitio_id);
				break;
			case 2:
				$nota['elementos'] = $this->_get_elementos_cancion($nota_id, $sitio_id);
				break;
			case 3:
				$nota['elementos'] = $this->_get_elementos_artista($nota_id, $sitio_id);
				break;
			case 4:
				$nota['elementos'] = $this->_get_elementos_ranking($nota_id, $sitio_id);
				break;
		}

		// tags de la nota
		$this->load->module('tags/tags_getters');
		$nota['nota']->tags_simples = $this->tags_getters->get_tags_simples_by_nota($nota_id);
		$nota['nota']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_nota($nota_id);
		$nota['nota']->tags_patrocinados = $this->tags_getters->get_tags_patrocinados_by_nota($nota_id);

		return $nota;
	}

	function get_tipo_nota_id($nota_id, $sitio_id)
	{
		$this->db->select('tipo_nota_id');
		$this->db->from($this->__tabla);
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$tipo_id = $query->row();
			return $tipo_id->tipo_nota_id;
		}
	}

	function update_contenido($params, $nota_id)
	{
		$key = $params[0];
		$secret_key = $params[1];
		$bucket = $params[2];

		$sharedConfig = [
			'region' => 'us-east-1',
			'version' => 'latest'
		];

		// Create an SDK class used to share configuration across clients
		$sdk = new Aws\Sdk($sharedConfig);

		// Create an Amazon S3 client using shared configuration data
		$credentials = new Aws\Credentials\Credentials($key, $secret_key);
		$this->cliente = $sdk->createS3(['credentials' => $credentials, 'http' => ['verify' => false]]);

		////////////////////////////////////////////////////////////////////////////////////

		$this->form_validation->set_rules('nota_titulo', 'Título', 'required|trim');
		$this->form_validation->set_rules('nota_contenido', 'Contenido', 'trim');
		$action = $this->input->post('action');

		if($this->form_validation->run())
		{
			// contenido a guardar
			$usuario_id = $this->session->userdata('usuario_id');
			$sitio_id = $this->session->userdata('sitio_id_session');
			$nota_titulo = $this->input->post('nota_titulo');
			$nota_contenido = $this->input->post('nota_contenido');
			$nota_seolink = $this->input->post('nota_linkseo');
			$nota_portada = $this->input->post('nota_portada');
			$nota_patrocinada = $this->input->post('nota_patrocinada');
			$nota_lista = empty($this->input->post('nota_lista')) ? 0 : $this->input->post('nota_lista');
			//$nota_estado = $this->input->post('nota_estado');

			// obtener el id del tipo de nota
			$tiponota_id = $this->get_tipo_nota_id($nota_id, $sitio_id);	

			// FB instant
			$nota_rss = $this->input->post('nota_rss');
			if(empty($nota_rss))
			{
				$nota_rss = 0;
			}

			//$texto_filtrado = $this->validate_embed($nota_contenido);
			$texto_filtrado = "";

			if($action === "corregir")
			{
				$nota = array(
				'nota_titulo' => $nota_titulo,
				'nota_contenido' => $nota_contenido,
				'nota_texto_rss' => $texto_filtrado,
				'nota_fech_update' => date("Y-m-d G:i:s"),
				'nota_usuario_mod'  =>  $usuario_id,
				'nota_flag_portada' => $nota_portada,
				'nota_flag_patrocinada' => $nota_patrocinada,
				'nota_seolink' => $nota_seolink,
				'nota_rss' => $nota_rss,
				'nota_lista' => $nota_lista
				);

			}
			else if($action === "actualizar") 
			{
				//$nota_fech_publicacion = $this->get_fech_pub($sitio_id, $nota_id);
				$fech_actualizacion = date("Y-m-d G:i:s");

				$nota = array(
				'nota_titulo' => $nota_titulo,
				'nota_contenido' => $nota_contenido,
				'nota_texto_rss' => $texto_filtrado,
				'nota_fech_update' => $fech_actualizacion,
				'nota_flag_portada' => $nota_portada,
				'nota_flag_patrocinada' => $nota_patrocinada,
				'nota_fecha_publicacion' => $fech_actualizacion,
				'nota_usuario_mod'  =>  $usuario_id,
				'nota_seolink' => $nota_seolink,
				'nota_rss' => $nota_rss,
				'nota_lista' => $nota_lista
				);

				/*if(!empty($nota_estado))
				{
					$nota['nota_estado'] = '1';
				}
				else 
				{
					$nota['nota_estado'] = '2';
				}*/
			}

			$this->db->update('notas', $nota, array('nota_id' => $nota_id, 'sitio_id' => $sitio_id ));

			// TAGS
			$tag_id_array = array();
			if(!empty($this->input->post('check_nota_tags')))
			{
				$nota_tags_extendidos = $this->input->post('nota_tags_extendidos'); // array
				$nota_tags_simples = $this->input->post('nota_tags_simples'); // array
				$nota_tags_featured = $this->input->post('nota_tags_featured'); // array
				$nota_tags_patrocinado = $this->input->post('nota_tags_patrocinado'); // array

				$this->_update_tags($nota_tags_extendidos, $nota_tags_simples, $nota_tags_patrocinado, $nota_tags_featured, $nota_id);
			}
			

			// si es tiponota artista, actualizar artista destacado
			if($tiponota_id == 3)
			{
				$artista_destacado = $this->input->post('artista_destacado');

				$this->db->select('tn.tag_id')
						->from('tag_notas tn')
						->join('tags', "tags.tag_id = tn.tag_id")
						->where('tags.tipotag_id', 3)
						->where('tn.nota_id', $nota_id);
				$tag_id = $this->db->get()->row();

				if(!empty($artista_destacado))
				{
					// activar el tag
					$this->db->where('tag_id', $tag_id->tag_id);
					$this->db->update('tags', array('flag_destacado' => '1'));
				}
				else
				{
					// desactivar el tag
					$this->db->where('tag_id', $tag_id->tag_id);
					$this->db->update('tags', array('flag_destacado' => '0'));
				}

				// para los tags de los elementos
				$tag_id_array[] = $nota_id;
			}

			// si es tipo ranking o noticia, borrar la relacion anterior e insertar nueva
			// agregar validación de check onchange
			$check_lista_canciones = $this->input->post('check_lista_canciones');
			if(($tiponota_id == 1 || $tiponota_id == 4) AND $check_lista_canciones)
			{
				$canciones_ids = $this->input->post('listaRanking');

				// borrar relación anterior
				$this->db->where('nota_principal_id', $nota_id);
				$this->db->delete('relacion_notas');

				// insertar nueva relación
				unset($i);
				$i = 1;
				$this->db->trans_start();
				$canciones_ids = array_filter(array_map('trim', $canciones_ids));
				foreach($canciones_ids as $id)
				{
					$relacion = array(
						'sitio_id' => $sitio_id,
						'tipo_nota_id' => $tiponota_id,
						'nota_principal_id' => $nota_id,
						'nota_interna_id' => $id,
						'relacion_orden_item' => $i,
						'relacion_fech_update' => date("Y-m-d G:i:s"),
						'relacion_usuario_mod' => $usuario_id
					);
					$i++;
					$this->db->insert('relacion_notas', $relacion);
				}
				$this->db->trans_complete();
			}

			$check_lista_noticias = $this->input->post('check_lista_noticias');

			if(($tiponota_id == 5) AND $check_lista_noticias)
			{
				$noticias_id = $this->input->post('listaNoticia');
				//var_dump($noticias_id);die();
				// borrar relación anterior
				$this->db->where('nota_principal_id', $nota_id);
				$this->db->delete('relacion_notas');

				// insertar nueva relación
				unset($i);
				$i = 1;
				$this->db->trans_start();
				$noticias_id = array_filter(array_map('trim', $noticias_id));
				foreach($noticias_id as $id_list)
				{
					$relacion_noticia = array(
						'sitio_id' => $sitio_id,
						'tipo_nota_id' => $tiponota_id,
						'nota_principal_id' => $nota_id,
						'nota_interna_id' => $id_list,
						'relacion_orden_item' => $i,
						'relacion_fech_update' => date("Y-m-d G:i:s"),
						'relacion_usuario_mod' => $usuario_id
					);
					$i++;
					$this->db->insert('relacion_notas', $relacion_noticia);
				}
				$this->db->trans_complete();
			}

			// ELEMENTOS
			$te_audio = 1;
			$te_imagen = 2;
			$te_gif= 3;
			$te_video = 4;
			$te_galeriafotos = 5;
			$te_galeriavideos = 6;			
			$te_cancion = 7;

			// IMAGEN
        	$imagen_id = $this->input->post('nota_imagen_id', TRUE);
        	$imagen_file = $_FILES['imagen_file'];
        	$imagen_nombre = $this->input->post('imagen_nombre', TRUE);
			$imagen_leyenda = $this->input->post('imagen_leyenda', TRUE);

			// tags: diferencia entre tiponota tag y las demás
			$imagentagsSimples = $this->input->post('imagen_tags_simples');
			$imagentagsExtendidos = $this->input->post('imagen_tags_extendidos');
			$imagen_tags = $this->_get_elemento_tags($tiponota_id, $imagentagsSimples, $imagentagsExtendidos, $tag_id_array);

			$this->_update_imagen($imagen_id, $imagen_file, $imagen_nombre, $imagen_leyenda, $nota_id, $imagen_tags, $bucket, $sitio_id, $te_imagen);

			// VIDEO
			$video_id = $this->input->post('video_id', TRUE);
			$video_url = $this->input->post('video_url', TRUE);
			$video_leyenda = $this->input->post('video_leyenda', TRUE);

			// tags: diferencia entre tiponota tag y las demás
			$videotagsSimples = $this->input->post('video_tags_simples');
			$videotagsExtendidos = $this->input->post('video_tags_extendidos');
			$video_tags = $this->_get_elemento_tags($tiponota_id, $videotagsSimples, $videotagsExtendidos, $tag_id_array);

			$this->_update_video($video_id, $video_url, $video_leyenda, $nota_id, $video_tags, $te_video);

			// GALERÍA IMAGENES
			$imagenes_nuevas = $this->input->post('gal_imagen_name', TRUE);
			$leyendas_nuevas = $this->input->post('gal_imagen_leyenda', TRUE);
			$galeria_nombre = $this->input->post('gal_imagen_nombre', TRUE);
			$imgs_antiguas_id = $this->input->post('galeria_fotos_actual', TRUE);
			$leyendas_antiguas = $this->input->post('leyenda_galeria', TRUE);
			$imagenes_borradas = $this->input->post('galeria_fotos_borradas', TRUE);

			// diferencia entre tiponota tag y las demás
			$galtagsSimples = $this->input->post('gal_imagen_tags_simples');
			$galtagsExtendidos = $this->input->post('gal_imagen_tags_extendidos');
			$gal_imagen_tags = $this->_get_elemento_tags($tiponota_id, $galtagsSimples, $galtagsExtendidos, $tag_id_array);

			$this->_update_galeria_imagen($imagenes_nuevas, $leyendas_nuevas, $imgs_antiguas_id, $galeria_nombre, $leyendas_antiguas, $imagenes_borradas, $nota_id, $gal_imagen_tags, $te_galeriafotos, $bucket, $sitio_id);

			// GALERIA VIDEOS
			$videos_url = $this->input->post('gal_video_url', TRUE);
			$videos_leyenda = $this->input->post('gal_video_leyenda', TRUE);
			$videos_borrar = $this->input->post('videos_antiguos', TRUE);
			$videos_actuales = $this->input->post('gal_video_url_old', TRUE);

			// diferencia entre tiponota tag y las demás
			$galvidtagsSimples = $this->input->post('gal_video_tags_simples');
			$galvidtagsExtendidos = $this->input->post('gal_video_tags_extendidos');
			$gal_video_tags = $this->_get_elemento_tags($tiponota_id, $galvidtagsSimples, $galvidtagsExtendidos, $tag_id_array);

			$this->_update_galeria_video($videos_url, $videos_leyenda, $videos_borrar, $nota_id, $videos_actuales, $sitio_id, $gal_video_tags, $bucket, $te_galeriavideos);

			// GIFS
			$gif_id = $this->input->post('nota_gif_id', TRUE);
        	$gif_file = $_FILES['gif_file'];
        	$gif_nombre = $this->input->post('gif_nombre', TRUE);
			$gif_leyenda = $this->input->post('gif_leyenda', TRUE);

			// tags: diferencia entre tiponota tag y las demás
			$giftagsSimples = $this->input->post('gif_tags_simples');
			$giftagsExtendidos = $this->input->post('gif_tags_extendidos');
			$gif_tags = $this->_get_elemento_tags($tiponota_id, $giftagsSimples, $giftagsExtendidos, $tag_id_array);

			$this->_update_gif($gif_id, $gif_file, $gif_nombre, $gif_leyenda, $nota_id, $gif_tags, $bucket, $sitio_id, $te_gif);

			// AUDIO
			$audio_id = $this->input->post('nota_audio_id', TRUE);
        	$audio_file = $_FILES['audio_file'];
        	$audio_nombre = $this->input->post('audio_nombre', TRUE);
			$audio_leyenda = $this->input->post('audio_leyenda', TRUE);

			// tags: diferencia entre tiponota tag y las demás
			$audiotagsSimples = $this->input->post('audio_tags_simples');
			$audiotagsExtendidos = $this->input->post('audio_tags_extendidos');
			$audio_tags = $this->_get_elemento_tags($tiponota_id, $audiotagsSimples, $audiotagsExtendidos, $tag_id_array);

			$this->_update_audio($audio_id, $audio_file, $audio_nombre, $nota_id, $audio_tags, $bucket, $sitio_id, $te_audio);

			// CANCIÓN
			$cancion_id = $this->input->post('nota_cancion_id', TRUE);
        	$cancion_file = $_FILES['cancion_file'];
        	$cancion_nombre = $this->input->post('cancion_nombre', TRUE);
			$cancion_leyenda = $this->input->post('cancion_leyenda', TRUE);

			// tags: diferencia entre tiponota tag y las demás
			$canciontagsSimples = $this->input->post('cancion_tags_simples');
			$canciontagsExtendidos = $this->input->post('cancion_tags_extendidos');
			$cancion_tags = $this->_get_elemento_tags($tiponota_id, $canciontagsSimples, $canciontagsExtendidos, $tag_id_array);

			$this->_update_cancion($cancion_id, $cancion_file, $cancion_nombre, $nota_id, $cancion_tags, $bucket, $sitio_id, $te_cancion);

			return TRUE;
		}
	}

	function _update_imagen($imagen_id, $file, $nombre, $leyenda, $nota_id, $tags, $bucket, $sitio_id, $te_imagen)
	{
		// se subió una imagen nueva
    	if(!empty($file['name']))
    	{
			if(empty($file['tmp_name']) || !exif_imagetype($file['tmp_name']))
			{
				$this->session->set_userdata('error_imagen', 'El archivo que subiste no es una imagen');
				return FALSE;
			}

			// si había un audio anterior, desactivar el elemento
			if(!empty($imagen_id)) { 
				$this->_delete_elemento($imagen_id);	
			} else {
			// borrar los elementos tipo canción relacionados con la nota
				$this->_delete_elemento_by_nota($nota_id, $te_imagen);	
			}

			// guardar la imagen nueva en la BD
			$this->load->model('notas/Notas_modelo');
			$this->Notas_modelo->store_imagen($file, $nombre, $leyenda, $nota_id, $tags, $bucket, $sitio_id, $te_imagen, $this->cliente);
    	}
    	// se escogió una imagen de la librería
    	else if(!empty($this->input->post('imagenNota')))
    	{
    		// si había un audio anterior, desactivar el elemento
			if(!empty($imagen_id)) { 
				$this->_delete_elemento($imagen_id);	
			} else {
			// borrar los elementos tipo canción relacionados con la nota
				$this->_delete_elemento_by_nota($nota_id, $te_imagen);	
			}

    		$elemento_libreria_id = $this->input->post('imagenNota');
    		$notas_elemento = array(
				'nota_id' => $nota_id,
				'elemento_id' => $elemento_libreria_id,
				'notas_elemento_estado' => '1',
				'notas_elemento_usuario_mod' => $usuario_id
			);
			$this->db->insert('notas_elemento', $notas_elemento);
    	}
    	else
    	{
    		// si ya existía un elemento imagen, se actualiza
    		if(!empty($imagen_id))
    		{
    			$usuario_id = $this->session->userdata('usuario_id');

    			$imagen_update['elemento_desc'] = $leyenda;
    			$imagen_update['elemento_usuario_mod'] = $usuario_id;
    			$this->db->where('elemento_id', $imagen_id);
    			$this->db->update('elementos', $imagen_update);

    			// actualizar los tags
    			$check_imagen_tags = $this->input->post('check_imagen_tags');
 
    			if($check_imagen_tags == '1')
    			{
    				// borrar la relacion de tags anterior
    				$this->db->where('elemento_id', $imagen_id);
    				$this->db->delete('tag_elementos');

    				// llenar con los tags nuevos
			        if(!empty($tags))
			        {
			        	$tag_elementos = array();
				        foreach($tags as $tag)
				        {
				        	$tag_elementos[] = array(
				        		'elemento_id' => $imagen_id,
				        		'tag_id' => $tag
				        	);
				        }
				        $this->db->insert_batch('tag_elementos', $tag_elementos);
			        }
    			}
    		}
    	}
	}

	function _update_gif($gif_id, $file, $nombre, $leyenda, $nota_id, $tags, $bucket, $sitio_id, $te_gif)
	{
		// se subió un gif nuevo
    	if(!empty($file['name']))
    	{
			if(empty($file['tmp_name']) || !exif_imagetype($file['tmp_name']))
			{
				$this->session->set_userdata('error_imagen', 'El archivo que subiste no es una imagen');
				return FALSE;
			}

			// si había un audio anterior, desactivar el elemento
			if(!empty($gif_id)) { 
				$this->_delete_elemento($gif_id);	
			} else {
			// borrar los elementos tipo canción relacionados con la nota
				$this->_delete_elemento_by_nota($nota_id, $te_gif);	
			}

			// guardar la imagen nueva en la BD
			$this->load->model('notas/Notas_modelo');
			$this->Notas_modelo->store_gif($file, $nombre, $leyenda, $tags, $bucket, $te_gif, $this->cliente, $nota_id);
    	}
    	// se escogió una imagen de la librería
    	else if(!empty($this->input->post('gifNota')))
    	{
    		// si había un audio anterior, desactivar el elemento
			if(!empty($gif_id)) { 
				$this->_delete_elemento($gif_id);	
			} else {
			// borrar los elementos tipo canción relacionados con la nota
				$this->_delete_elemento_by_nota($nota_id, $te_gif);	
			}

    		$elemento_libreria_id = $this->input->post('gifNota');
    		$notas_elemento = array(
				'nota_id' => $nota_id,
				'elemento_id' => $elemento_libreria_id,
				'notas_elemento_estado' => '1',
				'notas_elemento_usuario_mod' => $usuario_id
			);
			$this->db->insert('notas_elemento', $notas_elemento);
    	}
    	else
    	{
    		// si ya existía un elemento imagen, se actualiza
    		if(!empty($gif_id))
    		{
    			$usuario_id = $this->session->userdata('usuario_id');

    			$gif_update['elemento_desc'] = $leyenda;
    			$gif_update['elemento_usuario_mod'] = $usuario_id;
    			$this->db->where('elemento_id', $gif_id);
    			$this->db->update('elementos', $gif_update);

    			// actualizar los tags
    			$check_gif_tags = $this->input->post('check_gif_tags');
 
    			if($check_gif_tags == '1')
    			{
    				// borrar la relacion de tags anterior
    				$this->db->where('elemento_id', $gif_id);
    				$this->db->delete('tag_elementos');

    				// llenar con los tags nuevos
			        if(!empty($tags))
			        {
			        	$tag_elementos = array();
				        foreach($tags as $tag)
				        {
				        	$tag_elementos[] = array(
				        		'elemento_id' => $gif_id,
				        		'tag_id' => $tag
				        	);
				        }
				        $this->db->insert_batch('tag_elementos', $tag_elementos);
			        }
    			}
    		}
    		// si está vacío se borran los elementos tipo gif de la nota
    		else
    		{
    			// primero se selecciona el id del gif
    			$this->db->select('elementos.elemento_id');
    			$this->db->from('elementos');
    			$this->db->join('notas_elemento ne', "ne.elemento_id = elementos.elemento_id AND ne.nota_id = $nota_id");
    			$this->db->where('tipoelemento_id', 3); // gif
    			$this->db->where('elemento_estado', '1');
    			$query = $this->db->get();
    			if($query->num_rows() > 0)
    			{
    				$gif = $query->row();
    				// eliminar la relacion
    				$this->db->where('elemento_id', $gif->elemento_id);
    				$this->db->where('nota_id', $nota_id);
    				$this->db->delete('notas_elemento');
    			}
    		}
    	}
	}

	function _update_video($video_id, $video_url, $leyenda, $nota_id, $video_tags, $te_video)
	{
		if(empty($video_id))
		{
			// si video id está vacío, quiere decir que se ha insertado un elemento nuevo
			$this->load->model('Notas_modelo');
			$this->Notas_modelo->store_video($video_url, $leyenda, $nota_id, $video_tags, $te_video);
		}
		else
		{
			// si no, se actualiza el elemento existente
			if(!empty($video_url))
			{
				$usuario_id = $this->session->userdata('usuario_id');
				$video = $this->_get_provider($video_url);
				
				$video_update['elemento_titulo'] = $leyenda;
				$video_update['elemento_desc'] = $video['provider'];
				$video_update['elemento_ruta'] = $video['video_id'];
				$video_update['elemento_usuario_mod'] = $usuario_id;
				
				$this->db->where('elemento_id', $video_id);
	    		$this->db->update('elementos', $video_update);

	    		// actualizar los tags
    			$check_video_tags = $this->input->post('check_video_tags');
 
    			if($check_video_tags == '1')
    			{
    				// borrar la relacion de tags anterior
    				$this->db->where('elemento_id', $video_id);
    				$this->db->delete('tag_elementos');

    				// llenar con los tags nuevos
			        if(!empty($video_tags))
			        {
			        	$tag_elementos = array();
				        foreach($video_tags as $tag)
				        {
				        	$tag_elementos[] = array(
				        		'elemento_id' => $video_id,
				        		'tag_id' => $tag
				        	);
				        }
				        $this->db->insert_batch('tag_elementos', $tag_elementos);
			        }
    			}
			}
			else
			{
				// si video url está vacío, se borra el elemento
				$this->_delete_elemento($video_id);
			}
			
		}
	}

	function _update_audio($audio_id, $file, $nombre, $nota_id, $tags, $bucket, $sitio_id, $te_audio)
	{
		// se subió un audio nuevo
    	if(!empty($file['name']))
    	{
    		// si había un audio anterior, desactivar el elemento
			if(!empty($audio_id)) { 
				$this->_delete_elemento($audio_id);	
			} else {
			// borrar los elementos tipo canción relacionados con la nota
				$this->_delete_elemento_by_nota($nota_id, $te_audio);	
			}

			// guardar el audio nueva en la BD
			$this->load->model('notas/Notas_modelo');
			$this->Notas_modelo->store_audio($file, $nombre, $tags, $bucket, $te_audio, $this->cliente, $nota_id);
			var_dump('test');die();
    	}
    	// se escogió un audio de la librería
    	else if(!empty($this->input->post('audioNota')))
    	{
    		// si había un audio anterior, desactivar el elemento
			if(!empty($audio_id)) { 
				$this->_delete_elemento($audio_id);	
			} else {
			// borrar los elementos tipo canción relacionados con la nota
				$this->_delete_elemento_by_nota($nota_id, $te_audio);	
			}

    		$elemento_libreria_id = $this->input->post('audioNota');
    		$notas_elemento = array(
				'nota_id' => $nota_id,
				'elemento_id' => $elemento_libreria_id,
				'notas_elemento_estado' => '1',
				'notas_elemento_usuario_mod' => $usuario_id
			);
			$this->db->insert('notas_elemento', $notas_elemento);
    	}
    	else
    	{
    		// si ya existía un elemento audio, se actualiza
    		if(!empty($audio_id))
    		{
    			$usuario_id = $this->session->userdata('usuario_id');

    			// actualizar los tags
    			$check_audio_tags = $this->input->post('check_audio_tags');
 
    			if($check_audio_tags == '1')
    			{
    				// borrar la relacion de tags anterior
    				$this->db->where('elemento_id', $audio_id);
    				$this->db->delete('tag_elementos');

    				// llenar con los tags nuevos
			        if(!empty($tags))
			        {
			        	$tag_elementos = array();
				        foreach($tags as $tag)
				        {
				        	$tag_elementos[] = array(
				        		'elemento_id' => $audio_id,
				        		'tag_id' => $tag
				        	);
				        }
				        $this->db->insert_batch('tag_elementos', $tag_elementos);
			        }
    			}
    		}
    		// si está vacío se borran los elementos tipo audio de la nota
    		else
    		{
    			// primero se selecciona el id del audio
    			$this->db->select('elementos.elemento_id');
    			$this->db->from('elementos');
    			$this->db->join('notas_elemento ne', "ne.elemento_id = elementos.elemento_id AND ne.nota_id = $nota_id");
    			$this->db->where('tipoelemento_id', 1); // audio
    			$this->db->where('elemento_estado', '1');
    			$query = $this->db->get();
    			if($query->num_rows() > 0)
    			{
    				$audio = $query->row();
    				// eliminar la relacion
    				$this->db->where('elemento_id', $audio->elemento_id);
    				$this->db->where('nota_id', $nota_id);
    				$this->db->delete('notas_elemento');
    			}
    		}
    	}
	}

	function _update_cancion($cancion_id, $file, $nombre, $nota_id, $tags, $bucket, $sitio_id, $te_cancion)
	{
		// se subió un audio nuevo
    	if(!empty($file['name']))
    	{
    		// si había un audio anterior, desactivar el elemento
			if(!empty($cancion_id)) { 
				$this->_delete_elemento($cancion_id);	
			} else {
			// borrar los elementos tipo canción relacionados con la nota
				$this->_delete_elemento_by_nota($nota_id, $te_cancion);	
			}

			// guardar el audio nueva en la BD
			$this->load->model('notas/Notas_modelo');
			$this->Notas_modelo->store_cancion($file, $nombre, $tags, $bucket, $te_cancion, $this->cliente, $nota_id);
			
			 
    	}
    	// se escogió un audio de la librería
    	else if(!empty($this->input->post('cancionNota')))
    	{
    		// si había un audio anterior, desactivar el elemento
			if(!empty($cancion_id)) { 
				$this->_delete_elemento($cancion_id);	
			} else {
			// borrar los elementos tipo canción relacionados con la nota
				$this->_delete_elemento_by_nota($nota_id, $te_cancion);	
			}

    		$elemento_libreria_id = $this->input->post('cancionNota');
    		$notas_elemento = array(
				'nota_id' => $nota_id,
				'elemento_id' => $elemento_libreria_id,
				'notas_elemento_estado' => '1',
				'notas_elemento_usuario_mod' => $usuario_id
			);
			$this->db->insert('notas_elemento', $notas_elemento);
    	}
    	else
    	{
    		// si ya existía un elemento cancion, se actualiza
    		if(!empty($cancion_id))
    		{
    			$usuario_id = $this->session->userdata('usuario_id');

    			// actualizar los tags
    			$check_cancion_tags = $this->input->post('check_cancion_tags');
 
    			if($check_cancion_tags == '1')
    			{
    				// borrar la relacion de tags anterior
    				$this->db->where('elemento_id', $cancion_id);
    				$this->db->delete('tag_elementos');

    				// llenar con los tags nuevos
			        if(!empty($tags))
			        {
			        	$tag_elementos = array();
				        foreach($tags as $tag)
				        {
				        	$tag_elementos[] = array(
				        		'elemento_id' => $cancion_id,
				        		'tag_id' => $tag
				        	);
				        }
				        $this->db->insert_batch('tag_elementos', $tag_elementos);
			        }
    			}
    		}
    		// si está vacío se borran los elementos tipo audio de la nota
    		else
    		{
    			// primero se selecciona el id del audio
    			$this->db->select('elementos.elemento_id');
    			$this->db->from('elementos');
    			$this->db->join('notas_elemento ne', "ne.elemento_id = elementos.elemento_id AND ne.nota_id = $nota_id");
    			$this->db->where('tipoelemento_id', 1); // audio
    			$this->db->where('elemento_estado', '1');
    			$query = $this->db->get();
    			if($query->num_rows() > 0)
    			{
    				$cancion = $query->row();
    				// eliminar la relacion
    				$this->db->where('elemento_id', $cancion->elemento_id);
    				$this->db->where('nota_id', $nota_id);
    				$this->db->delete('notas_elemento');
    			}
    		}
    	}
	}

	function _update_galeria_imagen($nuevas, $leyendas_nuevas, $ids, $nombre, $leyendas, $borradas, $nota_id, $tags, $te_gal_imagen, $bucket, $sitio_id)
	{
		// Si existen imágenes nuevas, se insertan
		if(!empty($nuevas))
		{						
			$this->load->model('Notas_modelo');
			$this->Notas_modelo->store_galeria_imagen($nuevas, $nombre, $leyendas_nuevas, $nota_id, $tags, $te_gal_imagen, $bucket, $sitio_id, $this->cliente);			
		}

		// actualizar la leyenda
		if(!empty($ids))
		{
			$update_galeria = array();
			foreach($ids as $key => $id)
			{
				$update_galeria[] = array(
					'elemento_id' => $id,
					'elemento_desc' => $leyendas[$key]
				);
			}
			$this->db->update_batch('elementos', $update_galeria, 'elemento_id');

			// actualizar los tags
	    	$check_gal_imagen_tags = $this->input->post('check_gal_imagen_tags');
	    	if ($check_gal_imagen_tags == '1')
	    	{
	    		// borrar la relacion de tags anterior
	    		foreach($ids as $key => $id)
	    		{
	    			$this->db->where('elemento_id', $id);
	    			$this->db->delete('tag_elementos');

	    			// llenar con los tags nuevos
			        if(!empty($tags))
			        {
			        	$tag_elementos = array();
				        foreach($tags as $tag)
				        {
				        	$tag_elementos[] = array(
				        		'elemento_id' => $id,
				        		'tag_id' => $tag
				        	);
				        }
				        $this->db->insert_batch('tag_elementos', $tag_elementos);
			        }
	    		}
	    	}
		}

		// Eliminar fotos
		if(!empty($borradas))
		{
			$borradas = json_decode($borradas);
			foreach($borradas as $img_id)
			{
				$this->_delete_elemento($img_id);
			}
		}
	}

	function _update_galeria_video($videos_url, $videos_leyenda, $videos_borrar, $nota_id, $videos_actuales, $sitio_id, $tags, $bucket, $te_gal_video)
	{	
		// insertar los videos	
		if(is_array($videos_url))
		{
			if(!empty(array_filter($videos_url)))
			{
				$this->load->model('Notas_modelo');
				$this->Notas_modelo->store_galeria_video($videos_url, $videos_leyenda, $nota_id, $tags, $te_gal_video, $sitio_id);
			}
		}

		// Eliminar Videos
		$videos_borrar = json_decode($videos_borrar);
		if(!empty($videos_borrar))
		{
			foreach($videos_borrar as $video_id)
			{
				$this->_delete_elemento($video_id);
			}
		}

		// Actualizar tags y leyenda
		if(!empty($videos_actuales))
		{
			$update_galeria = array();
			foreach($videos_actuales as $key => $id)
			{
				$update_galeria[] = array(
					'elemento_id' => $id,
					'elemento_titulo' => $videos_leyenda
				);
			}
			$this->db->update_batch('elementos', $update_galeria, 'elemento_id');

			// actualizar los tags 
	    	$check_gal_video_tags = $this->input->post('check_gal_video_tags');
	    	if ($check_gal_video_tags == '1')
	    	{
	    		// borrar la relacion de tags anterior
	    		foreach($videos_actuales as $key => $id)
	    		{
	    			$this->db->where('elemento_id', $id);
	    			$this->db->delete('tag_elementos');

	    			// llenar con los tags nuevos
			        if(!empty($tags))
			        {
			        	$tag_elementos = array();
				        foreach($tags as $tag)
				        {
				        	$tag_elementos[] = array(
				        		'elemento_id' => $id,
				        		'tag_id' => $tag
				        	);
				        }
				        $this->db->insert_batch('tag_elementos', $tag_elementos);
			        }
	    		}
	    	}
		}
	}

	function _update_tags($tags_extendidos, $tags_simples, $tags_patrocinados, $tags_featured, $nota_id)
	{
		$usuario_id = $this->session->userdata('usuario_id');
		$tags_final = array();

		if(!empty($tags_extendidos))
		{
			$tags_final = $tags_extendidos;
		}
		
		if(!empty($tags_patrocinado))
		{
			$tags_final = array_merge($tags_final, $tags_patrocinado);
		}

		if(!empty($tags_simples))
		{
			$tags_final = array_merge($tags_final, $tags_simples);
		}

		// borrar los tags anteriores
		$this->db->where('nota_id', $nota_id);
		$this->db->delete('tag_notas');

		// insertar tags nuevos si existen
		if(!empty($tags_final))
		{
			$tags_update = array();

			foreach($tags_final as $tag)
			{
				$tags_update[] = array(
					'tag_id' => $tag,
					'nota_id' => $nota_id,
					'tagnotas_estado' => '1',
					'tagnotas_usuario_mod' => $usuario_id
				);
			}

			// Insertar tags
			$this->db->insert_batch('tag_notas', $tags_update);
		}

		if(!empty($tags_featured))
		{
			$tags_ft_update = array();

			foreach($tags_featured as $tag_ft)
			{
				$tags_ft_update[] = array(
					'tag_id' => $tag_ft,
					'nota_id' => $nota_id,
					'tag_featured' => '1',
					'tagnotas_estado' => '1',
					'tagnotas_usuario_mod' => $usuario_id
				);
			}
			// insertar en tags
			$this->db->insert_batch('tag_notas', $tags_ft_update);
		}
	}

	function _delete_elemento($elemento_id)
	{
		$data['notas_elemento_estado'] = '0';
		$this->db->where('elemento_id',$elemento_id);
        $this->db->update('notas_elemento', $data); 

        $data_tag['tag_elemento_estado'] = '0';
        $this->db->where('elemento_id', $elemento_id);
        $this->db->update('tag_elementos', $data_tag);
	}

	function _delete_elemento_by_nota($nota_id, $tipoelemento_id)
	{
		// conseguir el id de los elementos
		$this->db->select('e.elemento_id');
		$this->db->from('elementos e');
		$this->db->join('notas_elemento ne', "ne.elemento_id = e.elemento_id AND ne.nota_id = $nota_id");
		$this->db->where('e.elemento_estado', '1');
		$this->db->where('e.tipoelemento_id', $tipoelemento_id);
		$query = $this->db->get();

		// borrar de la tabla notas elementos
		if($query->num_rows() > 0)
		{
			$elemento = $query->result();
			foreach($elemento as $e)
			{
				$this->db->where('elemento_id', $e->elemento_id);
				$this->db->where('nota_id', $nota_id);
				$this->db->delete('notas_elemento');
			}
		}
	}

	function _get_elementos_noticia($nota_id, $sitio_id)
	{
		$elementos = array();
		// conseguir elementos
		$imagen = $this->Elementos_getters_modelo->get_imagen_by_nota($nota_id, $sitio_id);
		$video = $this->Elementos_getters_modelo->get_video_by_nota($nota_id, $sitio_id);
		$gif = $this->Elementos_getters_modelo->get_gif_by_nota($nota_id, $sitio_id);
		$audio = $this->Elementos_getters_modelo->get_audio_by_nota($nota_id, $sitio_id);
		$gal_imagen = $this->Elementos_getters_modelo->get_gal_imagen_by_nota($nota_id, $sitio_id);
		$gal_video = $this->Elementos_getters_modelo->get_gal_video_by_nota($nota_id, $sitio_id);

		$elementos['imagen'] = $imagen;
		$elementos['video'] = $video;
		$elementos['gif'] = $gif;
		$elementos['audio'] = $audio;
		$elementos['gal_imagen'] = $gal_imagen;
		$elementos['gal_video'] = $gal_video;

		// conseguir tags
		$this->load->module('tags/tags_getters');
		
		if(!empty($imagen))
		{
			$elementos['imagen']->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($imagen->elemento_id);
			$elementos['imagen']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($imagen->elemento_id);
		}
		
		if(!empty($video))
		{
			$elementos['video']->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($video->elemento_id);
			$elementos['video']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($video->elemento_id);
		}
		
		if(!empty($gif))
		{
			$elementos['gif']->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($gif->elemento_id);
			$elementos['gif']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($gif->elemento_id);
		}
		

		if(!empty($audio))
		{
			$elementos['audio']->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($audio->elemento_id);
			$elementos['audio']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($audio->elemento_id);
		}
		
		if(!empty($gal_imagen))
		{
			$elementos['gal_imagen'][0]->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($gal_imagen[0]->elemento_id);
			$elementos['gal_imagen'][0]->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($gal_imagen[0]->elemento_id);
		}
		
		if(!empty($gal_video))
		{
			$elementos['gal_video'][0]->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($gal_video[0]->elemento_id);
			$elementos['gal_video'][0]->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($gal_video[0]->elemento_id);
		}

		return $elementos;
	}

	function _get_elementos_cancion($nota_id, $sitio_id)
	{
		$elementos = array();
		$cancion = $this->Elementos_getters_modelo->get_cancion_by_nota($nota_id, $sitio_id);
		$imagen = $this->Elementos_getters_modelo->get_imagen_by_nota($nota_id, $sitio_id);
		$video = $this->Elementos_getters_modelo->get_video_by_nota($nota_id, $sitio_id);

		$elementos['cancion'] = $cancion;
		$elementos['imagen'] = $imagen;
		$elementos['video'] = $video;

		// conseguir tags
		$this->load->module('tags/tags_getters');

		if(!empty($cancion))
		{
			$elementos['cancion']->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($cancion->elemento_id);
			$elementos['cancion']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($cancion->elemento_id);
		}
		
		if(!empty($imagen))
		{
			$elementos['imagen']->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($imagen->elemento_id);
			$elementos['imagen']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($imagen->elemento_id);
		}
		
		if(!empty($video))
		{
			$elementos['video']->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($video->elemento_id);
			$elementos['video']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($video->elemento_id);
		}

		return $elementos;
	}

	function _get_elementos_artista($nota_id, $sitio_id)
	{
		$elementos = array();
		$imagen = $this->Elementos_getters_modelo->get_imagen_by_nota($nota_id, $sitio_id);
		$video = $this->Elementos_getters_modelo->get_video_by_nota($nota_id, $sitio_id);
		$gal_imagen = $this->Elementos_getters_modelo->get_gal_imagen_by_nota($nota_id, $sitio_id);
		$gal_video = $this->Elementos_getters_modelo->get_gal_video_by_nota($nota_id, $sitio_id);

		$elementos['imagen'] = $imagen;
		$elementos['video'] = $video;
		$elementos['gal_imagen'] = $gal_imagen;
		$elementos['gal_video'] = $gal_video;

		return $elementos;
	}

	function _get_elementos_ranking($nota_id, $sitio_id)
	{
      	$elementos = array();
		$imagen = $this->Elementos_getters_modelo->get_imagen_by_nota($nota_id, $sitio_id);
		$video = $this->Elementos_getters_modelo->get_video_by_nota($nota_id, $sitio_id);

		$elementos['imagen'] = $imagen;
		$elementos['video'] = $video;

		// conseguir tags
		$this->load->module('tags/tags_getters');

		if(!empty($imagen))
		{
			$elementos['imagen']->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($imagen->elemento_id);
			$elementos['imagen']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($imagen->elemento_id);
		}
		
		if(!empty($video))
		{
			$elementos['video']->tags_simples = $this->tags_getters->get_tags_simples_by_elemento($video->elemento_id);
			$elementos['video']->tags_extendidos = $this->tags_getters->get_tags_extendidos_by_elemento($video->elemento_id);
		}

		return $elementos;
	}

	function _get_elemento_tags($tiponota_id, $tagsSimples, $tagsExtendidos, $tag_nuevo_id)
	{
		$nota_imagen_tags = array();
		if($tiponota_id == 3)
		{
			$nota_imagen_tags = $tag_nuevo_id;
		}
		else
		{
			if(!empty($tagsSimples))
			{
				if(!empty($tagsExtendidos))
				{
					$nota_imagen_tags = array_merge($tagsSimples, $tagsExtendidos);
				}
				else
				{
					$nota_imagen_tags = $tagsSimples;
				}
			}
			else if(!empty($tagsExtendidos))
			{
				$nota_imagen_tags = $tagsExtendidos;
			}
		}
		return $nota_imagen_tags;
	}

	function _get_provider($video_url)
	{
	    $pattern_youtube = '#^(?:https?://|//)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com/(?:embed/|v/|watch\?v=|watch\?.+&v=))([\w-]{11})(?![\w-])#';
	    $pattern_daily = '/(?:dailymotion\.com(?:\/video|\/hub)|dai\.ly)\/([0-9a-z]+)(?:[\-_0-9a-zA-Z]+#video=([a-z0-9]+))?/';
	    $pattern_vimeo = '/[http|https]+:\/\/(?:www\.|)vimeo\.com\/([a-zA-Z0-9_\-]+)(&.+)?/i';

	     if(strpos($video_url,'youtu.be') !== FALSE || strpos($video_url,'youtube') !== FALSE)
	     {
	     	preg_match($pattern_youtube, $video_url, $matches);
	     	$provider = 'youtube';
	     }
	     
	     else if (strpos($video_url,'vimeo') !== FALSE )
	     {
	     	preg_match($pattern_vimeo, $video_url, $matches);
	     	$provider = 'vimeo';
	     }
	     else if (strpos($video_url,'dai.ly') !== FALSE || strpos($video_url,'dailymotion') !== FALSE)
	     {
	     	preg_match($pattern_daily, $video_url, $matches);
	     	$provider = 'dailymotion';
	     }else{
	     	return FALSE;
	     }	     	
	    if(!empty($matches))
	    {
	    	$video_id = $matches[1];
		    $video = array('provider'=> $provider, 'video_id' => $video_id);
		    
		    return $video;
	    }
	    return FALSE;
	}
}