<?php defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(300);

class Notas_getters_modelo extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'notas';
		$this->load->module(array('secciones/Secciones_getters', 'categorias/Categorias_getters'));
	}

	function get_by_nivel($seccion_id, $sitio_id, $nivel)
	{
		// verificar que el usuario tenga acceso a la sección
		$nivel_permitido = get_nivel_acceso($nivel);
		$nivel_seccion = $this->secciones_getters->get_nivel_acceso_seccion($seccion_id);

		if ($nivel_seccion > $nivel_permitido)
		{

			// verificar si la seccion categorias_ids existe y si la seccion está seteada
			if ($this->session->userdata('categorias_ids') && property_exists(json_decode($this->session->userdata('categorias_ids')), $seccion_id))
			{
				$categorias_ids = json_decode($this->session->userdata('categorias_ids'));
				if (is_array($categorias_ids->$seccion_id))
				{
					// si es objeto significa que la sección tiene categorías
					$notas = $this->get_by_categorias_ids($categorias_ids->$seccion_id);
				}
				else
				{
					// devolver las notas de la sección
					$notas = $this->get_by_seccion_id($seccion_id);
				}
			}
			else
			{
				// verificar si la sección tiene categorías
				$tiene_categorias = $this->secciones_getters->check_categorias($seccion_id);
				if ($tiene_categorias)
				{
					// verificar que se tenga acceso a las categorías
					$categorias_ids = $this->categorias_getters->get_categorias_ids_by_nivel($nivel_permitido, $seccion_id);
					$notas = $this->get_by_categorias_ids($categorias_ids);
				}
				else
				{
					// guardar que no tiene categorías en la sesión
					$categorias_ids = $this->session->userdata('categorias_ids') ? json_decode($this->session->userdata('categorias_ids')) : new stdClass();
					$categorias_ids->$seccion_id = 'Sin categorías';
					$this->session->set_userdata('categorias_ids', json_encode($categorias_ids));
					
					// devolver las notas de la sección
					$notas = $this->get_by_seccion_id($seccion_id);
				}
			}

			return $notas;
		}
		else
		{
			die('no tienes acceso');
		}
	}

	function get_by_seccion_id($seccion_id)
	{
		$this->db->select('n.nota_id, n.sitio_id, n.seccion_id, n.tipo_nota_id, n.categoria_id, n.nota_titulo, n.nota_contenido, n.nota_crudo, n.nota_seolink, n.nota_fech_update, n.nota_flag_portada, n.nota_fecha_publicacion, n.nota_estado, n.nota_fech_creacion, n.nota_usuario_mod, usuarios.usuario_user');
		$this->db->from('notas n, usuarios');
		$this->db->where("(nota_estado = '1' OR nota_estado = '2')", null, FALSE);
		$this->db->where('seccion_id', $seccion_id);
		$this->db->where('n.usuario_id', 'usuarios.usuario_id', FALSE);
		$this->db->limit(500);
		$this->db->order_by('n.nota_fech_update', 'DESC');
		return $this->db->get()->result();
	}

	function get_by_categorias_ids($categorias_ids)
	{
		$this->db->select('n.nota_id, n.sitio_id, n.seccion_id, n.tipo_nota_id, n.categoria_id, n.nota_titulo, n.nota_contenido, n.nota_crudo, n.nota_seolink, n.nota_fech_update, n.nota_flag_portada, n.nota_fecha_publicacion, n.nota_estado, n.nota_fech_creacion, n.nota_usuario_mod, usuarios.usuario_user');
		$this->db->from('notas n, usuarios');
		$this->db->where("(nota_estado = '1' OR nota_estado = '2')", null, FALSE);
		$this->db->where_in('categoria_id', $categorias_ids);
		$this->db->where('n.usuario_id', 'usuarios.usuario_id', FALSE);
		$this->db->limit(500);
		$this->db->order_by('n.nota_fech_update', 'DESC');
		return $this->db->get()->result();
	}

	// canciones son notas
	function get_canciones_by_sitio($sitio_id)
	{
		$this->db->select('notas.nota_titulo, notas.nota_id, tags.tag_nombre');
		$this->db->from($this->__tabla);
		$this->db->join('tag_notas', 'tag_notas.nota_id = notas.nota_id and tag_notas.tag_featured = "0"', 'left');
		$this->db->join('tags', "tags.tipotag_id = 3 and tag_notas.tag_id = tags.tag_id and tags.sitio_id = $sitio_id", 'left');
		$this->db->where('notas.nota_estado', '1');
		$this->db->where('notas.tipo_nota_id', 2);
		$this->db->where('notas.sitio_id', $sitio_id);
		$this->db->group_by('notas.nota_id');
		$this->db->order_by('notas.nota_titulo', 'asc');
		
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function get_noticias_by_sitio($sitio_id)
	{
		$this->db->select('notas.nota_titulo, notas.nota_id');
		$this->db->from($this->__tabla);
		$this->db->where('notas.nota_estado', '1');
		$this->db->where('notas.tipo_nota_id', 1);
		$this->db->where('notas.sitio_id', $sitio_id);
		$this->db->where('notas.nota_lista', 1);
		$this->db->order_by('notas.nota_fech_update', 'asc');

		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function validar_nota($sitio_id, $seccion_id, $nota_id, $usuario_id, $roles)
	{
		// verificar que se haya creado la sesion
        if($this->session->userdata('nivel_acceso'))
        {
            $nivel_acceso = json_decode($this->session->userdata('nivel_acceso'));
            $rol = $roles[$sitio_id];
            $nivel_acceso = $nivel_acceso->$rol;
        }
        else
        {
        	$this->load->model('roles/Roles_modelo');
            $nivel_acceso = $this->Roles_modelo->get_nivel_acceso_by_rol($roles[$sitio_id]);
        }

        // conseguir niveles
        $nivel_permitido = get_nivel_acceso($nivel_acceso);
        $acceso_seccion = $this->secciones_getters->get_nivel_acceso_seccion($seccion_id);
        
        return $acceso_seccion > $nivel_permitido;

	}

	function get_notas_relacionadas($nota_id, $sitio_id)
	{
		$this->db->from('relacion_notas');
		$this->db->where('nota_principal_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);
		$result = $this->db->get()->result();
		return $result;
	}

	function get_tipo_nota_id($nota_id, $sitio_id)
	{
		$this->db->select('tipo_nota_id');
		$this->db->from($this->__tabla);
		$this->db->where('nota_id', $nota_id);
		$this->db->where('sitio_id', $sitio_id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$tipo_id = $query->row();
			return $tipo_id->tipo_nota_id;
		}
	}





	// canciones son notas
	function get_canciones_by_tags($tags_id, $sitio_id)
	{
		$this->_select_database($sitio_id);
		$this->db->from('tag_notas');
		$this->db->where('tagnotas_estado', '1');
		$this->db->where_in('tag_id', $tags_id);

		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			$notas_id = $query->result();
			foreach($notas_id as $id)
			{
				$ids[] = $id->nota_id;
			}

			$this->db->from($this->__tabla);
			$this->db->where('nota_estado', '1');
			$this->db->where('tipo_nota_id', 1); // id de canciones
			$this->db->where_in('nota_id', $nid);

			$query2 = $this->db->get()->row();
	
			if($query2->num_rows() > 0)
			{
				return $query2->result_array();
			}
		}
		return FALSE;
	}

	function get_all($sitio_id)
	{
		$this->_select_database($sitio_id);
		$this->db->from($this->__tabla);
		$this->db->where('nota_estado', '1');

		return $this->db->get()->result();
	}

	function get_info($limit, $sitio_id)
	{
		$this->db->select('notas.nota_id, notas.sitio_id, notas.seccion_id, notas.origen_id, notas.fuente_id, notas.tipo_nota_id, notas.categoria_id, notas.nota_titulo, notas.nota_seolink, notas.nota_contenido, u.usuario_nombre, u.usuario_user, notas.usuario_id, notas.nota_estado, s.seccion_nombre, c.categoria_nombre, notas.nota_fech_update, notas.nota_fech_creacion');
		$this->db->from($this->__tabla);//notas, usuarios, seccion, categoria
		$this->db->where('notas.sitio_id', $sitio_id);
		$this->db->where('notas.nota_estado', '1');
		$this->db->join('secciones as s', 's.seccion_id = notas.seccion_id');
		$this->db->join('categorias as c', 'c.categoria_id = notas.categoria_id', 'left');
		$this->db->join('usuarios as u', 'u.usuario_id = notas.usuario_id');

		$query = $this->db->limit($limit)->order_by('notas.nota_fech_update','desc')->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

}