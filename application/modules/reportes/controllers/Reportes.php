<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Reportes_modelo');	
	}

	function index()
	{
		$this->load->view('reporte_general');
	}

	function grafico()
	{
		$sitio_id = $this->session->userdata('sitio_id_session');
		$cms1 = $this->Reportes_modelo->get_reporte_general(1);
		$data['query'] = $cms1;

		$this->load->view('grafico', $data);	
	}

	// ajax tabla para reporte general
	function get_datos()
	{
		$desde_fecha = $this->input->post('desde_query');
		$hasta_fecha = $this->input->post('hasta_query');

		$cms1 = $this->Reportes_modelo->get_reporte_general($desde_fecha, $hasta_fecha);

		echo json_encode($cms1); 
	}

	// tabla para grafico
	function detalle($fecha, $usuario)
	{
		$fecha = str_replace('_', '-', $fecha);
		// conseguir usuario_id
		$this->load->model('usuarios/Usuarios_modelo');
		$usuario_id = $this->Usuarios_modelo->get_usuario_id_by_name(urldecode($usuario));
		$reporte1 = $this->Reportes_modelo->get_reporte_usuario($fecha, $usuario_id);
		
		$data['reporte'] = json_encode($reporte1);
		
		$this->load->view('reporte_usuario', $data);
	}
}