<?php defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL);
ini_set('display_errors', 1);

class Reportes_modelo extends CI_Model
{	
	
	function __construct()
	{
		parent::__construct();
	}

	function _select_database($sitio_id)
	{
		switch ((int)$sitio_id) {
			case 1:
			case 2:
				$this->DB = $this->load->database('default', TRUE);
				break;
			case 3:
			case 4:
			case 5:
				$this->DB = $this->load->database('oasis', TRUE);
				break;
			case 6:
			case 7:
			case 8:
				$this->DB = $this->load->database('moda', TRUE);
		}
	}

	function get_reporte_general($desde = null, $hasta = null)
	{
		//$this->db->select('', FALSE);

		if(!empty($desde) AND !empty($hasta))
		{
			$myquery2 = "select n.nota_id, n.nota_titulo, CONCAT(s.sitio_dominio, '/', n.nota_seolink, '-' ,n.nota_id) as nota_seolink, DATE_FORMAT(nota_fech_creacion, '%Y-%m-%d') as fecha, nota_fecha_publicacion as update_nota, sec.seccion_id, sec.seccion_nombre, tn.tipo_nota_nombre as tipo_nota, u.usuario_nombre as nombre, s.sitio_id, s.sitio_nombre as sitio, CONCAT('/notas/editar/', s.sitio_id, '/', sec.seccion_id, '/', n.nota_id) as editUrl
			FROM notas n
			inner join secciones sec on n.seccion_id = sec.seccion_id
			inner join usuarios u on u.usuario_id = n.usuario_id
			inner join sitios s on s.sitio_id = n.sitio_id
			inner join tipo_nota tn on tn.tipo_nota_id = n.tipo_nota_id
			AND DATE(  n.nota_fech_creacion )
				BETWEEN  '".$desde."'
				AND  '".$hasta."'
			AND nota_titulo <> ''
			AND nota_estado = '1'
			GROUP BY n.nota_id";
		}
		else
		{
			$myquery2 = "select n.nota_id, n.nota_titulo as nota_titulo, CONCAT(s.sitio_dominio, '/', n.nota_seolink, '-' ,n.nota_id) as nota_seolink, DATE_FORMAT(nota_fech_creacion, '%Y-%m-%d') as fecha, nota_fecha_publicacion as update_nota, sec.seccion_id as seccion_id, sec.seccion_nombre as seccion_nombre, tn.tipo_nota_nombre as tipo_nota, u.usuario_nombre as nombre, s.sitio_id as sitio_id, s.sitio_nombre as sitio, CONCAT('/notas/editar/', s.sitio_id), '/', sec.seccion_id, '/', n.nota_id as editUrl 
			FROM notas n
			inner join secciones sec on n.seccion_id = sec.seccion_id
			inner join usuarios u on u.usuario_id = n.usuario_id
			inner join sitios s on s.sitio_id = n.sitio_id
			inner join tipo_nota tn on tn.tipo_nota_id = n.tipo_nota_id
			AND nota_fech_creacion >=  (CURDATE() - INTERVAL 30 DAY)
			AND nota_estado = '1'
			AND nota_titulo <> ''
			GROUP BY n.nota_id";
		}

		$query2 = $this->db->query($myquery2);
		if($query2->num_rows() > 0)
		{	
			return $query2->result();
		}

		return [];
	}

	function get_reporte_usuario($fecha, $usuario_id)
	{
		$this->db->select('n.nota_id, n.nota_titulo, CONCAT(s.sitio_dominio, "/", n.nota_seolink, "-" ,n.nota_id) as nota_seolink, DATE_FORMAT(nota_fech_creacion, "%Y-%m-%d") as fecha, nota_fecha_publicacion as update_nota, sec.seccion_id, sec.seccion_nombre, tn.tipo_nota_nombre as tipo_nota, u.usuario_nombre as nombre, s.sitio_id, s.sitio_nombre as sitio, CONCAT("/notas/editar/", s.sitio_id, "/", sec.seccion_id, "/", n.nota_id) as editUrl', FALSE);
		$this->db->from('notas n');
		$this->db->join('secciones sec', 'n.seccion_id = sec.seccion_id');
		$this->db->join('sitios s', 'n.sitio_id = s.sitio_id');
		$this->db->join('tipo_nota tn', 'tn.tipo_nota_id = n.tipo_nota_id');
		$this->db->join('usuarios u', 'u.usuario_id = n.usuario_id');
		$this->db->where('n.usuario_id', $usuario_id);
		$this->db->where('n.nota_estado', '1');
		$this->db->like('n.nota_fech_creacion', $fecha, 'after');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{	
			return $query->result();
		}

		return [];
	}

}