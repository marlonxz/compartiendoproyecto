<?php 
$titulo = "Reporte de usuarios | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="/assets/lib/jquery-ui/jquery-ui.css">
<link rel="stylesheet" href="/assets/css/lib/datatables-net/datatables.min.css">
<link rel="stylesheet" href="/assets/css/separate/vendor/datatables-net.min.css">
<link rel="stylesheet" href="/assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="/assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/main.css">
<link rel="stylesheet" href="/assets/lib/jquery-ui/jquery-ui.css">
<link rel="stylesheet" href="/assets/lib/select2/select2.css">
<link rel="stylesheet" href="/assets/lib/dropzone/dropzone.css">
<link rel="stylesheet" href="/assets/lib/jquery-toggles/toggles-full.css">
<link rel="stylesheet" href="/assets/lib/fontawesome/css/font-awesome.css">
<link rel="stylesheet" href="/assets/lib/timepicker/jquery.timepicker.css">
<link rel="stylesheet" href="/assets/lib/bootstrapcolorpicker/css/bootstrap-colorpicker.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
  <script src="/assets/lib/modernizr/modernizr.js"></script>

<style>
    #json-records {
      visibility: hidden;
    }
    #my-table {
      width: 1300px;
    }
    select {
      width: auto;
      max-width: 180px;
    }
  </style>
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
      <header class="section-header">
        <div class="tbl">
          <div class="tbl-row">
            <div class="tbl-cell">
              <h2>Reporte de usuario</h2>
            </div>
          </div>
        </div>
      </header>
      <section class="card">
        <div class="card-block">
          <div class="panel-body">

              <label>Desde:</label>
              <div class="input-group col-sm-4">
                <input id="desde_query" type="text" name="desde_query" class="form-control datepicker" placeholder="mm/dd/yyyy" required>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>

              <div class="mb20 invisible"></div>

              <label>Hasta:</label>
              <div class="input-group col-sm-4">
                <input id="hasta_query" type="text" name="hasta_query" class="form-control datepicker" placeholder="mm/dd/yyyy" required>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>
            
              <div class="mb20 invisible"></div>

              <div class="form-group">
                <button id="filtro-inicial" class="btn btn-primary btn-quirk">Buscar</button> 
              </div>
              
              

            </div><!-- panel-body -->
            <table id="my-table" class="table table-bordered">
            </table>
        </div>
      </section>
      </div><!--.container-fluid-->
  </div><!--.page-content-->
  <pre id="json-records"><?php //echo $reporte;?></pre>

<script src="/assets/js/lib/jquery/jquery.min.js"></script>
<script src="/assets/js/lib/tether/tether.min.js"></script>
<script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="/assets/js/plugins.js"></script>
<script src="/assets/lib/jquery-ui/jquery-ui.js"></script>
<script src="/assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="/assets/lib/timepicker/jquery.timepicker.js"></script>


<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<!-- para exportar a excel -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

<script>
$( document ).ready(function() {

  // Date Picker
  $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });

  $("#filtro-inicial").on('click', function(e){
    e.preventDefault();
    var desdeQuery = $("#desde_query").val(),
        hastaQuery = $("#hasta_query").val();

    $.ajax({
      type: 'POST',
      url: '/reportes/get_datos',
      data: {"desde_query": desdeQuery, "hasta_query": hastaQuery},
      dataType: 'json',
      success: function(data) {
        $("#json-records").html('');
        $("#json-records").html(JSON.stringify(data));
        get_table();
        // mostrar botones de filtro
        $("#search-input-box").show();
      },
      error: function(msg){
        console.log('errr');
        console.log(msg);
      }
    });
  });

  function get_table() {
    var json = JSON.parse($("#json-records").text());

    if ( $.fn.dataTable.isDataTable( '#my-table' ) ) {
        
        $('#my-table').dataTable().fnClearTable();
        $('#my-table').dataTable().fnAddData(json);
    }
    else {
        $("#my-table").append('<tfoot style="width:1300px"><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr></tfoot>');
        $("#my-table").dataTable( {
          initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            },
          "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json"
                },
          dom: 'B<"top"if>rt<"bottom"lip><"clear">',
          buttons: [
              {
                extend: 'csv',
                bom: true,
                filename: "Reporte THOR"
              },
            ],
          "aaData" : json,
           "aoColumns": [
              { "sTitle": "Gestor",  "mData": "nombre" },
              { "sTitle": "Sitio",  "mData": "sitio" },
              { "sTitle": "Creación",   "mData": "fecha" },
              { "sTitle": "Update", "mData": "update_nota" },
              { "sTitle": "Título", "sWidth": "200px", "mData": "nota_titulo", },
              { "sTitle": "URL", "sWidth": "100px", "mData": "nota_seolink"},
              { "sTitle": "Tipo",    "mData": "tipo_nota" },
              {
                "mData": null,
                "bSortable": false,
                "mRender": function(o){ return '<a href="'+o.nota_seolink+'" target="_blank" title="URL"><span class="fa fa-external-link"></a> <a href="' + o.editUrl + '" title="editar">' + '<span class="fa fa-edit"></span>' + '</a>'; }
              }
            ],

        });
    }
    

    
  }

    



});

</script>

</body>
</html>
