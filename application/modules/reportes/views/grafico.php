<?php 
$titulo = "Administración de Sitios | Zeus CMS";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/jquery-ui/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/datatables-net/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/datatables-net.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
 <script data-require="d3@3.5.3" data-semver="3.5.3" src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.js"></script>
  <script src="<?php echo base_url();?>assets/lib/d3/moment.min.js"></script>

<style>
    body {
      font: 10px sans-serif;
    }
    
    .axis path,
    .axis line {
      fill: none;
      stroke: #000;
      shape-rendering: crispEdges;
    }
    
    .x.axis path {
      display: none;
    }
    
    .line {
      fill: none;
      stroke: steelblue;
      stroke-width: 1.3px;
      transition: 0.3s;
    }

    .line:hover{
      stroke-width: 3px;
    }

    .strokewidth {
      stroke-width: 3px;
      z-index: 1;
    }

    div.tooltip {
      position: absolute; 
      text-align: center; 
      width:80px;
      background-color: #fff;
      color: #000;
      border: 1px solid black;
      padding: 3px;
      line-height: 15px;
      box-shadow: 1px 1px 1px #888888;      
      border-radius: 3px;
    }

    div.tooltip a{
      color: white;
      text-decoration: underline;
    }
    
    #json-records {
      visibility: hidden;
    }
  </style>

</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <div class="area_grafico"></div>
              
        <div class="col-row clearfix">
          <div class="col-md-5">
            <label>Desde:</label>
            <div class="input-group">
              <input id="desde_query" type="text" name="desde_query" class="form-control datepicker" placeholder="mm/dd/yyyy" required>
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
          </div>
          <div class="col-md-5">
            <label>Hasta:</label>
            <div class="input-group">
              <input id="hasta_query" type="text" name="hasta_query" class="form-control datepicker" placeholder="mm/dd/yyyy" required>
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
          </div>
          <div class="col-md-2">
          <div class="mb20 invisible"></div>
            <div class="form-group">
              <button id="filtro-inicial" class="btn btn-primary btn-quirk">Buscar</button> 
            </div>
          </div>
        </div>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<pre id="json-records"><?php echo json_encode($query);?></pre>

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>
<script src="<?php echo base_url();?>assets/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/lib/timepicker/jquery.timepicker.js"></script>

<script src="<?php echo base_url();?>assets/js/lib/datatables-net/datatables.min.js"></script>

<script>
Object.map = function(o, f, ctx) {
        ctx = ctx || this;
        var result = [];
        var i = 0;
        Object.keys(o).forEach(function(k) {
            result[i] = f.call(ctx, o[k], k, o);
            i++; 
        });
        return result;
    }

Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}

function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push( new Date (currentDate) )
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

function getItems(input) {
  var arr = input, obj = [], nombres = [];
  var maxmin = d3.extent(input, function(d) {
      return d.date;
  });
  var fechas = getDates(maxmin[0], maxmin[1]);

  for (var i = 0; i < arr.length; i++) {
    if(!obj[arr[i].nombre]) { 
      obj[arr[i].nombre] = {}; 
    }
    
    for (var j = 0; j < fechas.length; j++) {
      obj[arr[i].nombre][fechas[j]] = 0;
    }
  }

  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < fechas.length; j++) {
      if(fechas[j].getTime() == arr[i].date.getTime()) {
        obj[arr[i].nombre][arr[i].date] = obj[arr[i].nombre][arr[i].date] + 1;
      }
    }
  }
  return obj;
}

function dibujar_grafico() {
  
  var jsonData = document.getElementById("json-records").innerHTML;
  var json = JSON.parse( jsonData );

  var margin = {
      top: 20,
      right: 80,
      bottom: 30,
      left: 50
    },
    width = 1000 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

  var parseDate = d3.time.format("%Y%m%d").parse;

  var x = d3.time.scale()
    .range([0, width]);

  var y = d3.scale.linear()
    .rangeRound([height, 0]);

  var color = d3.scale.category10();

  var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

  var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .tickFormat(d3.format("d"));

  var line = d3.svg.line()
  .interpolate("linear")
    .x(function(d) {
      return x(d.date);
    })
    .y(function(d) {
      return y(d.cantidad);
    });

  // remove existing elements
  d3.select(".area_grafico div").remove()
  d3.select(".area_grafico svg").remove()

  // Define 'div' for tooltips
  var div = d3.select(".area_grafico")
    .append("div")  // declare the tooltip div 
    .attr("class", "tooltip")// apply the 'tooltip' class
    .style("opacity", 0);    

  var svg = d3.select(".area_grafico").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var data = json; // asignar la data a la variable

  // limpiar la fecha    
  data.forEach(function(d) {
    d.date = parseDate(d.fecha.replace(/-/g, ''));
  });

  data_ordenada = getItems(data); 

  color.domain(Object.keys(data_ordenada));

  // armar cada objeto y agruparlo por nombre   
  var cities = color.domain().map(function(nombre) {
    return {
      name: nombre,
      values: Object.map(data_ordenada[nombre], function(d, k) {
        return {
          name: nombre,
          date: new Date(k),
          cantidad: d
        }
      }) 
    };
  });

  // el dominio de x, y
  x.domain(d3.extent(data, function(d) {
    return d.date;
  }));

  y.domain([
    d3.min(cities, function(c) {
      return d3.min(c.values, function(v) {
        return v.cantidad;
      });
    }),
    d3.max(cities, function(c) {
      return d3.max(c.values, function(v) {
        return v.cantidad;
      });
    })
  ]);

  // ordenar por fechas ascending
  cities.forEach(function(d){
      d.values.sort(function(a,b){
          return d3.ascending(a.date, b.date)
      })
  });

  // leyenda
  var legend = svg.selectAll('g')
    .data(cities)
    .enter()
    .append('g')
    .attr('class', 'legend');

  legend.append('rect')
    .attr('x', width - 20)
    .attr('y', function(d, i) {
      return i * 20;
    })
    .attr('width', 10)
    .attr('height', 10)
    .style('fill', function(d) {
      return color(d.name);
    });

  legend.append('text')
    .attr('x', width - 8)
    .attr('y', function(d, i) {
      return (i * 20) + 9;
    })
    .text(function(d) {
      return d.name;
    });

  // los axis
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Cantidad");

  var city = svg.selectAll(".city")
    .data(cities)
    .enter().append("g")
    .attr("class", "city");

  city.append("path")
    .attr("class", "line")
    .attr("d", function(d) {
      return line(d.values);
    })
    .style("stroke", function(d) {
      return color(d.name);
    });

  city.append("text")
    .datum(function(d) {
      return {
        name: d.name,
        value: d.values[d.values.length - 1]
      };
    })
    .attr("transform", function(d) {
      return "translate(" + x(d.value.date) + "," + y(d.value.cantidad) + ")";
    })
    .attr("x", 3)
    .attr("dy", ".35em");

  // el circulo en cada punto
  var point = city.append("g")
    .attr("class", "line-point");

  point.selectAll('circle')
    .data(function(d){ return d.values})
    .enter().append('circle')
    .attr("cx", function(d) { return x(d.date) })
    .attr("cy", function(d) { return y(d.cantidad) })
    .attr("r", 4)
    .attr("transition", "2s")
    .on("mouseover", handleMouseOver)
    .on("mouseout", handleMouseOut)
    .style("fill", function(d) { return color(d.name); })
    .style("stroke", function(d) { return color(d.name); });

  var mouseStrokeG = svg.append("g").attr("class", "mouse-over-stroke");
  var mousePerStroke = mouseStrokeG.selectAll(".mouse-per-stroke")
    .data(cities)
    .enter()
    .append("g")
    .attr("class", "mouse-per-stroke");

  // funcion para interactividad
  function handleMouseOver(d, i) {  // Add interactivity
    d3.selectAll(".tooltip2").remove();
    
    // Use D3 to select element, change color and size
    d3.select(this).attr({
      fill: "orange",
      r: 4 * 2
    });

    // se agrega la clase strokewidth
    this.parentElement.parentElement.getElementsByTagName("path")[0].setAttribute("class", "line strokewidth");


    // cuando tienen la misma cantidad
    diferencia = 0
    $.each(cities, function(i, s){
      if ( s.name == d.name  ) return; // si es mi hover, salir
      $.each(s.values, function(j, p){
        if (p.cantidad == d.cantidad && p.date.getTime() == d.date.getTime()) {
          diferencia = diferencia + 38;
          // para el tooltip
          var name_encoded2 = encodeURIComponent(p.name);
          var url_encoded2 = "<?php echo base_url();?>reportes/detalle/"+moment(p.date).format('YYYY-MM-DD')+"/"+name_encoded2;
          // definir un nuevo tooltip
          var tooltip2 = d3.select(".area_grafico")
                      .append("div")  // declare the tooltip div 
                      .attr("class", "tooltip tooltip2")// apply the 'tooltip' class
                      .style("opacity", 0);
          tooltip2.transition()
              .duration(200)  
              .style("opacity", 1)
              .style("background-color", color(p.name))
              .style("color", "white")
              .style("border-color", color(p.name))
              .style("display", "block");

          tooltip2.html(
                 "<b>"+moment(p.date).format('DD-MM-YYYY')+"</b>"
         + "<br/>"  + 
        '<a href= "'+url_encoded2+'">Total: ' + // The first <a> tag
        d.cantidad
        + "</a>" // closing </a> tag
              )  
              .style("left", (d3.event.pageX) + "px")      
              .style("top", (d3.event.pageY + diferencia) + "px"); 
        }
      });
    });
    
    // para el tooltip
    var name_encoded = encodeURIComponent(d.name);
    var url_encoded = "<?php echo base_url();?>reportes/detalle/"+moment(d.date).format('YYYY-MM-DD')+"/"+name_encoded;

    div.transition()
      .duration(200)  
      .style("opacity", 1)
      .style("background-color", color(d.name))
      .style("color", "white")
      .style("border-color", color(d.name))
      .style("display", "block");  
    
    div.html(
        "<b>"+moment(d.date).format('DD-MM-YYYY')+"</b>"
         + "<br/>"  + 
        '<a href= "'+url_encoded+'">Total: ' + // The first <a> tag
        d.cantidad
        + "</a>" // closing </a> tag
      )  
      .style("left", (d3.event.pageX) + "px")      
      .style("top", (d3.event.pageY) + "px");
  }



  function handleMouseOut(d, i) {
    // Use D3 to select element, change color back to normal
    d3.select(this).attr({
      fill: "black",
      r: 4
    });
    d3.selectAll(".tooltip")

    // se borra la clase strokewidth
    this.parentElement.parentElement.getElementsByTagName("path")[0].setAttribute("class", "line");
  }

}
          
$( document ).ready(function() {
  // Date Picker
  $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });

  // grafico
  
  dibujar_grafico();

  $("#filtro-inicial").on('click', function(e){
    e.preventDefault();
    var desdeQuery = $("#desde_query").val(),
        hastaQuery = $("#hasta_query").val();

    $.ajax({
      type: 'POST',
      url: '<?php echo base_url();?>reportes/get_datos',
      data: {"desde_query": desdeQuery, "hasta_query": hastaQuery},
      dataType: 'json',
      success: function(data) {
        $("#json-records").html('');
        $("#json-records").html(JSON.stringify(data));
        dibujar_grafico();
      },
      error: function(msg){
        console.log('errr');
        console.log(msg);
      }
    });
  });

});
   
</script>

<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
