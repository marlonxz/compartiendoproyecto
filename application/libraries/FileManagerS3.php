<?php
/**
 * 
 * File Manager Class
 *
 * This class is used to manipulate Amazon S3 Storage
 * To use Amazon storage, It has been used Amazon Simple Storage Service( Amazon S3 ) 
 * Possible usages Write, read and delete objects containing from 1 byte to 5 terabytes of data each. 
 *
 * @package     FileManagerS3
 * @category    Library
 * @author      Md. Kausar Alam<kausar_ss2003@yahoo.com>
 * @link        http://www.mdkausar.wordpress.com
 * @version     1.0.0
 * @date		30/06/2011
 */
 
 //include your sdk class
 // require_once ($_SERVER['DOCUMENT_ROOT']."/path/to/sdk.class.php");
require_once(APPPATH.'libraries/aws-autoloader.php');
 
 // Define your bucket name
 // This bucket can be generated using program  
 // define('AWS_S3_BUCKET', 'your bucket name');

 //START OF CLASS
 class FileManagerS3 {

   /**
 	* 
 	* Object of AmazonS3 Class
 	*/
 	private $amazonS3;
 	
 	
   /**
	* Constructor is used to create an instance of AmazonS3 Class
	*
	* Amazon Simple Storage Service (Amazon S3) is storage for the Internet. 
	* You can use Amazon S3 to store and retrieve any amount of data at any time, 
	* from anywhere on the web.
	*
	* @param	Object - AmazonS3 Object
	*/
	public function __construct()
	{
		$sharedConfig = [
			'region' => 'us-east-1',
			'version' => 'latest'
		];

		// Create an SDK class used to share configuration across clients
		$sdk = new Aws\Sdk($sharedConfig);

		// Create an Amazon S3 client using shared configuration data
		$credentials = new Aws\Credentials\Credentials('AKIAIVBCHFT7AQHPYSIA', 'pYN+5vaNqIKgfTAscKzeSJZfBrzYE3sZ9uXKB0rq');
		// $client = $sdk->createS3(['credentials' => $credentials]);

		// $this->amazonS3 = new AmazonS3();
		$this->amazonS3 = $sdk->createS3(['credentials' => $credentials]);		 	
	}
	
   /**
    *
	* There is no option to create directory in a bucket
	* But its needed to upload file under different categories
	* To eliminate this problem, it has been used prefix that will work as directory
	*
	* @param 	String - $dir Where the file to be uploaded   
	* @param	Array  - $extraInfo  - Array structure is described below
	*		
	*	$extraInfo = array(
	*						"file_name"		=> "example.jpg",
	*						"dest_file_name"=> "12323312323",//without extension	
	*						"sub_dir"		=> "Sub Directory if neccessary"
	*		     		  );
	*
	*
	*
	* @return	String - File Path 
	*/
 	public static function getFilePath( $dir , $extraInfo )
	{
		$fileExt = "";
		$pathinfoArr = pathinfo( $extraInfo["file_name"] );		
		if ( $pathinfoArr['extension'] )
			$fileExt = $pathinfoArr['extension'];
		
		$subDir =  isset($extraInfo['sub_dir']) ? $extraInfo['sub_dir'] ."/" : "" ;
		return $dir . "/" . $subDir .  $extraInfo['dest_file_name'] . "." . $fileExt;	
		
	}
 	
	
   /**
 	* Upload a file to Amazon
 	*
 	* @param	String - Source file path
 	* @param	String - Destination file path
 	* @reutrn	Bool   - true if sucessfully uploaded, otherwise false 		
 	*/
 	public function uploadFile( $sourcePath, $destPath )
	{
		$response =  $this->amazonS3->create_object( AWS_S3_BUCKET , 
													 $destPath, 
													 array(
															'fileUpload' => $sourcePath,
															'acl'		 => AmazonS3::ACL_PUBLIC
				 										)
													);
		if($response->isOK()) {
			return true;
		}
		
		return false;													
	}

   /**
 	* Chech whether a file already exist or not of a specific path
 	*
 	* @param	String - Path of file 	
 	* @reutrn	Bool   - true if found, otherwise false 		
 	*/
 	public function isFileExist( $filePath )
	{
		$response = $this->amazonS3->if_object_exists(AWS_S3_BUCKET, $filePath);
		
		if( $response ) {
			return true;
		}

		return false;
	}

   /**
 	* Get File Content
 	*
 	* @param	String - Path of the file 	
 	* @reutrn	 		
 	*/
 	public function downloadFile( $filePath )
	{
		$response	 = $this->amazonS3->get_object(AWS_S3_BUCKET, $filePath);
								
		if($response->isOK()) {
			$contentType = $response->header['_info']['content_type'];
			header("Content-type: $contentType");
			header("Content-Disposition: filename=$filePath");
			echo $response->body;							
		}
		
		return false;
		
	}
	 
   /**
 	* Delete a specific file
 	* 	
 	* @param	String - Path of a file to be deleted  
 	* @reutrn	Bool   - true if sucessfully deleted, otherwise false 		
 	*/
 	public function deleteFile( $filePath )
	{
		$response = $this->amazonS3->delete_object(AWS_S3_BUCKET, $filePath );
		if( $response->isOK() ) {
			return true;
		} else {
			return false;
		}
	}

	
   /**
 	* Get the list of matching object names. If there are no results, return empty array
 	* 	 	
 	* @reutrn	Array  - List of matching object names as array		
 	*/
 	public function getUploadedFileList()
	{		
		return $this->amazonS3->get_object_list(AWS_S3_BUCKET);		
	}	
	
 }//END OF CLASS

?>