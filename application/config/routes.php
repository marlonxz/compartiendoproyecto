<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;
$route['logout'] = 'login/logout';
$route['sitios/(:num)'] = 'sitios/sitio_subhome/$1';
$route['sitemap'] = 'sitemap/index';

// ruta para subir elementos
$route['(:num)/elementos/audio'] = 'elementos/add_audio/$1';
$route['(:num)/elementos/imagen'] = 'elementos/add_images/$1';
$route['(:num)/elementos/gif'] = 'elementos/add_gif/$1';

// ruta para secciones
$route['secciones/(:num)/(:num)'] = 'secciones/index/$1/$2';

// ruta para notas
$route['notas/eliminar/(:num)/(:num)/(:num)'] = 'notas/eliminar/$1/$2/$3';

$route['notas/procesar/(:num)/(:num)'] = 'notas/notas_editar/procesar_edicion/$1/$2';
$route['notas/editar/(:num)/(:num)/(:num)'] = 'notas/notas_editar/editar/$1/$2/$3';

$route['(:num)/notas/crear'] = 'notas/crear/$1';
$route['(:num)/notas/(:num)'] = 'notas/get_notas_by_seccion/$1/$2';
$route['(:num)/notas/(:num)/edit/(:num)'] = 'notas/edit_nota/$1/$2/$3';
$route['notas/deleteImage/(:any)'] = 'notas/deleteImage/$1';

// ruta para noticias
$route['noticias'] = 'noticias/noticias_edicion/index';
$route['noticias/index'] = 'noticias/noticias_edicion/index';
$route['noticias/editar/(:num)'] = 'noticias/noticias_edicion/editar/$1';
$route['noticias/eliminar/(:num)'] = 'noticias/noticias_edicion/eliminar/$1';
$route['noticias/clonar'] = 'noticias/noticias_edicion/clonar';

//Listas de Videos
$route['videos/crear/(:num)'] = 'videos/add_lista/$1';
$route['videos/(:num)'] = 'videos/index/$1';
$route['videos/editar/(:num)/(:num)'] = 'videos/edit_lista/$1/$2';
$route['videos/eliminar/(:num)/(:num)'] = 'videos/delete_lista/$1/$2';
$route['videos/listar/(:num)/(:num)'] = 'videos/listar_videos/$1/$2';

// ruta para externos
$route['externos/(:num)'] = 'externos/index/$1';
$route['externos/nota/(:num)/(:num)/(:num)'] = 'externos/leer/$1/$2/$3';

// para tamaños de imagen
$route['tamanos'] = 'tamano_imagen/tamanos/index';
$route['tamanos/crear'] = 'tamano_imagen/tamanos/crear';
$route['tamanos/procesar'] = 'tamano_imagen/tamanos/procesar';
$route['tamanos/eliminar/(:num)'] = 'tamano_imagen/tamanos/eliminar/$1';



