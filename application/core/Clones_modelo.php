<?php defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(300);
require_once(APPPATH.'libraries/aws-library/aws-autoloader.php');
require FCPATH.'http://localhost/Milhoras-cms-panacea-2ab8427702b6/assets/Netcarver/Textile/Parser.php';
require FCPATH.'http://localhost/Milhoras-cms-panacea-2ab8427702b6/assets/Netcarver/Textile/DataBag.php';
require FCPATH.'http://localhost/Milhoras-cms-panacea-2ab8427702b6/assets/Netcarver/Textile/Tag.php';

class Clones_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
	}

	function select_database($sitio_id)
	{
		switch ($sitio_id) {
			case 15:
				$this->DB = $this->load->database('clones', TRUE);
		}
	}

	function get_provider($video_url)
	{
	    $pattern_youtube = '#^(?:https?://|//)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com/(?:embed/|v/|watch\?v=|watch\?.+&v=))([\w-]{11})(?![\w-])#';
	    $pattern_daily = '/(?:dailymotion\.com(?:\/video|\/hub)|dai\.ly)\/([0-9a-z]+)(?:[\-_0-9a-zA-Z]+#video=([a-z0-9]+))?/';
	    $pattern_vimeo = '/[http|https]+:\/\/(?:www\.|)vimeo\.com\/([a-zA-Z0-9_\-]+)(&.+)?/i';

	     if(strpos($video_url,'youtu.be') !== FALSE || strpos($video_url,'youtube') !== FALSE)
	     {
	     	preg_match($pattern_youtube, $video_url, $matches);
	     	$provider = 'youtube';
	     }
	     
	     else if (strpos($video_url,'vimeo') !== FALSE )
	     {
	     	preg_match($pattern_vimeo, $video_url, $matches);
	     	$provider = 'vimeo';
	     }
	     else if (strpos($video_url,'dai.ly') !== FALSE || strpos($video_url,'dailymotion') !== FALSE)
	     {
	     	preg_match($pattern_daily, $video_url, $matches);
	     	$provider = 'dailymotion';
	     }else{
	     	return FALSE;
	     }	     	
	    if(!empty($matches))
	    {
	    	$video_id = $matches[1];
		    $video = array('provider'=> $provider, 'video_id' => $video_id);
		    
		    return $video;
	    }
	    return FALSE;
	}

}