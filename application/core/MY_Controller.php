<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/* load the MX_Controller class */
require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {

	function __construct()
    {
        parent::__construct();
        
        if (! $this->session->userdata('usuario_id'))
        {
            redirect('login/logout'); // the user is not logged in, redirect them!
        }
    }

}
